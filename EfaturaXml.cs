﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.IO;


namespace Edonusum

{

    public class EFatXml
    {
        public static string tmpUUID;
        public static string tmpID;
        public static string tmpCanonicalizationMethod;
        public static string tmpSignatureMethod;
        public static string tmpReferenceId;
        public static string tmpReferenceURI;
        public static string tmpReferenceType;
        public static string tmpDigestMethod;
        public static string tmpDigestValue;
        public static string tmpReferenceId_1;
        public static string tmpReferenceURI_1;
        public static string tmpTransforms;
        public static string tmpDigestMethod_1;
        public static string tmpDigestValue_1;
        public static string tmpSignatureValue;
        public static string tmpSignatureValueId;
        public static string tmpX509Certificate;
        public static string tmpXmlPath;
        public static string tmpXmlFilePath;

        public void XmlFat(string[] args)
        {
            WriteToFile("Kayıt Var!");
        }
        public void WriteToXsltFile(string Message)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\einvoice_received";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\einvoice_received\\" + tmpUUID.ToString() + ".xslt";
            if (!File.Exists(filepath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }

        }
        public void WriteToXmlFile(string Message)
        {
            //string path = AppDomain.CurrentDomain.BaseDirectory + "\\einvoice_received";
            string path = tmpXmlPath.ToString();
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string XmlFilePath = tmpXmlFilePath.ToString();
            if (!File.Exists(XmlFilePath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = File.CreateText(XmlFilePath))
                {
                    sw.WriteLine(Message);
                }
            }

        }

        public void WriteToFile(string Message)
        {

            /*string m1 = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
            string m2 = "<Invoice xmlns=\"urn:oasis:names:specification:ubl:schema:xsd:Invoice-2\" xmlns:cac=\"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2\" xmlns:cbc=\"urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2\" xmlns:ccts-cct=\"urn:un:unece:uncefact:data:specification: CoreComponentTypeSchemaModule:2\" xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\" xmlns:ext=\"urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2\" xmlns:qdt=\"urn:oasis:names:specification:ubl:schema:xsd:QualifiedDataTypes-2\" xmlns:udt=\"urn:oasis:names:specification:ubl:schema:xsd:UnqualifiedDataTypes-2\" xmlns:xades=\"http://uri.etsi.org/01903/v1.3.2#\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"urn:oasis:names:specification:ubl:schema:xsd:Invoice-2 D:/Mappings/CP/21/UBLTR_1.2.1_Paketi/xsdrt/maindoc/UBL-Invoice-2.1.xsd\">";
            string m3 = "<ext:UBLExtensions>\n<ext:UBLExtension>\n<ext:ExtensionContent/>";
            string m31 = "<ds:Signature Id= \"Sign-Id-SignatureId-" + tmpID.ToString() + "\">\n<ds:SignedInfo>";
            string m32 = "<ds:CanonicalizationMethod Algorithm=" + "\"" + tmpCanonicalizationMethod.ToString() + "\"" + " />\n<ds:SignatureMethod Algorithm=" + "\"" + tmpSignatureMethod.ToString() + "\"" + "/> ";
            string m33 = "<ds:Reference Id=" + "\""+ tmpReferenceId.ToString() + "\"" + " URI=" + "\"" + tmpReferenceURI.ToString() + "\"" + " Type=" + "\"" + tmpReferenceType.ToString() + "\"" + ">";
            string m34 = "<ds:DigestMethod Algorithm="+ "\"" + tmpDigestMethod.ToString() + "\"" + " />";
            string m35 = "<ds:DigestValue>"  + tmpDigestValue.ToString()  + "</ds:DigestValue > \n</ds:Reference>";
            string m36 = "<ds:Reference Id="+ "\"" + tmpReferenceId_1.ToString() + "\"" + " URI=" + "\"" + tmpReferenceURI_1.ToString() + "\"" + " >";
            string m37 = "<ds:Transforms>\n<ds:Transform Algorithm = "+ "\"" + tmpTransforms.ToString() + "\"" + " />\n</ds:Transforms> ";
            string m38 = "<ds:DigestMethod Algorithm=" + "\"" + tmpDigestMethod_1.ToString() + "\"" + " />";
            string m39 = "<ds:DigestValue>" + tmpDigestValue_1.ToString()  + "</ds:DigestValue>";
            string m40 = "</ds:Reference>\n</ds:SignedInfo>";
            string m41 = "<ds:SignatureValue Id=" + "\"" + tmpSignatureValueId.ToString() + "\"" + "> "+ tmpSignatureValue +"</ds:SignatureValue>";
            string m42 = "<ds:KeyInfo>\n<ds:X509Data>";*/
            //string m43 = "<ds:X509Certificate>"+ tmpX509Certificate.ToString() + "</ds:X509Certificate>\n</ds:X509Data>";
            //string m43 = "<ds:X509Certificate></ds:X509Certificate>\n</ds:X509Data>";

            //string m4 = "</ext:ExtensionContent>\n</ext:UBLExtension >\n</ext:UBLExtensions > ";

            string path = AppDomain.CurrentDomain.BaseDirectory + "\\einvoice_received";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\einvoice_received\\" + tmpUUID.ToString() + ".xml";
            if (!File.Exists(filepath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }
    }

   

}