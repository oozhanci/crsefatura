﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Xsl;
using Edonusum.CrsEFatura;
using System.Globalization;
using System.IO.Compression;

namespace Edonusum
{
    public partial class crsEFatura : ServiceBase
    {
        Timer timer = new Timer();
        List<InvoiceType> _Invoices;

        public crsEFatura()
        {
            InitializeComponent();
        }

        SqlConnection con;
        SqlCommand cmd;
        SqlDataAdapter sda;
        DataTable dt;

        protected override void OnStart(string[] args)
        {

           // System.Diagnostics.Debugger.Launch();
            WriteToFile("Service is started at " + DateTime.Now);
            timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
            timer.Interval = 180000; // number in milisecinds 3 second
            timer.Enabled = true;
            
            // Firmalar kontrol
            WriteToFile("Firma Kayıtları Kontrol");
            //W3CustomerList();

            // Gelen Kutusu kontrol
            WriteToFile("Gelen Faturalar Kontrol");
            //InboxInvoiceList();


            // Giden EFatura Faturalar
            //WriteToFile("Giden Faturalar Kontrol");
            //SendInvoiceList();

            
            // Giden EArsivFatura Faturalar
           // WriteToFile("Giden EArsiv Faturalar Kontrol");
           // SendArchiveInvoiceList();

            


        }


        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            WriteToFile("Service is recall at " + DateTime.Now);
            // Firmalar kontrol
            /* WriteToFile("Firma Kayıtları Kontrol");
             W3CustomerList();

             // Gelen Kutusu kontrol
             WriteToFile("Gelen Faturalar Kontrol");
             InboxInvoiceList();
            */

            // Giden EFatura Faturalar
            WriteToFile("Giden Faturalar Kontrol");
            SendInvoiceList();


            // Giden EArsivFatura Faturalar
            WriteToFile("Giden EArsiv Faturalar Kontrol");
            SendArchiveInvoiceList();

        }

        public void W3CustomerList()
        {
            WriteToFile("W3CustomerList connection");
            con = new SqlConnection(SQLConnection.ConnectionString);
            string VknTckn;
            try
            {
                WriteToFile("W3CustomerList connect db");
                int i = 1;
                con.Open();
                SqlCommand cmd = new SqlCommand("SELECT TAXNO FROM workcube_catalyst.workcube_catalyst.COMPANY WHERE USE_EFATURA = 0 AND TAXNO IS NOT NULL AND LEN(TAXNO) = 10 AND RECORD_DATE= GETDATE()-1", con);
                //  SqlDataAdapter sda = new SqlDataAdapter();
                WriteToFile("W3CustomerList select db");
                SqlDataReader dr = cmd.ExecuteReader();
                WriteToFile("W3CustomerList select dr");
                // sda.SelectCommand = cmd;
                //  DataTable tab = new DataTable();
                //  sda.Fill(tab);
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                       
                        VknTckn = dr.GetString(0);
                        WriteToFile("W3CustomerList CustomerInfo" + VknTckn.ToString());
                        CustomerInfo(VknTckn.ToString());
                    }
                }
                

            }
            catch (Exception hata)
            {
                WriteToFile(hata.Message);
            }
            finally
            {
                con.Close();
                con.Dispose();


            }
        }

        public void CustomerInfo(string VknTckn)
        {
            WriteToFile("CustomerInfo = " + VknTckn);
            var client = CreateClient();
           // string VknTckn = "2150240232";
            var response = client.GetUserAliasses(VknTckn);
           // var aliasses = response.Value.ReceiverboxAliases.ToString();
           // WriteToFile("SenderboxAliases = " + aliasses);
            if (response.IsSucceded && response.Value != null)
            {
                WriteToFile("New Customer ");
               /* WriteToFile("Definition = " + response.Value.Definition.Title.ToString());
                WriteToFile("Identifier = " + response.Value.Definition.Identifier.ToString());
                WriteToFile("CreateDateUtc = " + response.Value.Definition.CreateDateUtc.ToString());
                WriteToFile("Type = " + response.Value.Definition.Type.ToString());
                WriteToFile("SystemCreateDate = " + response.Value.Definition.SystemCreateDate.ToString());
                WriteToFile("DespatchReceiverboxAliases = " + response.Value.DespatchReceiverboxAliases.Rank.ToString());
                WriteToFile("DespatchSenderboxAliases = " + response.Value.DespatchSenderboxAliases.IsReadOnly.ToString());*/
               // DateTime CreateDateUtc = todate.response.Value.Definition.SystemCreateDate;
                if (string.IsNullOrEmpty(response.Value.Definition.SystemCreateDate) == false)
                {
                    con = new SqlConnection(SQLConnection.ConnectionString);
                    try
                    {
                        con.Open();
                        WriteToFile("New Customer DB Start.....");
                        SqlCommand cmd = new SqlCommand("UPDATE workcube_catalyst.workcube_catalyst.COMPANY " +
                                                        " SET USE_EFATURA = 1, EFATURA_DATE =  @efaturaDate, USE_EARCHIVE = 0  " +
                                                        " WHERE TAXNO =  @taxNo", con);
                        cmd.Parameters.AddWithValue("@taxNo", VknTckn);
                        cmd.Parameters.AddWithValue("@efaturaDate", response.Value.Definition.SystemCreateDate.ToString());
                  
                        cmd.ExecuteNonQuery();
                        con.Close();
                        WriteToFile("New Customer DB Write" + VknTckn);

                    }
                    catch (Exception hata)
                    {
                        WriteToFile(hata.Message);
                    }
                    finally
                    {
                        con.Close();
                        con.Dispose();

                    }

                    
                }

            }
            else
            {
                WriteToFile(VknTckn +" New Customer Not Found!!!" );
            }

         }

        public void GetInboxInvoices(string InvoiceId)
        {

            WriteToFile("GetInboxInvoices = " + InvoiceId);

            var client = CreateClient();

            var GetInvoice = client.GetInboxInvoice(InvoiceId);
            if (GetInvoice.Value != null)
            {

                GetInvoice.Value.Invoice.ID.ToString();
                WriteToFile("schemaLocation = " + GetInvoice.Value.Invoice.schemaLocation.ToString());
                WriteToFile("schemaLocation = " + GetInvoice.Value.Invoice.schemaLocation.ToString());
                // _ = EFatXml.tmpUUID.ToString();
                WriteToFile("Signature = " + GetInvoice.Value.Invoice.Signature.ToString());
                WriteToFile("UBLExtensions[].ExtensionContent.Signature.Id = " + GetInvoice.Value.Invoice?.UBLExtensions[0]?.ExtensionContent?.Signature?.Id?.ToString()?? "");
                WriteToFile("UBLExtensions[].ExtensionContent.Signature.SignatureValue = " + GetInvoice.Value.Invoice?.UBLExtensions[0]?.ExtensionContent?.Signature?.Object?.ToString()?? "");
                //var base64EncodedBytes = System.Convert.FromBase64String(GetInvoice.Value.Invoice.UBLExtensions[0].ExtensionContent.Signature.KeyInfo.ItemsElementName[0].ToString());

               // WriteToFile("EmbeddedDocumentBinaryObject = " + Encoding.UTF8.GetString(GetInvoice.Value.Invoice.AdditionalDocumentReference[0].Attachment.EmbeddedDocumentBinaryObject.Value));

                WriteToFile("1 = " + GetInvoice.Value.Invoice.AccountingCustomerParty.Party.PartyName.Name.Value.ToString());
                WriteToFile("2 = " + GetInvoice.Value.Invoice.UBLExtensions[0].ExtensionContent.Signature.SignedInfo?.CanonicalizationMethod.Algorithm.ToString()?? "");
                WriteToFile("3 = " + GetInvoice.Value.Invoice.UBLExtensions[0].ExtensionContent.Signature.SignedInfo?.SignatureMethod.Algorithm.ToString()?? "");
                WriteToFile("4 = " + GetInvoice.Value.Invoice.UBLExtensions[0].ExtensionContent.Signature.SignedInfo?.Reference[0]?.Id?.ToString()?? "");
                WriteToFile("5 = " + GetInvoice.Value.Invoice.UBLExtensions[0].ExtensionContent.Signature.SignedInfo?.Reference[0]?.DigestMethod?.Algorithm?.ToString()?? "");
                //X509Data
                //WriteToFile("10 = " + GetInvoice.Value.Invoice.UBLExtensions[0].ExtensionContent.Signature.KeyInfo.ItemsElementName[0].ToString()?? "");
                //KeyValue
               // WriteToFile("11 = " + GetInvoice.Value.Invoice.UBLExtensions[0].ExtensionContent.Signature.KeyInfo.ItemsElementName[1].ToString()?? "");

                // XML Yazilacak..................
                Edonusum.EFatXml EFatXml = new Edonusum.EFatXml();
                
                EFatXml.tmpUUID = GetInvoice.Value.Invoice.UUID.Value.ToString();
                EFatXml.tmpID = GetInvoice.Value.Invoice.UBLExtensions[0].ExtensionContent.Signature.Id.ToString();
                EFatXml.tmpCanonicalizationMethod = GetInvoice.Value.Invoice.UBLExtensions[0].ExtensionContent.Signature.SignedInfo.CanonicalizationMethod.Algorithm.ToString() ?? "";
                EFatXml.tmpSignatureMethod = GetInvoice.Value.Invoice.UBLExtensions[0].ExtensionContent.Signature.SignedInfo.SignatureMethod.Algorithm.ToString() ?? "";
                EFatXml.tmpReferenceId =  GetInvoice.Value.Invoice?.UBLExtensions[0]?.ExtensionContent?.Signature?.SignedInfo?.Reference[0]?.Id?.ToString() ?? "";
                EFatXml.tmpReferenceURI = GetInvoice.Value.Invoice?.UBLExtensions[0]?.ExtensionContent?.Signature?.SignedInfo?.Reference[0]?.URI?.ToString() ?? "";

                EFatXml.tmpReferenceType = GetInvoice.Value.Invoice?.UBLExtensions[0]?.ExtensionContent?.Signature?.SignedInfo?.Reference[0]?.Type ?? "";
                EFatXml.tmpDigestValue = Convert.ToBase64String(GetInvoice.Value.Invoice?.UBLExtensions[0]?.ExtensionContent?.Signature?.SignedInfo?.Reference[0]?.DigestValue) ?? "";



                EFatXml.tmpReferenceId_1 = GetInvoice.Value.Invoice?.UBLExtensions[0]?.ExtensionContent?.Signature?.SignedInfo?.Reference[1]?.Id?.ToString() ?? "";
                EFatXml.tmpReferenceURI_1 = GetInvoice.Value.Invoice?.UBLExtensions[0]?.ExtensionContent?.Signature?.SignedInfo?.Reference[1]?.URI?.ToString() ?? "";
                //EFatXml.tmpTransforms = GetInvoice.Value.Invoice.UBLExtensions[0].ExtensionContent.Signature.SignedInfo.Reference[1]?.Transforms[0]?.Algorithm.ToString() ?? "";

                EFatXml.tmpTransforms = "";

                EFatXml.tmpDigestMethod_1 = GetInvoice.Value.Invoice?.UBLExtensions[0]?.ExtensionContent?.Signature?.SignedInfo?.Reference[1]?.DigestMethod?.Algorithm?.ToString() ?? "";

                EFatXml.tmpDigestValue_1 = Convert.ToBase64String(GetInvoice.Value.Invoice?.UBLExtensions[0]?.ExtensionContent?.Signature?.SignedInfo?.Reference[1]?.DigestValue) ?? "";
                EFatXml.tmpSignatureValueId = GetInvoice.Value.Invoice?.UBLExtensions[0]?.ExtensionContent?.Signature?.SignatureValue?.Id?.ToString() ?? "";
                EFatXml.tmpSignatureValue = Convert.ToBase64String(GetInvoice.Value.Invoice.UBLExtensions[0]?.ExtensionContent?.Signature?.SignatureValue?.Value) ?? "";
                //EFatXml.tmpX509Certificate = ((byte)GetInvoice.Value.Invoice.UBLExtensions[0].ExtensionContent.Signature.KeyInfo.ItemsElementName[0]).ToString();


                // EFatXml.tmpX509Certificate = ((byte)GetInvoice.Value.Invoice.UBLExtensions[0].ExtensionContent.Signature.KeyInfo.ItemsElementName[0]).ToString();
                DocumentReferenceType doc;
                AttachmentType attachment = null;

                for (int i = 0; i < GetInvoice.Value.Invoice.AdditionalDocumentReference.Length; i++)
                {
                    doc = GetInvoice.Value.Invoice.AdditionalDocumentReference[i];
                    attachment = doc.Attachment;
                    if (attachment != null && attachment.EmbeddedDocumentBinaryObject.filename != null)
                    {
                        EFatXml.WriteToXsltFile(Encoding.UTF8.GetString(GetInvoice.Value.Invoice.AdditionalDocumentReference[i].Attachment.EmbeddedDocumentBinaryObject.Value));
                    }

                }

                

                ShowInvoice(GetInvoice.Value.Invoice);

                con = new SqlConnection(SQLConnection.ConnectionString);
                try
                {
                    con.Open();
                    WriteToFile("New EInvoice DB Write.....");
                    SqlCommand cmd = new SqlCommand("INSERT INTO workcube_catalyst.workcube_catalyst_2022_6.EINVOICE_RECEIVING_DETAIL " +
                                                    "( SERVICE_RESULT, UUID, EINVOICE_ID, STATUS_DESCRIPTION, STATUS_CODE, ERROR_CODE, INVOICE_TYPE_CODE, " +
                                                    "SENDER_TAX_ID,  RECEIVER_TAX_ID, PROFILE_ID, PAYABLE_AMOUNT, ISSUE_DATE, PARTY_NAME, " +
                                                    " PAYABLE_AMOUNT_CURRENCY, PATH, PROCESS_STAGE, IS_APPROVE, EINVOICE_TYPE, PRINT_COUNT, RECORD_DATE, RECORD_IP)" +
                                                    " VALUES ('Successful', @UUID, @EINVOICE_ID, 'ONAY BEKLİYOR', 25, 0, @EINVOICETYPECODE," +
                                                    "@SENDER_TAX_ID, @RECEIVER_TAX_ID, @PROFILE_ID,@PAYABLE_AMOUNT, CONVERT(DATETIME,@ISSUE_DATE,21), @PARTY_NAME," +
                                                    "@PAYABLE_AMOUNT_CURRENCY, @PATH, 74, 1, 3, 0,GETDATE(), '127.0.0.1' )", con);
                    cmd.Parameters.AddWithValue("@UUID", InvoiceId);
                    cmd.Parameters.AddWithValue("@EINVOICE_ID", GetInvoice.Value.Invoice.ID.Value.ToString());
                    cmd.Parameters.AddWithValue("@EINVOICETYPECODE", GetInvoice.Value.Invoice.InvoiceTypeCode.Value.ToString());
                    cmd.Parameters.AddWithValue("@RECEIVER_TAX_ID", GetInvoice.Value.Invoice.AccountingCustomerParty.Party.PartyIdentification[0].ID.Value.ToString());
                    cmd.Parameters.AddWithValue("@SENDER_TAX_ID", GetInvoice.Value.Invoice.AccountingSupplierParty.Party.PartyIdentification[0].ID.Value.ToString());
                    cmd.Parameters.AddWithValue("@PROFILE_ID", GetInvoice.Value.Invoice.ProfileID.Value.ToString());
                    cmd.Parameters.AddWithValue("@PAYABLE_AMOUNT", GetInvoice.Value.Invoice.LegalMonetaryTotal.PayableAmount.Value.ToString());
                    cmd.Parameters.AddWithValue("@ISSUE_DATE", GetInvoice.Value.Invoice.IssueDate.Value.ToString("yyyy-MM-dd HH:mm:ss", new CultureInfo("en-US")));
                    cmd.Parameters.AddWithValue("@PARTY_NAME", GetInvoice.Value.Invoice.AccountingSupplierParty.Party.PartyName.Name.Value.ToString());
                    cmd.Parameters.AddWithValue("@PAYABLE_AMOUNT_CURRENCY", GetInvoice.Value.Invoice.LegalMonetaryTotal.PayableAmount.currencyID.ToString());//invoice.IssueDate.Value.ToString("MM")
                    var tmpPath = "einvoice_received/6/" + GetInvoice.Value.Invoice.IssueDate.Value.ToString("yyyy", new CultureInfo("en-US")) +"/" + GetInvoice.Value.Invoice.IssueDate.Value.ToString("MM", new CultureInfo("en-US")) + "/" + InvoiceId.ToString() + ".xml";
                    cmd.Parameters.AddWithValue("@PATH", tmpPath);

                    //cmd.Parameters.AddWithValue("@efaturaDate", response.Value.Definition.SystemCreateDate.ToString());



                    WriteToFile("Write sql " + cmd.CommandText.ToString());
                    WriteToFile("Write param " + GetInvoice.Value.Invoice.IssueDate.Value.ToString("yyyy-MM-dd HH:mm:sss", new CultureInfo("en-US")));

                    cmd.ExecuteNonQuery();
                    con.Close();
                    WriteToFile("New Invoice DB Write : " + InvoiceId);

                    /*  Gelen Faturaya Webservis tarafindan alindi kaydi gönderiliyor.*/
                    var guid = new String[] { InvoiceId.ToString() };
                    var response = client.SetInvoicesTaken(guid);
                    if (response.IsSucceded)
                    {
                        WriteToFile("Faturadaki yeni işareti kaldırılmıştır." + InvoiceId);
                    }
                    else
                    {
                        WriteToFile("Faturadaki yeni işareti kaldırılmadı." + InvoiceId);

                    }


                }
                catch (Exception hata)
                {
                    WriteToFile(hata.Message);
                }
                finally
                {
                    con.Close();
                    con.Dispose();


                }


            }
        }



        public void ShowInvoice(InvoiceType invoice)
        {
            var xslt = string.Empty;
            Edonusum.EFatXml EFatXml = new Edonusum.EFatXml();

            if (invoice.AdditionalDocumentReference != null)
            {
                AttachmentType attachment = null;
                DocumentReferenceType doc;
                byte[] xsltObject = null;

                for (int i = 0; i < invoice.AdditionalDocumentReference.Length; i++)
                {
                    doc = invoice.AdditionalDocumentReference[i];
                    attachment = doc.Attachment;
                    if (attachment != null && attachment.EmbeddedDocumentBinaryObject.filename != null)
                    {
                        string fileName = attachment.EmbeddedDocumentBinaryObject.filename;
                        if (Path.GetExtension(fileName) == ".xslt" || Path.GetExtension(fileName) == ".XSLT")
                        {
                            xsltObject = attachment.EmbeddedDocumentBinaryObject.Value;
                        }
                    }

                }


                if (xsltObject != null)
                {
                    using (var stream = new MemoryStream(xsltObject))
                    {
                        stream.Seek(0, SeekOrigin.Begin);
                        using (var reader = new StreamReader(stream))
                        {
                            xslt = reader.ReadToEnd();

                            XmlSerializer serializer = new XmlSerializer(typeof(InvoiceType));
                            using (MemoryStream mstr = new MemoryStream())
                            {

                                XmlSerializerNamespaces InvoiceNamespaces = new XmlSerializerNamespaces();

                                // InvoiceNamespaces.Add("", "urn:oasis:names:specification:ubl:schema:xsd:Invoice-2");
                                InvoiceNamespaces.Add("Invoice", "InvoiceType");
                                InvoiceNamespaces.Add("ext", "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2");
                                InvoiceNamespaces.Add("cac", "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2");
                                InvoiceNamespaces.Add("cbc", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2");
                               // InvoiceNamespaces.Add("cctc", "urn:un:unece:uncefact:documentation:2");
                                InvoiceNamespaces.Add("ds", "http://www.w3.org/2000/09/xmldsig#");
                              //  InvoiceNamespaces.Add("qdt", "urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2");
                              //  InvoiceNamespaces.Add("ubltr", "urn:oasis:names:specification:ubl:schema:xsd:TurkishCustomizationExtensionComponents");
                             //   InvoiceNamespaces.Add("udt", "urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2");
                               // InvoiceNamespaces.Add("xades", "http://uri.etsi.org/01903/v1.3.2#");
                               // InvoiceNamespaces.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance");


                                serializer.Serialize(mstr, invoice, InvoiceNamespaces);
                                EFatXml.tmpUUID = invoice.UUID.Value.ToString();
                                EFatXml.tmpXmlFilePath = "c:/WORKCUBE_DOSYA/CATALYST/documents/einvoice_received/6/" + invoice.IssueDate.Value.ToString("yyyy") + "/" + invoice.IssueDate.Value.ToString("MM") + "/" + invoice.UUID.Value.ToString() + ".xml";
                                EFatXml.tmpXmlPath = "c:/WORKCUBE_DOSYA/CATALYST/documents/einvoice_received/6/" + invoice.IssueDate.Value.ToString("yyyy") + "/" + invoice.IssueDate.Value.ToString("MM") + "/";

                                string xml = Encoding.UTF8.GetString(mstr.ToArray());
                                
                                EFatXml.WriteToXmlFile(xml);
                                //webBrowser1.DocumentText = TransformXMLToHTML(xml, xslt);

                                string text = File.ReadAllText(EFatXml.tmpXmlFilePath);
                                text = text.Replace("<InvoiceType ", "<Invoice ");
                                text = text.Replace("</InvoiceType>", "</Invoice>");
                                File.WriteAllText(EFatXml.tmpXmlFilePath, text);
                            }
                        }
                    }
                }
                else
                {
                    string msg = "Xslt Dosyası Bulunamadı";
                    WriteToFile(msg.ToString());
                }
            }
           
        }


        public void InboxInvoiceList ()
        {
            var client = CreateClient();
            InboxInvoiceListQueryModel ListInvoice = new InboxInvoiceListQueryModel();
            ListInvoice.PageSize = 1000;
            ListInvoice.PageIndex = 0;
            ListInvoice.CreateStartDate = DateTime.UtcNow.AddDays(-5);
            ListInvoice.CreateEndDate = DateTime.UtcNow;
            List<InboxInvoiceListItem> list = new List<InboxInvoiceListItem>();
            ListInvoice.OnlyNewestInvoices = true;

            try
            {
                var response = client.GetInboxInvoiceList(ListInvoice);
                
                WriteToFile("TotalCount" + response.Value.TotalCount.ToString());
                WriteToFile("TotalPages" + response.Value.TotalPages.ToString());

                if (response.IsSucceded && response.Value.Items != null)
                {
                    WriteToFile(response.Value.Items[0].InvoiceId.ToString());

                    list.AddRange(response.Value.Items);
                    WriteToFile("Fatura Sayisi :" + response.Value.TotalCount.ToString());
                    int v = response.Value.TotalCount + 1;
                    WriteToFile("Sayac Sayisi :" + v.ToString());
                    for (int i = 1; i < v; i++)
                    {
                        ListInvoice.PageIndex = i;
                        WriteToFile("Islenen Fatura " + i.ToString());
                        response = client.GetInboxInvoiceList(ListInvoice);
                        if (response.IsSucceded && response.Value.Items != null)
                        {
                            
                            list.AddRange(response.Value.Items);
                            WriteToFile("New Invoice ");
                            WriteToFile("InvoiceId = [EINVOICE_ID] = " + list[i].InvoiceId.ToString());
                            WriteToFile("DocumentId = [UUID]= " + list[i].DocumentId.ToString());
                            WriteToFile("CreateDateUtc = " + list[i].CreateDateUtc.ToString());
                            WriteToFile("Type Code = " + list[i].TypeCode.ToString());
                            WriteToFile("Type = [PROFILE_ID] = " + list[i].Type.ToString());
                            WriteToFile("TargetTcknVkn = [SENDER_TAX_ID] = " + list[i].TargetTcknVkn.ToString());
                            WriteToFile("TargetTitle = [PARTY_NAME] =" + list[i].TargetTitle.ToString());
                            WriteToFile("EnvelopeIdentifier = " + list[i].EnvelopeIdentifier.ToString());
                            WriteToFile("Status = " + list[i].Status.ToString());
                            WriteToFile("EnvelopeStatus = " + list[i].EnvelopeStatus.ToString());
                            WriteToFile("EnvelopeStatusCode = " + list[i].EnvelopeStatusCode.ToString());
                            WriteToFile("ExecutionDate = " + list[i].ExecutionDate.ToString());
                            WriteToFile("PayableAmount = [PAYABLE_AMOUNT] = " + list[i].PayableAmount.ToString());
                            WriteToFile("TaxTotal = " + list[i].TaxTotal.ToString());
                            WriteToFile("InvoiceTipType = " + list[i].InvoiceTipType.ToString());
                            WriteToFile("InvoiceTipTypeCode = " + list[i].InvoiceTipTypeCode.ToString());
                            WriteToFile("IsArchived = " + list[i].IsArchived.ToString());
                            WriteToFile("Vat0TaxableAmount = " + list[i].Vat0TaxableAmount.ToString());
                            WriteToFile("Vat1 = " + list[i].Vat1.ToString());
                            WriteToFile("Vat8TaxableAmount = " + list[i].Vat8TaxableAmount.ToString());
                            WriteToFile("Vat8 = " + list[i].Vat8.ToString());
                            WriteToFile("Vat18TaxableAmount = " + list[i].Vat18TaxableAmount.ToString());
                            WriteToFile("Vat18 = " + list[i].Vat18.ToString());
                            // WriteToFile("OrderDocumentId = " + list[i].OrderDocumentId.ToString());
                        
                           
                            GetInvoiceXslt_Click(list[i].DocumentId.ToString());
                            GetInboxInvoices(list[i].DocumentId.ToString());

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteToFile(ex.Message + DateTime.Now);
                //throw new ArgumentOutOfRangeException("index parameter is out of range.", ex);
            }


        }

        public void EfaturaInsert (String InvoiceId, String DocumentId, String TypeCode,String Type)
        {
            con = new SqlConnection(SQLConnection.ConnectionString);
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("SELECT * FROM EINVOICE_RECEIVING_DETAIL WHERE  UUID='" + DocumentId + "' AND EINVOICE_ID='" + InvoiceId + "'", con);
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmd;
                DataTable tab = new DataTable();
                sda.Fill(tab);
                if (tab.Rows.Count > 0)
                {
                    WriteToFile("Kayıt Var!");
                }
                else
                {
                    String ServiceResult = "Successful";
                    String InvoiceType = "SATIS";

                    EFaturaDbInsert(ServiceResult, DocumentId, InvoiceId, TypeCode, InvoiceType,Type);
                    WriteToFile("Kayıt Yok!");
                }
            }
            catch (Exception hata)
            {
                WriteToFile(hata.Message+ "Hata");
            }
            finally
            {
                con.Close();
                con.Dispose();


            }
        }

        public void EFaturaDbInsert (String ServiceResult, String UuId, String EInvoiceId, String TypeCode, String InvoiceType, String Type)
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("INSERT INTO EINVOICE_RECEIVING_DETAIL (SERVICE_RESULT, UUID,EINVOICE_ID,STATUS_DESCRIPTION,STATUS_CODE,ERROR_CODE,INVOICE_TYPE_CODE,"+
                "PROFILE_ID) " +
                                                            "VALUES (@ServiceResult, @UuId, @EInvoiceId, @StatusDesc, @StatusCode,0,@InvoiceType,@ProfileId)", con);
            cmd.Parameters.AddWithValue("@ServiceResult", ServiceResult);
            cmd.Parameters.AddWithValue("@UuId", UuId);

            if (Type == "BaseInvoice")
            {
                
                 cmd.Parameters.AddWithValue("@ProfileId", "TEMELFATURA");
            }
            if (Type == "ComercialInvoice")
            {

                cmd.Parameters.AddWithValue("@ProfileId", "TICARIFATURA");
            }
            if (Type == "InvoiceWithPassenger")
            {

                cmd.Parameters.AddWithValue("@ProfileId", "YOLCUFATURA");
            }
            if (Int32.Parse(TypeCode) == 0)
            {
                cmd.Parameters.AddWithValue("@StatusDesc", "SİSTEME KAYDEDİLDİ");
                cmd.Parameters.AddWithValue("@StatusCode", "24");
            }
            
            cmd.Parameters.AddWithValue("@InvoiceType", InvoiceType);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();

        }

        public IntegrationClient CreateClient()
        {
            var username = "";
            var password = "";
            var serviceuri = "";
            var LiveTestSystem = 1;
            if (LiveTestSystem == 0)
            {
                username = "CrsDemo85";
                password = "11223385";
                serviceuri = "https://connect-test.crssoft.com/Services/Integration";
            }
            if (LiveTestSystem == 1)
            {
                //username = "TurkpakInsaat_WebServis";
                //password = "Turkpak1234**";
                username = "Alya_WebServis";
                password = "@Alya@9248Deri";
                //username = "AvistaGlobal_WebServis";
                //password = "@Avista@9248Global";
                serviceuri = "https://connect.crssoft.com/Services/Integration";
            }

            var client = new IntegrationClient();
            client.Endpoint.Address = new System.ServiceModel.EndpointAddress(serviceuri);
            client.ClientCredentials.UserName.UserName = username;
            client.ClientCredentials.UserName.Password = password;

            return client;
        }
        public void WriteToFile(string Message)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\Logs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\ServiceLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
            if (!File.Exists(filepath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }

        public void GetInvoiceXslt_Click( string InvoiceId)

        {

            var client = CreateClient();
            InvoiceResponse response;
            //var txtGetXsltFromGuid = "48C937CB-D3BB-4C8C-A42D-FC520C83C001";
            WriteToFile("GetInvoiceXslt" + InvoiceId.ToString());
            response = client.GetInboxInvoice(InvoiceId.ToString());
            if (response.IsSucceded && response.Value != null)
            {
                WriteToFile("GetInvoiceXslt_Click Kayıt Bulundu...!");
                WriteToFile("GetInvoiceXslt_Click Kayıt Bulundu...!" + response.Value.LocalDocumentId);
                WriteToFile("GetInvoiceXslt_Click UBLExtensions...: " + response.Value.Invoice.UBLExtensions);
                   

            }
            else
            {
                WriteToFile("GetInvoiceXslt Kayıt Bulunamadı!");
            }
        }

        public void SendInvoiceList()
        {
            WriteToFile(" Check Send Invoice List ");
            con = new SqlConnection(SQLConnection.ConnectionString);
            try
            {
                int i = 0;
                con.Open();
                SqlCommand cmd = new SqlCommand("SELECT E.SENDING_DETAIL_ID,E.SERVICE_RESULT ,E.EINVOICE_ID ,E.STATUS_DESCRIPTION,E.STATUS_CODE,E.ERROR_CODE, " +
                        " E.ACTION_ID,E.ACTION_TYPE,E.IS_SUCCESFULL,E.SERVICE_RESULT_DESCRIPTION,E.INVOICE_TYPE_CODE,DATEPART(MONTH, I.INVOICE_DATE) AS AY, DATEPART(YEAR, I.INVOICE_DATE) AS YIL "+
                        " FROM workcube_catalyst.workcube_catalyst_2022_6.EINVOICE_SENDING_DETAIL E,  workcube_catalyst.workcube_catalyst_2022_6.INVOICE I   WHERE E.UUID = ''  AND E.ACTION_ID = I.INVOICE_ID", con);


                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmd;
                DataTable tab = new DataTable();
                sda.Fill(tab);

                WriteToFile(" Send Invoice Listcount 1 :" + tab.Rows.Count);

                if (tab.Rows.Count > 0)
                {
                    WriteToFile(" Send Invoice Listcount :" + tab.Rows.Count);

                    for (i = 0; i <= tab.Rows.Count ; i++)
                    {
                        WriteToFile(" SendDraftInvoice" + tab.Rows[i].ItemArray[2].ToString());

                        SendDraftInvoice(tab.Rows[i].ItemArray[2].ToString(), tab.Rows[i].ItemArray[11].ToString(), tab.Rows[i].ItemArray[12].ToString());
                    }

                }
                
            }
            catch (Exception hata)
            {
                WriteToFile(hata.Message + "Hata");
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }


        public void SendDraftInvoice (string tmpEInvoiceId, string  tmpMonth, string tmpYear)
        {
            var client = CreateClient();
            var data = new BinaryRequestData();
            WriteToFile(" Send Draft Invoice ");

            //string xmlFileName = "c:/WORKCUBE_DOSYA/CATALYST/documents/einvoice_send/6/" + tmpYear.ToString() + "/" + tmpMonth.ToString() + "/" + tmpEInvoiceId.ToString() + ".xml";

            //string sFileName = "c:/WORKCUBE_DOSYA/CATALYST/documents/einvoice_send/6/" + tmpYear.ToString() + "/" + tmpMonth.ToString() + "/" + tmpEInvoiceId.ToString() + ".zip";
            //ZipFile.CreateFromDirectory(xmlFileName, sFileName);

            if (tmpMonth.Length == 1)
            {
                tmpMonth = "0" + tmpMonth.ToString();
            }

            string fileName = tmpEInvoiceId.ToString() + ".xml";
            string sourcePath = @"c:/WORKCUBE_DOSYA/CATALYST/documents/einvoice_send/6/" + tmpYear.ToString() + "/" + tmpMonth.ToString() ;
            string targetPath = @"c:/WORKCUBE_DOSYA/CATALYST/documents/einvoice_send/6/" + tmpYear.ToString() + "/" + tmpMonth.ToString()  + tmpEInvoiceId.ToString();

            string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
            string destFile = System.IO.Path.Combine(targetPath, fileName);

            System.IO.Directory.CreateDirectory(targetPath);
            System.IO.File.Copy(sourceFile, destFile, true);

            string sFileName = "c:/WORKCUBE_DOSYA/CATALYST/documents/einvoice_send/6/" + tmpYear.ToString() + "/" + tmpMonth.ToString() + "/" + tmpEInvoiceId.ToString() + ".zip";
            
            if (File.Exists(sFileName))
            {
                File.Delete(sFileName);
            }

            ZipFile.CreateFromDirectory(targetPath, sFileName);

            if (System.IO.Directory.Exists(targetPath))
            {
                try
                {
                    System.IO.Directory.Delete(targetPath, true);
                }
                catch (System.IO.IOException e)
                {
                    WriteToFile("Silme Hatası");
                }
            }


            using (FileStream stream = File.OpenRead(sFileName))
            {
                byte[] file = new byte[stream.Length];
                stream.Read(file, 0, file.Length);
                data.Data = file;
                var response = client.CompressedSaveAsDraft(data);

                if (response.IsSucceded)
                {
                    WriteToFile(response.Value[0].Id.ToString() + response.Value[0].Number.ToString());
                    UpdateSendInvoiceState(tmpEInvoiceId.ToString(), response.Value[0].Id.ToString(), "Successful");

                }
                else
                {
                    WriteToFile("Fatura Gönderme Hatası " + response.Message);
                    UpdateSendInvoiceState((tmpEInvoiceId.ToString()??""),  "", response.Message);
                }
            }

        }

        public void UpdateSendInvoiceState (string tmpInvoiceId, string tmpInvoideUUID, string tmpErrorCode)
        {
            WriteToFile(" Check Send Invoice List ");
            con = new SqlConnection(SQLConnection.ConnectionString);
            try
            {

                con.Open();
                WriteToFile("Update Send Invoice ");
                SqlCommand cmd = new SqlCommand("UPDATE workcube_catalyst.workcube_catalyst_2022_6.EINVOICE_SENDING_DETAIL " +
                                                " SET SERVICE_RESULT = @Error, STATUS_DESCRIPTION ='Portalda Onay bekliyor', UUID = @UUID  " +
                                                " WHERE EINVOICE_ID =  @InvoiceId", con);
                cmd.Parameters.AddWithValue("@InvoiceId", tmpInvoiceId.ToString());
                cmd.Parameters.AddWithValue("@UUID", tmpInvoideUUID.ToString());
               
                if (tmpErrorCode != "Successful")
                {
                    cmd.Parameters.AddWithValue("@UUID",DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@Error", tmpErrorCode.ToString());
                }
                WriteToFile(cmd.ToString());

                cmd.ExecuteNonQuery();
                con.Close();


            }
            catch (Exception hata)
            {
                WriteToFile("Update Send Invoice Error : " + hata.Message);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }


        public void SendArchiveInvoiceList()
        {
            WriteToFile(" Check Send Archice Invoice List ");
            con = new SqlConnection(SQLConnection.ConnectionString);
            try
            {
                int i = 0;
                con.Open();
                SqlCommand cmd = new SqlCommand("SELECT E.SENDING_DETAIL_ID,E.SERVICE_RESULT,E.EARCHIVE_ID ,E.STATUS_DESCRIPTION,E.STATUS_CODE,E.ERROR_CODE, " +
                        " E.ACTION_ID,E.ACTION_TYPE,E.SERVICE_RESULT_DESCRIPTION,E.INVOICE_TYPE_CODE,DATEPART(MONTH, I.INVOICE_DATE) AS AY, DATEPART(YEAR, I.INVOICE_DATE) AS YIL " +
                        " FROM workcube_catalyst.workcube_catalyst_2022_6.EARCHIVE_SENDING_DETAIL E,  workcube_catalyst.workcube_catalyst_2022_6.INVOICE I   WHERE E.UUID = ''  AND E.ACTION_ID = I.INVOICE_ID", con);


                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmd;
                DataTable tab = new DataTable();
                sda.Fill(tab);

                WriteToFile(" Send Archice Invoice Listcount 1 :" + tab.Rows.Count);

                if (tab.Rows.Count > 0)
                {
                    WriteToFile(" Send Archice Invoice Listcount :" + tab.Rows.Count);

                    for (i = 0; i <= tab.Rows.Count; i++)
                    {
                        WriteToFile(" SendDraftArchiceInvoice" + tab.Rows[i].ItemArray[2].ToString());

                        SendDraftArchiceInvoice(tab.Rows[i].ItemArray[2].ToString(), tab.Rows[i].ItemArray[10].ToString(), tab.Rows[i].ItemArray[11].ToString());
                    }

                }

            }
            catch (Exception hata)
            {
                WriteToFile(hata.Message + "Hata");
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        public void SendDraftArchiceInvoice(string tmpEInvoiceId, string tmpMonth, string tmpYear)
        {
            var client = CreateClient();
            var data = new BinaryRequestData();
            WriteToFile(" Send Draft Archice Invoice ");

            if (tmpMonth.Length == 1)
            {
                tmpMonth = "0" + tmpMonth.ToString();
            }


           // string xmlFileName = "c:/WORKCUBE_DOSYA/CATALYST/documents/earchive_send/6/" + tmpYear.ToString() + "/" + tmpMonth.ToString() + "/xml/" + tmpEInvoiceId.ToString() + ".xml";
           // string sFileName = "c:/WORKCUBE_DOSYA/CATALYST/documents/earchive_send/6/" + tmpYear.ToString() + "/" + tmpMonth.ToString() + "/zip/" + tmpEInvoiceId.ToString() + ".zip";
           // ZipFile.CreateFromDirectory(xmlFileName, sFileName);


            string fileName = tmpEInvoiceId.ToString() + ".xml";
            string sourcePath = @"c:/WORKCUBE_DOSYA/CATALYST/documents/earchive_send/6/" + tmpYear.ToString() + "/" + tmpMonth.ToString() +  "/xml";
               
            string targetPath = @"c:/WORKCUBE_DOSYA/CATALYST/documents/earchive_send/6/" + tmpYear.ToString() + "/" + tmpMonth.ToString() + "/xml/ + tmpEInvoiceId.ToString()";

            string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
            string destFile = System.IO.Path.Combine(targetPath, fileName);

            System.IO.Directory.CreateDirectory(targetPath);
            System.IO.File.Copy(sourceFile, destFile, true);

            string sFileName = @"c:/WORKCUBE_DOSYA/CATALYST/documents/earchive_send/6/" + tmpYear.ToString() + "/" + tmpMonth.ToString() + "/zip/" + tmpEInvoiceId.ToString() + ".zip";
            if (File.Exists(sFileName))
            {
                File.Delete(sFileName);
            }

            ZipFile.CreateFromDirectory(targetPath, sFileName);

            if (System.IO.Directory.Exists(targetPath))
            {
                try
                {
                    System.IO.Directory.Delete(targetPath, true);
                }
                catch (System.IO.IOException e)
                {
                    WriteToFile("Silme Hatası");
                }
            }


            // Archive Fatura Gonderilecek
            using (FileStream stream = File.OpenRead(sFileName))
            {
                byte[] file = new byte[stream.Length];
                stream.Read(file, 0, file.Length);
                data.Data = file;
                var response = client.CompressedSaveAsDraft(data);

                if (response.IsSucceded)
                {
                    WriteToFile("Arşiv Fatura Gönderildi " +  response.Value[0].Id.ToString() + response.Value[0].Number.ToString());
                    UpdateSendArchiceInvoiceState(tmpEInvoiceId.ToString(), response.Value[0].Id.ToString(), "Successful");

                }
                else
                {
                    WriteToFile("Arşiv Fatura Gönderme Hatası " + response.Message);
                    UpdateSendArchiceInvoiceState((tmpEInvoiceId.ToString() ?? ""),  "", response.Message);
                }
            }

        }

        public void UpdateSendArchiceInvoiceState(string tmpInvoiceId, string tmpInvoideUUID, string tmpErrorCode)
        {
            WriteToFile(" Check Send Archice Invoice List ");
            con = new SqlConnection(SQLConnection.ConnectionString);
            try
            {

                con.Open();
                WriteToFile("Update Send Archice Invoice ");
                SqlCommand cmd = new SqlCommand("UPDATE workcube_catalyst.workcube_catalyst_2022_6.EARCHIVE_SENDING_DETAIL " +
                                                " SET SERVICE_RESULT = @Error, STATUS_DESCRIPTION ='Portalda Onay bekliyor', UUID = @UUID  " +
                                                " WHERE EARCHIVE_ID =  @InvoiceId", con);
                cmd.Parameters.AddWithValue("@InvoiceId", tmpInvoiceId.ToString());
                cmd.Parameters.AddWithValue("@UUID", tmpInvoideUUID.ToString());

                if (tmpErrorCode != "Successful")
                {
                    cmd.Parameters.AddWithValue("@UUID", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@Error", tmpErrorCode.ToString());
                }

                WriteToFile(cmd.ToString());
                cmd.ExecuteNonQuery();
                con.Close();


            }
            catch (Exception hata)
            {
                WriteToFile("Update Send Archice Invoice Error : "+ hata.Message );
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

    }
}
