<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"
	xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"
	xmlns:ccts="urn:un:unece:uncefact:documentation:2" xmlns:clm54217="urn:un:unece:uncefact:codelist:specification:54217:2001"
	xmlns:clm5639="urn:un:unece:uncefact:codelist:specification:5639:1988"
	xmlns:clm66411="urn:un:unece:uncefact:codelist:specification:66411:2001"
	xmlns:clmIANAMIMEMediaType="urn:un:unece:uncefact:codelist:specification:IANAMIMEMediaType:2003"
	xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:link="http://www.xbrl.org/2003/linkbase"
	xmlns:n1="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2"
	xmlns:qdt="urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2"
	xmlns:udt="urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2"
	xmlns:xbrldi="http://xbrl.org/2006/xbrldi" xmlns:xbrli="http://www.xbrl.org/2003/instance"
	xmlns:xdt="http://www.w3.org/2005/xpath-datatypes" xmlns:xlink="http://www.w3.org/1999/xlink"
	xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsd="http://www.w3.org/2001/XMLSchema"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	exclude-result-prefixes="cac cbc ccts clm54217 clm5639 clm66411 clmIANAMIMEMediaType fn link n1 qdt udt xbrldi xbrli xdt xlink xs xsd xsi">
	<xsl:decimal-format name="european"
		decimal-separator="," grouping-separator="." NaN="" />
	<xsl:output version="4.0" method="html" indent="no"
		encoding="UTF-8" doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN"
		doctype-system="http://www.w3.org/TR/html4/loose.dtd" />
	<xsl:param name="SV_OutputFormat" select="'HTML'" />
	<xsl:variable name="XML" select="/" />
	<xsl:variable name="today" select="4"/>
	<xsl:template match="/">
		<html>
			<head>
				<title/>
				<style type="text/css">
				  body {
				  background-color: #FFFFFF;
				  font-family: 'Tahoma', "Times New Roman", Times, serif;
				  font-size: 11px;
				  color: #666666;
				  }

				  h1, h2 {
				  padding-bottom: 3px;
				  padding-top: 3px;
				  margin-bottom: 5px;
				  text-transform: uppercase;
				  font-family: Arial, Helvetica, sans-serif;
				  }

				  h1 {
				  font-size: 1.4em;
				  text-transform:none;
				  }

				  h2 {
				  font-size: 1em;
				  color: brown;
				  }

				  h3 {
				  font-size: 1em;
				  color: #333333;
				  text-align: justify;
				  margin: 0;
				  padding: 0;
				  }

				  h4 {
				  font-size: 1.1em;
				  font-style: bold;
				  font-family: Arial, Helvetica, sans-serif;
				  color: #000000;
				  margin: 0;
				  padding: 0;
				  }

				  hr {
				  border: 0;
				  color: #9E9E9E;
				  background-color: #9E9E9E;
				  height: 1px;
				  width: 100%;
				  text-align: left;
				  }

				  p, ul, ol {
				  margin-top: 1.5em;
				  }

				  ul, ol {
				  margin-left: 3em;
				  }

				  blockquote {
				  margin-left: 3em;
				  margin-right: 3em;
				  font-style: italic;
				  }

				  a {
				  text-decoration: none;
				  color: #70A300;
				  }

				  a:hover {
				  border: none;
				  color: #70A300;
				  }

				  table{
				  border-spacing:0px;
				  border-color:black:
				  }

				  td{
				  /*border-color:gray;*/
				  }

				  #despatchTable{
				  border-collapse:collapse;
				  font-size:11px;
				  float:right;
				  /*border-color:gray;*/
				  }

				  #ettnTable{
				  border-collapse:collapse;
				  font-size:11px;
				  /*border-color:gray;*/
				  }

				  #customerPartyTable{
				  border-width: 0px;
				  border-spacing: ;
				  border-style: inset;
				  /*border-color:gray;*/
				  border-collapse: separate;
				  background-color:
				  }

				  #customerIDTable{
				  border-width: 2px;
				  border-spacing: ;
				  border-style: inset;
				  /*border-color:gray;*/
				  border-collapse: collapse;
				  background-color:
				  }

				  #customerIDTableTd{
				  border-width: 2px;
				  border-spacing: ;
				  border-style: inset;
				  /*border-color:gray;*/
				  border-collapse: collapse;
				  background-color:
				  }

				  #lineTable{
				  border-width:2px;
				  border-spacing: ;
				  border-style: inset;
				  border-color: black;
				  
				  border-collapse: collapse;
				  }

				  .lineTableTd{
				  border: 1px solid black;
				  padding: 1px;
				  border-style: inset;
				  background-color: #FFF;
				  }

				  #lineTableTr{
				  border-bottom: 1px solid #ddd;
				  }

				  #lineTableDummyTd {
				  border-width: 1px;
				  border-color:white;
				  padding: 1px;
				  border-style: inset;
				  border-color: black;
				  background-color: #FFF;
				  }

				  .lineTableBudgetTd{
				  border-width: 2px;
				  border-spacing:0px;
				  padding: 1px;
				  border-style: inset;
				  border-color: black;
				  background-color: #FFF;
				  -moz-border-radius: 5px;
				  }

				  .notesTable{
				  border: 1px solid black;
				  }


				  #notesTableTd{
				  border-width: 0px;
				  border-spacing: ;
				  border-style: inset;
				  border-color: black;
				  border-collapse: collapse;
				  }

				  .budgetContainerTable{
				  border-width: 0px;
				  border-spacing: 0px;
				  border-style: inset;
				  border-color: black;
				  border-collapse: collapse;
				  background-color: #FFF;
				  margin-top:10px;
				  }
				  .mustafa th{
				  border-bottom: 2px solid black;
				  }

        </style>
				<title>Elektronik Fatura</title>
			</head>
			<body style="margin-left=0.6in; margin-right=0.6in; margin-top=0.79in; margin-bottom=0.79in">
				<xsl:for-each select="$XML">
					<!-- Hİzmet NO Hizmet Türü Tablosu-->
					<table  border="0" cellspacing="0px" width="800" cellpadding="0px" >
						<tbody>
						<!-- 1. Satır-->
							<tr valign="top" >
								<td width="35%" align="right" valign="top" 	>
								<br/><br/>
									<table align="center" border="0" width="100%" >
										<tbody>
										<tr align="left" valign="top">
											<td>
												<xsl:value-of select="//n1:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PartyName/cbc:Name"/>
												<br/>
											</td>
										</tr>
										<xsl:if test="//n1:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PostalAddress/cbc:StreetName">
										<tr align="left" valign="top">
											<td>
												<xsl:value-of select="substring(//n1:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PostalAddress/cbc:StreetName,0,40)"/>
												<br/>
												<xsl:value-of select="//n1:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PostalAddress/cbc:BuildingNumber"/>
												<xsl:text>&#160;</xsl:text>
												<br/>
												<xsl:value-of select="//n1:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PostalAddress/cbc:PostalZone"/>
												<xsl:text>&#160;</xsl:text>
												<xsl:value-of select="//n1:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PostalAddress/cbc:CitySubdivisionName"/>
												<xsl:text>&#160;/&#160;</xsl:text>
												<xsl:value-of select="//n1:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PostalAddress/cbc:CityName"/>
												<br/>
											</td>
										</tr>
										</xsl:if>
										<xsl:if test="//n1:Invoice/cac:AccountingSupplierParty/cac:Party/cac:Contact/cbc:Telephone or //n1:Invoice/cac:AccountingSupplierParty/cac:Party/cac:Contact/cbc:Telefax">
										<tr align="left" valign="top">
											<td>
												<xsl:if test="//n1:Invoice/cac:AccountingSupplierParty/cac:Party/cac:Contact/cbc:Telephone != ''">
													<b>
														<xsl:text>Tel:&#160;</xsl:text>
													</b>
													<xsl:value-of select="//n1:Invoice/cac:AccountingSupplierParty/cac:Party/cac:Contact/cbc:Telephone" />
													<xsl:text>&#160;</xsl:text>
													<xsl:text>&#160;</xsl:text>
												</xsl:if>
												<xsl:if test="//n1:Invoice/cac:AccountingSupplierParty/cac:Party/cac:Contact/cbc:Telefax != ''">
													<b>
														<xsl:text>Faks:&#160;</xsl:text>
													</b>
													<xsl:value-of select="//n1:Invoice/cac:AccountingSupplierParty/cac:Party/cac:Contact/cbc:Telefax" />
												</xsl:if>
											</td>
										</tr>
										</xsl:if>
										<xsl:for-each select="//n1:Invoice/cac:AccountingSupplierParty/cac:Party/cac:Contact/cbc:ElectronicMail">
										<tr align="left" valign="top">
											<td>
												<b>
													<xsl:text>E-Posta&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;: </xsl:text>
												</b>
												<xsl:value-of select="." />
											</td>
										</tr>
										</xsl:for-each>
										<xsl:for-each select="//n1:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PartyTaxScheme/cac:TaxScheme/cbc:Name">
										<tr align="left" valign="top">
											<td>
												<b>
													<xsl:text>Vergi Dairesi&#160;&#160;&#160;&#160;&#160;: </xsl:text>
												</b>
												<xsl:value-of select="." />
											</td>
										</tr>
										</xsl:for-each>
										<xsl:for-each select="//n1:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PartyIdentification">
										<tr align="left" valign="top">
											<td>
												<xsl:if test="normalize-space(./cbc:ID/@schemeID) = 'VKN'">
													<span>
														<b>
															<xsl:text>Vergi Numaras&#305;: </xsl:text>
														</b>
														<xsl:value-of select="." />
													</span>
												</xsl:if>
											</td>
										</tr>
										</xsl:for-each>
										<xsl:for-each select="//n1:Invoice/cac:AccountingSupplierParty/cac:Party/cbc:WebsiteURI">
										<tr align="left" valign="top">
											<td>
												<b>
													<xsl:text>Web Sitesi&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;: </xsl:text>
												</b>
												<xsl:value-of select="." />
											</td>
										</tr>
										</xsl:for-each>
											<xsl:for-each select="//n1:Invoice/cbc:Note">
												<xsl:choose>
													<xsl:when test="substring(.,0,6) = 'UPHR:'">
														<tr align="left" valign="top">
															<td >
																<span style="font-weight:bold; ">
																	<xsl:value-of select="normalize-space(substring-before(substring(.,6),':'))"/>
																	<xsl:text>:&#160;</xsl:text>
																</span>
																<xsl:value-of select="normalize-space(substring-after(substring(.,6),':'))"/>
															</td>
														</tr>
													</xsl:when>
												</xsl:choose>
											</xsl:for-each>
											
										</tbody>
									</table>
									
								</td>
								<td width="20%" valign="top">
									<br/>
									<br/>
									<center>
										<img style="width:91px;" align="middel" alt="E-Fatura Logo" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/4QBoRXhpZgAASUkqAAgAAAADABIBAwABAAAAAQAAADEBAgAQAAAAMgAAAGmHBAABAAAAQgAAAAAAAABTaG90d2VsbCAwLjIyLjAAAgACoAkAAQAAAKYBAAADoAkAAQAAAKYBAAAAAAAA/+EJ9Gh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8APD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iWE1QIENvcmUgNC40LjAtRXhpdjIiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczpleGlmPSJodHRwOi8vbnMuYWRvYmUuY29tL2V4aWYvMS4wLyIgeG1sbnM6dGlmZj0iaHR0cDovL25zLmFkb2JlLmNvbS90aWZmLzEuMC8iIGV4aWY6UGl4ZWxYRGltZW5zaW9uPSI0MjIiIGV4aWY6UGl4ZWxZRGltZW5zaW9uPSI0MjIiIHRpZmY6SW1hZ2VXaWR0aD0iNDIyIiB0aWZmOkltYWdlSGVpZ2h0PSI0MjIiIHRpZmY6T3JpZW50YXRpb249IjEiLz4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8P3hwYWNrZXQgZW5kPSJ3Ij8+/9sAQwADAgIDAgIDAwMDBAMDBAUIBQUEBAUKBwcGCAwKDAwLCgsLDQ4SEA0OEQ4LCxAWEBETFBUVFQwPFxgWFBgSFBUU/9sAQwEDBAQFBAUJBQUJFA0LDRQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQU/8AAEQgAaQBpAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A/VOiioL6+ttMsp7y8njtbSBGlmnmcIkaKMszMeAABkk0bgT1458QP2nfDvhbxDJ4W8N2F/8AEHxsvB0Hw6gla3PTNzMf3cC567jkelcJqHjHxT+1FJeL4Z1a48B/Bq03i88Vg+Tfa0qZ8wWpb/UwDBzMeTjj+IVTl+JHhz4QeArPT/gf4dtJ7SG/FtqEj6dcuVLQmSGaX7ssiT4wtyPMU/wiQkLXuUcCoO1Vc0/5dkv8T6P+6tel09DzqmIurwdl36v0X6/mdDdaJ8c/HdpJfeJ/GWh/B7QgNz2OhwpfXqIf4ZbubEaN/tRrisTSv2evhJ4v8XXnhrxD4w8W/EHxDaq7Twa9r94UOzZ5gTyzHG2wyR7lTOzeoYDIr1P4l/CeL41aDod415eeGNUjETuypuZ7dmjkmtJoyQGB2Lz1VlBHcHW0D4L+GfDPxC1Xxlp0E9vq2pl3uFWUiFncIHfb3J8tepIB3FQCzZFjeSD5ZcktdIpKz0teW7W/VsHQ5parmXdu/wCGy+4+KPi34e+Cvwt8W+NPDSfBfSr+60p7VNLaTUrkG/zBHcXhY7iV8qKRW4znPOK9b1f4H/Anwn4p1LQNHvPFPgTXtOsZdSdtB1bULYeVFGskjRu7NExVWUkD1I6g4+gfEHwW8EeK9VudS1bw5aX1/cGQy3Eu7e3mQJA/IPG6KKNDjsorD1/9m7wVr2peItQa3vbO/wBes7yyvZ7a8flLpY1nZEYsiMwhQZC9j611vNIzjCLqTTS195u706N7aN7dTH6m4tvli9dNLaa+W/8AkeYeFtE+Lek28M/gP4lP4th+wWuonw98RNM/exxTqWRDf24GZcKQV+bbwTwwJ6rw/wDtT2mka3beHfin4cvfhdr87eXBNqMizaVeN6Q3q/Jnvh9pGQOTVHx/8NvF1l4ss4fBPnpqOq+IV1m8164RFstPtY7B7RINgk3SMn7t1j27WYnJA3Yk8G+L734o+MvEnw08V+FYtY8L6bFNaTXWq+XLPN5TJHHLcIMAGf8AeSJhFwqBlLZ+XOfsq8eecVJWu2rRkvu0evdXfdFR56cuWLad+uqf6r5Ox7+jrKiujBkYZDA5BFOr5QdtX/Za8SX9p4K1R/Hfw/05EuNX8Dtci41bw9A+SJ7XJ3vDgE+U3IAyDySPpTwX400X4h+GLDxD4e1CHVNHvoxLBcwHIYdwR1BByCpwQQQRkV5NfCuilUi+aD2f6NdH+fRtHbSrKb5XpJdP8u/9XNuiiiuI6Ar5m8X3M37U/wARNR8IW9y9t8I/CtwE8R3sTlBrV6mG+wq4/wCWMfBlIPJwPQ13X7TfxD1Twd4FtdE8MMP+E18W3iaFovPMUsv37g+ixR7nz0BC5615L9v8P+GPDKfBnw7pZ8XeE7SyxfX3htxeX9ldQXCec9/aEDzElmOSiszOvmDYV5HuYGhKEPbr4nt5Jby9VtHzvbVI87EVE37N7Lfz7L/Py9To/EfirUNS+KZ8F6PpNv4T1rS7SCTw3GYhPb6rp5a4juIrpIgwhtD9nQKRypeFiMkR17N8P/hZoXw6tIYtMt2MsMBtIZ5yHlitfNeSO2V8AmKMyFUByQuBmsr4HfCWP4R+CLPSJboajfRhla4HmbIkLErDCJHdkiXsm4jJYjGcV6LXHia6b9lRfur8fPv52u92b0aTXvz3/IK+Zf2vv2s4/gnYL4e8NyQ3PjS6QPl1DpYRHo7joXP8Kn6njAPo/wC0d8cLH4D/AA5utcmEc+qz5t9Ns2P+unI4JHXav3mPoMdSK/IfxL4k1Hxdr1/rOr3cl9qV9M09xcSHJdiefoPQdAOBXw2dZo8JH2FF++/wX+Z/QfhlwLHiCs80zGN8NTdkn9uS6f4V17vTue6f8N7/ABl/6GC0/wDBbB/8RR/w3t8Zf+hgtP8AwWwf/EV88gV9ifsa/sejx6bXxx41tSPDiMH0/TZRj7eQf9Y4/wCeQPQfxf7v3vksJWzHGVVSpVZX9Xp5s/oTiDLeDuGsDLH47A0lFaJKEbyfSKVt3+C1eh6x+zL4o+P/AMaGg13X/EMWheDshll/suAT3w9IgU4X/bIx6A84+vJ45HtpEjlMUrIVWXaDtOODjoa8y8Y/tIfDH4XeILfwzrXiW00zUFCJ9kiid1twQNocopWMYxwxGBg9K9NtrmK8t4p4JEmhlUOkkbBldSMggjqCK/RsHGNKLpqpzyW93d39Oh/GHEdevjq8ca8EsNRn/DUYcsXHunZc77v7rI+PtE8Az/AL4gQeJ/HGpy3K27XN3ay2d0ss+vag8TrPcSeZGv2aPyNm6NphCrxxnICiti51K1+AOqad8WPBwkl+DfjDybrX9KjjIXS5JwPL1KGP+FTuUSoB3BweNv0Z478B6L8RNBfS9c0201S3DrNFHexeZGsqnKkgEEjPBGRuUsp4JFeA/DbT00Dxj4p0/wCKfivStd1TXZW0aHR5rZlmisnfy4FMccrxW9vMVbYpRSTJEGkZ2Ar7WniliIudTV2tKP8AMvJdGt79H5Oy/O5UXSkox23T7Pz/ACt1Ppu2uYb22iuLeVJoJUEkcsbBldSMggjqCO9S18//ALM+o3nw/wBd8T/BbWbmS5n8LFLvQbmc5e60aUnyee5hYGInpwor6Arw8RR9hUcL3W6fdPVP7j0aVT2kFLZ9fXqfPujIPib+2HrmoS/vdL+HOjxadaKeVGoXo8yaRT6rCqIfTdXp9z4K8I6t8RYtZ/s5I/F2mQpI1/brJBI8UgkRUkdcLMvyP8jFgCAcDg185fCLwrrvjv4f6x400S2g1W5vviPqHiGXSrq9e0j1G3haS3hhMqq2PLZI5FDAqWiAPByPoL4O2fiCHSdcvfEMipPqOrz3dvpyagb4adGQim387AziRJX2jhPM2DhRXp42PsnaM7ciUbX+/wA9Xd7W13vocdB8+8d3e/5fojvqQkAEk4Apa8a/a6+I7/DL4DeI7+3l8rUL2MabaMDgiSX5SR7qm9h/u187WqxoU5VZbJXPosuwNXM8ZRwVH4qklFerdvwPz3/a++Nknxm+Ld9Lazl/D+kFrHTUB+VlU/PKPd2Gc/3Qo7V4dSk5NPghe5mjiiRpJXYKiKMliTgAe9fjVetPE1ZVZ7tn+leV5bh8mwNLA4ZWhTikvlu35t6vzPe/2Pf2eG+OPj/7RqcLf8Ino5Wa/PIFwx+5AD/tYy2Oig9CRX6ZfEfxEnw3+F/iLWrSCONdG0ue4t4FXCAxxkogA6DIAxXP/s6/Ci2+C3wm0Tw8FRdQ8sXOoSDGZLlwC/PcDhB7IK7Lxn4bs/G3hHWvD95JttdUs5bOVlIyqyIVJHuM5r9Oy7A/UsLyx+OS19ei+R/DHGfFS4mz5VarbwtKXLFd4p+9L1la/pZdD8SNV1S71vU7rUL+eS6vbqVp555TlpHY5ZifUkmv1v8A2P7m9u/2bfAz6gzNOLNkUuefKWV1i/DYFr4y8N/8E8fH9547GnaxPYWXhuKb95q8NwrmaIH/AJZx/eDEdmAA9T3/AEg8PaDZeFtC0/R9NhFvp9hbpbW8Q/gjRQqj8hXkZDgsRQq1KtZNaW1667n6L4s8T5RmmBwuX5ZUjUafPeO0VytJeTd9ultbaGhXgP7Q/g/RdD1jSvHz6fpE+p200apJrt9cR2cdwvMMwtoI3a5nGAqjggKMHgY9+rmPiWryeCNVSK6ns7howIZLW+SylaTcNqJM4IQscLnH8XHNffYWo6VVNddHrbRn8v1oKcGjwb4n63eaTqXwP+M11YTaPe/aYdD1+2miaFktL9Qp8xW+ZVjnCMFbkbuea+ntwr4+8T6HonjP9lH4ry2N5p93qklnLcmWx8XzeIpGazUXCb5pMbJAwJ2IMAFTnnjiP+Hg8v8Aeh/Svell9bG00qEbuDcflo136trfZHnRxMKEm5v4rP57P8kdx+y7oHj/AFX4LfCu78Ha5ZaJYQ2niBdSfU7R7yCSd9UQxAwJPES4CXGHyQo3DHzivqbwXpGoaH4dt7XVptOuNT3yy3E+k2Js7eR3kZyyxF3Kk7ssSxy2498V4/8AsZn+y/h74p8LtxJ4Z8W6vpZX0X7QZlP0KzAj6175XnZnWlPEVIWVuZtaa6tta79TpwkEqUZdbL8kv0Cvh7/gpz4keLRvA2gI/wAk9xc30i+6KiIf/Ij19w1+eX/BTcufHXgsHPl/2dNj6+aM/wBK+LzuTjgKlutvzR+yeF1CNfizCc/2ed/NQlb8dT4ur2n9jvwUnjr9obwnaTxiS0s521GYEZGIVLrn2LhB+NeLV9c/8E1LBJ/jNr90wy1vocgX2LTw8/kP1r87y2mquMpQe11+Gp/ZHGuMngOHMdXpu0lTkl5OXu3+VzqP26vhv8RPiV8X7STw94U1jVNH0/TIrdLi0gZo3kLO7kEf7yj/AIDXxz4o8O674K1mbSNdsrrStThCmS0ugUkQMAy5HbIIP41+4dfjh+054k/4Sz4/+O9QDb0/tSW2RvVYcQr+kYr6DPsFCh/tCk3Kb26H5B4T8TYnNUsmlQhGlh6fxK/M3dWvd21u2dd+xBo8mv8A7SfhbeWeKzFxeOCScbIX2n/vorX6w1+cP/BNLQftnxX8Sasy5Wx0jyQcdGllTH6RtX6PV7fD8OXBcz6t/wCX6H5f4wYlVuJfYx2p04x++8v/AG5BXE/GL4dWnxP8C3ujXUl5HgrcxGwEJmMiZIVRMDGd3K/MMfNnIxkdtRX1MJypyU47o/DpRU4uL2Z8xaH8N20L4XfEjVNb0vxVaan/AMI9c2aXPimbTC7W4tWUpGLBtmwBEyJOcgEdzX46ea3qfzr90f2rfES+Fv2b/iNfswUnRbi1Q/7cy+Sn47pBXxR/w781T/nxH5Gv0nh7NKWGp1a2JdudpL/t1a/mj5bMsHOrKEKWvKvzf/APpZRqvw7/AGjfif4e0Z0trvx54fXX9AeXHlLqVvEYJk54JP7mQ54xXoXwV07xPazXt1qy6vaaXcQqYrHxBqAu7xZlmlBkJGRGrxeSSgOA2QAMZOV+1L4O1W+8L6P468MW5uPF/gW8/tmyhT711AF23VrxziSLPA5JVRWP4cTRLvXLH4ueFf7X8W3fiy13WFjaxqEVSiArPO3ESRkMNpIwcgK7KK/OsfF1I0sYtbe7LyaVk/nG3q79j7/KqkXSxGXSsnL3otq7fXlvdKKvd8z+Fdrs+g6+F/8Agp1oDNaeA9bVfkR7qzkb3YRug/8AHXr7V0DWU1my3GS2e8gIhvI7SbzY4Z9qsyB8DONw5wPoOleJftzeBW8bfs9a1LDH5l1oskeqxgDnahKyflG7n8K8LNKft8FUjHtf7tf0PrOBcb/ZPE+DrVdFz8r/AO304/d71z8oa+tP+CbGpLa/GzWbRiAbrQ5dvuVmhOPyz+VfJhr2P9kLxingj9obwdeTSeXbXN0dPlJOBidTGufYMyn8K/M8uqKli6U33X46H9v8Z4OWP4dx2Hhq3Tk16xXMl87H62a7q0Wg6JqGp3BxBZ28lxIfRUUsf0FfhzqV9Lqmo3N5O26e4laaRvVmJJ/U1+vX7WHiT/hFf2dvHV5u2PLp7WSnvmdhDx/38r8fB1r6TiWpepTpdk39/wDwx+MeCGC5cHjca18UoxX/AG6m3/6Uj9Bv+CY/h/yPCXjbWyv/AB9XsFmrY/55Rs5/9HCvtevm/wD4J/aF/ZH7OWnXO3a2p391dk+uH8ofpFX0hX1OVU/Z4KlHyv8Afr+p+C8e4v67xPjqt9puP/gCUf0CuH+KPjy28IWFvZzWWrXU2q77aBtJVRKH25IR3KqHCCRwM5PlnGTgHtycCvJbnVj4/wBUlstbtbGz0+xiD614X8T2CSoI1LEXUE/3HXjr8y/LzsYGu6tJ25Y7v+v6/I+Vy+lCVT2tZXhDV6/dtrv6K9k5K6PKPiP4hs/i5p3wp+H2leIb3xTbeJ9fXUr+41G3WCddNsSJ5Y5UWNMEuIlBKjOe/Wvq/wApfQV82fss+GrPxj4t8T/Fm208afoV4G0TwpbFSuzTY5WeW4weczzln55wo7Yr6Wr1cTF0YU8LLeC97/E9X92i+R5U5069eriKSajJvlva/L0vZJeeitqJ1r5Y1jT4/wBmPxpqWl6g1xb/AAT8bXLH7TbTPD/wjmoyn51LoQY7eY8hgQEY44Byfqis3xH4c0zxdoV9o2s2UOpaXexNBcWtwu5JEPUEf5xUYetGneFRXhLRr9V5rp92zZnOMrqdN2lHVM5rwNoGuaHf3Ee7R9P8JRIbfTNF023JaGNT8kpmyAS4LFk24Hy4YncW2v7U0fxe+uaEHW+S3X7JfxhSYwZEOYi3TdtIJXqAy56ivnk3niv9keGXS9SfU/FHwbZSlnrdsv2jU/DCngJMuCZrdOqvglAMEEYB6DTLfXbhvDdp8MdaEngO9iiY67Zm2ulkZmle8nuJHzIZmxGEKjG9239MDmxWHlhIxlBc9N7Nflbo+6e3TTU9vBzhmdSbq1FTqpJ66LTd3Sbk+1ruTbbd1Z/CPjn9kf4k+HvGOs6bpnhDV9W022upI7W+t7Yuk8W47HBHquM++ax7b9mr4uWdxFPB4D8QRTROHR1tGBVgcgj8a/UTwt8dvDHie11+88+TTdM0dofN1G/Ait5Y5c+VIjk/dbgjODhlPRhXf2t5BfQRT280c8MqLJHJEwZXQjIYEdQR0NfGLh/CVHzQqP5WP3qfi9n+CgqGKwcLpJNtS1dk9dbXaabXmfLH7Ulr45+Kn7MPhqz07wrqkviLU7i1fVNNSAiS32I5k3L6eYq49QQa+If+GXPiz/0IGuf+Apr9hbi7gtQhmmSISOI03sF3MeijPUn0rF8XePND8CwW8utXjW32gsIY4oJJ5JNq7m2pGrMcLknA4AzXdjcoo4ufta1RqyS6Hy/DHiLmPD+GeX5dhISUpykl7zevRWetkkvRHOfs9+ErjwL8E/BmiXlu1re22mxG4gcYaOVhvdSPUMxBr0JmCgkkADnmuR1T4r+GtI1Pw7Y3F8wk1/Z9glWFzDJvH7vL42jd0AJycivH9evL342DxFoeuLL4E13w5L9qt9RWZVhNoXKyxu5Yh0IjyzYAGY2xkc+sqkaMI0qXvNaJei/yPz14TEZliamNxn7uM25Sk1tzSabS3aUtHa9vz634h+L4vHWv6n8NLRr/AEbV2jjnhvLi3Js73ad7QOUO9Y2ClSw2kgNgnGG828VPqPxg1OH4HeGNVvbjQdNC/wDCb+IjcGZreAncNLinwC8jfcLH5lRfmyxYU6Txtrvx11N9A+FFwfsUUX9na38W7q0jSR4g2WgsSqqJZMk/OoCKeRyQa9++GPwx8P8Awi8IWnhzw5afZrGDLvJId01xKfvyyv1d2PJJ+gwAAPco0f7Pbr1/4r+Ffyro5ea6L5vpfwcXjI4ulHB4ZWpLWT/mlazadk7O3Xbpvpv6PpFnoGk2emadbR2dhZwpb29vCu1Io1AVVUdgAAKuUUVwttu7OZK2iCiiikMa6LIhVgGVhggjIIrwnxD+y8NA1y68SfCXxHP8NdcuH825sLeIT6PfN6zWhwqk9N8e0jJOCa94oroo4iph23Te+63T9U9H8zKdOFT4l/n958uT+MPF/ga3Nl8RvgvLeWIv4tSm1v4cAXltc3ERUpLLa/LMMFEJ3bvuj0qj4c+NvwXuvi/qfjFviTb6Tqd3bmA6br1tPYS2zeXHHsLSlF8seXu2bfvOx3dMfWNfOn7YX/Iqx/7hrso0sHjasYVKXK77xdlf0af4NI1+v47BU5unWbTTTTV9Ha6v52XnoZfgnxZ4P0HwVbabe/G3wjqM9vrttqa3T+IonP2eNoy8RZpOS2x+w+98xY7naT46fHD4HeM9N0uzv/iXoTzWF8LuNbGIat5v7t42jMUYcMGWQ8EEZA4Nfmrqf/IeH+9/Wvvr9h/oP9w/yr3Mbw5g8BhueTlJW2ul+NmctHiXH4nFqtFqM027pdXo9PToa2neNJfFmk+HNO+H3we8R+M20OD7PY+IPGoGl2IXKMJD5mGmAaNGCiMbSi7cYGOtg/Zp8QfFHUU1X40+KU8QxAqy+E9ARrPSE2klRKc+bc4JJG8gDJ4wa+hx0FLXzscTGhphaah57y+97fJI6KrrYp3xVRz30e2ru9PN6+pU0vSrLQ9Ot7DTrSCwsbdBHDbW0YjjjUdFVRwAPQVbooribbd2VtogooopAf/Z"/>
										<xsl:for-each select="n1:Invoice/cbc:ID">
										  <xsl:if test="substring(.,0,2) = 'E'">
											 
												
												<xsl:for-each select="//n1:Invoice/cbc:ID">
												<xsl:choose>
													
													<xsl:when test="substring(.,2,2) = '30'">
													 
														<h1 align="center">
															<span style="font-weight:bold;">e-Arşiv Fatura</span>
														</h1>
														<img style="width:90px;" src="data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAAF4AAABNCAYAAAFYl+PNAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAFxEAABcRAcom8z8AABTTSURBVGhD7VsHfFVFus977/csq/QQSK+khxJISAi9KCIIuAooRUIVpElRBFx12VUUVkDAt/osgIiACoggVfpKZxGQJkVFpHcwhJD89ytnzjm5CUXf6n36u/9k7sx8bb75Zs6cOefO9cNPxK+mUKCZhYICU+fcXSaFAlyTAkmh5N1BiI+rLAr890TvAcojmYiwJMoL4DdxwuuinJ8P5OTkCFFBBEEBgiqmomePJxFcMZFdKkBwYAxZZQEjDJQuGYYtW7ZSyaEx/v+G9dbxqyvQcNmj7IJNyoffSy+NscfACHPeo/sTUh776kTkF1xDcHAUc3hq6IhOmvgmVq9aI+Xw8ATLaj7KlohHTFQmli5ZKTy/T+Z+itcnvUnFa6hfrynlNLuu5dM04LnDxswUUfwexuGn4nfQgE4iYNOmLWjdsi2V8vHXv46ReSFLB6Vly5ZjyJBhIsdQupZPnz7NFE1CLMCiRUsxZvRYvPPOFO3BmTNnbQWdN1pp1rS1GHPPpVYt29GnGvLE0aOnEB2VjmHPvEh618Smn65bDDUSHBgtOS96ocHxVMqX8tat2xDgHyFKxpns7J5Iis/ClZxcJRA4Ipzy8vIop0tHicJTSFl74dC5CXVAGzAMV27LFoZvmt4Uv2gDPuPF4rdsvNgrja9Oa39EAnqVWlWBqTDDyiXpla7gO6loFSAyIhFnz54VckQ4rzNEtW7JISGVpG7gLAduuJcPoEypIDYONKjXTClcsQR4MTp//jwGDXya1ZgpRs+fv4DAihG2I4whg4bTZwGOHTtBK+85fPvNd+KYX2JiDWG4W80nRpDc0rk9ZfDS2vKBh6wyy1sLG4Xi3nv+iGeH/wWjRk1AXGyqvbT7DXxyhIiwkNmHxpPAU0+NsA0Y/PgjLdkuL1hn11f7kBCbRet7JjLTH0CpEuEIC0lBeGgS/MJCdNPB3uzZvRdz58wX/fxrlhHKTp48pWUB3xwKcPnHy/jD7QGYO/szVK9WV3preslYvHiJTsWVK1YhoHyEEN3o3u1xq8Q9yEPbhzvgztvLo0t2d6FqT8kg/3v0kuGa56ZVp3XbEcmtGEvZU5ZzU3bgu/yLhc94sfhFjf/S8DnvLfic9xYs591rv5PLfUKq9MEVw2JIWZ96lOzc9Yq7Sd0cxrhnzuDyDW98LMIO6J5gzty5qFY1AxX9Y1GBUosWDyE/n/nMtQy52lHHCzB5ymTa7ZyzO3I9MN+dLKorow/JrQB5bAkYsg0zxJdeHEMbmao4d/ac1AsbBu2ExqBdm8fEGHfEdKJ37/60mUmWLZqOgJBtfbedGTNmIatWI9rsJKA8PaPGRCejfLkIeUBmGDl96M1Hbm4u4ipVQWq1WkiMT0XnTj1x7ZoGWDaou2mTFB9XTQgm8grHcQa/Ixr9yjhMnfoBtmzdirJlgrFj+07hqZ5GR3d12rFRo0YjOCgaM2d+JHUGy7ItdZR1VDY6Mllyps+ePR8DnxyK8xcuonpqbezcuQuLFy23ddg3v8PfH0GtzHtsJcnlUw0qjSOXj1OnTiIyMgEVA2KFxw4YQ27wJm3Wh3NQt46xSzKW7cJQ2/n513Bg/0HExqShfr2WGPr0i0hKzEB4SDpq12qOq7l51kiDfNBdJwfHb8GCzzBwAO932ZA6In9U50avXctD3dpNZLqIEtFXrVqLR9pmS10dMFBnGEEU7cJw5ExbV69exZw58xBQLhYpiVmYMP5N3HlbGD7+aD7uuD0UDRu2lE17cGAS2rXtiv/8D3/Rv+O/K4gNumALcN+9rfHUEO6A8ASdOnRDtSq1ZH+tHTFMzQMDYrBnz24pM4n5ixcvRVqNWkJKoGk4aeIb5GAupSu4ePEinh0xEv5lIkT35VFjrVfHDBpBu20eJS25/WEYP4wv8sRn5md8XBVkpDfCKXkgMKOgPGOwKNwMNkyfYtzSKwIXT1RZwZQZduGmcC2VpmGrWqhxT0fcrTHPlK3cZhue2rb5AnfZoDja9fF7uMP+NuFz3lvwOe8t+Jz3FnzOews+570Fn/Pegs95b4Gc14cFJ9GneWAWEuf0MJHPDxTKF4YUiWb9mQcWlTFyHiiW7Enktjl309W2J1xf/FHBVnIruuE8GUmyxbhjVpHgLheGpWfDKktGHzbLFNztGXBZ6da00YpG7Zp8r9e7dx/5Fsy/TDQiw1OwZMnnImlHVuxpbkZkw8YNWLR4kV2/EVhGkh1VkxuobaazOZVXGX4VwnVx3s3o0qUn/MsFY9p705kj6dSp06hYIRpTp04nOZaSD9Ezjd7frBWiIpOwZcsWqbuh9inR1DO6jEuXLuHC+fNSVluapGxVqSb1lStW4+TJk0hOro6ypSuyiuP8uXPnEBWRgJdefEUYYstSVMNA+bLRuHLlRxetAHv27kNQYAxGjnxJZPRFlMLomRdGO3bsQPfuvZCSXBMB/pFymi4sNA5BFSph86bNJK/t8miYN3cffDAL/mUjUTurEQIpgBs3bBI6x1pe93GqGBCFbdu2E5mo0qgmt6Nv/e9k9OjWX9mEceNeo6kVh6NHj7OkLavybEbzpUuXo1rVdFStkoH33puG3NwrQjfgd4/lyUF+c6Y62tnszr3xl5GjMXHiG1Q2X6XqwR2Wk8jXqFEHq1aupd7ou0Z1nGFyhpbLlAqRvEnj+9H5MfMdrEaKdSVJAwVy7VStUhPNmrXExYuXLFnmmaTtcfnzz1di2dKVFh2oXesenDl7Ed26DcA777yHKVNmIC/P0WH48UvWxo2aS8X0uJDPdkV59eo0RURoMg2z+6Ak51rWjuTj+PETMp02mGEWtjMqDriz+Th2/DheHfN3HDl8DE8PeQEXLlxGYkIWTa14PNCiHRYuXGEv12KCkl+jhi2w/8AhaVBNGVgdcVGGDv0TqqRkom+fQVSjJgvyJHdDnKe/6MgUfLlth9hVhz3khMZtFODQwUMY+ecxaH5/e6SlNULXLv0QFlIVZUslYOeOr3HixBmxy7h08bJVpmnD78YVRCB73DBDeii0ArpIryA5MQ0TJ7whPD30qjKi5AI7O+/T+WhQn0fT4rNcITGOnk4BRnREKsqWjEFIYDW6hjIQE51B5RoY+cKriI/NwOCBz6HDo91QukQUypWOwYL5i6Vtv7Klw8SAaUidd+bV9PdnICQoBvv2HlAnCLExqXJqwkTDATtVgL59B2Pduo22DbXthuqdOXMGzZu3wW3/FURL5mVyOIWczUKbNl0QHp6KTxcsR2hIFSQkpKNF8/ZoWL8lumb3wYjhI9X5ju174h9r11GFDep8NW22eag9sjIbyVLn0AtQv+79+OjDObaO+laAjz+aTTQ+mjwQa1b/Q8oKt/MFmPfJfESEJ9B6HYkGdVvJhZhGi8b6LwrfI8zIuLFgwQJcyc0V2368CvAytX8/RdbCtm1fEi2cpsnfLYobBZj/6WeoW5vPvyqMj8lJVbF3zz6ydZCGP1EOwuTm5sh7+Hnz5qHpPQ/irjsqoFqVLKyk1S0396rloBpgO+yUsed0XmHqJpcTvMeOHcP4cRPwzNBn5Sxjlcrp1gkcE3GeTCYKdIEdOoQ7biuDnBxer5Vu+GY0Vq1cQ9OrOu68PQB33xmMynRjmuX6akfeyUtiGF12yqTrw3aeBbly8OB+lLgrFK+NM9FmwyxEhj3s2Y4WZdgdLW7ItVGms6CQCFbZXb9FWBszR0EaKKLvbtBiclZIjiuWnMUQU0VkDIycoVmdlWrRjhcHy3kWpmTbtuoCm2gb1gjqiDlyLlhyhle4AwyLIFkR5i3Dcv63CZ/z3oLPeW/B57y34HPeW/A57y38pp3/LcMXeC/BF3gvwRd4L8EXeC/BF3gvwRd4L8EXeC/BF3gvwRd4L+EnBv7nv0EviuvZYvq/s51/B27Vn1v3+zqBtwwUiYEh6Fch/DWH5iqkdSl6gInur024XqxgsRCbty5uwSjdSipcVF+pYtctFJJhcMV87WOSp5InVIYCT4K2QUfJBFS/WufTRM7ZGf4Knw9DzJn9Cfr2eRJdu/TCkMFP44sv1uPCBf5ZHUON6kDwB+tbjilLYZdJlvjFfXsoIpY/PwnGtkdiM07/3MzrgXnuwJqy5SeRPG06idncL/1G1dCs80GWNgtIRY2ar1hzKdCzP56DB1u3Q0hwNCIj4lC3TmO0bPkwhg9/QX4i2KN7X6I1RWDFWMRVSsU7b03WRkifTapdB1p3kralMqdPn8Yrr4xB9dQsBAVG4umhw4imv6S7HjztO9DOqm2rX3a9MPQ7cvOVszs5vomui8V1J4mIwMSO8dVXX6H34/3Qrm0nLFu6nCgy4wtDneICMOODWdT5TOp8NBo1vB9Tp07D0aPHlE/QmWl+U6kN8bGHiRP+B4EVopCSlIYtm/8pdDZogitOWo6Z9vhwFh8aSE6ugeCgGPzxwUexcOFi5OTkCJ/FXP0SOB1WjmfdIC8vT85W8XG9sWPH4akhz6DPE/3RqWM22rZtj/btO8sPYbl9+6eSbILbLGSKbeuVz3Ttswrs2E7BJRsVAsLlPNfsjz9Bh/aPoXSpEMTHplI5G599tkjOtrCunJQQR60Gzp8/S8vGcIQExaJe3Sby60S7I5xRWYPmzA6j7+70ieOnUDOtIUIC42hJmic09lNkrKAzVq1cLb81Le8fjvr1msrPMvPlzCKb5Da0c27bDFP3pPOpum3btuG5P41EWlptlC8fgpjoJKSn10G7dh0p8JMoAEuwYsVqrFyxBmvXfIHVK9fg82Wr0a/PIJowkaiV2RD7vz5kLFr2qR2rz+bI4uXLOXh2+EiUKxeC1q0ewbxPFqIXzWyeOOk16uHDWXOt0yQMq0/UH7ZHSw0VLOKcufNoGUlB/bpNsWmjOahDjZBgPnfQPvlxM6jMkSM/oFGDFjTiNbB5i5n5oPvDSXLwCQSUD0c1mh3Tp8+U+4aCrh5JzuBox3WwZfSIKyQLW7duRY8eveQXztFRldHsvpZ4bfwkbNy4ydJlqA0TPJNrYJ0yD1yX7F7y8/GzZ/mAqg6sxkmxc8cuZNVqjIyMxli/fjMNwBVkZ/dEfHxVvPvuNLKhcjx/tB0+e2dip1b05koYMOAp+PtHYMyY8VIXFWPBkikKY8jN17rpsP6cPBX3NmmNkX9+GbGxlZGcVB3jxr7uHK0kUUnkpATXw6Y6T0GxAsY4fPgwunXrLUsan2V6++0pOPLDD8IzsAPGetaAuW0rX+ts18zkr/cfQEZ6A/xtzCSpG7wyajwyMxvTKrBK6mvXbqb7XAfUr9+MlsaOdKU0ptkfidtu80fHjt1wJeeqyGk7UpSck6zxfMC1XNkwvD99ljDFEZFkh1VJqpzkwwSIYeoKkXfVFyxYhMSEGihdMhLtH+1OS5mz69GAiFGrrsFxO8owwWNe7tWrGPbM87Q0RVFnH8H3339vy4BlWJH/xQDbY10RsXNPqH3H58PfHUGrlp0wb+5S7NnztQR19MsThLd+w2Y0vbcNtZ+A0OBUlLw7BjFRabRCOFe0A+2PQvtl4LeAFnwO+hO9hwjBHPp1C3Gp6Gw0STN3pzaSc/c1bQn/sqHo128QFtKaWjO9Pu67t5X++p1ktA29vAvZKgYmKGfOnkFWVkOEhSRi5gw9WacnpYkvyThyM1vM59wkXj52IiIsDhX8Y1GmZCX4l4mncjLR0hEZmUY3zSSEh1dHUmIdREdmonzZZLRr0wvHj52mG+teWuM7UAwHiy0GH4IeNnQkmjRqjbYPZ2PxouXqq+Wb37ixr8m59YEDRgihkFNWH1jYPYslaFR3D86+fXvxeE9at8uF0yXXBF+sXW9xVGb8uEmoUD4K70+bKXXn8r8R1AEjN2jgUFSkpeXtt6dKXddN5jl+3BgsW3h2M/gHB2NGj0dYcGWUKx2LV2mJ+f7wUQlaudLxKF0iAeFh1ZGSUo9mdyZCQ9IRFVGTJqwOUkC5RMTFpCMxrqYcai1dMhql7opBtZT6sjPs3KkXSpUIpe3kYxJ89ttvy9Z/IiI8Be0f6aFHgaUP/GE6RI7KH8MKuPBo53LyJEYMe45mRTylRFq3J+HihYvCE10eIKuPfNA6I70JEuLTaMdzUmgaANOOG551RZ8+g+k5Ihbr1m2Qugy8LcoFkzzh0HjHtGrVGnru6IPYSlUQVCEWd90eiooBieiW3Z92JkvQrWtv3HVnRUSF18Dggc9j+5e7RHfDhq3o23coFi7iI/1CEqxatQEff7RADpYzvv32CO0MR2D58jU4duw4MjPq0cDF48svvxI++y1r/NtvTZEDt4MHDtPOEPSYs842kxi8D508+T05Cs0H1Tt36ol9e78WHsPtkBtsq1vXx2n5CUPvXgOU5g46Z5RM+zNnfgj/coH0HJGBvZb9F154UXZC/foMlLrCUrQmgxvffXcYS5cuw/PPjaTO09Y2KAEl/hBES0g0kui+06lDD+zauYf2+XoTZHD7F85fop3KjxZFacYvzTUp3cSp+OQJQ6PtJE99fgDYRTuPVhTMUNpXN8SuXbup8ct0515HN5kFsi/m3Qj/ZuLhhzrKOm7g3CQ5ec5grmtQtm7ZRjMrhi7fCHrafVUGV0HylooZ5Lyredi1eze++eYbnQTgLRnwLi0zlaIr07IVgfS0erL3b9jgHjSo3wR1ajdB9Wp16B4QSze9Cih1dxCqptRG756DMHXKNNfrDAMODm8h1W+7zP8mMd3yn+k3gtExMEH2zBnyMzomcKMG3NG/0bYyJDAJZWm9KlMyHB0e7YLt23dYEqzDwdZ12m2QOFYqDNPG0iWfy1Pj8uUrLD1nYByQrARCZ42WOZk1nd8X5WAPbVWHDh2Bnt370XNBfwzoN1h+9HXu3Hnk/Oj8bKnwL4HUpgO3D27f3WWGZ/3/BuuVARvkjmntxIkT8vKrP3XkwIGDSiTYgXClIg55VAvDFUxrKbuRvLBtHulYwVEbSlVliy43WxNc1bXb44rIOpnCRXOnIrgu42fB412NGXWdAeqswikrrzAcnUIQFcNzbDlw63nyDc1Fp6K6YXRUhmluXxWedQOm30z2ejKetJ8Pj8D78GvBF3gvwRd4L8EXeC/BF3gvwRd4L8EXeC/BF3gvwRd4L8EXeC/BF3ivAPgX81h6Y2sO57AAAAAASUVORK5CYII="/>															
														
															
													</xsl:when>
													<xsl:otherwise>
														<h1 align="center">
															<span style="font-weight:bold;">e-Arşiv Fatura</span>
														</h1>
														<img style="width:90px;" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCACaAL0DASIAAhEBAxEB/8QAHQABAAICAwEBAAAAAAAAAAAAAAcIBQYCBAkDAf/EAEEQAAEDBAECAwUGAgcHBQAAAAECAwQABQYHEQgSEyExFCJBUXEJFSMyYYEWUhcYJEJDYrElY2SCkZKhdIOio7T/xAAUAQEAAAAAAAAAAAAAAAAAAAAA/8QAFBEBAAAAAAAAAAAAAAAAAAAAAP/aAAwDAQACEQMRAD8A9U6UpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUqDOrS/ZCMTxTVuKXqXZ7ltLK4WJruUNzw5MKAtt2ROdaX6oc9mjPISoeYU4kjjjkBms36sOnHXV7cxjK9wY8ze2VFDtrivmbNQr17VR44W4FcfAp5rW09dHTaojuyTKm2z5B13Ar+hv91KhAD6mpS1zqrXOpMfj4vrbDLVj1tjoCA3CjJQpz5qcX+ZxZPmVrJUT5kk1tdBClp61OlC8S/YGd/YbDkhQQWLncE29xKj8CmT2EH9CKluy5BYckhi4Y7e4F0iq44fhSUPtn/mQSK43nHMeyOOYmQ2K3XRhQ4LU2Kh5BHy4WCKiG+dFXTLdpirta9XQcUupJKbliMh6wykq9ee+Ctrnz8+FAjn4UE4Uqvh0j1F69Hjae6lpt9itfkseyLei7NLHwSJ8fwZaPl3LLx+JBPrwHVLketlph9T+n7tgkcKCDlNodN7xxXnwFOSGkB6ID/wAQyhI8+V8eZDZt7blyPDLjYdXansMTINmZp4xtEOYtSYNuiNcePcpyke8mM13JHan3nFqShPmSRq7fSjl+SNpuW0uqvcF0vTg7nv4cvYxy3NK+TMaGkEJHPA8Vxw8Acknmvh0tybbtfP8AanUszMj3KNfr2cRxeYy4l1r7itX4fcyseXY9MVLd5BIUPDPPkALH0FdH9IdS+uECbpjqYnZQ0wkkWDZ0Jq4syDz6C4xUNSmeByAVB31HIPFd/Cuq+3tZVD1h1AYVO1Nm89fhW9m6SEP2e9L54H3fckANPKPKfwl+G6CsDsJqfK13PdfYRtLFZ2E7Dxe35BY7gjskQpzIcbV8lD4oWk+aVpIUk8EEEc0GxUqqL6dydGH9qjuX3aujmCS8y4TKyXDmP5kKPvXGC358pP4zSOOCpLZ5sjhGc4hsnFbdm+B5FCvliuzIfhzobne26nngj5pUCClSSApKgQQCCKDO0pSgUpSgUpSgUpSgUpSgVAfU7zBzvp+v7iSWImzWojp+CTKtVwZQT/7ikJ+qhU+VA3W5Cko6eLzmlvaU5O1/cLXm0dKRyT91zWZTgHyJZadHP60E80rrwJsW5QY9xgupdjS2kPsrT6LQoBSSPqCK7FApSlAr4TVsNQ33JaEqYQ0pTqVDkFAB5BHxHFfeuhfmnHrHcWWgStyI8lIT68lBA4oKP9L+j9g4z054LvPp6yFVsy+/Wz79v2KXGStVhyUvuLdKFIJPsUjsUENvtcAdqQtKk+ltNMbesG6sKbyyyw5dtlR5L1tvFnnJ7JlouLJ7X4j6fgtCvj6KSUqHkoVpnRG82/0i6iW0UlIxG3IPb80tAH/yDWMwVhFh619o2expCLdfsKxzIbq2j8ibp7TOihzj0C1x47PJ9SG08+lBYGlKUCqr7E1Jm3TZlVz370xWN242W4PGbnetY3kzdU8fiXG1o9GZ6UjlTaQEvhIHHeB32opQapq7Z+E7kwS07I15emrpYr0x40d9HkpJB4W24n1Q4hQKVJPmFAitrqpeawv6nW9Y+17Cn2bUW2bs1b82gJ92PYb+8QiNeED8rbT6uGpB90dxQslR7Ui2lApSlApSlApSlApSlArGZNj9tyzG7rit5YD1vvMJ+3y21DkLZebKFpI/VKiKydKCD+jHILldunrHcdyB9Tt9wVcrCruVklXtVrfXEKlc8nlaGm3PPz4cFThVfNZAa66sto65X+Hb9gW+DsO0o8gkyUJTb7kkfM9zMJw/H8Y/tYOgUr89K+MybDt0ZydcJbMaOynuceecCEIHzKj5AfWg+9KxGW5TZ8KxO85rfpIYtVht0i6THuRwiOy2pxav2Skmqjat69M9zZ+9YO/o1+97ISIM+0WDG5SnYjFvmxUSGTc57qQ1DW13eG73e8VDhDaqDYdA7Zw7QHTlk1tzqephjWOa3/EY8NlAclS1Ce47AiRmk+bjrkeRGDaB5kEE8AEiRumrAMwtMLJNubVhJh57s2c1dLnAS4HBZ4TTfhwbYFDyV4DXJWR5F114jyNVB6BZ0bc/V5vfKtzWO1v51id8S9bI8GS85arfISXIMuRFYc8vFKYkVPtCk+IUn+7yRXpPQKUpQKUpQaZuXWdo3JqnK9W31DZiZNapFvK1p7vBcWg+E8B/M24EOJ+SkA1pHRfsa7bX6W9dZrf1uuXZ60fd9wdeV3OPS4bq4jzij/MtyOtR/VRqVcqyW0YZjF4zDIJIjWuxQJFynPH0bjstqccV+yUk/tUHfZ92G7490ea2ZvjPhS7lClXwp+TdwmvzW/8A65CDQSbtzc2CaTx5m/5tNlFc+QmFa7Zb4q5dwuktX5Y8WM2Ct1w/IDgDzUQOTUaRss6y9lp9rxfXeF6os7vJaey+S7eLutHwUYUNbbDKj69qpCyPIEck8Yvct9suo+qLEd2bUa8PBHsWfxaFfXm+6JjN3dlhxTshX+AiU14bXjnhKSwEqIDlWRhTYdyiM3C3S2ZUWQgOMvsOBbbiCOQpKh5EEfEUEFHSHUpdOHL/ANZt7jK495qwYXaIbPPzHtCJDn/VZodAb6j8KtvWvnyFjzHteN2CQn9x7Gn/AFqfKUEBrwTrRsKi5ZOoTX2TIB91jIcEdjKI+SnYcxI5PzDY+lcFbO6usQ88z6a7BlkVH55WDZegvcfMRbi1HJ+gdJ+Hn61P9KCBYXWrpSJLatWzv4l1ZcniEJj51ZH7U0VfJMxQVEX9UvEHzI5HNTdaLzaL/b2btYbrDuUGQnuZkxH0vNOD5pWkkEfQ19Lhb4F1hu2+6Qo8yK+nsdYkNpcbcT8lJUCCPrUH3no01MzcXsi1LJvupL+6vxVTsInG3sOr/wB/BIVDfHzC2Sf1HAICeKVXdWV9WOl+TnGKW/dGLs/mu+KsJtuQstj1U7bXF+BJP/p3EKPwaqTNVbw1humBIma+ylmdIgL8K4215tca4W534tyYroS6yrnke8kA8eRNBHXU+Bg+Yaj300fDaxTKm8fvTnoPui8hMNZUf5USjBc/TsP0PczrqPcwLqv19oC72+E3Z8/sM+VEuCu/x03NhfKGeeewNqbQ4PMclakAEeh3ffmARtqaUzfXslwNfftjlxWXioDwXy2Sy7yfQocCFA/5a859x7uwbqOuWi73iNxnZJs+RgE5TcLFWEz7jZMnbVbpkRa09yUIAfiyQ53qTw0HCrgGgmHqf2fm+x+mDq0tUi6eEjBcpjWi0Lio8FxmIz92vuJK08FRKlPEk+fCuPQAVvXVLv7WDvRpdrNkeWx3Mn2Brh2RarPFSqVOlOP27xEu+C0FLS0CQpTqgEJAJKhxVSrWnqA23oPqzvd8v9m1/Eh3u7XXK8URATNur05mCyVxy+tXYzEJYHatCFLUUr4XwOas9026w1hYPs9zluJ4fbYN5yjWUlV5uiWvElzHRAcQsOPL5WUBST2t89iB5JSAOKCG8gkdT20vs74uTZFmtrw/DYmEW+3RoFr/ALddclBDUXxZ0lxITHac57iy0kuHkhTnmRVlOkrWuB6z3LvrHdaYtCsOPWe4Y5Yo8eKjhK1sWlD7i1K81LWVTD3KUSonzJqJrRKJ+yCtdxVytNvxONLXwOfcj3BK1D/o2amTpvvzEbWe591F0eBfs5yq9NPd/KTFgH2FpQPy7LeFfUmgq30IyEWncGNbQYWBG2fl2xLBIcBPC3eYc+Nz8D7sKVwf14H5q9O68t+k2MbD0RaK2MkEKx3dEWU6vnj+zzZjtre5Py7ZfP1SK9SKBSlKBSlVz3X1XLtGVL0X072BrYm3ZKSlUBlZNsx5B8varrIT7rSEevhA+Io9qfdK0Ehgure8zt0ZjjfRRhct1L2XhF6z6bGWQq1Ysw6kuIKh+V2U4Eso9fIq7h2rBq0MCBCtUGNbLbFaixIbKGI7DSQlDTaAEpQkDyAAAAHyFRJ04dPiNKWm8X3Kckdy3Y2aSU3HL8nkICVz5ITwhppP+HGZSShpscBI54A57RMdB1rhb4F2gv2y6wY82HKbUy/HkNJcadQocKSpKgQpJHkQRxUEO9G+GY1KduOi8/zbUbzzinlw8YuQXaVuH1JtstD0VPn5/hoR58/M82ApQQL/AAV1oY8oJsu9tc5UwkjyyLCX4j5T+rkKYlBUfmGgP8tfi7110QHFBevdJXdoH3VR8oukRah+qVwlhP8A3Gp7pQQGNl9ZENYTN6WcOno595dv2UEn9kvQE8/uoUXvTqMhOdtz6KsqcQP8S25hY5CT9AuQ2r/4ip8pQQEep7PIZAu/Rzupr4ExGLPLA/7J/J/YVyd6u7TC4+9+n7e8Dn1JwCXJCfqY3ij/AM1PdKCA/wCuzpmOObvZNm2rj19s1xfU8fXtiGot23tzoW2nLjZlc9pTsEzS2p/2bmES1XGyXWHx6IW49HQHmfmy8Ftkc+78audSg8uNY7L6YL9sJ/XnV5v5G1HHCuZZMomZjI/hiayFKIZk21K0MQpCRx7rqVtr4BSvngVk9a7K0fhcbW8DFdgYO2xr3f8Ae7fFbhXWKO6xXRE5DMlASr3mAJ0dBcHuJDah3e4QPQnYOrNf7SxS64VnWLW+62m8xlxZTTrKe4pV/eSsDuQsHhSVAgpUAQQRXm6jRn8JYD1R6oy+22bKMy18/Zs3sE6Rbo6pVwtMVll1onhHmXGreWnwB7y1ucg94JDtZbrLBM3wLq52vjWzxYMst+SZUlqXa7i0tu+2b7njOuW+QySUPsrKnAhYHc24SpCuQQf3p+6m71rz7P696/znUmaqbx6xZLYImTW2Ii42xclKpPYzJLCi7D7VOIb73UBshPd3gEVqOIWnpx2D069XOf2jVeGTJEHLLnGw+W3Zo3jRGprbUa3JiKSnlsF7hTaUcDuWSByTW6Zj0P4vDz/INCWbp6DxzS82K7WfNI8JaYdksqWmE3llUhJCUOJXFcDbXqozkkccEgMjqHfvT3cvsw5epMh3PidsyZGBXuAu1zLi01MTJ4kllCWVELcUT4falIJVyAOSa3u35XiOO/ZbWrD8Iyi0zr1kWDQbAzFjzEKe+9LyUMOIUlJK0qD8xwq5HI4UT6VBcDo+6eMn0fr6xowRZz/K9p3LDWZybxO5j2+JfJrktaWPG8EBFviLRz2c8qSee491SfbPsaNRo2lOut4yiQ9gHiyZMGzxvGbuILqUhEd2Up1SFMskKUhSW0uK7gFqVx5h2rZjkHEPszM8g2xkmPh+U3u4Q0p8lAWzJ1vN/wD5h+1ehKFpcQlxCuUqAII+IryNndGGubP0bbQ2TaM82NbrhjN3yW0xrRDvwTbpXst1eixmX45bPidwS2FAEFRJ9Oat9D6LcowK1O3GF1071tsOGwX5Dl3yGPNjx0ITypR8drtQgAEn0AA5oLbVHO5uoPUmgrOzddm5dHt701Xh262MpVIuNydJCUtxorYLjpKilPIHaCodxSPOqpYXZOsraF8R/Qd1ZZQ5r8JWlzM8qw+0lqb6gG2R/AS9LRz5+OtTLR8iguip46dOkjF9JlzMMyvP9Iu0Z7ry7lnd3jLM99Clq8NtoOuvezIS0UtlLagFBPn8AA0d6L1YdWH4cs3Xp81bJ/M0lSDmd4YPwURyi1pIPmPeeSUkHuSqp50/pDV2hsUTh2q8SiWSAVeLIWjlciY98XZDyuVvLP8AMongeQ4AAG9UoFKUoFKUoFKUoFKUoFKUoFKUoFUE3fJm67+0nxvYRKFWC8YrZMZyKO413NvwrpOnQ0qV8OES24HcD8F+flV+6oh17wX059kM+MQl5GlbxeIyiPyy7PerdPYX/wAp7j+5oJPtX2f2osWz203rBZ8/HMKhqt8244VEAXCu1wgSH34Ul95wqePhuSVqLfJSsoa54CO02grqWqe3dbXDujI/DmR25CPP4LSFD/Wu3QV21Z0pzcC3pe9oXjNUXWwMTrzccRsaYpR90ybw8h+4urcKj3qK0FDfAHa2tY+PlYmlKCiKsYzG+Z7kvSE3h95atdz22vYN0vDkNxNu/hhT8e69iHyOxbrs/mMG0kkBKyRwmrK9TeFX/OtX/d9hsCci+7rxbLxNx1T6Wk32HFkoeeg9yiEcrSg9qVkIWpKUrISpRqWaUGm602rgW0LU7Iwu6JL1uUmPcLVIaMafanu0HwJUVYDjCwOPdUkcjgjkEE7lWj53pnANhXCNf7xa3oWQwUeHCv8AaZLkC6Rk9wV2JkslKy2SkEtKKm1ccKSoeVYNmHvvX4SiPcrfs+ztADtm+Far6hAB8w42kQ5Sye0AFEUepKzQSpStUw3ZGPZrIlWqM3Otl8tyEOT7LdI5jToqVEhKyg8hbZKVAOtqW0opUErPB42ugUpSgUpSgUpSgUpSgUpSgUpSgVSX7QtabXeoVzcADVx1Fsy1KUTx75gw3Wx+/hqP7VdqqYfaTa6zfYcDUtnwe1zJbt4yp7FbiuOwpwR4FziOMPuuFI9xCUJKio8AFI5+RC2OAsORsFxyM6T3tWmGhXPzDKQaz1cG222W0tNICUISEpSPQAegrnQKUpQKUpQKUrD5Ldp9ujMRbNHbfulxe9mhod58JCu0qU65x59iEpUogEFRASCCoUGq5mxHuu0cDj2ptJvFnfl3Kc8jkKYtS4rzKm1kD8rslUbtbUQFGOpY5LB4kKsNjWMRMcYfWH3ZlwnuB+4T3+PGlu8AdyuPJKQAEpQOEpSAAKzNApSlApSlApSlApSlApSlApSlApSlApSlApSlApSlArG+zeNkYlrTx7HCLbR+fjOAr5+ngN8fU1kq4hCQ4XAn3lAJJ+YHPH+poOVKUoFKUoFKUoFKUoFKUoFKUoFKUoFKUoFKUoFKUoFKUoFKUoFKUoFKUoP/2Q==" alt="UO_imza_16x13mm.jpg"/>
													</xsl:otherwise>
												</xsl:choose>
												</xsl:for-each>
										  </xsl:if>
										  <xsl:if test="substring(.,0,2) = 'A'">
											 
												<h1 align="center">
													<span style="font-weight:bold;">e-Fatura</span>
												</h1>
												
											
										  </xsl:if>
										</xsl:for-each>
										</center>
									
								</td>
								<td width="53%" align="right" valign="top">
								<br/>
								<xsl:for-each select="//n1:Invoice/cbc:ID">
												<xsl:choose>
													
													<xsl:when test="substring(.,2,2) = '30'">
													 
														<img style="width:214px;height:68px;" align="middle" alt="TIVIBUGO Logo" src="data:image/png;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wAARCABaAQQDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9U6KKKACiiigAoor4i/ao+Evxs0W48c/ELQvi3d6V4YtI2vYNFt7iZGjjVFBRcHaOQT+NdFGkq0uVysYVqjpR5lG59u0V+an7MPgP47/H7wpa+M7X4zalZafBqRt5bG7up3aQRlGYZBxghsV9hXXiX4wR/H620q20PTJ/hhsTztU3r9pVvK+bjfn7/H3a2q4X2cnBTTa3MaWJ9pFScGk9j2mivGPgn4k+LureMPE9r8RdE0vS9FhY/wBkzWTqZJhvI+cBz/Dg9BW7r37R/wAN/C3jC+8L6v4rstN1uxhM9xb3O5BGgTeSWI2/d561zulLm5Y6+mpuqseXmenroelUV8sftNftLRXH7Lus+N/hd4iDlb+PTxqUEZBUlsOF3Dg4I5rqPgb8XNJ8F/sm+CfF/jrXxbQSWKG41C+dneWRpGHuzMT6Vo8PNU+d97W6kLEQc+Tyvfoe/wBFc54B+IWhfE7w3Dr3hy7a/wBJmYrFcmF4w+OpXcASPccV5l+0x8EfGvxkt9CTwf8AEG68CPYtKbhrYyj7QGC7Qdjr0wevrWUYJz5JvlNJTahzQVz3Civyb+G/g34u/Eb4/eJvhdH8ZNesbrRDcBtRa7uHSXynCnCeYCM59a+3NF+Jnhv9kX4baT4c+KPxGXXNfVpZPtMnmS3U6M5IOzLNgA4yTjiuytg/ZtRjLmk+iT2OWji/aJylHlS6to+iKTNeafCT9o/4efHBp4vCHiKDULyBd8tlIrRTqucbtjAEjOORkc184t8VfFerf8FJLDwdc61cnwzp8Exg0xG2w7jYs5ZgPvHJPJzisIYecnKL0cU3qbTxEIqLWvM0tD7aorjPif8AGLwf8G9FXVPF+t2+kW0h2xLIcySsOyIOWP0FcP8AC/8AbG+FHxd15NE0DxKo1aQ4htb6Jrdpj6JuGGPsDmslRqSjzqLsaOrTjLkclc9ror4s/bC+L3i3wz+0n8JfB2lazcaf4f1K5tJ7y2tzsM7fatuGYclcAfL0r6Q+In7QHgH4Va3YaP4m8Qw6fqt8N1vZqjyyuCcD5UBPJ4HrWksPNRi1rza6Gca8HKSenLoeh0V5F8b/ANqTwH8AtOtZvEt/I17eJ5lvptrHvuZF/vFTjaPdsV538LP+CiHws+JniKDRJJL7w1e3LiO3bVo1WGRj0XerEKT74ojhq0oc8Yuw5YilGXJKSufUNFcd8Q/i94R+FMemSeKtah0ePUp/s9q8wYrJJxxkA469TXG/8NffCL/hNl8Kf8JrYf2s0vkDG4w+ZnGzzcbM5469azjSqSV4xbRcqsIu0pJHsdFeKeKv2y/g/wCDPFUnh7VPGVrHqMUnlTeUjyxwvnBDuoKgjvzxXsWm6laazp9tf2FzFeWVzGssNxC4ZJEIyGUjqCKUqc4JOStccakJtqLvYs0UUVmaBRRRQAUUUUAFFFFABRRRQAV5R+1b/wAm4/ET/sDz/wAq9Xrh/jh4Nv8A4hfCPxb4a0sxDUdU0+W2gM7bU3sMDJwcCtaTUakW+6M6ibhJLsfPf/BL8/8AGNk//Ybuv/QIq89/Yr1S9uP2yfjLbT3c80Ecl5sikkZlXF32BOBX0D+xX8EfEXwC+EE3hnxM1o2otqc12DZSmRNjKgHJA5yprx/xj+yl8Wvhv8fdc+I3wa1fSdmvNI91Z6scCMyMGcEEYZdw3AggivW9pTnVrx5kubZ9NzyvZ1IU6MuV+7uuphaNqV3/AMPS760+1zG1+zyfuPMOz/jxB+7nHWvOPjR8KrP40f8ABRa68J6jPJb6deGF7loTh2jS2DsoPbOMfjXt3wd/ZD+JPg/9p7Tvih4s1/Tddae3mk1KWFmSRZ5ImTYibcFB8oByPpXVH9mPxZ/w24Pi0JtP/wCEZ8rYY/Ob7Rn7P5f3duPve9bKvTpzvGW0LX8zL2M6kLSjvO/yOY/bQ+E/hn4N/sa6t4f8J6f/AGbpa6nbTGIyvIS7OAzEsSecCvkv4S+Kov2lfGHwp+FfinVv7B8F6JbeQlvuI+2zjcxBPQM33RnoBxya/R39rj4P618cvgrqHhPQJbWHUri5glRrxykeEbJyQD29q8O8YfsD3HiH9nTwVoWntpukfErw0NyalbsVhnZpCzhnC7vRg2Mgr71OFxNONG1SXvNvXtdbjxOHnKrenH3Ulp3s9j7L0XRbHw7pNppmmWsVlYWsawwW8K7URQMAAVdrjvhHb+MLLwBpVp47axl8S20fk3Nxp8jPFPt4EnIGGI6jHXNdjXhSVpNXue3F3SdrH53/ALM/y/8ABRj4o/7+of8Ao1a8c8KeJtQ8a/tUePvEOsfDe8+LF7a3lysWjq/yWqrMUVmQqcqoAUDGMmvsj4Q/steKvAf7WfjL4mX95psmg6w10YIYZGM6+Y4K7lK47etYXxh/Yt8WWfxZuvib8GPFUfhfxDfO0l7ZXRKxSOxy5UhSMMeSrAjPNfQRxFH2jV94pX137aangvD1fZp22k3b/hzxPS/CPju7/ah8EeOfCnwU1j4bWEN1DDqcFvH/AKPJGX2yOQFUKChwRjHGetdfZ/L/AMFXvrBJ/wCm5q9l+Cfw5/aOsfiJZa38R/HWlXmhwRyRy6TaJuEu4cH5UUAggHPPf1rj/jl+yd8TtQ/aXg+K/wAM9d0ywu3iRZPt7EPCwjMT4XYwZWQ/XJNT7aEpuEpL4Wr62+9l+xlGCnGL+JO2n5I8H/ak1zUvF37dA0u98MXPjmy0hYY7Tw1FMYxcKIhIcdepOT6gYpvxy8F+PPiZceHdQ8I/s56h8Pdb0m5EovtMVR5ijlVKqq8hgCG69RX1L+0p+xvefGDXNF8c+GPEP/CL/EbTYY0a9XIhuGQfKSVGVYHOGA6HBFc74R+Dv7VVz4m0ebxL8UNKi0mwuY5pI7dd5uVUglGCxrkEccnvVwxNPkhKLScVbW/6aO5E8PPnmpJtSd9LfrqrHif7fni678JfHL4PeJ7+0Z77T9Ktr6e1b5SZEnLsh9OQRXS/sL6Tpv7Rfxe8V/FbxpqUeqeLbG4BtNHkHy2qMPklUHqF+6uOhGTyRXr37Un7KviT43fHDwB4psDpcmg6KIk1C2vpGDyKs+9gq7SCCpxyap+Nf2S/Fng/9o3Tvij8IrnS9Nin/wCQvo95K0MM2SA6qFU8OOfZhkVCr0pYdUlK0rPX57fMp0asa7qNXjdafLf5Hkfwg8J6T8dv29/iRL48iTUzobTNY6ZefNE3lyLEg2nghV5x6nNem/8ABQv4I+BIfgPqHiq20fT9E1/SZYPsl1ZQrA0u6RVMTbQNwwSRnpipv2kP2OfFPiD4jRfFT4Ta0vh3xuwVru1eby0mcKF3I4GMkAAhhhuv189b9lD9oP8AaF1zTYfjN4ohsvDNlIHa1tpo3eTHXakQ25PTc3T0ojUhKcKyqWUUtOunS3mEqc4wnRdO7k3r018/I88/ar8S6n4x/Y3+Bmp6w7vfyO8byyfekCJsVie5KqDmu+/bE/Z98CeAP2R9A1jQ/D9rY61aSWX/ABMokxPN5i/Pvfq2Sc89MCvY/wBr79lPWfjB8N/BXhXwN/Z9hb+H5cLHfStGoiEYRQCFOTxXVftOfAnxB8Yv2ebXwRos1lFq8TWhL3UjLF+6ADcgE/TipjioL2XLKy5m2vK5UsNJ+1urvlSXrY+ddR/Z/wDAsf8AwTxHidtAtZfE0mjJqzaw6ZuvPZwT8/XGDjHTFe7f8E8dWudW/ZY8M/apWmNvNc28ZY5IRZm2r9ADWtd/AnX7n9jhPhYJ7IeIRoaab5xkb7P5gI53bc449K1/2R/g/rXwL+CuneEtfmtZ9St7ieVnsnLxkO5YYJAPQ+lc9asqlGcXK75rr0OijRdOtGSjZctn6ns1FFFeSeoFFFFABRRRQAUUUUAFFFFABVTVNWtNFs3u764S2tkwGkkOAMnAq3Xn3xrvbdvAeowCeMzho/3e8bvvjtWtKHtJxh3ZzYmr7CjOot0mzZ/4Wf4V/wCg5af991u6Xq1nrVml3Y3CXVsxIWSM5BwcGvi+vpT4K6nZ23w/so5bqGJxJJlXkAP3jXp4rBRoQ5ots+fy3NqmMrOnUikrX/I9HrhvGXjG88M+IIEiCy2zQhnhb1yeQexrsxdQNb+eJozDjPmbht/OuE8W6LB4m16CUalaw2qQhWkMqk5yeAM18BxFDHywVstv7Xmja3rrfpbvfQ+spypqXvvQ6Tw/4y0/xEoWKTyrnHMEnDfh61u1zvh3SdC0bbHZS28ty3HmGQM7f59q2JtUs7eQxy3UMbjqryAEV6uWxxyw0VmFnU68u3/D97aESlC94vQtUVUj1aymkVI7yB3bgKsgJP61br07NbiTT2CiqX9taeODfW//AH9X/GpYdRtbnd5VzDLtGW2ODgetFn2FzRezPBf2mvj14g+DuueG7TRYLKaLUVczfaoyxGGUcYI9TVf40ftBeIvh78W/CnhnToLGSw1SK2eZp42LgyTMjbSGHYVQ/ao+FWsfFjXfDF34fuNNli09XE/n3ixkZZSMevQ1X+N3wh1zx98ZPB/iHSJ9Ol0/TorWOfzLxVfckzOwC9+CK8OssXzVORO1429Op+hYD+xnSwvt3C/LU5rvr9m/n2PYPi5481HwLaadLp6Qu1w7q/nKT0AxjB968z/4aD8Sf88LH/v03/xVdR+0h/yDdF/66yfyFeE1+gYLD0qlBSlG7PwHNsdiaOLlCnNpafkfZfh+/k1TQtPvJQoluIElYL0yVBOK0K5vw3q9jpfg/RWvLuG1U2cWDNIF/gHrWnYeItL1STZZ6jbXL/3YpVY/lmvn5Rd3ZaH21OpFxinLVpGjRRVe4v7azYLPcRQkjIEjhc/nWRu2lqyxRVL+2tP/AOf63/7+r/jVma4it4/MlkWKP+87AD86dmLmi9mSUVS/trT/APn+t/8Av6v+NWYbiO5jEkMiyoejIQRRZrcFJPZklFFFIoKKKKACiiigAooooAKKKKACvlP4t/8AJQ9Z/wCui/8AoC19WV5f8b/D+mR+Eb7UksYFv2ljzcBBvPIHX6V6WAqqnWs1voeDnOHliMK2nbl1+5M+dKKK+hvg/wCENE1bwLaXN7pdrdXDSSAySxBmOGOOa+ixFdYeHO1c+FwODljqrpRdtLkOmf8AJvsn/XrJ/wChmvn/ADX2dHothDpv9nJZwrY42/Zwg2YJzjFfLXxOsoNP8darb2sKW8CSALHGuFHyjoK87AVlOc4pbu57udYSVKlSm38KUSb4S/8AJQtG/wCup/8AQTXW/tDaF9l1yx1VBhLqPynP+0vT9D+lcj8Jf+Sh6N/11P8A6Ca9z+M+g/234Fu2Rd01oRcJj24b9Cfyqq9T2eMg+6sZ4Oh9YyutFbp3XySPmrR9RfSdWs72M4e3mWUfgQa+q/FniSPTvA95q0TjDWu+I+pYfL/OvkivRNf8bf2h8J9F0oSZuFmZJhnnYn3f/Qh+VbYvD+2nTfmcuWY36rSrRb3V167fr+B54zFiSTkmvcvgpoP2XwTrmqSLh7pHjQn+6qn+p/SvD4Y2mlSNBl3IUAepr6/8K6HHofhfT9MKAiOBUkUjgsR836k1nmNTkpqPd/kbZFh/a15VH9lfi/6Z8fN1NaXhj/kZNL/6+o//AEIV1vxu0uz0jxkILK2itYfs6N5cKhRnJ5wK5Lwx/wAjJpf/AF9R/wDoQrvjUVSlzrqjxp0Xh8T7Ju9mex/tIf8AIN0X/rrJ/IV4TXu37SH/ACDdF/66yfyFeE1zZf8A7vH5/mehnX+/T+X5IluLqa6ZTNK8pVQq72JwBwAKZHK8MivG7I6nIZTgivoP4V/DHRZPCVrfalYxXtzeL5mZhkKp6AenFeUfFLwpD4P8XT2dqCLSRVmiUnO0Ht+BBq6eKp1KjpR6GOIy6th6EcTN6P71fY9R+C3xMuNeZtE1WXzbuNN0E7fekUdVPqRXPftHf8h3Sf8Ar3b/ANCrz3wPqT6T4u0m6Q4KXCA/QnBH5GvQv2jv+Q7pP/Xu3/oVcnsY0sZFx2aZ6f1qeIyqcaju4tL5HksBPnR/7w/nX0p8a/8Akm0/+/F/MV81Qf66P/eH86+lPjX/AMk2n/34v5irxn8aj6/5GeV/7piv8P6M+aM19O/A/wD5J3Zf9dJP/QzXzFX078D/APkndl/10k/9DNLMv4K9R8P/AO9P/C/zR31FFFfMH6EFFFFABRRRQAUUUUAFFFFABXAfHL/knl5/11j/APQq7+sHxx4ePinwrqGmqQJZY8xk/wB8HI/UVvQkoVYyeyaOTF05VcPUhHdpnyFX0n8F9Us7T4dWpnuoYRHJJu8xwNvzHrXznfWNxpt1LbXULQTxna8bjBBqHccYzxX1eIoLEwUb2PzTA4x4Cs6nLd2tbY+0rG/t9TtUubSZLiB87ZIzlTg44NfLfxb/AOShax/10H/oIr3n4Pf8k70j/db/ANDNeDfFv/koWsf9dB/6CK8nAR5MROK6X/M+mzqo62BpVH1af3oT4S/8lD0b/rqf/QTX1Rc26XdvLBIN0cilGHqCMGvlf4S/8lD0b/rqf/QTX1bWeZ/xY+hvw/rhprz/AER8aeINKfQ9cvrCQYa3maP6gHg/lWfXqP7QGg/2f4qg1BFxFfRcn/bXg/ptry6veoVPa04z7nxmMo/V686XZ/h0Oy+Emg/294609GXdDAxnk+ijI/XFfVNeN/s66D5On6jq7rhpnEEZP90ck/mf0r2SvnMwqc9a3Y+8yOh7HCKT3lr/AJHzd8fv+R5X/r2T+ZriPDH/ACMml/8AX1H/AOhCu3+P3/I8r/17J/M1xHhj/kZNL/6+o/8A0IV7uH/3aPofHY3/AH+f+I9j/aQ/5Bui/wDXWT+Qrwmvdv2kP+Qbov8A11k/kK8LjjeaRY0Us7HaqjqSe1RgP93j8/zNc6/36fy/JH0n8J/HWlXXg2ytZ72G2urKPypI5nCnA6MM9RivIPi94mtvFHjKaezcS2sMawpIvRsZJI/EmuMlieCRo5EaORThlYYIPpTACxwBk1VLCQpVXVT3M8RmVXEYeOGkrW/GxseD7F9S8VaVbIMs9ynT0Byf0Fej/tHKRrekN2Nuw/8AHq0Pgf8ADm4tLoeINShaHCkWsTjDc9XI7cdPrXQ/HLwdP4j0CG9s4zLdWJZjGo5aM4zj6YB/OuOpiIPFxV9Fp956tHA1VldR21lZ28l/TPnKFgsqE9AwJr6A+MXijSr74erDbX0M8tw0ZjSNwxwOScdq+fSCpIIwaltLOe/m8q3ieaTBO1Bk4AyTXo1aKqThNu3KeHhsXPD06lGMb86t/X3kNfTvwP8A+Sd2X/XST/0M18xV9O/A/wD5J3Zf9dJP/QzXFmX8Fep6vD/+9P8Awv8ANHfUUUV8wfoQUUUUAFFFFABRRRQAUUUUAFFFFAGTrPhTR/EODqOnW92w4DyINw/HrWba/DHwtZyB49FtSw/vru/nXUUVoqk4qyk7GEsPRlLmlBN+iI4LeK1hWKGNYolGFRAAB9BWVe+DtD1K6e5utJtLieQ5aSSIFj9TWzRUqUou6ZpKnCStJXRjWfg3Q9Ouo7i10m0t54zlJI4gGX6GtmiiiUnLdhGEYK0VYo6poen64iJqFlDeLGcqJkDbT7ZrO/4QHw3/ANAOx/78L/hW/RTU5RVkyZUacneUU36FbT9OtdKtVtrO3jtoFyRHEoVRn2qzRRU3vqzRJRVkZWpeFdH1i48++021u5sbfMmiDHHpmq8PgXw9BKkkejWSSIQyssKggjoRW7RVe0mlZMydGm3zOKv6FDVNC0/XFjXULKG8WMkoJkDbc+maox+BfDsMiumi2SOpyrCFQQR3rdooU5JWTHKlTk+aUU36GJrPgnQvEEnmX+l29xL/AM9CmG/Mc1Bpfw98O6NMJbTSLZJV5Dsm4j6ZroqKftJ25eZ2JeHouXO4K/eyEpaKKzNzndU+Hvh3WpzNd6RbSTNyXVdpP1x1q3pPhHRtDhkisdOt7dZAVfagywPYnqa16K09pNrlbdjBUKSlzqCv3sYH/CA+G/8AoB2P/fhf8K1tP0210m1W2s7eO1gUkiOJdqjPXirNFJzlLRsqNKnB3jFL5BRRRUGoUUUUAFFFFAH/2Q=="/>
															
													</xsl:when>
													<xsl:otherwise>
														<img style="width:214px;height:68px;" align="middle" alt="TTNET Logo" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAB2MAAAKNCAIAAACSs1eaAAAACXBIWXMAAGunAABrpwHCCSECAAAAB3RJTUUH4AEUCCsGqYtRvgAAIABJREFUeNrt3T2OXNeZgOGSwB0YTCZ0RCUTEbABcRLR65hA4A4mmSVMMuFkgldiOWoBFqBoEjLSBgRvgZygNMVmd3X1/T33+3keOLTNrntP3Z+3Px5+9enTpxMAAAAAAI197RAAAAAAADSnFAMAAAAAdKcUAwAAAAB0pxQDAAAAAHSnFAMAAAAAdKcUAwAAAAB0pxQDAAAAAHSnFAMAAAAAdKcUAwAAAAB0pxQDAAAAAHSnFAMAAAAAdKcUAwAAAAB0pxQDAAAAAHSnFAMAAAAAdKcUAwAAAAB0pxQDAAAAAHSnFAMAAAAAdKcUAwAAAAB0pxQDAAAAAHSnFAMAAAAAdKcUAwAAAAB0pxQDAAAAAHSnFAMAAAAAdKcUAwAAAAB0pxQDAAAAAHSnFAMAAAAAdKcUAwAAAAB0pxQDAAAAAHSnFAMAAAAAdKcUAwAAAAB0pxQDAAAAADn88y9//udf/uw47EEpBgAAAAAyEYv3oBQDAAAAAMkYLt6cUgwAAAAApKQXb0gpBgAAAAASE4s3oRQDAAAAALkZLl5PKQYAAAAAKtCL11CKAQAAAIA69OJllGIAAAAAoBqxeC6lGAAAAAAoyHDxLEoxAAAAAFCWXjyRUgwAAAAAFKcXP0spBgAAAABaEItvUIoBAAAAgC4MFz9FKQYAAAAAetGLH1OKAQAAAICO9OL7lGIAAAAAoC+x+EwpBgAAAABaM1x8UooBAAAAAE7te7FSDAAAAADwu7axWCkGAAAAAPis53CxUgwAAAAA8FC3XqwUAwAAAABc16cXK8UAAAAAALd0iMVKMQAAAADAM8oPFyvFAAAAAACTFO7FSjEAAAAAwAwle7FSDAAAAAAwW7FYrBQDAAAAACxRabhYKQYAAAAAWK5GL1aKAQAAAADWyt6LlWIAAAAAgG3kjcVKMQAAAADAZpIOFyvFAAAAAAAbS9eLlWIAAAAAgF0kisVKMQAAAADAXrIMFyvFAAAAAAD7it+LlWIAAAAAgBEi92KlGAAAAABgnJixWCkGAAAAUnp7995BcFIgqYDDxUoxAAAAkJUuGcqHX789nxTnBTJ64RAAAAAAeZ2j5I9vvnEoDnRuxM4LTPeHv/0j2o+kFAMAAADp6ZJHedyInRd4VsBMfFKKAQAAgDJ0yZFuN+IH58VJgbOYjfjMPsUAAABAKTbJHWB6Jr6cFOeF5v7wt39EzsQnM8UAAABAPYaL9zO3ETsvEDwQXyjFAAAAQE265LbWNGLnhbayZOKT3ScAAACA2mx6sImtMrHzQh/xt5t4wEwxAAAAUJwh1jU2b8TOC+XlCsQXSjEAAADQgi45136N2HmhqqSN+MzuEwAAAEAjb+/e2/fgWR9+/XZMJr5/Xhx2skudiU9KMQAAANCQLnnD4EZ8/6Q4LySVbkviq+w+AQAAAHRk04PHjmrEzgt5FQjEF0oxAAAA0JcueRahETsvpFMpE5+UYgAAAIDOXTJaI35wXsRiYirWiM/sUwwAAABwOrXcvDhyJr6cFJsXE0qNLYmvMlMMAAAA8Ls+w8XxG3HP80JkVQPxhVIMAAAA8IXaXTJXI+5zXgiufCY+KcUAAAAAV9Xrknkb8YPzIhYzUodGfGafYgAAAIAnldkkt0YmvpwUmxczQOEtia8yUwwAAABwS/bh4kqNuNJ5IbJWgfhCKQYAAAB4XsYuWbURZz8vBNczE5/sPgEAAAAwXaJNDzpk4oznhci6bTfxgJliAAAAgBniD7G2asSJzguRdQ7EF0oxAAAAwGwxu2TPRhz/vBCZRnxh9wkAAACAhd7evQ+y78GHX7+Vie+fFweBKWTi+5RiAAAAgFUO75Ia8dWTohdzQ/Mtia+y+wQAAADAWkdteqARxzwvRCYQP0UpBgAAANjGyC6pEcc8LwQnE9+gFAMAAABsae8uqREvPi9icWca8bPsUwwAAACwvZ02yZWJV54Umxc3ZEviicwUAwAAAOxi2+FijTjmeSEygXgWpRgAAABgR+u7pEYc87wQnEw8l90nAAAAAHa3bNODD79+KxMHPC8EZ7uJZcwUAwAAAIwwd4hVI455XohMIF5DKQYAAAAYZ0qX1Ihjnhci04jXU4oBAAAARnuqS2rEMc8LwcnEm7BPMQAAAMAxHmySKxPHPC9EZkviDZkpBgAAADjMOUr+z7+8cygCnhfDxZEJxJtTigEAAAAO9qe///V0Ov383fcORSh6cUwa8U7sPgEAAABwpF8+/N5n/vT3v56TMaHYjCIUmXg/SjEAAADAMX758PUlE1+IxQG9vXuvF1OeUgwAAABwgMeN+MJwcUx6MbXZpxgAAABgqBuN+D6bF8dk82KqMlMMAAAAMMjV7SZuM18ck+Fi6lGKAQAAAHa3oBHfJxYHZDMKilGKAQAAAPa1phFfGC6OSS+mDPsUAwAAAOxlk0Z8n82LY7J5MQWYKQYAAADY3srtJm4zXByT4WJSM1MMAAAAsKX9AvF9hotjMlxMXmaKAQAAADYzJhNf2Lw4JpsXk5GZYgAAAIANDG7E95kvjsl8MbmYKQYAAABYZdctiaczXByT4WKyUIoBAAAAlovQiC9sRhGTzShIwe4TAAAAAEuEasT32YwiJptREJyZYgAAAIB5gmw3cZv54pgMFxOWUgwAAAAwVYpGfJ9YHJDNKIhJKQYAAACYJFcjvjBcHJNeTDT2KQYAAAB4RtJGfJ/Ni2OyeTFxmCkGAAAAeFK67SZuM18ck+FiIlCKAQAAAK4o1ojvE4sDshkFh1OKAQAAAB6q2ogvDBfHpBdzIPsUAwAAAHxWvhHfZ/PimGxezCHMFAMAAACcTqW3m7jNcHFMhosZTCkGAAAAOPVsxBc2o4jJZhSMZPcJAAAAoLXmjfg+m1HEZDMKxnApBAAAAJpqu93EbeaLYzJczN5cDQEAAIB2NOJnicUB2YyCXbkmAgAAAL1oxBMZLo5JL2Yn9ikGAAAAutCIF7B5cUw2L2Zzro8AAABAfbabWMl8cUyGi9mQSyQAAABQmUa8IbE4IJtRsBUXSgAAAKAsjXhzhotj0otZzz7FAAAAQEEa8a5sXhyTzYtZw0UTAAAAKMV2E8OYL47JcDHLuG4CAAAARWjEhxCLA7IZBQu4egIAAAAVaMQHMlwck17MLF99+vTJUQAAAADSuSQwjTiO168+Oggx2byYZ7mSAgAAQBQv37x7+ead4zCLTMxO66rY0jJczLNcTAEAACAWvXgWQ6xsruqvH2xGwW1KMQAAAEQkFk/3+tVHvTiCn7/7PvtH6PCPIurFPOWFQwAAAAAxnWPxb3c/OBRTnGOxzSgOUaMRtzplb+/e27mYB5RiAAAACE0vnkUvHkwjzus8WawXc+G6CQAAAAnYjGIWm1EM8PN338vEBdiMggszxQAAAJCD4eJZDBfvSiMuxnwxJ6UYAAAActGLZ9GLN6cRF2bz4uaUYgAAAMhHL57l9auP4uB6GnEHhos78/UAAACArGxePN3rVx9tXryGTNyKzYt7MlMMAAAAiRkunsVmFAtoxG2ZL+5GKQYAAID09OJZ9OKJNGJONi/uRCkGAACAIvTiWWxefEOBRnySibdjuLgJXxgAAAAoxebF09m8+Koao8Qy8eZsXlyemWIAAACoxnDxLDajuLDdBM8yX1yYUgwAAAA16cWzNO/FGjGz2Ly4JKUYAAAAKtOLZ2m4ebEtiVnGcHE9vkUAAABQn82Lp2u1ebEtiVnJ5sWVmCkGAACAFgwXz1J+MwrbTbAhm1HUoBQDAABAI3rxLCV7sUbMHmxGUYDvFQAAALRjM4pZymxG8fN338vE7MpmFKmZKQYAAICODBfPUmC4WCNmGPPFSSnFAAAA0JdePEvSXqwRcwibF6ejFAMAAEB3evEsr199zBIuNWKOZbg4F182AAAA4HSyefEcr199jL95sUxMEDYvzsJMMQAAAPA7w8WzhN2MQiMmIPPF8SnFAAAAwBf04llC9WKNmOBsXhyZUgwAAABcoRfPcvjmxQUa8Ukm7sFwcVi+fgAAAMCTbF483YGbF9cYJZaJW7F5cUBmigEAAIBbDBfPMngzCttNkJr54lCUYgAAAOB5evEsA3qxRkwZNi8OwhcSAAAAmMpmFLPstBnFz999LxNTjM0oIjBTDAAAAMxguHiWzYeLNWIKsxnFsZRiAAAAYDa9eJZNerFGTBM2oziKUgwAAAAspBfPsrgXa8R0Y7j4EL6lAAAAwCo2L55l1ubFtiSmM5sXD2amGAAAAFjLcPEsE4eLNWI4mS8eSCkGAAAAtqEXz3KjF2vE8IDNiwdQigEAAIAt6cWzvH718X5U1YjhKYaL9+arCwAAAGzP5sXTXXYuLpCJ//c//9sJZVc2L96PmWIAAABge2aKZynQiP/t3016Mo754j0oxQAAAMCWNOJZ/j91/XQ6nT78+m3Gj6ARcxSbF29LKQYAAAA2IxNP97hwvfpjvl4sE3Msw8UbUooBAACADWjEs9wIW6/++FOKWKwRE4devAmlGAAAAFhFI55lSswKPlysEROTzShWUooBAACAhTTiWeY2rIC9WCMmOMPFayjFAAAAwBIy8XRrulWcXiwTk4VevIxSDAAAAMyjEc+ySa46dvNijZiM9OK5lGIAAABgKo14lm0T1SHDxRox2dm8eDqlGAAAAHieRjzLfmVqWC/WiCnDcPFESjEAAADwDJl4ujE1au/NKGRi6tGLn6UUAwAAAE/SiGcZGaF2Gi7WiKlNL75BKQYAAACu0IhnOSo8bdiLNWL6sHnxVUoxAAAA8AWNeJYIvWl9L5aJ6cZw8WNKMQAAAPCZTDxLqMy0bPNijZjO9OL7lGIAAADgdNKIZ4qZlmYNF2vEcKYXnynFAAAA0J1GPEv8nPRsL9aI4TGbFyvFAAAA0JpMPF2uivRUL5aJ4SnNh4uVYgAAAGhKI54laTy6v3mxRgxTtO3FSjEAAAC0oxHPkj0YvfrjTy/fvHMeYZaGm1EoxQAAANCIRjxLgU6kEcNi3YaLlWIAAADoQiaerkYbkolhvT69WCkGAACA+jTiWYwSAw906MVKMQAAAFSmEc+iEQM31N68WCkGAACAmjTiWTRiYIrCw8VfO7sAAABQj0w8i0wMzPL27v05GVdiphgAAABK0Yhn0YiBxYrNFyvFAAAAUIRGPItGDGyizObFSjEAAABUIBNPV6PpyMQQR43hYqUYAAAActOIZzFKDOwkey9WigEAACArjXgWjRgYIG8vVooBAAAgH414Fo0YGCzj5sVKMQAAACQjE09nS2LgKOmGi792zoA+Xr555wELKHZZcxCwoqCb3+5+kImnqzFK7PoMqb29e39OxvGZKQa8/QK4pmFFWVGQgEA8i+0mgFBSbEahFAPefgFc07CigNA04lk0YiCm+JtRKMWAt18A1zSsKCAumXg6WxID8UXuxUox4O0XwDUNKwqISCOexSgxkEjMXhyrFLsmeqQAFxOcUzcIrH+sKMD9ehaNGEgq2ubFZooBb78ArmlYUUAUGvEsNbabOJ93V2ngcEox4O0XwDUNKwqAg51/SeCKDX0E/F2XUgx4+wVwTcOKAiAEw8XQQdi/D6EUA95+AVzTsKgAiMJwMdQWedscpRjw9gvgmoYVBUAsejHUE39rdaUY8PYL4JqGFQVARDajgBqy/PObSjHg7RfANQ0rCoCgDBdDalka8ZlSDHj7BXBNw6ICIDS9GDLKlYlPSjHg7RfABQ2LCoAU9GLIIl0jPlOKAW+/AK5pWFEApGHzYogsaSM+U4oBb78ArmlYUQBkYrgYAkrdiM+UYsALMIALGhYVAPnoxRBHgUx8UooBAAAA8tKL4Vg1GvGZUsxDe99dzvewUD/Vsh8JAAAAgrB5MYxXqRGffe2kAgAAAGT3290PBqFgmHqZ+GSmGAAAAKAMm1HA3ko24jMzxQAAAAClGC6GPfz45pvCmfhkphgAAACgHsPFsKHagfjCTDEAAABATTYvhvWaZOKTmWIAAACA2swXwzJ9GvGZmWIAAACA+gwXw3TltyS+ykwxAAAAQAuGi+FZDQPxhZliAAAAgEZsXgxP6ZyJT2aKAQAAABoyXwz3NW/EZ2aKAQAAAJoyXAw9tyS+ykwxAAAAQF+Gi2lLIH7ATDEAAABAdzYvphuZ+LFYM8WHXJIG/NLMpRbADcLdAQCALA/e5oupTSN+ipliAAAAAD4z00BVtiS+TSkGAAAA4As2o6AejfhZ/kU7AAAAAK6wGQU1aMQTmSkGAAAA4EmGi8nLdhOzmCkGAAAIZ/0En7IDbMhwMekIxAsoxQAAAMfbvL88+D8UjoH19GKykImXUYoBAACOMbK23P+zVGNgDb2YyDTiNZRiAACAoQ7PK5cfQDIGFvvt7gexmFA04vWUYgAAgEGiVZXzz6MXA8sYLiYIjXgrSjEAMM5TLxIiBdYVPVdptB/Pt2bwMkh9wK+uakuoJ72YY8nEG1KKyXFr98ABkP3VcfH/xC0A64puS/fwn9YXZNh5T9GOZ61hv7oTE/RiRtKIN6cUAxu/3ngKBNeKXf+fXWQsqs3/zy0qMq7hAT+5r8YhJz3IfPfmq9fceh82L2YMjXgnSjEUf4tY9ijm1p76LdHzN4WXt1dN68qicrpT3CVrPEql6MVbHeqVH3OPM37IpWnA0nXJ7eBf/+s/TqePv3z42qFgJzLxfpRioNQrjaPqmZs+lwuvmtaVReWkx7xLFnumevnmXYdvxOKPOSyt7n0WjvotnettYa9ffTydTnox29KI96YUAxpxHR616Xm58KppUVlUBLlLVn2m8o2IcLr3S/bHrlurqzy9mK1oxGMoxeABlyKH1xM2zS8XXjUtqv1+QuvKXdJj1anNcHHkc735nS7OonUTL+/1K5tRsJxGPJJSDB5wcYShzmL2qmlR7fQDW1Tukm765a+xE7/ph5/ura5IAdetm3hthotZRiYeTCkGr2dUOMIeqXGt8KppUVlUlsEhd8mGj1Vtf3dSZgI3+KL1y7na9GKm04gP4csJnnHxAgxXlnGBa4XLnTPiq8FJJvbB637kZT9SinPnSlveuRfDU358841MfBSlGDzj4gUYyl4odD0nwnek+dGTiX38TT5U2A879wdLdNZcact7/eqjXsxjGvHhlGLwQI8XYPi8gFu9+eP4+1Dukq6WjkP27/v0Hy/jHvG+ZeXpxdynEUegFIN3M7wAQ/2rhGvgUYe98JG3qNreJZ16t4+MP2TS0+Tr1oRejFHiOJRi8ASPF2A4GQrDAV/2Ga0rC5vCh6XMXg2pT5AvXR9icU8acTRKMVQmJgJTXsD6vIN523SofdgmR2mnRyDnusPBuXyQep/IRyA+w8WtaMQxKcXgwR0vwFi6PjIOso/sLuksO0S5P8vVn1nvJiO9uAONOCylGDyy4wUYS9cHx+H1wd0lnV8HqtrxdzpITS+uyihxcEoxgBdgLF0fHwfWx69AJvZF8BEKr1vfxJ7E4ko04hReOAQe3VI8oB/7ufK2OY9TXoDBxeHGQfCdsq6otCp8o6H83c29u6dzLP7lg0nH3DTiLHzTALwAY906FDiYjoO7pHPqoDkFEJfNKPIySpyLUgzgERzrFqwrR8NxcDYdOgffpyM6vTgXjTgjpRggwUOqgWLwwukYOiaOwOC7pPPoAAIxicXxacR5KcUA0cnEeGl3WBw9cJcE3Hq4MFwcmUacmlIMEPrx1AswXquwqBwcn338XdLydhiB+PTiaIwSF/DCIQBo+AKMdcuD4+PrhnXlLpnimvnUpw77M7vAAnt7/erjLx/MQR5MIC5DKQZo9wIMXP12+9KFvR5iVXS+S078pA/+a76huGvTynmyWC8+hEZcjFIMkPi1ECa+TeVa2AKHRbX5ujrqB9YyLO+jngQu//MIH6fPF+HZj5nxBnf7Q7llE4pePJ5MXI9SDJDs1RFyrdsFdeCowCHqZZFoUbnaxFkDha+ZKRZ/7Qvs9I+W6Fo0d5h9/Cdy1+YpevEYGnFVSjFAoxdgyLWMz/8n0l7n6+Hm62p81GjSMtwl438uV9RQZ+23ux/CnotlH8oCIxqbF+9HI65NKQbwAoylG3oNj3z5NKAU1h7rStFwl4xwzRx86z8w59W7wG6yQ0i6zXxcWknEcPHmNOIOfGEA6rxywOELeL817NsRxPgEsN+62nXFHnvQ3CV9KFfULMcwzrmo94ng7PWrj+dkzEoycRNKMUCUN3wP1iRdusMWsKjXzZiS69rrLnnUd3/Y7yri/Ok1LrCbH7oIV6Gkn8gtm+n04jV+fPONTNyH3ScAL9UtXoChxgL211qbXBJPw/8+vnXlLtnzacrij7MeUm8Bb2mRhc2L5xKIG/INASY96t3/jxdgsHr3fsOM88dx1H2n2J9Y8oZS6UPV+0sYYX+Y7MvGLy3cr8nOcPF0MnFPSjFw6/GuWxr21gEpVq+od4jy06Muxa4zbT+OxR/hQDkLMJJefJvtJjpTioErz6kC8cnfqMXqjb16fXEK34McBNeZwUui8F/CiPmDJf1tXMlt0/f+E13SCU4sfkwjRikGvniY8zxX7wUYrF6yXBUPX1S7/gD2anCdCf5xXNXdx12OaMhw8YVGzJlSDHg98AIMKVevqAfuki6bjrkj49DBenqxRsyFUgwYJfbcTCkDIk6c1et75KpoUVkP8a+ZudbVmB/Vb+MOXyqudfBYz15slJgHlGLwPOox0asLwMEXxiY3owK3GHfJDsvbwyHQWZ9YrBFzlVIMXl044AXYkSfvGrZ6cXN0hXEKcE3AAaSw8sPFGjE3KMXgoQ0vwOBSduQ1AVdFd8mj1oPfrh34Y7vGuupCcFV7sUbMbUoxeECk7AswWMAkujzCyb9i54cHCKZSLDZKzBRKMXgNw5EHcGF0ub7FP/uGa5TrD5CXRsx0SjF43sULMFYy4Hva7i659+cqcN/f+yO4bQHsTSNmLqUY8NIuE0PuZez7BdnvkgDA5jRiFlCKoRc9xQsw4BIR5wi4K1F1JZT5RL6kvm5uVZCRUWIWe+EQAHgiB4DH/LUbAMhFIGYlM8XQiDcxL8BgJQPukj6UcwRQkkzMekox4AXYyxWWNBwg+BWy+QW8/F3SNdPNC6AS202wFbtPgBdyryWOOXiZx6LCkwlYpUA+AjHbMlMM4NUC4Aol16l3l/TRPNUAhGWOmD0oxYAXYABwG3KXBIA0NGJ2YvcJaMEkiAMOAO6SAJCdRsyulGIAL8AA4C4JAKFpxAxg9wkAL8AA4C7pA3rCAQjKlsQMoxQDeIkCAHdJAIhII2YkpRi8mOE4k55/gQpwl3TNdAsDKMYoMePZpxgAAAAAohCIOYqZYoBtmIgBAHdJAFhJJuZASjGA12AAcJcEgIPZboLDKcUAXoMBwF0SAA6jEROEfYoBAAAA4AACMaGYKQbYmIEpAHCXBIBnycREoxQDeA0GAHdJABjHdhPEZPcJAAAAjvHb3Q8OAtCKQExkZooBdmFgCq/ZgC9szLukUwDAIcwRE59SDFDzNRgA3CV9KB8NIAiNmBSUYgCvVQDgLgkAuzBKTCL2KQYAAACAjQnEpGOmGGBfBqYAwF0SgFbMEZOUmWKAEa/B/vEcsrOGsa5wl+x8jhwEgOk0YvIyUwwAwBXiHSwgqgJ0ZpSY7JRiAO+NYA0DrjAAsJxGTA1KMYDXYADodZfcdWS+2B1/14/j7y4ANWjElKEUA5R9DaYVL9uAuyQADGaUmGKUYgAAjqEMAgBJacSUpBQDDCWLYPUClL/O+CAAhWnEFKYUA3jpArjOriYUvkta3q4wAAtoxNSmFOONAsArN7iJg7Xt6wlwi1FiOlCKAb4wprV59SIpSxfcJV1qmnwQF3yAC42YPpRiz/fAMV8QL2CAC6PrIVYFAJFpxHSjFAM8NKwXew0mI+sWmt8fK90l/SKk9sEHWEkjpiGlmPQ0C/Z4Y/H2QoFl7MKLuznukpa37yPAAkaJaUspxpMlHPnS6ysD4HqY8S7p91IAlKQR05xSDHD9ddceFGDd4nrI6egJ4r1XheR9yI9qLB0ISCOGk1JMdl4p2fVdxWsMxZa0KzDgLump1YUd4DGNGM6UYjxi4gV41X/BtwYXfFwtLSp3ydSrwvIe/OP5HQMQilFiuE8pJjEvk1R6UbGeSbq2LV32WFTWlbtk1UtN2A/iSwc0pBHDY0oxnrPxAhzlVdmqxgXZj1rjsmlduUs6zrk+iGsj0JBGDFcpxd70PG7iWxDoNRgSXeofXPbj/63q80/oDpVI/JNVdfy54V2y7TVz/E/imQo4nFFiuEEpJuXbnfd8fHHAAp7+U/lmbWV84ol5+s4/1f0fzBpzl8z4caxboBuNGJ6lFOOpmnaWlQ5/u5YmS339Ag41KHf1hxGbfMbNF5VLR4G75FHXzFZfKwPFwFE0YpjohUPArg+gmz8Oimgc+/JsBcL0a3W37OJKOObMHjLXfNRjj7Xhguk6CbCeRgzTmSnu+yif8UnUcy2tXhoh0dX+qZU8ZgDz8gdN/7NGfsv88wB7nO6Ai4oCd8kDr5kjF3bPWxLQk1FimMtMMSMefNc/F3pPI4hhA1Nm0yh2I9g2E7gpPHVMxlw3IoyO3v8BNvnUm3wil+4Cd8ljl/d+X+TDv7OeaoDBBGJYRikmwVOvIoDXYIi/Ypddz28v8r3/cdR6X7HbN9wNk33kdfXsp/Nc4S6Z4ou8yVfVagca0ohhDaXYc3yU11ePtgBlLvhzbw04qmU+vl/yuWbutJhTP0L7UgDDyMSwklJMuEde1QDvkPe/Dl6uyLVo092SfMUsKtwlUyzvvF83l1lgDI0YNuFftOv+HH/4I+/j/zgv+PrUeDMEXAl7ct12lwRgJP9sHWxIKQaAFkS9q2Qm6wqsbYcRSEojhs0pxZ7ePL1B9K+PkoVrPuTiul3jLuma6QACkWnEsAelGKD1azAWLQO+X7Ure7AsAAAN3klEQVQPu0WFuyQAIxklhv0oxXjBgxy8BuOyj0WFtTH4LmltO3RAKBox7E0pBvAihHXbnd/EWFQWFda2gwYEpxHDAEoxHuYgzTdId8CVH4sKC2P8XdLadriAYxklhmGUYjzSgddgrFscbR/TFdtd0tp2oIBwNGIYTCnGsymAyyainkUF1rZDBASiEcMhlGI820Gyb5CehYs/8ReVdeWKXfIuaWE7OMAYGjEcRSmmzhOex1O8BoPrZ+RvVrfjbF1R8i5pYTsswK6MEsOxlGKKPOddfmzPqTQhFrP5VdT1Ew8VLtcOu4XtgABH0YghAqWYCk97Hk+xGsEC3oSx4j0+snVFva+AVe1QABvSiCEOpZj0z3weT2m7Js2psdMC7nxddU9xYHGXtKodBGAkjRhCUYrJHQuu/oSeWelDLKbzLSDpR257k+r5Swgj1bXvks1PrrUNrGSUGAJSikn8COjxFIsTrORNPubgT9p8atu6otKR8SsQgAU0YghLKSbrs6DHU7wGnxkrRg7I+OmMIlY9AueP5iml212y1Rm3vIGVNGKITCkm30PhlBcwj7C0IhYz5sJb6Z+iivBx3KqKFVWBuPldss+wvBUFLGaUGOJ74RAw99HwwCbl2ZR0X5lh35eXb975gtDhRlDsJjLyKmFReThxl7SYLXXgKAIxZKEUk+Mh2LMpXoMh2o3glCSIBL+DuEpYVO6SHpUteKAwjRhyUYqJ/ka3+NnUuzfdGCum8L1g2L3jwJ/WPcuianvGx5zrwXfJMg+iVj6whkwM6Xz16dMnR4Gtnr89lQJwSBwpc9cYcPQyHqujipunEfKuXusfAFhAKSbQY7HnUQC3A/eLDQ9d4QO1+bryEEK6K+F+fB0AoC2lGAAAYHfxe7FGDADNKcUAAADj2IYbAIhJKQYAADjAsclYIAYAHlCKAQAAjjQyGQvEAMBTlGIAAIAo/EOgAMBRlGIAAIC4ZrVjURgAWEwpBgAAAADo7muHAAAAAACgOaUYAAAAAKA7pRgAAAAAoDulGAAAAACgO6UYAAAAAKA7pRgAAAAAoDulGAAAAACgO6UYAAAAAKA7pRgAAAAAoDulGAAAAACgO6UYAAAAAKA7pRgAAAAAoDulGAAAAACgO6UYAAAAAKA7pRgAAAAAoDulGAAAAACgO6UYAAAAAKA7pRgAAAAAoDulGAAAAACgO6UYAAAAAKA7pRgAAAAAoDulGAAAAACgO6UYAAAAAKA7pRgAAAAAoDulGAAAAACgO6UYAAAAAKA7pRgAAAAAoDulGAAAAACgO6UYAAAAAKA7pRgAAAAAoDulGAAAOMzLN+8cBACACJRiAADgSC/fvNOLAQAOpxQDAADHE4sBAI6lFAMAACEYLgYAOJBSDAAABKIXAwAcQikGAADC0YsBAAZTigEAgKDEYgCAYZRiAAAgLsPFAABjKMUAAEB0ejEAwN6UYgAAIAe9GABgP0oxAACQiVgMALAHpRgAAEjGcDEAwOaUYgAAICW9GABgQ0oxAACQmF4MALAJpRgAAEhPLAYAWEkpBgAAKjBcDACwhlIMAADUoRcDACyjFAMAANWIxQAAcynFAABAQYaLAQBmUYoBAICy9GIAgImUYgAAoDi9GADgWUoxAADQglgMAHCDUgwAAHRhuBgA4ClKMQAA0IteDADwmFIMAAB0pBcDANynFAMAAH2JxQAAZ0oxAADQmuFiAICTUgwAAHDSiwGA9pRiAACA34nFAEBbSjEAAMBnhosBgJ6UYgAAgIf0YgCgG6UYAADgOr0YAOhDKQYAALhFLAYAOlCKAQAAnmG4GAAoTykGAACYRCyO5u3d+7d37x0HANjEC4cAAADgWb/d/eAgxHSOxT+++cahAIA1zBQDAAA8QyaOz3wxAKxkphgAAOBJGnEu5osBYDGlGAAA4AqNOK+3d+/FYgCYSykGAAD4gkZcgOFiAJjLPsUAAACfycSV2LwYAKYzUwwAAHA6acR1mS8GgCmUYgAAoDuNuAObFwPAbUoxAADQl0bciuFiALjBPsUAAEBTMnFPNi8GgKvMFAMAAO1oxNiMAgAeUIoBAIBGNGIubEYBAPfZfQIAAOhCJuYxm1EAwJmZYgAAoD6NmNvMFwOAUgwAAFSmETOdzYsB6EwpBgAAatKIWcBwMQBt2acYAAAoSCZmDZsXA9CQmWIAAKAUjZitmC8GoBWlGAAAKEIjZg82LwagCaUYAABITyNmV4aLAejAPsUAAEBuMjFj2LwYgNrMFAMAAFlpxIxnvhiAqpRiAAAgH42YY9m8GIB67D4BAAAkIxMTgc0oACjGTDEAAJCGRkw0NqMAoAylGAAASEAjJjKbUQBQgFIMAACEphGTguFiALKzTzEAABCXTEwuNi8GIC8zxQAAQEQaMXmZLwYgI6UYAACIRSOmBpsXA5CLUgwAAEShEVOM4WIAErFPMQAAEIJMTFU2LwYgBTPFAADAwTRiOjBfDEBwX3369MlRAAAAIJ2kg7piMQAx2X0CAAAAxrEZBQAxKcUAAAAwml4MQDRKMQAAABxDLwYgDqUYAAAAjiQWAxCBUgwAAAAHM1wMwOGUYgAAAAhBLwbgQEoxAAAABCIWA3AIpRgAAABiMVwMwHhKMQAAAESkFwMwklIMAAAAcenFAIyhFAMAAEB0YjEAe1OKAQAAIAHDxQDsSikGAACANPRiAHaiFAMAAEAyejEAm1OKAQAAICWxGIANKcUAAACQleFiALaiFAMAAEBuejEA6ynFAAAAUIFYDMAaSjEAAAAUYbgYgMWUYgAAAChFLwZgAaUYAAAACtKLAZhFKQYAAICyxGIAJlKKAQAAoDLDxQBMoRQDAABAfXoxALcpxQAAANCFXgzAU5RiAAAA6EUsBuAxpRgAAADaMVwMwANKMQAAADSlFwNwoRQDAABAa3oxACelGAAAADjZvBigPaUYAAAAOJ0MFwP0phQDAAAAn+nFAD0pxQAAAMBDYjFAN0oxAAAAcIXhYoBWlGIAAADgSXoxQBNKMQAAAPAMvRigPKUYAAAAmEQsBihMKQYAAACmMlwMUJVSDAAAAMyjFwPUoxQDAAAAS+jFAJUoxQAAAMByYjFADUoxAAAAsIrhYoAClGIAAABgA3oxQGpKMQAAALAZvRggKaUYAAAA2JhYDJCOUgwAAABsz3AxQC5KMQAAALAXsRggC6UYAAAAAKC7Fw4BAAAAsIcf33zjIABkoRQDAAAAG9OIAdKx+wQAAACwJZkYICMzxQAAAMA2NGKAvJRiAAAAYC2NGCA7u08AAAAAq8jEAAWYKQYAAAAW0ogBylCKAQAAgNk0YoBilGIAAABgBo0YoCT7FAMAAABTycQAVZkpBgAAAJ6nEQPUphQDAAAAt2jEAB0oxQAAAMB1GjFAH/YpBgAAAK6QiQFaMVMMAAAAfEEjBmhIKQYAAAB+pxEDtKUUAwAAABoxQHf2KQYAAIDuZGIAzBQDAABAXxoxAGdKMQAAAHSkEQNwn90nAAAAoB2ZGIAHzBQDAABAIxoxAFcpxQAAANCCRgzADUoxAAAAFKcRA/As+xQDAABAZTIxAFOYKQYAAICaNGIAplOKAQAAoBqNGIC5lGIAAACoQyMGYBn7FAMAAEARMjEAi5kpBgAAgPQ0YgBWUooBAAAgMY0YgE0oxQAAAJCSRgzAhuxTDAAAAPnIxABsy0wxAAAAZKIRA7AHpRgAAABy0IgB2I/dJwAAACABmRiAXZkpBgAAgNA0YgAGUIoBAAAgKI0YgGGUYgAAAAhHIwZgMPsUAwAAQCwyMQDjmSkGAACAKDRiAI6iFAMAAMDxNGIAjqUUAwAAwJE0YgAisE8xAAAAHEYmBiAIM8UAAABwAI0YgFCUYgAAABhKIwYgIKUYAAAABtGIAQjLPsUAAAAwgkwMQGRmigEAAGBfGjEA8SnFAAAAsBeNGIAs7D4BAAAAu5CJAUjETDEAAABsTCMGIB2lGAAAADajEQOQlFIMAAAAG9CIAUjNPsUAAACwlkwMQHZmigEAAGA5jRiAGpRiAAAAWEIjBqASpRgAAADm0YgBqMc+xQAAADCDTAxASWaKAQAAYBKNGIDClGIAAAB4hkYMQHl2nwAAAIBbZGIAOjBTDAAAANdpxAD0oRQDAADAQxoxAN0oxQAAAPCZRgxAT/YpBgAAgN/JxAC0ZaYYAAAANGIAulOKAQAAaE0jBoCTUgwAAEBbGjEAXNinGAAAgI5kYgC4z0wxAAAAvWjEAPCYUgwAAEAXGjEAPEUpBgAAoD6NGABus08xAAAAxcnEAPAsM8UAAACUpREDwERKMQAAAAVpxAAwi90nAAAAqEYmBoC5zBQDAABQh0YMAMsoxQAAAFSgEQPAGkoxAAAAuWnEALCefYoBAABITCYGgE189enTJ0cBAAAAAKAzM8UAAAAAAN0pxQAAAAAA3SnFAAAAAADdKcUAAAAAAN0pxQAAAAAA3SnFAAAAAADdKcUAAAAAAN0pxQAAAAAA3SnFAAAAAADdKcUAAAAAAN0pxQAAAAAA3SnFAAAAAADdKcUAAAAAAN0pxQAAAAAA3SnFAAAAAADdKcUAAAAAAN0pxQAAAAAA3SnFAAAAAADdKcUAAAAAAN0pxQAAAAAA3SnFAAAAAADdKcUAAAAAAN0pxQAAAAAA3SnFAAAAAADdKcUAAAAAAN0pxQAAAAAA3SnFAAAAAADdKcUAAAAAAN0pxQAAAAAA3SnFAAAAAADd/R+yv7VfFVktawAAAABJRU5ErkJggg=="/>															
													</xsl:otherwise>
												</xsl:choose>
							     </xsl:for-each>

								<br/>
								<br/>
								<br/>
								<table border="0" width="100%">
										<tr align="left">
											<td width="35%">
												<xsl:text>&#214;zelle&#351;tirme No</xsl:text>
											</td>
											<td width="65%">
												<xsl:value-of select="//n1:Invoice/cbc:CustomizationID"/>
											</td>
										</tr>
										<tr align="left">
											<td>
												<xsl:text>Senaryo</xsl:text>
											</td>
											<td>
												<xsl:value-of select="//n1:Invoice/cbc:ProfileID"/>
											</td>
										</tr>
										<tr align="left">
											<td>
												<xsl:text>Fatura ID</xsl:text>
											</td>
											<td>
												<xsl:value-of select="//n1:Invoice/cbc:ID"/>
											</td>
										</tr>
										<tr align="left">
											<td>
												<xsl:text>Fatura Tarih</xsl:text>
											</td>
											<xsl:for-each select="//n1:Invoice/cbc:ID">
												<xsl:choose>
													
													<xsl:when test="substring(.,2,2) = '27'">
														<td>
														<xsl:value-of select="substring(//n1:Invoice/cbc:IssueDate,9,2)"/>
														<xsl:text>-</xsl:text>
														<xsl:value-of select="substring(//n1:Invoice/cbc:IssueDate,6,2)"/>
														<xsl:text>-</xsl:text>
														<xsl:value-of select="substring(//n1:Invoice/cbc:IssueDate,1,4)"/>
														<xsl:text> </xsl:text>
														<xsl:value-of select="substring(//n1:Invoice/cbc:IssueDate,12,5)"/>
															
														</td>
															
													</xsl:when>
													<xsl:when test="substring(.,2,2) != '27'">
														<td>
														<xsl:value-of select="substring(//n1:Invoice/cbc:IssueDate,9,2)"/>
														<xsl:text>-</xsl:text>
														<xsl:value-of select="substring(//n1:Invoice/cbc:IssueDate,6,2)"/>
														<xsl:text>-</xsl:text>
														<xsl:value-of select="substring(//n1:Invoice/cbc:IssueDate,1,4)"/>
															<xsl:text> </xsl:text>
															23:59
														</td>
															
													</xsl:when>
												</xsl:choose>
											</xsl:for-each>
											
										</tr>
										<tr align="left">
											<td>
												<xsl:text>Fatura Tipi</xsl:text>
											</td>
											<td>
												<xsl:value-of select="//n1:Invoice/cbc:InvoiceTypeCode"/>
											</td>
										</tr>
										<xsl:if test="n1:Invoice/cac:OrderReference">
										<tr align="left">
										  <td>
											<xsl:text>Müşteri Talep No</xsl:text>
										  </td>
										  <td>
											<xsl:value-of select="n1:Invoice/cac:OrderReference/cbc:ID"/>
										  </td>
										</tr>
										</xsl:if>
									</table>
								</td>
							</tr>
							<!-- 2. Satır-->
							<tr style="height:80px; " valign="top">
								<td width="45%" valign="top" >
								<br/>
									<table  border="0">
										<xsl:for-each select="//n1:Invoice/cbc:Note">
											<xsl:choose>
												<xsl:when test="substring(.,0,5) = 'UPR:'">
												<tr align="right" >
													<td width="55%" align="left" >
													<b>
													<xsl:value-of select="substring-before(substring(.,5),':')"/>
													</b>
													<xsl:text>&#160;:</xsl:text>
													</td>
													<td width="45%" align="left" >
													<xsl:value-of select="substring-after(substring(.,5),':')"/>
													</td>
												</tr>
												</xsl:when>
											</xsl:choose>
										</xsl:for-each>
									</table>
									
									
								</td>
								<td width="20%" align="center" valign="top">
									
										
								</td>
								<td  align="left" valign="top" width="35%">
								<br/>
									<table  border="0">
										<xsl:for-each select="//n1:Invoice/cbc:Note">
											<xsl:choose>
												<xsl:when test="substring(.,0,6) = 'UPRM:'">
												<tr align="right">
													<td align="left" >
													<b>
													<xsl:value-of select="substring-before(substring(.,6),':')"/>
													</b>
													<xsl:text>&#160;:</xsl:text>
													</td>
													<td align="left" >
													<xsl:value-of select="substring-after(substring(.,6),':')"/>
													</td>
												</tr>
												</xsl:when>
											</xsl:choose>
										</xsl:for-each>
									</table>
									<br/>
									
								</td>
							</tr>
							<!-- 3. Satır-->
							<tr  >
								<td align="left" width="35%" valign="top">
									<table id="customerPartyTable" align="top" border="0" height="60%" >
										<tbody>
											<tr style="height:71px;"  >
												<td >
												<!-- Kişi Ad Soyad Adres-->
												<br/>
													<table align="center" border="0">
														<tbody>
															<tr>
																<td align="left">
																	<xsl:if test="normalize-space(//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyName/cbc:Name) != ''">
																		<span style="font-weight:bold; ">
																			<xsl:text>SAYIN</xsl:text>
																		</span>
																		
																		
																		<span>
																		<xsl:text>&#160;</xsl:text>
																		<xsl:text>&#160;</xsl:text>
																		<xsl:text>&#160;</xsl:text>
																			<xsl:text>&#160;</xsl:text>
																		</span>
																		
																		<span>
																			<xsl:value-of select="normalize-space(//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyName/cbc:Name)"/>
																		</span>
																		<br/>
																	</xsl:if>
																</td>
															</tr>
															<tr>
																<td align="left">
																	<xsl:if test="normalize-space(//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PostalAddress/cbc:StreetName) != ''">
																		<span>
																			<xsl:value-of select="normalize-space(//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PostalAddress/cbc:StreetName)"/>
																		</span>
																	</xsl:if>
																	<xsl:if test="normalize-space(//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PostalAddress/cbc:BuildingName) != ''">
																		<span>
																			<xsl:text>&#160;</xsl:text>
																			<xsl:value-of select="normalize-space(//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PostalAddress/cbc:BuildingName)"/>
																		</span>
																	</xsl:if>
																	<xsl:if test="normalize-space(//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PostalAddress/cbc:BuildingNumber) != ''">
																		<span>
																			<xsl:text>&#160;</xsl:text>
																			<xsl:text>NO:&#160;</xsl:text>
																			<xsl:value-of select="normalize-space(//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PostalAddress/cbc:BuildingNumber)"/>
																		</span>
																		<br/>
																	</xsl:if>
																</td>
															</tr>
															<tr>
																<td align="left">
																	<xsl:if test="normalize-space(//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PostalAddress/cbc:CitySubdivisionName) != ''">
																		<span>
																			<xsl:value-of select="normalize-space(//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PostalAddress/cbc:CitySubdivisionName)"/>
																		</span>
																		<xsl:text>&#160;/&#160;</xsl:text>
																	</xsl:if>
																	<xsl:if test="normalize-space(//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PostalAddress/cbc:CityName) != ''">
																		<span>
																			<xsl:value-of select="normalize-space(//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PostalAddress/cbc:CityName)"/>
																		</span>
																		<br/>
																	</xsl:if>
																</td>
															</tr>
															<xsl:for-each select="//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cbc:WebsiteURI">
																<xsl:if test="normalize-space(.) != ''">
																	<tr align="left">
																		<td>
																			<strong>
																				<xsl:text>Web Sitesi:&#160;</xsl:text>
																			</strong>
																			<xsl:value-of select="normalize-space(.)"/>
																		</td>
																	</tr>
																</xsl:if>
															</xsl:for-each>
															<xsl:for-each select="//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:Contact/cbc:ElectronicMail">
																<xsl:if test="normalize-space(.) != ''">
																	<tr align="left">
																		<td>
																			<strong>
																				<xsl:text>E-Posta: </xsl:text>
																			</strong>
																			<xsl:value-of select="normalize-space(.)"/>
																		</td>
																	</tr>
																</xsl:if>
															</xsl:for-each>
															<xsl:if test="//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:Contact/cbc:Telephone or //n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:Contact/cbc:Telefax">
																<tr align="left">
																	<td>
																		<xsl:if test="//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:Contact/cbc:Telephone">
																			<strong>
																				<xsl:text>Tel:&#160;</xsl:text>
																			</strong>
																			<xsl:value-of select="//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:Contact/cbc:Telephone"/>
																			<xsl:text>&#160;</xsl:text>
																		</xsl:if>
																		<xsl:if test="//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:Contact/cbc:Telefax">
																			<strong>
																				<xsl:text>Faks:&#160;</xsl:text>
																			</strong>
																			<xsl:value-of select="//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:Contact/cbc:Telefax"/>
																		</xsl:if>
																	</td>
																</tr>
															</xsl:if>
															
														</tbody>
													</table>
													<table border="0" align="left"> <!--Vergi Dairasi Vergi No-->
														<tr align="left">
															<td>
																<xsl:for-each select="//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyIdentification">
																	<xsl:if test="cbc:ID/@schemeID = 'VKN'">
																		<b><xsl:text>Vergi Dairesi: </xsl:text></b>
																		<xsl:value-of select="//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyTaxScheme/cac:TaxScheme/cbc:Name"/>
																	</xsl:if>
																	<br/>
																	<xsl:if test="cbc:ID/@schemeID = 'VKN'">
																		<b><xsl:text>Vergi No: </xsl:text></b>
																		<xsl:value-of select="cbc:ID"/>
																	</xsl:if>
																	<xsl:if test="cbc:ID/@schemeID = 'TCKN'">
																		<b><xsl:text>TC Kimlik No: </xsl:text></b>
																		<xsl:value-of select="cbc:ID"/>
																	</xsl:if>
																</xsl:for-each>	
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
									<xsl:text>ETTN: </xsl:text>
									<xsl:value-of select="//n1:Invoice/cbc:UUID"/>
								</td>
								<td width="20%" align="center" valign="center">
								</td>
								<td align="left" width="53%" valign=" top" >
									<br/>
									<table  border="0" ><!--Son Ödeme Tarihi-->
										<xsl:for-each select="//n1:Invoice/cbc:Note">
											<xsl:choose>
												<xsl:when test="substring(.,0,5) = 'UPL:'">
												<tr align="right">
													<td align="left" >
														<span style="font-size:15px"><b>  <xsl:value-of select="substring-before(substring(.,5),':')"/>
														</b>
														</span>
													</td>
													
													 <xsl:if test="substring(.,6,1) = 'D'">
														<td align="left" ><xsl:text>&#160;: </xsl:text> 
															<span style="font-size:15px">
															<xsl:value-of select="format-number(number(substring-before(substring-after(substring(.,5),':'),'TL')), '###.##0,00', 'european')"/>
															<xsl:text>&#160;TL</xsl:text> 
															</span>
														</td>
													 </xsl:if>
													 <xsl:if test="substring(.,6,1) != 'D'">
														<td align="left" ><xsl:text>&#160;: </xsl:text> 
															<span style="font-size:15px">
															<xsl:value-of select="substring-after(substring(.,5),':')"/>
									
															</span>
														</td>
													 </xsl:if>
												</tr>
												</xsl:when>
											</xsl:choose>
										</xsl:for-each>
									</table>
									<BR/>
									<table  border="0" >
										<xsl:for-each select="//n1:Invoice/cbc:Note">
											<xsl:choose>
												<xsl:when test="substring(.,0,7) = 'NOT1C:'">
												<tr align="left">
													<td style="text-align:left;">
														<xsl:value-of select="substring-before(substring(.,7),'!#')" />
													</td>

													<td style="text-align:left;">
													:	<xsl:value-of select="substring-after(substring(.,7),'!#')" />
													</td>

												</tr>
												</xsl:when>
											</xsl:choose>
										</xsl:for-each>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
					<div id="lineTableAligner">
						<span>
							<xsl:text>&#160;</xsl:text>
						</span>
					</div>
					<!--Ücret Kalemleri Tablosu-->
					<xsl:for-each select="//n1:Invoice/cbc:ID">
						<xsl:choose>
							
							<!--Kablosuz Ihope Ücret Kalemleri Tablosu-->
							<xsl:when test="substring(.,2,2) = '18' or substring(.,2,2) ='23'">
								<table border="0" class = "mustafa"  width="800" >
									<tbody>
										<!--Ücret Tablosu Başlık Satırları-->
										<xsl:for-each select="//n1:Invoice/cbc:ID">
											<xsl:choose>
												<!--KABLOSUZ IHOPE Ücret Tablosu Başlık Satırı-->
												<xsl:when test="substring(.,2,2) = '18' or substring(.,2,2) ='23'">
												<tr  style=" border-bottom: 2px solid black; " align="left;">
													<th  align="left" width="45%" style="font-weight:bold; ">
														<span style="font-weight:bold; ">
															<xsl:text></xsl:text>
														</span>
													</th>
													<th  align="right" width="10%">
														<span style="font-weight:bold; ">
															<xsl:text></xsl:text>
														</span>
													</th>
													<th  align="right" width="10%">
														<span style="font-weight:bold; ">
															<xsl:text></xsl:text>
														</span>
													</th>
													<th  align="right" width="10%">
														<xsl:for-each select="//n1:Invoice/cbc:DocumentCurrencyCode">
														  <xsl:if test="substring(.,0,4) = 'TRY'">
															  <span style="font-weight:bold; ">
																<xsl:text>TUTAR (TL)</xsl:text>
															  </span>
														  </xsl:if>
														  <xsl:if test="substring(.,0,4) = 'EUR'">
															  <span style="font-weight:bold; ">
																<xsl:text>TUTAR (EUR)</xsl:text>
															  </span>
														  </xsl:if>
														</xsl:for-each>
													
													</th>
												</tr>
												</xsl:when>
											</xsl:choose>
										</xsl:for-each>
										<!--KABLOSUZ IHOPE InvoiceLine ücret kalemleri-->
										<xsl:for-each select="//n1:Invoice/cac:InvoiceLine">
												<tr align="left" >
													<xsl:if test="./cbc:Note ">
														<td style="width:45%;text-align:left; ">
															
															<xsl:value-of select="./cbc:Note"/>
															
														</td>
														<td style="width:10%;text-align:right; ">
														
														</td>
														<td style="width:10%;text-align:right; ">
														
														</td>
													</xsl:if>
													<xsl:if test="./cbc:LineExtensionAmount ">
														<td style="width:10%;text-align:right; ">
															<xsl:value-of select="./cbc:LineExtensionAmount"/>
														</td>
													</xsl:if>
												</tr>
										</xsl:for-each>
										<!--KABLOSUZ IHOPE cbc:Note alanındaki SUM ile başlayan ücretler-->
										<table border="0"  width="800" >
											<tr align="left" style=" border-bottom: 2px solid black; ">
												<td style="width:45%;text-align:left;  border-top: 2px solid black; ">
													
												</td>
												<td style="width:10%;text-align:right; border-top: 2px solid black; ">
														
												</td>
												<td style="width:10%;text-align:right; border-top: 2px solid black; ">
														
												</td>
												<td style="width:10%;text-align:right; border-top: 2px solid black; ">
													
												</td>
											</tr>
											<xsl:for-each select="//n1:Invoice/cbc:Note">
												<xsl:if test="substring(.,0,5) = 'SUM:'">
													<tr align="left">
														<td style="width:45%;text-align:left;">
															<strong>
																<xsl:value-of select="substring-before(substring(.,5),':')"/>
															</strong>
														</td>
														<td style="width:10%;text-align:right;">
																
														</td>
														<td style="width:10%;text-align:right;">
																
														</td>
														<td style="width:10%;text-align:right;">
															<strong>
																<xsl:value-of select="format-number(number(substring-after(substring(.,5),':')), '###.##0,00', 'european')"/>
															</strong>
														</td>
													</tr>
												</xsl:if>
											</xsl:for-each>
										</table>
									</tbody>
								</table>
							</xsl:when>
							<!--ADSL ME VOIP ve IPTAL Ücret Kalemleri Tablosu-->
							<xsl:when test="substring(.,2,2) = '16' or substring(.,2,2) ='17' or substring(.,2,2) ='24' or substring(.,2,2) ='21' or substring(.,2,2) ='27' or substring(.,2,2) = '26'or substring(.,2,2) = '30'">
								<table border="0" class = "mustafa"  width="800" >
									<tbody>
										<!--Ücret Tablosu Başlık Satırları-->
										<xsl:for-each select="//n1:Invoice/cbc:ID">
											<xsl:choose>
												<!--VOIP Ücret Tablosu Başlık Satırı-->
												<xsl:when test="substring(.,2,2) = '21'">
												<tr  style=" border-bottom: 2px solid black; " align="left;">
													<th  align="left" width="45%" style="font-weight:bold; ">
														<span style="font-weight:bold; ">
															<xsl:text>KULLANIM ÜCRETLERİ</xsl:text>
														</span>
													</th>
													<th  align="right" width="10%">
														<span style="font-weight:bold; ">
															<xsl:text></xsl:text>
														</span>
													</th>
													<th  align="right" width="10%">
														<span style="font-weight:bold; ">
															<xsl:text></xsl:text>
														</span>
													</th>
													<th  align="right" width="10%">
														<span style="font-weight:bold; ">
															<xsl:text>TUTAR (TL)</xsl:text>
														</span>
													</th>
												</tr>
												</xsl:when>
												<!--VOIP Ücret Tablosu Başlık Satırı-->
												<xsl:when test="substring(.,2,2) ='27'">
												<tr  style=" border-bottom: 2px solid black; " align="left;">
													<th  align="left" width="45%" style="font-weight:bold; ">
														<span style="font-weight:bold; ">
															<xsl:text>KATMA DEĞERLİ SERVİSLER KULLANIM ÜCRETLERİ</xsl:text>
														</span>
													</th>
													<th  align="right" width="10%">
														<span style="font-weight:bold; ">
															<xsl:text></xsl:text>
														</span>
													</th>
													<th  align="right" width="10%">
														<span style="font-weight:bold; ">
															<xsl:text></xsl:text>
														</span>
													</th>
													<th  align="right" width="10%">
														<span style="font-weight:bold; ">
															<xsl:text>TOPLAM (TL)</xsl:text>
														</span>
													</th>
												</tr>
												</xsl:when>
												<!--IPTAL Ücret Tablosu Başlık Satırı-->
												<xsl:when test="substring(.,2,2) = '24'">
												<tr  style=" border-bottom: 2px solid black; " align="left;">
													<th  align="left" width="45%" style="font-weight:bold; ">
														<span style="font-weight:bold; ">
															<xsl:text></xsl:text>
														</span>
													</th>
													<th  align="right" width="10%">
														<span style="font-weight:bold; ">
															<xsl:text></xsl:text>
														</span>
													</th>
													<th  align="right" width="10%">
														<span style="font-weight:bold; ">
															<xsl:text></xsl:text>
														</span>
													</th>
													<th  align="right" width="10%">
														<span style="font-weight:bold; ">
															<xsl:text>TUTAR (TL)</xsl:text>
														</span>
													</th>
												</tr>
												</xsl:when>
												<!--ADSL, ME Ücret Tablosu Başlık Satırı-->
												<xsl:when test="substring(.,2,2) = '16' or substring(.,2,2) ='17' or substring(.,2,2) = '26' or substring(.,2,2) = '30'">
												<tr  style=" border-bottom: 2px solid black; " align="left;">
													<th  align="right" width="45%" style="font-weight:bold; ">
														
															<xsl:text></xsl:text>
													</th>
													<th  align="right" width="10%">
														<span style="font-weight:bold; ">
															<xsl:text>Tutar</xsl:text>
														</span>
													</th>
													<th  align="right" width="10%">
														<span style="font-weight:bold; ">
															<xsl:text>&#304;ndirim</xsl:text>
														</span>
													</th>
													<th  align="right" width="10%">
														<span style="font-weight:bold; ">
															<xsl:text>Toplam</xsl:text>
														</span>
													</th>
												</tr>
												</xsl:when>
											</xsl:choose>
										</xsl:for-each>
										<xsl:for-each select="//n1:Invoice/cac:InvoiceLine/cac:Item/cbc:ModelName">
											<xsl:choose>
												<xsl:when test="substring(.,136,1) = 'a'">
												<tr   align="left" >
													<td style="width:45%;text-align:left; border-left: 2px solid black; border-top: 2px solid black; border-bottom: 2px solid black;">
													<b>	<xsl:value-of select="substring(.,0,76)" /></b>
													</td>
													<td style="width:10%;text-align:right; border-top: 2px solid black; border-bottom: 2px solid black;">
													<b>	<xsl:value-of select="format-number(number(substring(.,76,20)), '###.##0,00', 'european')" /></b>
													</td>
													<td style="width:10%;text-align:right; border-top: 2px solid black; border-bottom: 2px solid black;">
													<b>	<xsl:value-of select="format-number(number(substring(.,96,20)), '###.##0,00', 'european')" /></b>
													</td>
													<td style="width:10%;text-align:right; border-right: 2px solid black; border-top: 2px solid black; border-bottom: 2px solid black;">
													<b>	<xsl:value-of select="format-number(number(substring(.,116,20)), '###.##0,00', 'european')" /></b>
													</td>
												</tr>
												</xsl:when>
												<xsl:when test="substring(.,136,1) = 'c'">
												<tr  style=" border-bottom: 2px solid black; " align="left;">
													<td style="width:45%;text-align:left; ">
														<xsl:value-of select="substring(.,0,76)" />
													</td>
													<td style="width:10%;text-align:right;">
														<xsl:value-of select="format-number(number(substring(.,76,20)), '###.##0,00', 'european')" />
													</td>
													<td style="width:10%;text-align:right;">
														<xsl:value-of select="format-number(number(substring(.,96,20)), '###.##0,00', 'european')" />
													</td>
													<td style="width:10%;text-align:right;">
														<xsl:value-of select="format-number(number(substring(.,116,20)), '###.##0,00', 'european')" />
													</td>
												</tr>
												</xsl:when>
											</xsl:choose>
										</xsl:for-each>
									</tbody>
								</table>
							</xsl:when>
						</xsl:choose>
					</xsl:for-each>
					
					<table border="0"  width="800" >
						<xsl:for-each select="//n1:Invoice/cbc:Note">
							<xsl:if test="substring(.,0,7) = 'SUMTF:'">
								<tr align="left">
									<td style="width:45%;text-align:left;">
										<strong>
											<xsl:value-of select="substring-before(substring(.,7),':')"/>
										</strong>
									</td>
									<td style="width:10%;text-align:right;">
											
									</td>
									<td style="width:10%;text-align:right;">
											
									</td>
									<td style="width:10%;text-align:right;">
										<strong>
											<xsl:value-of select="format-number(number(substring-after(substring(.,7),':')), '###.##0,00', 'european')"/>
										</strong>
									</td>
								</tr>
							</xsl:if>
						</xsl:for-each>
					</table>
					<table border="0"  width="800" >
						<tbody>
							<xsl:for-each select="//n1:Invoice/cac:InvoiceLine/cac:Item/cbc:ModelName">
								<xsl:choose>
									<xsl:when test="substring(.,136,1) = 'b'">
									<tr align="left">
										<td style="width:45%;text-align:left;">
											<xsl:value-of select="substring(.,0,76)" />
										</td>
										<td style="width:10%;text-align:right;">
											<xsl:value-of select="format-number(number(substring(.,76,20)), '###.##0,00', 'european')" />
										</td>
										<td style="width:10%;text-align:right;">
											<xsl:value-of select="format-number(number(substring(.,96,20)), '###.##0,00', 'european')" />
										</td>
										<td style="width:10%;text-align:right;">
											<xsl:value-of select="format-number(number(substring(.,116,20)), '###.##0,00', 'european')" />
										</td>
									</tr>
									</xsl:when>
								</xsl:choose>
							</xsl:for-each>
						</tbody>
					</table>
					<table id ="lineTable" border="0"  width="800" >
						<xsl:for-each select="//n1:Invoice/cbc:Note">
							<xsl:if test="substring(.,0,7) = 'SUMOT:'">
								<tr align="left">
									<td style="width:45%;text-align:left;">
										<strong>
											<xsl:value-of select="substring-before(substring(.,7),':')"/>
										</strong>
									</td>
									<td style="width:10%;text-align:right;">
											
									</td>
									<td style="width:10%;text-align:right;">
											
									</td>
									<td style="width:10%;text-align:right;">
										<strong>
											<xsl:value-of select="format-number(number(substring-after(substring(.,7),':')), '###.##0,00', 'european')"/>
										</strong>
									</td>
								</tr>
							</xsl:if>
						</xsl:for-each>
					</table>
					<table  border="0"  width="800" >
						<tr>
							<td >
								<xsl:for-each select="//n1:Invoice/cbc:Note">
									<xsl:choose>
										<xsl:when test="substring(.,0,5) = 'YZY:'">
											<xsl:text>Vergiler Dahil </xsl:text>
											<xsl:value-of select="substring(.,5)" />
											<br />
										</xsl:when>
									
									</xsl:choose>
								</xsl:for-each>
								
							</td>
						</tr>
					</table>
					<br/>
					<br/>
					<!-- TV  Tablosu-->
					<table  id ="lineTable"   border="0"  width="800" >
						<xsl:for-each select="//n1:Invoice/cbc:Note">
							<xsl:choose>
								<xsl:when test="substring(.,0,7) = 'SUMTV:'">

									<tr align="left  border-left: 2px solid black; border-top: 2px solid black; border-bottom: 2px solid black;  ">
										<td style="width:45%;text-align:left;">
											<xsl:value-of select="substring-before(substring(.,7),':')" />
										</td>
									
										<td style="width:10%;text-align:right;  ">
											<xsl:value-of select="format-number(number(substring-after(substring(.,7),':')), '###.##0,00', 'european')" />
										</td>
									</tr>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</table>
						<br/>
							<br/>
					<!-- Vergiler  Tablosu-->
					<table id ="lineTable" border="0"  width="800" >
						<xsl:for-each select="//n1:Invoice/cbc:Note">
							<xsl:choose>
								<xsl:when test="substring(.,0,6) = 'N2CM:'">

									<tr align="left">
										<td style="width:45%;text-align:left;">
											<xsl:value-of select="substring-before(substring(.,6),'!#')" />
										</td>
										<td style="width:10%;text-align:right;">
												
										</td>
										<td style="width:10%;text-align:right;">
												
										</td>
										<td style="width:10%;text-align:right;">
											<xsl:value-of select="format-number(number(substring-after(substring(.,6),'!# :')), '###.##0,00', 'european')" />
										</td>
									</tr>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</table>
					<br/>
					
					<!-- Kampanaya Taahhut Tablosu -->
					<table  id ="lineTable" border="1"  width="800" >
						<xsl:for-each select="//n1:Invoice/cbc:Note">
							<!-- 1. 2. 3. satır 2 Stun -->
							<xsl:if test="substring(.,0,8) = '1NOT13:'">
										<tr align="left">
											<td style="wtext-align:left;">
												<xsl:value-of select="substring(.,9,70)" />
											</td>
											<td style="wtext-align:left;">
												<xsl:value-of select="substring(.,79,70)" />
											</td>
										</tr>
							</xsl:if>
							<!-- 1. 2. 3. satır 3 Stun -->
							<xsl:if test="substring(.,0,8) = '2NOT13:'">
										<tr align="left">
											<td style="wtext-align:left;">
												<xsl:value-of select="substring(.,9,70)" />
											</td>
											<td style="wtext-align:left;">
												<xsl:value-of select="substring(.,79,70)" />
											</td>
											<td style="wtext-align:left;">
												<xsl:value-of select="substring(.,149,70)" />
											</td>
										</tr>
							</xsl:if>
							<!-- 1. 2. 3. satır 4 Stun -->
							<xsl:if test="substring(.,0,8) = '3NOT13:'">
										<tr align="left">
											<td style="wtext-align:left;">
												<xsl:value-of select="substring(.,9,70)" />
											</td>
											<td style="wtext-align:left;">
												<xsl:value-of select="substring(.,79,70)" />
											</td>
											<td style="wtext-align:left;">
												<xsl:value-of select="substring(.,149,70)" />
											</td>
											<td style="wtext-align:left;">
												<xsl:value-of select="substring(.,219,70)" />
											</td>
										</tr>
							</xsl:if>
							<!-- 1. 2. 3. satır 4 Stun -->
							<xsl:if test="substring(.,0,8) = '4NOT13:'">
										<tr align="left">
											<td style="wtext-align:left;">
												<xsl:value-of select="substring(.,9,70)" />
											</td>
											<td style="wtext-align:left;">
												<xsl:value-of select="substring(.,79,70)" />
											</td>
											<td style="wtext-align:left;">
												<xsl:value-of select="substring(.,149,70)" />
											</td>
											<td style="wtext-align:left;">
												<xsl:value-of select="substring(.,219,70)" />
											</td>
											<td style="wtext-align:left;">
												<xsl:value-of select="substring(.,289,70)" />
											</td>
										</tr>
							</xsl:if>
							<!-- 1. 2. 3. satır 5 Stun -->
							<xsl:if test="substring(.,0,8) = '5NOT13:'">
										<tr align="left">
											<td style="wtext-align:left;">
												<xsl:value-of select="substring(.,9,70)" />
											</td>
											<td style="wtext-align:left;">
												<xsl:value-of select="substring(.,79,70)" />
											</td>
											<td style="wtext-align:left;">
												<xsl:value-of select="substring(.,149,70)" />
											</td>
											<td style="wtext-align:left;">
												<xsl:value-of select="substring(.,219,70)" />
											</td>
											<td style="wtext-align:left;">
												<xsl:value-of select="substring(.,289,70)" />
											</td>
											<td style="text-align:left;">
												<xsl:value-of select="substring(.,359,70)" />
											</td>
										</tr>
							</xsl:if>
						</xsl:for-each>
						<xsl:for-each select="//n1:Invoice/cbc:Note">
							<xsl:choose>
								<xsl:when test="substring(.,0,10) = '5NOT13_4:'">

									<tr align="left">
										<td style="text-align:left;">
											<xsl:value-of select="substring(.,11,70)" />
										</td>
										<!-- Kampanaya Taahhut Tablosu satır 4, stun 2-->
										<td style="text-align:left;">
											<table border='0'>
														<xsl:if test="substring(.,81,7) != '   0.00'">
														<tr>
														<td>
															<xsl:text>İnternet: </xsl:text>
															<xsl:value-of select="substring(.,81,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,88,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu İletim: </xsl:text>
															<xsl:value-of select="substring(.,88,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,95,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu İçerik: </xsl:text>
															<xsl:value-of select="substring(.,95,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,102,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Go: </xsl:text>
															<xsl:value-of select="substring(.,102,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,109,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Cep: </xsl:text>
															<xsl:value-of select="substring(.,109,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,116,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Smart: </xsl:text>
															<xsl:value-of select="substring(.,116,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,123,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Web: </xsl:text>
															<xsl:value-of select="substring(.,123,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,130,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Alo: </xsl:text>
															<xsl:value-of select="substring(.,130,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,137,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Alo (DSL): </xsl:text>
															<xsl:value-of select="substring(.,137,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														
											</table>
										</td>
										<!-- Kampanaya Taahhut Tablosu satır 4, stun 3-->
										<td style="text-align:left;">
											<table border='0'>
														<xsl:if test="substring(.,144,7) != '   0.00'">
														<tr>
														<td>
															<xsl:text>İnternet: </xsl:text>
															<xsl:value-of select="substring(.,144,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,151,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu İletim: </xsl:text>
															<xsl:value-of select="substring(.,151,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,158,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu İçerik: </xsl:text>
															<xsl:value-of select="substring(.,158,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,165,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Go: </xsl:text>
															<xsl:value-of select="substring(.,165,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,172,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Cep: </xsl:text>
															<xsl:value-of select="substring(.,172,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,179,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Smart: </xsl:text>
															<xsl:value-of select="substring(.,179,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,186,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Web: </xsl:text>
															<xsl:value-of select="substring(.,186,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,193,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Alo: </xsl:text>
															<xsl:value-of select="substring(.,193,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,200,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Alo (DSL): </xsl:text>
															<xsl:value-of select="substring(.,200,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														
											</table>
										</td>
										<!-- Kampanaya Taahhut Tablosu satır 4, stun 4-->
										<td style="text-align:left;">
											<table border='0'>
														<xsl:if test="substring(.,207,7) != '   0.00'">
														<tr>
														<td>
															<xsl:text>İnternet: </xsl:text>
															<xsl:value-of select="substring(.,207,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,214,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu İletim: </xsl:text>
															<xsl:value-of select="substring(.,214,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,221,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu İçerik: </xsl:text>
															<xsl:value-of select="substring(.,221,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,228,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Go: </xsl:text>
															<xsl:value-of select="substring(.,228,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,235,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Cep: </xsl:text>
															<xsl:value-of select="substring(.,235,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,242,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Smart: </xsl:text>
															<xsl:value-of select="substring(.,242,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,249,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Web: </xsl:text>
															<xsl:value-of select="substring(.,249,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,256,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Alo: </xsl:text>
															<xsl:value-of select="substring(.,256,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,263,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Alo (DSL): </xsl:text>
															<xsl:value-of select="substring(.,263,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														
											</table>
										</td>
										<!-- Kampanaya Taahhut Tablosu satır 4, stun 5-->
										<td style="text-align:left;">
											<table border='0'>
														<xsl:if test="substring(.,270,7) != '   0.00'">
														<tr>
														<td>
															<xsl:text>İnternet: </xsl:text>
															<xsl:value-of select="substring(.,270,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,277,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu İletim: </xsl:text>
															<xsl:value-of select="substring(.,277,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,284,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu İçerik: </xsl:text>
															<xsl:value-of select="substring(.,284,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,291,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Go: </xsl:text>
															<xsl:value-of select="substring(.,291,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,298,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Cep: </xsl:text>
															<xsl:value-of select="substring(.,298,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,305,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Smart: </xsl:text>
															<xsl:value-of select="substring(.,305,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,312,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Web: </xsl:text>
															<xsl:value-of select="substring(.,312,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,319,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Alo: </xsl:text>
															<xsl:value-of select="substring(.,319,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,326,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Alo (DSL): </xsl:text>
															<xsl:value-of select="substring(.,326,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														
											</table>
										</td>
										<!-- Kampanaya Taahhut Tablosu satır 4, stun 6-->
										<td style="text-align:left;">
											<table border='0'>
														<xsl:if test="substring(.,333,7) != '   0.00'">
														<tr>
														<td>
															<xsl:text>İnternet: </xsl:text>
															<xsl:value-of select="substring(.,333,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,340,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu İletim: </xsl:text>
															<xsl:value-of select="substring(.,340,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,347,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu İçerik: </xsl:text>
															<xsl:value-of select="substring(.,347,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,354,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Go: </xsl:text>
															<xsl:value-of select="substring(.,354,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,361,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Cep: </xsl:text>
															<xsl:value-of select="substring(.,361,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,368,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Smart: </xsl:text>
															<xsl:value-of select="substring(.,368,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,375,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Web: </xsl:text>
															<xsl:value-of select="substring(.,375,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,382,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Alo: </xsl:text>
															<xsl:value-of select="substring(.,382,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,389,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Alo (DSL): </xsl:text>
															<xsl:value-of select="substring(.,389,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														
											</table>
										</td>
									</tr>
								</xsl:when>
								<xsl:when test="substring(.,0,10) = '4NOT13_4:'">

									<tr align="left">
										<td style="text-align:left;">
											<xsl:value-of select="substring(.,11,70)" />
										</td>
										<!-- Kampanaya Taahhut Tablosu satır 4, stun 2-->
										<td style="text-align:left;">
											<table border='0'>
														<xsl:if test="substring(.,81,7) != '   0.00'">
														<tr>
														<td>
															<xsl:text>İnternet: </xsl:text>
															<xsl:value-of select="substring(.,81,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,88,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu İletim: </xsl:text>
															<xsl:value-of select="substring(.,88,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,95,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu İçerik: </xsl:text>
															<xsl:value-of select="substring(.,95,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,102,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Go: </xsl:text>
															<xsl:value-of select="substring(.,102,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,109,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Cep: </xsl:text>
															<xsl:value-of select="substring(.,109,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,116,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Smart: </xsl:text>
															<xsl:value-of select="substring(.,116,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,123,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Web: </xsl:text>
															<xsl:value-of select="substring(.,123,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,130,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Alo: </xsl:text>
															<xsl:value-of select="substring(.,130,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,137,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Alo (DSL): </xsl:text>
															<xsl:value-of select="substring(.,137,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														
											</table>
										</td>
										<!-- Kampanaya Taahhut Tablosu satır 4, stun 3-->
										<td style="text-align:left;">
											<table border='0'>
														<xsl:if test="substring(.,144,7) != '   0.00'">
														<tr>
														<td>
															<xsl:text>İnternet: </xsl:text>
															<xsl:value-of select="substring(.,144,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,151,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu İletim: </xsl:text>
															<xsl:value-of select="substring(.,151,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,158,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu İçerik: </xsl:text>
															<xsl:value-of select="substring(.,158,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,165,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Go: </xsl:text>
															<xsl:value-of select="substring(.,165,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,172,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Cep: </xsl:text>
															<xsl:value-of select="substring(.,172,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,179,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Smart: </xsl:text>
															<xsl:value-of select="substring(.,179,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,186,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Web: </xsl:text>
															<xsl:value-of select="substring(.,186,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,193,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Alo: </xsl:text>
															<xsl:value-of select="substring(.,193,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,200,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Alo (DSL): </xsl:text>
															<xsl:value-of select="substring(.,200,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														
											</table>
										</td>
										<!-- Kampanaya Taahhut Tablosu satır 4, stun 4-->
										<td style="text-align:left;">
											<table border='0'>
														<xsl:if test="substring(.,207,7) != '   0.00'">
														<tr>
														<td>
															<xsl:text>İnternet: </xsl:text>
															<xsl:value-of select="substring(.,207,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,214,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu İletim: </xsl:text>
															<xsl:value-of select="substring(.,214,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,221,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu İçerik: </xsl:text>
															<xsl:value-of select="substring(.,221,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,228,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Go: </xsl:text>
															<xsl:value-of select="substring(.,228,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,235,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Cep: </xsl:text>
															<xsl:value-of select="substring(.,235,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,242,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Smart: </xsl:text>
															<xsl:value-of select="substring(.,242,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,249,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Web: </xsl:text>
															<xsl:value-of select="substring(.,249,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,256,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Alo: </xsl:text>
															<xsl:value-of select="substring(.,256,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,263,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Alo (DSL): </xsl:text>
															<xsl:value-of select="substring(.,263,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														
											</table>
										</td>
										<!-- Kampanaya Taahhut Tablosu satır 4, stun 5-->
										<td style="text-align:left;">
											<table border='0'>
														<xsl:if test="substring(.,270,7) != '   0.00'">
														<tr>
														<td>
															<xsl:text>İnternet: </xsl:text>
															<xsl:value-of select="substring(.,270,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,277,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu İletim: </xsl:text>
															<xsl:value-of select="substring(.,277,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,284,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu İçerik: </xsl:text>
															<xsl:value-of select="substring(.,284,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,291,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Go: </xsl:text>
															<xsl:value-of select="substring(.,291,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,298,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Cep: </xsl:text>
															<xsl:value-of select="substring(.,298,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,305,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Smart: </xsl:text>
															<xsl:value-of select="substring(.,305,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,312,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Web: </xsl:text>
															<xsl:value-of select="substring(.,312,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,319,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Alo: </xsl:text>
															<xsl:value-of select="substring(.,319,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,326,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Alo (DSL): </xsl:text>
															<xsl:value-of select="substring(.,326,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														
											</table>
										</td>
									</tr>
								</xsl:when>
								<xsl:when test="substring(.,0,10) = '3NOT13_4:'">

									<tr align="left">
										<td style="text-align:left;">
											<xsl:value-of select="substring(.,11,70)" />
										</td>
										<!-- Kampanaya Taahhut Tablosu satır 4, stun 2-->
										<td style="text-align:left;">
											<table border='0'>
														<xsl:if test="substring(.,81,7) != '   0.00'">
														<tr>
														<td>
															<xsl:text>İnternet: </xsl:text>
															<xsl:value-of select="substring(.,81,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,88,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu İletim: </xsl:text>
															<xsl:value-of select="substring(.,88,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,95,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu İçerik: </xsl:text>
															<xsl:value-of select="substring(.,95,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,102,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Go: </xsl:text>
															<xsl:value-of select="substring(.,102,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,109,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Cep: </xsl:text>
															<xsl:value-of select="substring(.,109,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,116,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Smart: </xsl:text>
															<xsl:value-of select="substring(.,116,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,123,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Web: </xsl:text>
															<xsl:value-of select="substring(.,123,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,130,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Alo: </xsl:text>
															<xsl:value-of select="substring(.,130,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,137,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Alo (DSL): </xsl:text>
															<xsl:value-of select="substring(.,137,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														
											</table>
										</td>
										<!-- Kampanaya Taahhut Tablosu satır 4, stun 3-->
										<td style="text-align:left;">
											<table border='0'>
														<xsl:if test="substring(.,144,7) != '   0.00'">
														<tr>
														<td>
															<xsl:text>İnternet: </xsl:text>
															<xsl:value-of select="substring(.,144,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,151,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu İletim: </xsl:text>
															<xsl:value-of select="substring(.,151,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,158,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu İçerik: </xsl:text>
															<xsl:value-of select="substring(.,158,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,165,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Go: </xsl:text>
															<xsl:value-of select="substring(.,165,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,172,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Cep: </xsl:text>
															<xsl:value-of select="substring(.,172,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,179,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Smart: </xsl:text>
															<xsl:value-of select="substring(.,179,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,186,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Web: </xsl:text>
															<xsl:value-of select="substring(.,186,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,193,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Alo: </xsl:text>
															<xsl:value-of select="substring(.,193,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,200,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Alo (DSL): </xsl:text>
															<xsl:value-of select="substring(.,200,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														
											</table>
										</td>
										<!-- Kampanaya Taahhut Tablosu satır 4, stun 4-->
										<td style="text-align:left;">
											<table border='0'>
														<xsl:if test="substring(.,207,7) != '   0.00'">
														<tr>
														<td>
															<xsl:text>İnternet: </xsl:text>
															<xsl:value-of select="substring(.,207,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,214,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu İletim: </xsl:text>
															<xsl:value-of select="substring(.,214,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,221,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu İçerik: </xsl:text>
															<xsl:value-of select="substring(.,221,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,228,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Go: </xsl:text>
															<xsl:value-of select="substring(.,228,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,235,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Cep: </xsl:text>
															<xsl:value-of select="substring(.,235,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,242,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Smart: </xsl:text>
															<xsl:value-of select="substring(.,242,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,249,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Web: </xsl:text>
															<xsl:value-of select="substring(.,249,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,256,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Alo: </xsl:text>
															<xsl:value-of select="substring(.,256,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,263,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Alo (DSL): </xsl:text>
															<xsl:value-of select="substring(.,263,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														
											</table>
										</td>
									</tr>
								</xsl:when>
								<xsl:when test="substring(.,0,10) = '2NOT13_4:'">

									<tr align="left">
										<td style="text-align:left;">
											<xsl:value-of select="substring(.,11,70)" />
										</td>
										<!-- Kampanaya Taahhut Tablosu satır 4, stun 2-->
										<td style="text-align:left;">
											<table border='0'>
														<xsl:if test="substring(.,81,7) != '   0.00'">
														<tr>
														<td>
															<xsl:text>İnternet: </xsl:text>
															<xsl:value-of select="substring(.,81,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,88,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu İletim: </xsl:text>
															<xsl:value-of select="substring(.,88,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,95,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu İçerik: </xsl:text>
															<xsl:value-of select="substring(.,95,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,102,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Go: </xsl:text>
															<xsl:value-of select="substring(.,102,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,109,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Cep: </xsl:text>
															<xsl:value-of select="substring(.,109,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,116,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Smart: </xsl:text>
															<xsl:value-of select="substring(.,116,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,123,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Web: </xsl:text>
															<xsl:value-of select="substring(.,123,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,130,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Alo: </xsl:text>
															<xsl:value-of select="substring(.,130,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,137,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Alo (DSL): </xsl:text>
															<xsl:value-of select="substring(.,137,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														
											</table>
										</td>
										<!-- Kampanaya Taahhut Tablosu satır 4, stun 3-->
										<td style="text-align:left;">
											<table border='0'>
														<xsl:if test="substring(.,144,7) != '   0.00'">
														<tr>
														<td>
															<xsl:text>İnternet: </xsl:text>
															<xsl:value-of select="substring(.,144,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,151,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu İletim: </xsl:text>
															<xsl:value-of select="substring(.,151,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,158,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu İçerik: </xsl:text>
															<xsl:value-of select="substring(.,158,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,165,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Go: </xsl:text>
															<xsl:value-of select="substring(.,165,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,172,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Cep: </xsl:text>
															<xsl:value-of select="substring(.,172,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,179,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Smart: </xsl:text>
															<xsl:value-of select="substring(.,179,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,186,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Web: </xsl:text>
															<xsl:value-of select="substring(.,186,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,193,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Alo: </xsl:text>
															<xsl:value-of select="substring(.,193,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,200,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Alo (DSL): </xsl:text>
															<xsl:value-of select="substring(.,200,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														
											</table>
										</td>
									</tr>
								</xsl:when>
								<xsl:when test="substring(.,0,10) = '1NOT13_4:'">

									<tr align="left">
										<td style="text-align:left;">
											<xsl:value-of select="substring(.,11,70)" />
										</td>
										<!-- Kampanaya Taahhut Tablosu satır 4, stun 2-->
										<td style="text-align:left;">
											<table border='0'>
														<xsl:if test="substring(.,81,7) != '   0.00'">
														<tr>
														<td>
															<xsl:text>İnternet: </xsl:text>
															<xsl:value-of select="substring(.,81,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,88,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu İletim: </xsl:text>
															<xsl:value-of select="substring(.,88,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,95,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu İçerik: </xsl:text>
															<xsl:value-of select="substring(.,95,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,102,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Go: </xsl:text>
															<xsl:value-of select="substring(.,102,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,109,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Cep: </xsl:text>
															<xsl:value-of select="substring(.,109,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,116,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Smart: </xsl:text>
															<xsl:value-of select="substring(.,116,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,123,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Tivibu Web: </xsl:text>
															<xsl:value-of select="substring(.,123,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,130,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Alo: </xsl:text>
															<xsl:value-of select="substring(.,130,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														<xsl:if test="substring(.,137,7) != '   0.00'">
														<tr>
														<td>
														<xsl:text>Alo (DSL): </xsl:text>
															<xsl:value-of select="substring(.,137,7)" />
															<xsl:text> TL</xsl:text>
														</td>
														</tr>
														</xsl:if>
														
											</table>
										</td>
									</tr>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
						<!-- 4 satır not kısmı -->
						<xsl:for-each select="//n1:Invoice/cbc:Note">
							<xsl:if test="substring(.,0,8) = 'NOT13N:'">
								<tr align="left">
									<td style="wtext-align:left;" colspan="6">
										<xsl:value-of select="substring(.,9,200)" />
									</td>
								</tr>	
							</xsl:if>
						</xsl:for-each>
					</table>
					<!-- Note  Tablosu-->
					<xsl:for-each select="//n1:Invoice/cbc:ID">
						
						<xsl:choose>
							<xsl:when test="substring(.,2,2) = '16'">
								
							</xsl:when>
							
						</xsl:choose>
						
							<!-- TADSL ME VE VOIP FATURALARI Not Tablosu -->
							<table class="notesTable" width="800" border="1" align="left" height="50"  style="margin-top:10px;margin-bottom:10px;width:750px;text-align:left;">
							<xsl:if test="substring(.,2,2) ='16' or substring(.,2,2) ='17' or substring(.,2,2) ='21'or substring(.,2,2) ='26'or substring(.,2,2) ='30'">
								<tr >
									<td id="notesTableTd">
										<table style="text-align:left;float:left; " width="800" align="left"  >
											<xsl:for-each select="//n1:Invoice/cbc:Note">
												<xsl:choose>
													<xsl:when test="substring(.,0,5) = 'N3C:'">

														<tr align="left">
															<td style="width:33%;text-align:left;">
																<xsl:value-of select="substring-before(substring(.,5),'!#')" />
															</td>

															<td style="width:33%;text-align:left;">
																<xsl:value-of select="substring-before(substring-after(substring(.,5),'!#'),'!#')" />
															</td>

															<td style="width:33%;text-align:left;">
																<xsl:value-of select="substring-after(substring-after(substring(.,5),'!#'),'!#')" />
															</td>
														</tr>
													</xsl:when>

													<xsl:when test="substring(.,0,13) = ':N3C-AYIRAC:'">
														<tr align="left">
															<td  colspan="3">
																<hr />
															</td>
														</tr>
													</xsl:when>

												</xsl:choose>
											</xsl:for-each>
										</table>
										<table border='0' style="text-align:left;float:left;" width="800" align="left" >
											
											<h><b><xsl:text>HATIRLATMA!</xsl:text></b>	</h>
											
											<xsl:for-each select="//n1:Invoice/cbc:Note">
												<xsl:choose>
													<xsl:when test="substring(.,0,5) = 'N2C:'">
														<xsl:if test="substring(.,6,6) = 'Fatura'">
															<tr align="left">
																<td style="width:50%;text-align:left;">
																	<xsl:value-of select="substring-before(substring(.,5),'!#')" />
																</td>
																<td style="width:10%;text-align:right;">
																	
																</td>
																<td style="width:50%;text-align:left;">
																
																</td>
															</tr>
														 </xsl:if>
														<tr align="left">
															<xsl:if test="substring(.,17,1) = 'T'">
															<td style="width:50%;text-align:left;">
																<xsl:value-of select="substring-after(substring(.,5),'!#')" />
															</td>
														 </xsl:if>
														 <xsl:if test="substring(.,17,1) != 'T'">
															<td style="width:50%;text-align:left;">
																<xsl:value-of select="format-number(number(substring-after(substring(.,5),'!#')), '###.##0,00', 'european')" />
															</td>
														 </xsl:if>
															<xsl:if test="substring(.,6,6) != 'Fatura'">
																<td style="width:10%;text-align:left;">
																	<xsl:value-of select="substring-before(substring(.,5),'!#')" />
																</td>
															</xsl:if>
														
														 <td style="width:50%;text-align:left;">
																
														</td>
														</tr>
													</xsl:when>
												</xsl:choose>
											</xsl:for-each>
										</table>
										<div style="text-align:left;float:left;" width="795">
											<xsl:for-each select="//n1:Invoice/cbc:Note">
												<xsl:choose>
													<xsl:when test="substring(.,0,6) = 'NOTH:'">
														
														<xsl:value-of select="substring(.,6)" />
														<br />
													</xsl:when>

													<xsl:when test="substring(.,0,13) = ':NOT-AYIRAC:'">
														<hr />
													</xsl:when>

												</xsl:choose>
											</xsl:for-each>
										</div>
										<br/>
										<div style="width:100%;text-align:left;float:left;">
											<br/>
												<b><xsl:text>SİZE ÖZEL</xsl:text></b>
											
											<br/>
											<xsl:for-each select="//n1:Invoice/cbc:Note">
												<xsl:choose>
													<xsl:when test="substring(.,0,6) = 'NOTS:'">
														
														<xsl:value-of select="substring(.,6)" />
														<br />
													</xsl:when>

													<xsl:when test="substring(.,0,13) = ':NOT-AYIRAC:'">
														<hr />
													</xsl:when>
												
												</xsl:choose>
											</xsl:for-each>
												<br/>
										</div>
										<br/>
									</td>
								</tr>
							</xsl:if>
							<xsl:for-each select="//n1:Invoice/cbc:Note">
								<xsl:choose>
									<xsl:when test="substring(.,0,6) = 'NOTE:'">
										<tr align="left" >
											<td style="text-align:left;float:left;" width="795">
												<xsl:value-of select="substring(.,6,100)" />
												
												<br/>
											</td>
										</tr>
									</xsl:when>
								</xsl:choose>
							</xsl:for-each>	
							</table>
						
						
					</xsl:for-each>
				</xsl:for-each>
				
			</body>
		</html>
	</xsl:template>	
</xsl:stylesheet>

