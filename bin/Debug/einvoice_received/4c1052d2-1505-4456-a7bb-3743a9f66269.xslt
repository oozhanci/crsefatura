<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"
                xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"
                xmlns:ccts="urn:un:unece:uncefact:documentation:2"
                xmlns:clm54217="urn:un:unece:uncefact:codelist:specification:54217:2001"
                xmlns:clm5639="urn:un:unece:uncefact:codelist:specification:5639:1988"
                xmlns:clm66411="urn:un:unece:uncefact:codelist:specification:66411:2001"
                xmlns:clmIANAMIMEMediaType="urn:un:unece:uncefact:codelist:specification:IANAMIMEMediaType:2003"
                xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:link="http://www.xbrl.org/2003/linkbase"
                xmlns:n1="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2"
                xmlns:qdt="urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2"
                xmlns:udt="urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2"
                xmlns:xbrldi="http://xbrl.org/2006/xbrldi" xmlns:xbrli="http://www.xbrl.org/2003/instance"
                xmlns:xdt="http://www.w3.org/2005/xpath-datatypes" xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:ext="urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2"
                xmlns:ds="http://www.w3.org/2000/09/xmldsig#"
                xmlns:xades="http://uri.etsi.org/01903/v1.3.2#"
                exclude-result-prefixes="cac cbc ccts clm54217 clm5639 clm66411 clmIANAMIMEMediaType fn link n1 qdt udt xbrldi xbrli xdt xlink xs xsd xsi ext ds xades">
  <xsl:decimal-format name="european" decimal-separator="," grouping-separator="." NaN=""/>
  <xsl:output version="4.0" method="html" indent="no" encoding="UTF-8" doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN" doctype-system="http://www.w3.org/TR/html4/loose.dtd"/>
  <xsl:param name="SV_OutputFormat" select="'HTML'"/>
  <xsl:variable name="XML" select="/"/>
  <xsl:template match="/">
    <html>
      <head></head>
      <body>
        <table border="0" cellpadding="0" cellspacing="0" style="border-color: blue;" width="800">
          <tbody>
            <tr>
              <td style="width: 205px;">
                &#160;
                <table align="left" border="0" cellpadding="0" cellspacing="0" style="font-size: 9px; border-top-color: rgb(0, 0, 0); border-bottom-color: rgb(0, 0, 0); border-top-width: 3px; border-bottom-width: 3px; border-top-style: solid; border-bottom-style: solid;" width="350">
                  <tbody>
                    <tr align="left">
                      <td align="left">
                        <span style="font-size: 12px;">
                          <span style="color: rgb(105, 105, 105);">
                            <xsl:for-each select="//n1:Invoice">
                              <xsl:for-each select="cac:AccountingSupplierParty">
                                <xsl:for-each select="cac:Party">
                                  <xsl:if test="cac:PartyName">
                                    <xsl:value-of select="cac:PartyName/cbc:Name" />
                                    <br />
                                  </xsl:if>
                                  <xsl:for-each select="cac:Person">
                                    <xsl:for-each select="cbc:Title">
                                      <xsl:apply-templates />
                                      <span>
                                        <xsl:text>&#160;</xsl:text>
                                      </span>
                                    </xsl:for-each>
                                    <xsl:for-each select="cbc:FirstName">
                                      <xsl:apply-templates />
                                      <span>
                                        <xsl:text>&#160;</xsl:text>
                                      </span>
                                    </xsl:for-each>
                                    <xsl:for-each select="cbc:MiddleName">
                                      <xsl:apply-templates />
                                      <span>
                                        <xsl:text>&#160;</xsl:text>
                                      </span>
                                    </xsl:for-each>
                                    <xsl:for-each select="cbc:FamilyName">
                                      <xsl:apply-templates />
                                      <span>
                                        <xsl:text>&#160;</xsl:text>
                                      </span>
                                    </xsl:for-each>
                                    <xsl:for-each select="cbc:NameSuffix">
                                      <xsl:apply-templates />
                                    </xsl:for-each>
                                  </xsl:for-each>
                                </xsl:for-each>
                              </xsl:for-each>
                            </xsl:for-each>
                          </span>
                        </span>
                      </td>
                    </tr>
                    <tr align="left">
                      <td align="left">
                        <span style="font-size: 12px;">
                          <span style="color: rgb(105, 105, 105);">
                            <span style="font-family: verdana,geneva,sans-serif;">
                              <xsl:for-each select="//n1:Invoice">
                                <xsl:for-each select="cac:AccountingSupplierParty">
                                  <xsl:for-each select="cac:Party">
                                    <xsl:for-each select="cac:PostalAddress">
                                      <xsl:for-each select="cbc:StreetName">
                                        <xsl:apply-templates />
                                        <span>
                                          <xsl:text>&#160;</xsl:text>
                                        </span>
                                      </xsl:for-each>
                                      <xsl:for-each select="cbc:BuildingName">
                                        <xsl:apply-templates />
                                      </xsl:for-each>
                                      <xsl:if test="cbc:BuildingNumber">
                                        <span>
                                          <xsl:text>No:</xsl:text>
                                        </span>
                                        <xsl:for-each select="cbc:BuildingNumber">
                                          <xsl:apply-templates />
                                        </xsl:for-each>
                                        <span>
                                          <xsl:text>&#160;</xsl:text>
                                        </span>
                                      </xsl:if>
                                      <br />
                                      <xsl:for-each select="cbc:PostalZone">
                                        <xsl:apply-templates />
                                        <span>
                                          <xsl:text>&#160;</xsl:text>
                                        </span>
                                      </xsl:for-each>
                                      <xsl:for-each select="cbc:CitySubdivisionName">
                                        <xsl:apply-templates />
                                      </xsl:for-each>
                                      <span>
                                        <xsl:text>/</xsl:text>
                                      </span>
                                      <xsl:for-each select="cbc:CityName">
                                        <xsl:apply-templates />
                                        <span>
                                          <xsl:text>&#160;</xsl:text>
                                        </span>
                                      </xsl:for-each>
                                    </xsl:for-each>
                                  </xsl:for-each>
                                </xsl:for-each>
                              </xsl:for-each>
                            </span>
                          </span>
                        </span>
                      </td>
                    </tr>
                    <tr align="left">
                      <td align="left">
                        <span style="font-family: verdana,geneva,sans-serif;">
                          <span style="font-size: 12px;">
                            <span style="color: rgb(105, 105, 105);">
                              Tel:&#160;
                              <xsl:value-of select="n1:Invoice/cac:AccountingSupplierParty/cac:Party/cac:Contact/cbc:Telephone" /> Faks:
                              <xsl:value-of select="n1:Invoice/cac:AccountingSupplierParty/cac:Party/cac:Contact/cbc:Telefax" />
                            </span>
                          </span>
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <td align="left">
                        <span style="font-family: verdana,geneva,sans-serif;">
                          <span style="font-size: 12px;">
                            <span style="color: rgb(105, 105, 105);">
                              Web Sitesi:&#160;
                              <xsl:value-of select="n1:Invoice/cac:AccountingSupplierParty/cac:Party/cbc:WebsiteURI" />
                            </span>
                          </span>
                        </span>
                      </td>
                    </tr>
                    <tr align="left">
                      <td align="left">
                        <span style="font-family: verdana,geneva,sans-serif;">
                          <span style="font-size: 12px;">
                            <span style="color: rgb(105, 105, 105);">
                              E-posta:
                              <xsl:value-of select="n1:Invoice/cac:AccountingSupplierParty/cac:Party/cac:Contact/cbc:ElectronicMail" />
                            </span>
                          </span>
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <td align="left">
                        <span style="font-family: verdana,geneva,sans-serif;">
                          <span style="font-size: 12px;">
                            <span style="color: rgb(105, 105, 105);">
                              Vergi Dairesi:
                              <xsl:value-of select="n1:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PartyTaxScheme/cac:TaxScheme/cbc:Name" />&#160;
                            </span>
                          </span>
                        </span>
                      </td>
                    </tr>
                    <xsl:for-each select="//n1:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PartyIdentification">
                      <tr align="left">
                        <td>
                          <span style="font-family: verdana,geneva,sans-serif;font-size: 12px;color: rgb(105, 105, 105);">
                            <xsl:value-of select="cbc:ID/@schemeID"/>
                            <xsl:text>: </xsl:text>
                            <xsl:value-of select="cbc:ID"/>
                          </span>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </tbody>
                </table>
              </td>
              <td style="width: 165px; text-align: center;">
                <span style="font-size: 10px;">
                  <span style="color: rgb(105, 105, 105);">
                    <span style="font-family: verdana,geneva,sans-serif;">
                      <img align="middle" alt="GIB Logo" src="data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABkAAD/4QMZaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjYtYzEzMiA3OS4xNTkyODQsIDIwMTYvMDQvMTktMTM6MTM6NDAgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjZDNDJBNEI2QjVCRDExRThCQjM0REIwQkZGMEQxODY0IiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjZDNDJBNEI1QjVCRDExRThCQjM0REIwQkZGMEQxODY0IiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzQgV2luZG93cyI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSIzREVENkU1N0FDREVDNEJBNzkxNUM2M0NCN0RENzM0NyIgc3RSZWY6ZG9jdW1lbnRJRD0iM0RFRDZFNTdBQ0RFQzRCQTc5MTVDNjNDQjdERDczNDciLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7/7gAOQWRvYmUAZMAAAAAB/9sAhAABAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAgICAgICAgICAgIDAwMDAwMDAwMDAQEBAQEBAQIBAQICAgECAgMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwP/wAARCABmAGkDAREAAhEBAxEB/8QAtwAAAgMAAQUBAAAAAAAAAAAACAkABwoGAQIEBQsDAQABBAIDAQAAAAAAAAAAAAAGAAQFBwgJAQIDChAAAAYBAwMCAwUHAwQDAAAAAQIDBAUGBwARCCESExQJMSIVQVEyIxbwYXGBoRcKkbHB0VIzJEI0JxEAAgECBAIHBAcGBAQHAAAAAQIDEQQAIRIFMQZBUWEiMhMHcYEUCJGhscFCIxXw0VJiMwlyU3Mk4YKiFtJDg9NUJRf/2gAMAwEAAhEDEQA/AN/GlhYmlhYmlhYprMnILDXH6BJYsvZDrlKZOD+CLZyb0hpywPTbAlGVqutgXnLFKLGMAEbsm66xhH8O3XUjtu1bhu0/kWMZdqVJqAB7SSAPpz6MNbi8t7ZdUzU9x+4HA4o8kORGWa9LT+DePCmO6yyKdRtkLl3KymIGT1gkQyzmaj8bRUJZMgpxaDb80DzKUEYwFEDFKUO/Up+k7VY0ivrlpL6tDDFHqoa+EyaqV7AD1YbC4u7gFoUCRUyYkfTpp9uB4XuHJG15XisNWf3DMc48v1hWZNoiAwjxNeSVZdyEtT5HIUbVm+XMqSF7obu5vaBEOJpCMI7byq0UT1hGfpzEOM2YNtG1ncYtnY28dSztcnXQOIy5irrCCRhHr0aNZ0atWWGzC4acQPcjWeHcyrStK8K0FaVrTOlMAhycznyH475S5B48ccs+TFun8Y4wr9uxikwDAMAXLN/ev8TtLLTE45fBsjFViNrTHM8PKKujLuVPpyL9UUCpMjrGK9i2PZd6sbO9+GjiinuCkpLFhDHSUrJ0FyxgkULQd7QNRLgCPupJ7aSSLWWZUquXiPdqOoU1Ka55VNKDBeROQOVddttBx7Ec9q/KZCvdLirdCVjkdxCVSo8uuvQ3eSJesQOZsXp4qrMpLRFSinr9fsO4XQZtV1ToGMgskmNGz2trWa9/SxJaxSFWZLo60GsRqzQhtSguVUErp1MBU1FX2q4jdUE5EjCoBTI5EkBiKEgAmnGg7McrxB7hXJFxUavdMr8RZ/ItFtGPqXlJnkPig7l7+6aUTISEm6p07N4juUPUrwU8ywiFXXpIVaedpIGIfxGTUTOdpuPKu3W7vbx3ax7kk0kRhZSQJI2AdPNqF7tfERQnu1qDjm33G7Kh5Iy0BUNqqB3TmDQD9uOGB4L5RYF5KRTqSw3kmvW5zFH8FirJF1Iu71F6XYqkdcKRLpMbVVn6Rx7RSetEDCPw3DqItuWz7ltThL2Mx1FQahgfeCRXs48MsxiTt723uFDRNU+/7wMX/qNw6xNLCxNLCxNLCxNLCx0H/YP266WFhdeU+WVtyLklbjjxHcVBe7lmVqneM73h0iOMcXT6UU9mpGp1eHFyye5nzJHV5gvIfpyMWIixaoHWkXTVMuxjOy5fgsbH9Y34P5QXUkKhiZFqFJaRCfKAZ08YFSQtQTiHnvJLiQW1kQGJzbLLp8LDPgeHtwAY3zGuPWrm54PreUs88nJeWjJcnKnLVLYZDtmUMVQl2PRM2XHjTBsnFkNCRuGbOo2aT9dY16OexUWqeRTiZYiBBWKfgbu90W27yQwbLGrBbcPp8iVo/MhjnkKJQTqC0beYUZqIXjOas0MUA1QBnuSRV6eJQaMyrU+AnvClQMwG6T6s+Icoc0OH9HcWyfmsF8h1KraWjO0hVLDXooXVgjLBjm1Gs+JJqZZTDjHeVqY6Uepwc2ZrMRZHjNydKPlmJU24rBuNhyzzHKIES72bzFqmtWailZF0TBSolicBfMjBR9LKC8TnVJNDLe2S6iY7mhzoQMwVNVr4WGek5ioOTDK4obhjhyFzND55YJ2COv7CEpsVLhBygQkBaHlCqTukViWnWrRuM24COrT0W304JEIhyVBso5auFmrdVOMk5k3KXbG2lyjWZZyuoamQSOHYKT3RVhXVp1irBWAZgXAsoVmFwKiSg4ZA0FBXp4dFacMshjzsm8LuN+Xrn/cK+Y+JMXAz6wSSk0E5YW6yj6zYlfYQklwboygM2/8A+dPzNkiIppJpOk0ngF9WkRcvFjzNvW3Wxs7SbTbFVXTpU5LMJwMxX+qAanMiq+EkYUtlbTOJJFq4JNaniVKH/pNPr44q+1+3dhGXkbnYqhK3jG9st1TyBWUp+vSzKRWr7zI2J4TDExaYn9RxsrIFsDClVxmRiY7oUGiyZjppgCyxVHtvzhukSRQ3KxT28UkbaWBAYRytMEbSVGkuzaqCpB45Cnk+3wsWZCyOwIqOiqhaitc6AUwM0zTc14bzfW8Z8dyPrFk3IGUrxkG6P31fyPCYMxlg+HwvH4OwUxus8oRlWrfCY1gGMa9SrUW99fPWtqsdII9I7t8zm4bnbNy2t73eSsdjBbpGgDRm4lnaYzzlFzZGkYuDK66Y4SAdZCI7VkmhnEVuCZXck1B0KoXSteg0FO6DVmqchUjjlKjcXc+8hT673FVywVl+jQknJYo5hY0lH9NzFLtavYGdTcy91QjaPCU+PaWyUclkWdTcy9yYLMCroyKce/arM0u94t1yrZosVwk9lMwE9vRdI1KW0JIWaRgo7rvoiGqhQSIwc+axx7g7dwpKtdD51yNKlaBRXoFWy46SCBdOL+Y17wjkNvx25oy1QmXQ2tpjuicrqAdmjji7297GsZmIoOZaswcP1MA5kkYWWZroMX6oRU0Dkh2C/cYEdRN9y/DuNou6bCrpqTW1uVbuKCU1JJIR5oZkbwVoarxAGO8F7LayC1vaNnQPUZmgNCqjKlRx48cNIAQMACA7gPUBDfYQ+z+ICH+ugsmhNeIxNY7tc4WJpYWOm/XbSp04WAa5EXG9ZVsT/jlh+0KUGOaRqcvyLzo2cN2quIaEugZ6EBVJB2PoEcm29i2UBFVYDJQ0cKj5UO4G5Dlmz29ttcUe9X6+ZMx/28IrWQhtLMStdOg5qGXvHgOnEVdNLdObWE6UHjbq6RkaVr2HLC+5q3wsvIYzwXxQM1uHGWbZ3TGuPYDjO8MllWlZ2gkcf3qIz7l/JttgUT4yewzhxKO0zOyvGc5FeoduTTakszikyqG1eKObc9+URb2nlyyNcgCKSA+ZEYIooyPOL0QMRR1koB5XlvKWpapWC1Oq3NVGjxBsm1Mx8NM6DgR/FqC4a/gjj/D4nhVX88SAsOSrHZH+RbrYYmIcxdYJlCz12JgshWjHFVk5KcLjdrf3UYeRlWkeumk9lH7x0oHe5UDQFu27ybhKFi1pZRoI41JBfylYtGsjgL5pjBCqzCqoqqMlGJa3t1hWrUMhOokcNRFCVBJ014kDiSTxJwRJjFIUxzCBSlARMY3QAAOoiI/YABqGHbhyATkMzjOh7nfuPPTOZfBeC7Q8g0IZx47zkKDknMa+I8bggv8ARq9KMFW7psdsoBiuVSH+ICQPt2qDnnnRrQ/p21OVkU95x0cMgGUgjtB/47QPlE+U223iKLnv1EtxLaTLWC2Y5FalTIzw3IYN/I6ZDt8OeWZ5f52TeLgnygzaBUznD8vKt2AoABhD8JZrp8Nvu1VLc47+h7t24PsX/wAONldp8sXo0YVL8u21SP8ANn6P/XxU07zr5IKPm0JWuQ2fpiYfrps2LZrlK9rLuXK5wTSSSbpzYnUOY5gAOn26UPNfNFy6xQ3T6iacE6faowx3b0G+Xzlyyk3HdtitUtolJbv3bUp/gkY/QDh9vtmYG5Vz9ormVORPIbkQ9FJRGUhsdI5Xui0IQiyCgpFtiDuVWI/MIKFHwB8hRD5u7VzcpbXvwVLzd7lmPEIQvt4o33Y1R/Mj6l+kUpn5c9M9igt4xVGnWW4JND/l3NuCOB4P78aMMg46ta2JcutuOi9HxDm/IUDJuYnIbinRrhBS9LNFEmNjtabJqkrNSZPIcib12m/FqqcFzt3ZCGbLXHt9/b/H2rbyJbna4nAaPWQfLBzVSfCOwFa8Ayk6hr4niYxyfC6UmYGhp09Z/Y+w8MJlwfx0xbS7dkeD5QMnWO8QytUk8ZSOLs0RFZtmcc0ucszUJb7Vk3OeTcZ3iwsbfjLGuYSS5anfJqtwbxm8lVzKS6SBUklrM3jeLu9tIW2fTcX+sSeZEWW3hEStGsUEMsSlJZIdBlhjlkUhFpHqBKwUFrGjsLmqw0pRs3YsQxZmVjVQ1dLFQcznTicfHa/5G4r5fh+EvIKxS91ptoZyL7h3n+xLCvKXOswjcF3mC8oy6opldZlo0ckZZm9EpAsEOQFdvVIOAMI7pa229WDcwbaAs8dPiYxWiFm0q4LEatfEhQafizqS9tXktJlsZjVGroPXQVIy6u0+zDPdtvhoNz92JboxOv36WOKHrxTGfssJYaxhPXBJr9Usaws65RK8Uf8A2LTkCyuk4am1tonsYyisrOu0SD2gJgT7jbDttqT2jb/1K9WBiBAAWcnhpXM9IOfDI9PVhtdz/Dwlx4zkPbUDtwoPM7fK+LpzFGI5CauOB5q62mwM8n8lsnMofIHC/PqeV6n3Wen5RpERLCZja7FlB9H1SutZd9TZFvClVWYTC+/0x3Ym1Dbr6K53MJFdhEVktoS0d7A0TZSQyFf6axB5XKJOoOlXiWgkSIlE0Hl24JiqSC7UMTBhwYA8SxCipQ8SGPhLJ+K/FtlghCzXSzOWM/mLJKqr25zLVpV146rMX1hn7qpjCiWKJx/QLNM4yrlxt8s5ijWBN7MESdgks5OmiiRMK37fW3Ux2sAKbbAKItXq5CqnnSK0kirK6Igfy9KVWoUEkmVtLQW+qRzWd+JyyFSdIIVSVBJpqqc+OC80O4eDCmvdM5knwFjlPGNJkSoZMyKycJeoRUTFeuVg3e3eyxiGIYSOHQgZFubpsfcwD8ugLnnmP9HsPhrc0vJRQdgyqc1I6eGMzvk79CT6o85DmDeIw3LG3SKWBP8AUlz0r3ZopAAQDqAZa5EHOmK3LuRVnay8OxcnOUVFDO1xP3KLrKj3HUOcdzHOcwiIiI9RHWM13cM0lK59P1dmPoE5U2CGyt0OmiKoCipNAAOnUa8OnAf2KZdeVGMjE13krILJtWrRukZdy5dLn8aaSSae51FFDjsAAHx14W1u08qQJ4mNB+1cSHNW/wBrsG3SXly2m3jQljQnICvQrHgOgHGlT2sPa+j6dHlzrnVi1/UyccacVCUKb0NJh0SerV3Kr+T9S8CfcooIfl/hAQ66yB5P5Sg2iAX17/XpU8e7w/hcg/RjRh8zXzIbx6ncxHk7lZv/AKsyGNRRD5rE0p+baxOmf89O3BjWz3ocH8ecpwNLh8RvZbF4yhoeVv6Ms3bvfGgqLUZiPizs1PUR5TmA4gKyZhT3EAEdgHtJ6l2druS2SRarQtQvqYdnh8sn68Se0fIJzXvnIEnNF5f+Tvwh1rbiGFw1aMAZhfKgyPHRl1VxozpVur96rEHbKu9RkIOwRbKWjHaBgOmuyfoEcNlSmD/uTUD/AF1a0MyTxLNHmjCo9hxrq3XbrvaNxm2y+Gm6gkKMKg0Ycc1JB9xI7cL/AOeXEaiZMjZbPB6vWrLZabXI1xc6lfLlM0rFt/q1FSt54dxlKXgKndbWWoY6hsg2V7IRcC3YubbHulYiRWcsDg0Mdcpcx3dhIu1CSSOCRzoeNFeWN5NFfKDPGmuQxRKryFhCwEsYWQasDe4WUcymegLACoJIVgK01UBNAGaoFNQOlqjLFNUnFeW+YnFS0Uzk1c/0pyztMLSOQGPm7C1UdwGBrxGM/LjiyY9pEBFRF4oMNVbxFLx8q1nDSjpy4I9aqSTryLopP76823YN/S42CPXsUUjw6yrgy0JqZJCWR20srKYwq6aflqDn4JDLe2ZS9IF0wDUqO77AKECoIzqa9JwbnCbkQ+5L8f6zebTFp1rKcBITuN82UvYCL0zL+PpVzWbvCrIB8yDVxIsfXsBH/wAsa8bqhuU4aFuY9qj2jdpLeA6rQ6WQ9YKg04k5EkZmpAB6cPNuuvirZWbKQZH3ZdQ40wWmoPD7CtOVOa6rHcs8R1m3hKvaRx4qsdnOwwtejFZ6ftWWsq3aOwFxxokLApmIaSsVjtlnkFI0m5Sg5bgc5kyl8pDnZdrnk2KURIPiL2oR2OlUjgDSTOx6ECK5b/DlU8Ia5nX45S3gh4gcSZAAoHaWIA49tMHKhlSMsGVk8RxrCMcykNTY293+LsTuTh7JXIiwrqpUCTgYVetPYG7R0jNwUm0kHDWWS+jPGSRTFVOsAEFzYvFYfqDlhG0hSMqAVYqPzAzagyEKyFQUOsMeFM5PzQ0vkilQtTXiK8KClDmDXPKmLm1HY9sehs9hjanXZqzTK5WsVAxjyVkHB9+1FoyQOuuce0pjfKmQR+A68ppY4I2nk8CAk+zEhtW23O77lBtdmNV1cSqijIVZjQZkgD2kgdZxgz5t8lZjM2UMgZUk3ah/rko6jaw3Oc4kYVdk4XRh2qJDAUUwFtsocAAA8ihh1ipzZvcu7bk92fATRR1AUHHSCfeK4+kb5cfSq09O+Q9v5atV0yrGHmNT3pGIdmI82QA50oracqgCtMKYnpYyaTp+5OInMBj9xuoiOw/fvoJHeI7cZVTOtlbgdAH2fThm/tEcNls65IVzZcoVSQgK9IGjaWzdpFO0eS4ABnUodM+4KFj0z7JdNgOO/wBmrk9O+XTLJ+pTr3QRpz+ng32jGpf55PXN7RP+wdnlo7qTOdPAVGkd+3NaiuaSZVzxqL9xtw548e31kN1XyKM3EqWErUo6bE2WTjZx+kzf7mT+YpVEFBKI/Zvqy+d7h9v5alkg40C17CR119mMHPk+2S05x+YDbLPcQHiBllUZjvJE7Ke6ycDQ5mlejGEzPmQFbi/iYyLFRycSJs2iCZDCos6dLJlApSiACJjG2ANYxRNJeXSBRVy33+7H0C75Jb8sbBM9wdKrCSeJ8K8ctfUeGPoVe2OWyxvFnFNctSi6ktB0mEZugXEfIRQjRIfCbcR6oFMBB+4Q1lxy+kkO2QxSZHR2fdj5l/WG8s9x5+3G+s/6UtwxB73RRfxAHOnUMMXEpTFEpgAxTAJTFMACBgENhAQHoICGpwZcMVbhLdLxZTeE3JpS3HpecpynSFrHHEbekIvEOMeN2K4zP1vpxWS7lL9Rt8t5xyROSqUBEy80VlKEVXjwcuytjoLuwsq43C45o2L4cS2iXKp5hQmeW5mNuj1AOkwwRIvmOiakIDFV1AquIVIksbrXpkMZOmvcVF1kcc9TMTQE0PCppmcXFj4n9gvc9y9j5EAZUPmniCJzzXGRNko9tmjDBozH+TisG5Pywd22lScLJOxApRM4ZKKmEx1h1FXbfqfJ1tIo/M293VyTxErilB2dwdJ4nLpUNbbdpEHgmAPvVa/v6sND0FYmsIDs2RsNPuTnNH+/0Ra31Gy1yOwpxnjrRTf1YSfxa54+cdJ/kTBX2GdUSOk7izmYHJ8K0PHuY8qa7CSfouxOVNBQDW3a2e6nYdvm2p41ubS2uJQr6NMqzTxQvERIQhVknbWrEhkDLTPAyskJupfiFYrKyCorVSqswbLMEFBQjgaHDHuIFbwvIK3LKeOuQGSeTllk2cFQ5fI2VJmIk5+v1yAVk7HCUWNZV+i46iIuPbObSu6WUNHHkXqqpBeOVzIpAkF8xT7ioisLuzgsYFLOI4lYKzNRWkJeSRiToAA1aFodCrU1mLNIe9LHI0rGgqxFQBUgZBR09VT0k0wb2hjD7CwPdvy6ti3iFa42Pc+nl8jv2FKZ9pxIqLR84TWlzJG3ASmJHInDcPh3aCufdyNhsEgXxy0X3VFeg9H24yz+TLklOcfWuxkuFraWKvMf8XluEGToeIJqK8OB6MLuXpoziSQjCHHxNkw7vtDuETb/AB/ntrFm6l1vkch99MfRhyxaCG2Eh8RA+wduB9Tr8nfLdWKDCpmWkrNNx0K2TTL3GFZ+5SQ7gAA3MCYKdw9PgGvXa7R729jt08TEftxHR24G/U7mmDljlm73iY0jt4S3AnOlAMkc5mg8JxsjwfyA4ve3DVKRh65Qlxf2SvUqvvpAavCNZBo2cSTBJc53Sp3iC4vnCvcqYBJ0KYvXWSS8wbJytGm13RYOi9Ac1qAa8GpXjxONEMnoX6s/MVc3HP8AsUcb2d1O1NclslCraSBWSFjpIpUxrXiK8cWFmn3XuB/JLE10xBeK5lBStW+GcxbwFq03RcNBUTN4HzU5pAQTdslRKomb7DF/lprf88cp7pYSWc7PokWnhk9vQB0jrwTcjfKB8x/pzzbZc17JBbJuVrLqU+fZN2EUeZ1zB4lTTjTCJuIeDOEWROYELSaJL5RyFZirS8pV21trMPH1uLbwzdd4s4kFmko5XcOG6JfyzeHtE4B0D46C+Utv5dk3cJZsZJeIykWgFTXM0OMmvmk5r9c7P03+L5pjWzsnYLKA1hMGJ0gKDGmsAE8QBWufDG3fDdFRo1WZx6JSlEECAPaGxR6BsABsGwB2/dq+7eNUjBHGn7v3Y03bnctdXTO3iqf24DFwa98R+FIc96rjNHM+Mckz87doG7xsMwr9RnsZ4MwRkq7VqdbTj2WauK1kHkHH2Sh4wt0uzfGK1OWPRk3SDYfTODqFSTLYHKV1ejbp7KJYntGYs6Sz3EUbrpA78dsySSopFT3iqk94UqcRG4RxGZJGLCQDIqqMwPYXBCk+yp6MftyugnGL8re1ZkBaw3C1S1W5Iu8OzNqvx4Y93moLPeLbPAPAs6tdiIKEJKL26LhjrJs2bZqUyHaRIpSlAPPZrmK62jfIgscYlSJlWPVoXy2diF1FmI4ULMT0kk48r1Cl5aMxZiC2ZpU5DjSgr7BhtG4fsA6A8TeoYVpwKga/I5k5/KzkUyf2mlc9L5Y6++et0l30Cjb8K4ziEn0YsYBO0UkoMXTYTF2MLdQ5N9jCAmfMDzx7Pt3lsRDLbsrAHxaXVsx0gEqR2jsxCbYEM82rNlK0y4VBH14aZoMxOY6Dv9n7f76WF0duM4Pv73QyCHHujlWMUqzy5WZZDu2KcGreJjUjmL/8thdG2+7rqm/Vm6IjtbToOth7tGXDtHTjaj/bQ2KK43XmHemH5kS2sYNTlr8+opqAzp/CfaMZHLm7M6n5FUR/Cqcob7CIAXcNugiHTb+Q6x/clmJHHG67bYxFZov7ccEZ7Y1D/uTzgoBXDUrplUzurQsVQonTIrHp9rUTBv07lDhtv032/dqwfTyz+I3lZWGahvrU9o4YwP8Ano5rfZPS+S0RqPdTxgmlaqk0bEU0N2Z1B9uHJ8q/bC5l5YzbdcnxeS6a3iLlLgvBQxU5UxouCIRNtGMVe5sZIDoNiABgKO2++jXfOQtx3XcHvjNpDUoNCmgAA/zB1dWMWPRz50+RPTfkiz5T/TNc0Wss3xFwKs8jOTp+BlAqWOQcgdFOGEWXz9ZUCRt9ZlZNhIuavKyEA4k2SQkQdOWK6rRdRDcpDdvlTMAbgA9NUxfQyWdy9oWqUNK0A6jwz+3G1/k7dbPm7l+y5iij8r4uLWF1M1MyBmQleFfCPZg0/Y3h39g5iWO2FKYxqzVlkU3AlEdlppwZmokU4dwAY7cTCP7g1ZvpZbh9wkmI8I/eOvtxrw/uJ8wNHylY7NXKWV6j/DoINdPSRw1DG+2DIYkWzA/4/CTu/jsAf021kMnhGNJLmrk9Zx7bXbHXCQ+cPGyNh85TOWn94lk6/myGvsfZohr7cWRuaY1JpN42wfiu2PyW/GLtZpj8V67iiKXizzMQ/dmcLSZUzuWe7RvaXKu9PPtS7ckS+faNGVY7nFY6iss8qApNnJRpnDeW6AAR1Cv3mgdwtwlwZix0SBqjyGlpVUU5r4clFNQPTxGQ57zKpMLRsccA6jXH8nJpSHuE8WbEwcTDd20knPrcjkuM4ANZNMsywatGHqBRavVFXTJomVsZQQSKAQW2yz3v6lcSBVK21DShGVQOGRJpmVyJq1M8d75Ar26irDUTnl1H9vow3/QVibwrnjssGOvcw564tcm9O1y1ROPXJaqIGH5XSKNef4ivKjYdxARZWCrs1HAbbgL5LfoIaNt6CS8n7VdA1kRrhX7PzO7/ANK9A9vbDWf5e5TxdiU+j/jhjtfu1RtjycYVizQc+7rEkaHsbaIlGkgvBy5Cd54uWSbKqHYP0yDuZFUCnKA9Q0DJIj10EGmCa626+so0lu4njjlBKE/iApWnsqPpGOUfw6fv/n+8NdtQpXDLGV7/ACDActsrcdHZu4Gq9NurUphEOwF0pWFUMX/t7jJqgP8AANUf6uEieyY/wy/bHjcF/bBKvtHNMP4/iLE/VdYy2WDcZOTEQ+Ky/wDpubbVGv3WoMbgrbK2WnVhm/sUxCUpy+uiypQE7KmNRSKbqOy8ukicd9h6CX4/Dpq3PSkK19IeJCj9vrONVf8AcXuJF5YsIwe6Z5K8P5ezG6e2Giaxjmcsr5NBNKv1eQkzLKFLsmVnHnWAw7lHt6k1fc7LDaPKclVSTjTdy/aTbrzDabfCNUs1wiKMh4iBStR9JI9ox84/PkyEg0np9UpE3dmn5SZXKUfwqSLpy9OXfoIgB1R1hzuMjXE5l6WJ+2vZj6nOSbJNs2G226LuxW8KIOnIKKcST0dJPtw3X/HdohnsvlS5rIAJX1kiItssJQMPhZMnSyxCiO+xfIcN9h1dnpXZlLOS4YcSPt9v3Y1E/wBw3mA3PNFptde7DHJX2kIf4R19ZxtJak8bZAgB0KmUP4dP+NXNjV0ak1x5OuOnCxXOSMt4zw/GRkzk+8VuhxEzMtq9GSlolGsQwdzTtBw5bRqbt4dJD1KzdmqcpRMG5UzD9mvGa4t7UB520gmnT92JfaNj3TfZng2qEzSohYgFRkOrURU9QFSegHAE8sHaOSea/ty4kjVUX7OBumVuTViKgcFE04fHOOHtVqL1QxREh2rmzZBIdI24h5m5dvs1YPL3l2/KG8XjLVpFgRDX+chuvodePu7A7cEeTdbaEZGNn1dmQ/dhne4/cP8AT/roFxNYVBz6UPx75A8Quc7dM6FUplwe8buQsikA+GMwpnh3GMYu3zB9wKlCUHJsbFvHZx6Jt1zqdRTApjrlZV3XaNw5ZC6ru4jVoc6UZCWbPIZ0XiQBn7DCbkTa3cF9/wCUrd/6qdZ6+AxxHjpjyJ4s8uJesW23YXoqOWnFzNixhEybgck5+hpmac3H6zdUAj2seErTJKQM0j3C7t66dA4dJoikkYiQ0zY267TuRs5GCliQgpm4FSSaVAp/MangMZVc7b3ceo3Iq7/YQNObUq104YKtuzlURVVtDSaxn+UjKgNWNSxw4z+nX/fcf36LBwxjfXrxnF/yIaS7cYuwRkpoj3I1i6TcHKLAUfymk/HMhbCYdhAAM8YgHUQ6jqn/AFctC+2295Sojdgf+bQB09nVjaJ/bI5jitOed55alOd3bRSKO2ETsTkp4A9LAZ8CcZDrITeQWULsX1BO8vUDfjDf/nWPRpWvHG8C1JMAXq/fhj3sd2tlVucbiEegHfcKi8YsxEwFAHEe5Rfh8TAAicpR+8eurT9LJ0h3RofxEfvxrM/uHbFNd8gW+5x+CCdieH4mjHSw7eAPDGx73C72FB4S5jmU1gRcvqf9CYnMYCiLqccNo9IC7iXc4g4HbYd/u1dfNd18Jy7PKDmUAHvI7DjVb8svL45i9b9j2+QVjFyzt7Eidv4l6R0GvVj59Gf34IsmbMDCAJoKKiUBEdvlECgI9R3ER+/WJ87apVIGYP7sfSvt6eTtrt06B9mNRv8Aj14/CI49sLAZM3ks1imJg5zAPVPv9MgIGH4lEiYgGsj/AE8t/K2SJz+KpJ9/t93140D/ADub8dz9XLuAHKAKn0qK/hH3+3GnoA2AAD7P3f8AGrIyrXGEWOgjsAjv9giH8g1xkw7Mc4RzyyyNyQtvKOpYMLjKnZawdY7lTl5Gt3DETy/4xkafLyo1yzHLldCGbRlQyLRCV5xJBHugXVEZY4CYWzYq2g7cbjc33dLSNQ9mzAU7tDUD8R7wIPUcZUchbFyBaenc2/XM7W3NixMwlAuC0dHIX8oMYZFdaCrLpFRUVrggOHnZnzljyp5eokIvQID6RxJwA8KIHau6xi96rI5jssSqQfTrsLJlFQjEFCbhtAATcDFOGrw5oij2bYdv5bHd3GJXe4GfFiGjB4qaKxHdY+EagDQDDuwd72+n3JzqSRu59Ybt6uI9mGj7D9/9NAWWJzLFbZixTTs54ryDh7IManL0rJVSnKbZGBw2MpGTrBZiss2U6Gbvmgqgs3WKJTorpkOQQMUBB3t17Pt15HewEiaNq5ZV6COB4gkHI5HhjwngS4iaKTwH9vtwjHGNcsVkRleNeZa3L5A5u8BY+NQxW3C2sqC45T4CRtEHK4jyF+rXxPGSOZOayzRsKaaoqpvmSyS3/wBzrIeoGwQXqw84bTDqtLipCaiCjLk9SzZ1cMa041AyKknnpPz3ebBLLyjfXYstsuP6kvlCbTRW0jQEZjqqEqGGnVqNQCMNJ4dZ+msy1mbibVYa9e7nSJaRh7vdsfQzuKxgnaTP1nTqiVV9KO1HtocUlg6bs3kkimVs4XIYRBFXuRKHbTePdQ6ZW1yrxalBx4AAUyGVRWvHtw/9QuWINhv1msoDbWEwGiMuXYBVUFmLMXGs1ajBaV00BBA4b7neBj8huGWYaUyag7n4+CNaqymAdx/rdbUJKNgT+Yo96hW5ybB1EB2+3bUZzjtn6tsM1sB3wNQ9oIPWOivTTFgfK56g/wD5v60bRv0jabRpjFJlXuyKy0/pyHMkCqrXPI4+eFJyiJSg2diZB6yUUauElQEpyKIHMRQhwNsJTkMUQEB+AhrEWRGjbQ/jGPp2s7+1ubeO7ib8mRQwyPAjtAP0gYsPidlxDCXLLCmTCPASjoy7xDaXMU+xRipJwRi8BQR32TKkt3D+4uiPlK//AE7fIJm/ip7a9HA9OMb/AJn+TYee/S7dNrhFZ/I1xmpyZSGJzeMHKuRNOzGxD3nMxQ7DhvjmJLIJla5ItVfeJOCKD41Y+JZFnCqfL+NNU/jH4h1H4Dq7/Uq+ROXUiU5yMOjoUqerqxqe+QLky6vvWue+ZavY2rAioGciSCvjHDT2/fjEpm+0x8xIrCxcEWRAhEExDuDcfw9Nw32MI/cG+2sdF/OnB/DUfdjePfk2G0NE+TCM1+j343e+zdj8KbxWxMzM29Ot+jop0uXsEgmVfInemOYPh3GIuXffqOssuUrX4baIUHAJ9uY6T0Y+aT5gt7be/Urc74NqRrgjhTwgD+FekHow5jfpv8P2/ftopNffiieGAb5n8i6tjKuR+Mo3NCeHcvZFcxsfRrWWlrZAjqq/cTMaziX94hkm67eMqNimHKEQd04MgXve/lnKcvcWG3XcIbdVtxL5V1J4G0lqUIrlSmYNM/aOGLT9NeT77eLmTf59s/UuXbIHzozOLepZW00bUHJQjXRQQaaSQGwDVwY3Pj/jpHj9iiBr1a5587Zd2pZ4Oh2202bHON2g+rj8k8jIyJmjphUq7GxCyj9RJEjb1Uyuk2KqooUhtHPp5y/bwLJzPu0YXbLahk7x7ztlHQK1RRmDZClaA5V0inqlzlLzDuceyWFwZ9uiqIWKBCoYL5gOpFdqU06nNSFrxNS3DBOGadx6w/j3CtCbGbVPHdaY16MFUpfVPlEAMvJzMicvRaVnpVdd67U+KrlwoceptR+7bjcbvuMu5XJ/NkIrw4ABVGQAyUAVoK0qcCFtbpbQLBH4FH2mp6+k4trUfj3xNL7MLAIc0+IcnnxtS8tYYtKOKOW+CnD2bwZlUUTqRyhnpCJz2NsiNGxfU2HGF4YlM1fs+4DIHOVwl85BIoUct78u2M9juKebsdzQTR1pWldLBlBcaSdVFIr9GI2/sWuNM8B03cZqp49XQTTo6cC/xCtWLuRGcHM5kAuQOPnMPj5CBXsg8TSWRGvUuqndyK0hZ8iUWAiW7ZrkSgZUfPkVTyx1XqRyJoFEqC+51WXMfJse03MW82rGbaJKmGXw1yGqqaiwpmKsADxFKgYMdu9Sd0uOXX5QcKok0iYEBmkKsGQltHdoQKBWGVAcq1LjHvLOvZdy1lSlRLCP/s/jtRtTHmV5CTj20DZMmvkWCr2hwqbx02dvn0Q1eHK68bdZEFg8flBQDJ6DbfcUu7p49I+FSlHr4iRUilARQ4L955BuOXNi2/cmmZ+YbkuzWwQVhVGoraw7KwZaNwFCSpqQcIh5Few1iLJF8suT8bZRuDeu3+Zf2hmwriNaka81CXeKu1koV4kgfzR/mUMKY95w2Hbfpqvb3022u5u3vA2UjV4Mew5+aPsGMyeVfnx9ROWdhteXLqEvJaxBNWq2WoGY7v6e1Mj/ABMes4HEv+PXCC5QU/ujk0gpHKYpixlf3KYpgEDFN6cAAxTBuGmy+mW3qKq2Y6aN/wC7iduP7gPOlzGY5baqkZ/mQZ/Rtww0HOvtcuOS2A8I43v2ZMmIDg2tKQEUsyZQR1bIYyLdu3lZwjhmoASLZk1KiUUhKUSfEBHroj3jk2HeLOC2uXyhBpkemnU69XWcUl6XfNPvHpTzNum/cv2lLjdTHrHmx5aNf+ZaTA18wnJUpTp6Fguv8eqAcSCapsnZKWSSdpq9ikbXwBUpFCnMU4+m3ADEDqO3TfUBF6ZbZCRKGrQ9T55/6uLlv/n8533G2eCa3prUiuu3yr7NuGNSXFXFwYixnA1dfuSQgIaNikllu1MRbRbFJmmor2lIQoiRABN9m+rSs4BawJEPCopX6us417cy7pJvW7y3x/qSuzU7WNepfsGKmyNz8xjEZmmuLNfdu4fPD1oszpg2qKUQq0vMStdZS1PcRjn1SATrGxvZAWzbsURKdVi77zpkRAx4yffrX4xtrhb/AH9KcDkSuoZ00nI9dMWBs/pBzC/K8HqFuUQ/7QJ1MweOpVZjC4KiUSr3wQSELdIHTgJ3Frs2Am1AyTysrEZm/wBwS1P7fX+MGHKUWOHJrmvWorJyepZHf06T/RkzU6jKJqPVZVwkEbENiidJUypTqiT8i8lXu+L+q7+/lWttVpJ6A+WCDp7iONZagGQOgGrdAw09U/Ubl7bp7nlT0srFyrdpEGj/ADGErIAxIa6jM0emSte8oemXd4nxxE4uWfF8jcc9Z+sLHIXK7M6LJTINoYpiNax9Wm+ziHwziwi6ZXTGg1dcxjnVU/8AZlHxjuVhAvgRRIuZeYItwEe2bYnk7FbEiNKljmalizAPRjmAxJFfcKT26yaCtzcHVdyZseHuoDT6Bg59CuJTE0sLE0sLE0qdOFgMOWPCTGXKVOuW1eTsGKs8Y3UO+xHyExw6+kZIoEgALCVqDsglbWWpPFFzethpEq7F0Uw/KRTZQpHsXMl3soe2AEu2Tf1YjQBxQimrSWXj+Hj01xHXu3RXbLL4Z08LZmnuqAffhQ/I1jlinV1jj33EMUT7mtwc5OTtb59cRsfI22nKy0/WXdMkLfyBwaELKuaVYzQDxMAkyNnzJu8SKZqsgKaYnc7nyTy9zlCrcsSiHdCtRasHYrQd6ksjqr1C6uJpXiOGDvkP1X5j9OtwM94nxO2yUEg1ImvTXRQrG7JpLdAowyYEcCLwRkbKpJJ3N8Tch4Qz9w9x/iOyR+N8d4stEFYLUs8qNMrTLHVVnIt6CVrr19krOq+PKHVdC3FumQiyCbpTvCvr3YOZuW79oLmMi0jFAn5fVQUcFq5940JHEVrliyH5m9L+deXUa+Uwc5zzFprpmuWy83UT5SqsNBF+WoABrTIDPBGOeYORsc27CmMMw8en43bJcVWHlinadKJoUauyFpsbKATg4qSs7eOJYp2tpvQdyzFFcrtBqQTN03QiUotTulxDLFBNDR3rUhuHTwpmejjxwxg9O9k3fab3e9m3Mm2tWUKrQHvlm0kljICi5agShJUioBqMdkR7hNSuCLUarj22RJmXI6k4Fn0bBGMJHyEt6ksRvYYxzB2AzFOMVSjAWK5Ms4MkkoQx2xu8ADm23uK5r5YJAdQewNWh4dnD68cbt6QblsZj+NnUrNbySIQozMWjUuUhNPzF7xAP8uPUcv8APHLDGOeMc0XA2HVb3S5itx9sm3zWl2ObCUcRl6gI6yUUtqYtlKxTpuaqD5yrGuZVZq1SWRMqqoKZOw/Xc7ndIbyNLGPWhFTmo6sswfu+nD3095b9Pt25Zu9w5rvvhdxjbShMcz0qG7wEbqp6BQhj0kUxWucWubnUtndnyzzzizCHEGx1WXhq4ynbnB1a0oSKUrX5+mT0dJVdCu2UyRTt3UfKMFZgx3gAUiaaqapgFxact8y8xXktgqs9jMUCKAmXAmrBlI7wz1sB0cMe0PPXppyPtWz7ry9aludbQ3HxE3mXFHDlkQmOZHhFInIHlKc6MxDChpjAFxyxeqpTqXwaxWa22auUlbHMr7iPISmzNNoqdKPYH0ulFYuhJlBW55caQblyQWCCPhhfI3KCqyfzdll2HJuycoxKea5vM3KJai1AYFwaFQZI3dVpXrzApqFSBUfO3qDufPG6XE21R/D7VOymlVcAhAHNWjjY6m1EigFTwPHDNeMXDSj8eH9iyJMWGw5k5DZAQbkyZnzISpHdwsZUDGOlCQDFIfpFEpTFQ+zeIi00UNilMuZdUPKLHf8Ame73tI7RFEW0QZQwih0AgA9/SrNUiverTgMCljt0NnWXxXDeJs8/dUge7BjaGgAMhiRxNLCxNLCxNLCxNLCxNLCx+K/g9Ot6rw+l8SnqPP2eDwdg+XzeT5PF49+7fpt8dLCwh7kzE+ytN5XcMJW0QtQ5GLLmB7OcLG2WneYWsl5zdhrShxXr9lWNMA4/8f1poo4327Om2rQ2B/UePbq7apfba8H8gdX8ZElPqwNXS7E0/fbTcV6pDnl1ZY5/ReMOfV49s/49+5pzTgYVQoGZQnKTjDKZAVQSETCgmc+VMeYkyJsUNwN6t6ocS7biHQdNJd42eI03XZ4JZOkx3YXPpyhFPrx2jtpSf9tckHLjH7KeL3YshvgX3MQRBuT3BsBnYFeCmMgThYw+qKPu05RcKNE8xkRLN7fMYu/d39Nea77yHkRsTaq//Mn92JE2nMHTd9//AEo8cUt3GXkYRms9zx7lnLWWiSFOZeJ4zcYTUJdYgFMKyZVKNR8v3jtMnuBfSukzgO3aO+2vVN52KVqbVs0EUnW92GFej+sKYYG2kUf7i5LL/p06v4Tir8JQns6QGVGUdZbgtd8/pPA+lz3OxpmZle3Mv8DDUG/KGtVWBJNd4CJ/ojQjrr83TbT/AHh/UiTbGN0nl7XpGSG3PdqKU0EyU4cOjjljpZrsauBG2qbrIkH25Yekz9J6Vt6H0/ofAl6P0nj9L6bxl8Hp/D+V4PHt29vy9u22qrOqve8WCMUplwx5OuMc4mlhYmlhYmlhY//Z" style="width: 70px;" />
                      <br />
                      <br />
                      <xsl:choose>
                        <xsl:when test="//n1:Invoice/cbc:ProfileID = 'EARSIVFATURA'">
                          <font align="center">E-Arşiv Fatura</font>
                        </xsl:when>
                        <xsl:otherwise>
                          <font align="center">E-Fatura</font>
                        </xsl:otherwise>
                      </xsl:choose>
                    </span>
                  </span>
                </span>
              </td>
              <td style="width: 277px; text-align: right;">
                <img alt="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQgAAABNCAYAAABaFPqUAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAACc7SURBVHhe7Z1/aFzXlcdvFhdG4MAIUpAggU5JoBMaiEwDK7P5ow5ZSEwKsUmhNim0SgOt3cLGbmHj7P7RTbrQtbvQ1l1I4xYa7EKLHGixAy1x/sgiLbhIBRerkJApxCBBAjMQgwQNeL+f8+55uu/pzViyNI5iv6+5fu/dd3+ce+455577443uuCaEGjXAR/G6I14HYiVeHY14zVB+C1ZT8LYqf7GMGh8/agNRYxNIlXytcvd/W2U+QG0gthv+IV5r1Mg8CPcirot+Sp6h/HZtamLSUGM7ovYgamwCqWLXHsStiNpA1LhxuLfB1dYtevyne1f0KoUfbFRqbC/UBqJGgvLIPkCBU+Pg2CEDscPLGGQkasPwSUFtIG5b9HPzy6hQ5tQo+D0ehBmHQUamNgyfNNQG4rbG9YzEOhU6n2KslrghU5Dkr7G9UBuIGqtIPQPQR2mrzErZd8BAuJG4rrGoDcS2Rb3NeRuirMxbjQ0ZB1Abh22L2oO4TWBGQSN1Y4AymjJveDTPTM1KYgryu4/0jgDY2bByG2FlRakbWaoVvW/kux41thtqA3FLIfULikqXvgG9q/oPg6FkUVdLOSJSg7Hi25gKKL5npHSroKkrN4TlLH1qIMBYS3G6j8+8ray3xrZAbSBuKURlNKxVuyUZhebO/grZM2Ow6gsU0rmi+xUDQHqv84NoPJY6Ibx3Wc9LetUNPRmMzuJiWFjqhX3fPBYaD+9RHaNKSOnyJiwTdfajqsbHidpA3HJA5dahbCQzZVdgNLcsmbquQs8YgY/wBnTfkxFY0T3XD7t27fUW5Sj0wrKee3/rhPGdjdCUkVm5qnh5EDaVaI6HbqMZRu7bHca+MqX6xrNyFec11uZhe6I2ELcT0hHfjYPFSel92kDcVUJmAFa6XTMAGIbuFXkFGAalGyGXris9PQsYgtEdI1J0jEnmPTQ4F7ETIzASOqqn0d4TWl87optVAwGotTYQ2xO1gbhd4MYBIyDltXu8gg8WQ3hfQaN9T/eM+stXl5VExgFDIcNgyouCy4iwwEgZGAQzEgojim/sHA1d5TPj8ZE8B71hOhNkNHrK0lFotCdD+2tH5VHEdYh8DaPGdkVtIG4xSA+rR2MMA+HKQuhdmpM3sGCGYeUq3oFGexQ7T2c5DOww2M4H0xCuAkaii6HASGhKQZ6OimmMTYR2+/7Q+MyYPJBOWLl4IXT+qnoao4prh96nxsPEU5pi3DuhUhIqY7k1th9qA3ELAeNAcNXLVVAjevioGzq/PhN6Vy6H7vtLGuU1wisBy4QjCg2MgqYV+AX5tiMeg+Jt8VJGYFnX8bGxsCKvQE9h7L77Q3hIyk76jqYVO9sh3Klpw05RwfTiPRmJP8+FxU4nK6MpA/ElGYgH96hi5bFyFaiOa20oth1qA3ELQWYgMHnAE0DnLGAcPuiEpddOhsVLMwz/odlshtHR0bCwcDmMjbEewHRiOYzuVM6r5NM/KfCKQvOh3SFMKFycC0tvd8Lldzrh/sndYezJ/Sp9OfRmZ0OTmial9DubofPLk6Fz8WwYlxfRfv5EWHn9XFh867wMkrwOpWs/JgNBwKh8JKOyoym6sz0MQo3thfok5S0EFGx1AzEqHIuNC3Ma4S+HliLachtQ1s7bMg53t0LrvnZoTUyGtpS+q1G8cfdYWJQRabTbofmtoyG05BUsaJogQzH25YNh18O7wyJrFsLK/HyYm50Js7MXQiDYouZiGFcdNnVZWgwNeQ0scjblHYwyfVkhr/s6rGBw9f9rbDfUBuIWAtOEJkHa1kDjOAy1JKX983xY5vqBjAXKr3/jYzIOUngU3zyEh/eE9pNTYVF6vSIjEZ7YG8L8TOicPxsWZATmXz6hQqXoky2VLYOxoxMaV2bCqELz6lxYuSIDoWkMaxmjrEtgmDS1CC0WJH2hU0TZwmdiDhQH3fa+xrZDbSBuNTCXT6+9blh8b5GVhdBsjGpklzLKvR97KDuwtHJxISy9LuV+a15zk2ZojbfC+D1Sak0Xlt5ZDL0lKTrKrrD02rSsD4uWUme2N5ujoaXnFlMTdkZ2hjCuqUsDI4QxsDQyRkqHYcAQ2JYp5SU+A8ahNhDbE7WBuJXAwiCuA9rGgh/PUlROMzY0qjek9HgPtkEpQxAuXg4Lb86FxYudsKT7cPZ86HUW5BGg3MrzoTwGKfXoRyOhqclL933Fc8iJiczbUvTWpJ5bYfFqIywyc5DyN+7iuDVnIHThVOWVpdDkLASHrQTOVZAuX6D0UGNbojYQtyowDmYkGKv1T9ceionxYAqwU0ouw9HU6D9+FysXI6GjEZ+dCg5L2jvWHzjfoAiVFtqPPk5hmUJfkUVQvpVGds7BMD+nd9nDiLyVzntLYeUtph6K2CGjtKMZunYIKzMWGUQLdNZGYluiNhC3FKR8UUFzoPxy87t615XR6Ekfe6RhsfDe8dDTgL/YWA6XlWL04d2h9ezRsPip0dCblUfx5P7QOrAvNB7aFdrPToXwaZV1cca+wFy6qvydubC8czk0VcYo5V6aDytsbcoYLGsa0pMhWHhb3omMwoqMA7sibHda/WapMBpGZY1tCjMQ2ek4e67xiUb2KbWBdQKUT9rbHJUG635E98t6v4Dr/77CZDs072lqtjEadj+8KzSflIfQXQw9KfnC7IXQY81Bytx6bI+ERFMDFiv/eN7ul5VuQffsVnCSsivvBO9gYalr25k91b8s72RF10U9d+VRdHcoQKN5FJFGDIXdK9TYdqg8B5F+rz8Q3tHCuvPUuD7cWLvSJHwu3KewUVnBlc6gZzsDcSp0Lp4PrbsaYezAwRDeWwzzvz8f2g9NhMZje5VOLj/zhLfkHXQ0LbgiY0AdmoowPWFJkRJHlYSrTVnIZWscWbwZAqYQmnKMWOJGaDRbYbw5HsaY0uhduFMeA6co72qZF9FEXuzTb6WP4Hg36yU1Qjj72tkw/dvpMH73eBi9czQc+vYhO8NyM7ExA1EW3BpbiyrlL8fp2X5kxZQrxuXvUVuUOkvTxFj0OqH3+pnQeWs6TLTGQ3hU3sD73dD54wVLw4EpuQP5VuMKn22zCCFFx9hkhqAhpZcXwMdaukeBR1jD0LV5p56hRR5C+IyUHyOgKQ27ILbOgWFwOplSWNqmGYjyD8UUn25vnPivE2FGHtvR7x0NEw9OhNOvng4X3roQTr186qYOxOs2EGviUsFdI6g1bhh9+Lok5RzjfILirC98lI3p0/5BqYE98aMtb54Os/IixvSmdZeUUwm6+g+lt+8syMv6BFMHypJxoCybCtgIPyr9b4bRz7XNENhuCIaArzHNUOhK3SxAUql5DFSuNERAUN4ulUvZ3Ar+K1dkW1QbW7GNOQ9uQ5yXd3fq1VNh+jea4iXAaNDvh755KMYMH5UGoqqDlpaWwqlfnsreCQjrwacO1u7gFsMUnZE14T9u99Hnj4YZTQGIHx8fDy88/0KY+LymCChmP2WirzhqPXs+XHj1eLi/ORqWe11bKMymIhlcXRssaGpqMCKXtjnWkhOB4isOt/YueR/koT5XfJMF4rgK8goMKT0YmCgzll/vSCWqwuV3FsLR546GlffVZj3vmmiH499/MTTZKr1NwU7TQU0DT//q9Fo+iI/PfOuZcOz5Y6H1mVaMHC6qDUQZImz+L/Nh10O7ss5WJ+/54h5rxNiYLH6NrUPkb4qe3P62lAcjzXsEh1Fm8h8ns/QOFBTtMyXmmf8U8ZcLYf53p0P3g46mBiNh7J52GOFHXPR2nEXMe6IX0MRDkPKb0Y+KLYOV0+SegddJXSBWZzDaMTnZS05dFNLrkS1XbudkIPZqyrOyxNcYeqXy5y7O3TTh347AS6D9+57aF2OKOPmTkzYoTH1jKsYMF9k2p3dgP9ChLB75qKP0y8tsb2UWjlGPUGMLYAoWEfvFvDTu4zMGozx/z/OlHgVX0kngJh7dG/Y8ezhMPnsktJ4+FMaeOhDaCs0n9mdfV94t488PuezAxVe/2mivYIZDgavVGeO5UpfVF4PRmPkjmU9AiOBdfPQ3o8qT/RjNqvyMcuryNsXCXxZsXaifcQC7J3eH2Yuz8Wn4WD0HEYWvgDQuFVygd/maRHpfY+sgnvuUA8XBUMBnv3qavG9M86Rseua8w4o9KzA9mJS38aDCvfIS5D2EnQSN1BgEzijQ17a4qCvl8RzLNoX36YODOmLI35ihyG6pNgdxMZ5ybZFVtw3WOpSHD7l45kg2z7frYHP+9fPhgIz2IEx8gd/SCKHzTseuw0ZmINIOTJAKRd5xpc6uscWApyW+wnf6Ai/O+gAli9cUWX/5CL4aLB1rCVz5wVgUOUnHfaM5ttYIOFDg0gBQmbJChtaAsqx+gROa8obMOPHIc0W7bgdcePNCWFhYCK17rz+9mpiYCPPz8/FpuCiepKRjFOynxoSyUNizp3FjAQYYDBNu0paR1JO/r8jvsGPC8X1BiNI4wPOAcnL0S9Mvv8cl7wp0lJC22e/TuL4QL01RIx15HyT12JTD00XAnwaegOpg4sfKEDktNz8Oa0lx3/U2lsU77wOryxWXshOaLV2kJ4fuqcdyxCJ6fuaavJSXPa3mczmJ4D3ts1y844doqI/23WY49fNT4fEnHo9Pg8EaRefKzfQgHNZJ6rTYQQidC4p1ZLwH7J/bopmQChawdFGgyJcbFpAIC/XkAir0rvZC52+dzBg4lJ45N+sdvAdWXqzLr6RZ+OtCmP/zvF15dkBPWg9IlatgqChPwdvmeXnGBWQv2ty72DavP+UN9/aOe+X1e786aKfR/Kd5C7P/N2vbmcZPbx9I7wWnl/KcV/DH6vK+EGx+H9FgS5IA/5XG+dOkrxUHP6wNsX8KNNNW0ZC3kfTqC3jCT8ot/DXjuZdFHksveow+6Pc2lNriz+5FfNKAYhfk9QZAn9vC5JP91x5S7Hpgl32hezOwZhfDhEwdTef/9Mc/DSPsd6vz+JEQVlAN6tS25rKPP/a4GYrlvy9bGlfgxx99vGANKfPEf58Ii4uLmfCo/KmvTpk7hWJM/27alM8ZPfHAhO2SsCDjcy6H0wfIe/rMaRPOmYszmdCLNr4eHL1rNOx5eI8t+HD1PLRr7C7m3VlZXBevLIYTPz5h77nf/+T+cODpA9ZxbO1CnxsuFJE2jGteD43wgN0dW7BFyKPA5/C4RAGw/riUs2/NhssLl61s6IJGyqbdbCFPTk6a4KCY9z9wf2HUmJudswM08HXh7QUzDOSFlgNfOVAwUildHu9tAS/8+wur/aer88yB8fCy2D05+/uz4fKly5khjn3Gzsr42Hhot9vGl4NfOZiXT5lpGQ5TjLbaRz8ItJWtXDvvMWQc/OpBowv5uVHQh898/Zlw5HtHNnU2Ab2CP0f+5UiMuT5e+s+XjM+2kzVMYCAK+Ht2eePCGxiO/mFH/7hD3z50bXl5OQ9z83PX1Pl5GgnKtZnZmWuvvPzKNTVwTX4P7c+1rx3/0fFr3W43oyvSJutpdUgALZ0Uq3AtBynxtXPnz1leKyti+cNlu85dnMvTQps629ovQa1uZxJIc+zfjuVl0V5Heu84/avT19qfb1eWVaaf9p/73TmjWYZyNZ1olGGx8l78wYvFPCpbhngNPXaN/EshhSzkp73TZ6fj2wzkfeMPb1yb/Cf1VRU/PE5XaOOefp3+zfQqD7zu5Ppu5928DwnICH07bCBT8BManY8poDmnewCOfPfINSm1XTeDF7//osngRiDDtqafhoG1BiKiLDj9lM8CApIIDgqWAgF3pTCBUNpj/3rMFKBQRtW9QsoIhEojZOE9IaWPe3uOdCG0PMsjygU0NRQIv6WL+RDUsbGxXNidHo2S+b2lTd7L44ilCRWKiMJqZM7KIL/ypsqRl6WQ1k36qW9MFXjFOxfsd99+95o8sZwuAorpcEORIlVajJvnI2BMC0ZUaV/5xSs53YSUVtqQvrPgPFK61HimoFxot36K+W6Ggei+3zXDgBxhXAkpeA+vf/ozycoA0Kapr03ZIIc83iio7/gPj8entUA2GSTKQCcwTsNGcQ0igTowO8mFa6rgrr06PUsgSDhW3WcCiOkduLRdftVoKZszmYsrdxjXHRcVcNiKcvNDV7EsysftbN/Xtmfc2YNPHwxnfn3GaDH6gOrjVKGMjrlpNqVgLh7LoU7oP/zc4XDy53Ln9EzZXC2N8pOeNIRF+/sQcU4uSGDsYMr+J/aHqaenzK1L31PGmVfP2CGXMiydypQghrO/PZvP/S1ebiXTLKZjuKgaSYJGaYsnANJPvzZtvEp5Dy8og/ysans7CNO/z47okhc+FaB0Xs7SB0v2QVCaBv5avyod5UPT4e8czuiO/Uo80wBoZYrFlEzKkk0NYhpAOngC330q4kjXSm4mOJGKa45cte5uBSlofJMB+aANu/kpvgFARpC/+9v35311I+B7i5RnZSDrp3+9dhoEffB36IiGIkMy8jGKYLksyD3HDSK5B3N/FY+ngZVjysAzQfPTrJBktCZ95Sik0QZLiEuPNWX02/fkvjwdoxdgxKnyHBiV8/qEdLTCnWdUIh0jHVdG29wjEX2Uy2hs5YkWT0eARsqAftKl/MEb8bKtLTGv02LpuYoeXNo8XSwbfjByMB1wOO3UV/CwEu+AwKibuqT0EXH+Hg+EETKnN6E7vYe3abnUZ/kiGB29zyiTK/yDNkb/HLFM6CaPTc2UNqUJfpU9iZvtQTDVnXhwwuQMILMFz09Axmgj8jgI5KVfoXczHgQe1iAPgvf9pjC8q5oibSUqpxgu3CmY86SCynzUgWCYkJQE0uLj1ZXJyojloFC+NmCI+agfpuHuEodg2TRAeVKBcsbm9Hr9EcRjwDRC5/VxhfZUEMtrEH5PnSZMSbmpQmPMXBksqF0oD3CaUDhXLg+4uJTtKCiO16UrQuhTs5QueEC7PB1I1ygINp0CJZ4Ap80MsfeprqzrGJQH/hT6TFeeU3fX6E7Lj/cYSfquQLPuMQguE6TNDURS/jANBFPfnC8C9cubyOlGDuAJtKfTtCqgDxgIeLBZAzFoLYGB2WWqDOQ/bc8w0HcNoox8tImdaQbChSMVkgqgVFjlXBhjYLTJEctIlT29R6nS/DB2Tf19rgisK6kLLR3sWLNQp8DIYvV7WX3APDTNh4ClCs/71KhhpHx0LxgGkNbFvUJKe1pG6jX5yG3v4ZECI6Ujrcd5ysjjBtO8BOVBYfw96x6pghvdvE9pjjSWQRmufJ6f8s3gJ2BENxpivw7TQGDo6YuUfuTS+iu2GZnA4HO1dbSKtjlQTAw4vGcAGpR2EFD+QV4ARgAPIjesCdDJYRuIvmsQwwbzeLbzcpTnYXr2eersn2az9Qrm2QJ5D/KT7Z6Hq8/BhXxOGJ/lOts8Gvi8jS0qwPqAhLQwv+d+76N7szgvuw9a9xVPvkno4l22TiAlWJ0rqizm6va9ge5lOLJ4R1oX9wpSGtsytPQJjZTp5UI/vMzXZFQGaz7WRt1bPbFcL4PtSt+WZn1h3xP77IdJeG90X0zoFvixErYxCzRHGsugDOhmu9TSk0b1z12as+8NHHl7vM1DBGsL1JfSz7PJSqyf8y3QzTY9/HP+VAHeWnkKbHlvBv2+P6FfZBjsx2LSvnCwhrKZ9Y/14OYaiEQQWCiS+xyf+iAKH52B0LpAoQye1xbPQCKoa5ipdywu2iJahJUZF/FSZQPEQZ+jICglYeYsQlo3C388Ux4Cx8Etz8P5i6lnp7JFwIgyreUOh5aDXz2Ynd2IyNMktMAPzk54HHSwwFUFeKlRspAfw+X8RYn5etchDyYcfvZwQbkqkfJGPIAe+OPlcu7BDTOwheEhC7gDw815hRSm4NAQ+4A0e/95b25oWVyvAmdW4CFngchPOYOMySBwzsj5Uwa8IXTtL6rHyAT2Yz8V8VuJj82DaPEHVQRG8BQFZsXGywWzq3ckgkoHccDIA4emEGx7lmJyj9fhJytZdR7Zoc6IQs4IQVpgQpooOTSYkMT6B624s8NSeE8eBeI4W5+CHQe8mRwxXYqqulAyRveCEUO4FdI4fkcAWBtVNsqYK2A0WgDvwAxXBMZl98Py5mJ7jd/cR55Ad2pc7V1MW0DCQwAPMeZpH3Ooq4CqcrYYHMKDV3gHawDNkW4OouUH/BSHjDmQqZf+4yW756+fm9cJn5UOvrosbRQma8mAkcJlwfu6DOo34zFE3HwDETtjPZ/1unC7l+DKffJ/Tob2A+3w2fs+a4LN6UE6lusjX3wk7Hlsj8V7IB5BpRNdWLl2+MtPAm5cKqijO/kDM6tC7fUaIv0G5eH3FQwx3pQ2GpzLb2eGzd/tmtiV3UTYqMO7VEliWldmF1I3qAZPH9P6M6dOMSZOO0JtI3Z870LGScgUnGpFSL1ORsgUZbqtXq+7H6hTgWPBhpi+y1/3iu9uFjAQqUfoMH6IDvjFMXcMuhtwZMDlA7AdbHIi0CepTHDPVPJGUDUgOOgPwvinS1O7CKYYw/Yibr6BUGNSi+hX66gooI68A6OhcMHHYOCu+jt7llCTn1GAZzqRqzFZ+bjmdUVhHb8nmzua8iUCTzqjJVWCPp2wzO83pul07+0ol0FH+zuwRjiSOnJaIxBYB232cqyMWAd5mMIYYpz9ClhCgxmNP666+Yzy+/gGQHW7ENrReRDp6TfCGUhTxRv4IDq9zLStbkBH7HcvY+SQAJ/o//ToeArjs2iwKZc/C/SryxuYmZ3JjQyGI113wIv0QWwrgdeGzFA3sD6v4LXrxzDwsUwxCg1ah4D4/NuFjTkx7qIFWVGEnHsY6u/sXsHSyEUmsMiGsFOer2HAdBulE8a7ABdQRafiyG/CH/O74gKz7glyjyLC25Pzo6IOvisBZRe2Xx4OLdFWB8LlB9IAo2n6zOEvS5+Uw0/apcCo3AhQNq/L6bWFzmgslvlrW1XGZQsB7QwAVSMwwEhdvnTZvsEhnRsFM14J6Ff34rpXu9mULAL+uXexUdhAFr3EKmCszWMRLzH2vtgOaBO8TA3ZVuOmGgi3zmuAkEhA/X3BUioeoeIKI0hz+FuHwxtvvmFuHWFhfiG7Xlqwj32YY3M/f3Heni1uds7ysBCF0hDnv+m4vLy8OtLpOVf6iIEdEGnPoWdvB8YqbQc0ebmpISnA0wtWbyzb12GAl5/D8+hKnTbSxTiEyzwGPVNeukiIsVzjeisdRpc6vJ7Z2dlVeik3lm2APm9/jE/5lbve8Z0Zn5ieMgcZx61A+aRoGSxEnvhJdgJ2+UPJQcJb/zjO2i76kEPu4WF60pI2DVLyQUD2/JRxFUZGRvJ1BvolnTIaRFdfWdoCVBqI6yqEAOGFznWiuaYNSOILQpbmKQmHKZE3XGn2f3l/npa4c384l3kOEnDzCOQp+D0eA53MyIuyEOxd9DJ4ZwqgOlLB8TZTfiokYJCAGZ0Jv8hrVl3l7H1sr9Vt7RH9LAwisJ7O21SoT+W5sHm99klxaZXcDY3B+RdpOfTsoTyOZ6tTz+xMnOcP3wjGB418fLWa9wNQOtsyFd3ev3gv7ubmdUXQzrJyeD4WjjHEhphvDz+7H2F94G13GkrlbxbQzUDQDyz+Ogqek+hglwKgwIvvLeb9wfQ2lQmmG/12PK4H9Iiy+2HyC6u/O8qXstBlO2UR9K95YkNCtQdR0UlllwtraxDxpsieh2tFfmtE8q4g4DAgFdIIS6P0TAlYgENowez/ztrPglu9gs//CoqV0FBQQKB3libWmY5qG4XT6IAmFIQ6CbaKr7hc2UV37uKX64z00E5vC3lP/OhEwQgNAvUwfWIv38HUBmV1TwJAI99RGJwOr1/GdC9//j8+045nnn1mdZ6t9M573pkxgb4Y7898f2Kf18uAUxb1mTeYItYxFKhsaGGRugr0AwYErxJ+d/izgdG4sZhqCimkn+JjLCiT4ODeFt1voC2UWTawKfDwOGsDX+1nB5Xe1r0ieGZ3blioNBAF5Y1I9+EBjTKBkVBYejEnXahJV8J5b4tsMDAy0TvCgIC6kKaIcYz8rLSnBoC5GEJvQiCB5p0LJhbeFQrGohh8c8+5AP4QCfNwQxRoA3RV0bAOpEYiteZ0nv/6sNPDAaWTPz6Z88faFHniZbiSMXJPfX3KrrTxuojlUK8tPEbAj5MvnwzTZ6dzvuNFwdMq0I9HnsvODMBr6GEu/+IPX8zXI2hzKthuAJ0G0rJbQrwtKKtt8MKmXWXcIN+vB7wlX3cqg35gqgpvj//wuA1AtIff1wBm5GObGOFZLATsdJSNnKdL13bWDbU9/Z2PNdB7+gOZNRkQf9PtcxZM0/ahF+6lOug/dv5uBOv2IFL3CwGEGQiBCTrpFWgAwsCXjfwlIIPiYTbnEFKkSnVdKB3bmPkZAjEJhSOOE3JuHACdxYhlgina+Bpx/4H9xjhW+O0rUk7zuTKJjtxIRBhtG4ApXSwPT8vzc0VR85E6Apo5tGOKD92RD25EKIvpCD/k4oedUuNrqOJdEscimi/EAv6Emx1+inTi6qeLmRYf89OPjFZ4AE4TdPJl5ks/eCkzzEq7RvFUBn3BwS7ODCAnnp+Tmvl6R6TBoHI2yu/1gn4uLxQDvqrFk4M/DFwoP4uU9BODiR9u850jToC6YcVATjy0dsrCtjs/WrRRpNvSlRAdDJD2Ra/as3/ffqMRg8HfRhn9dPZjxgB5Qu8wfgC5Ji0GY2AdgxCPXGcYcJ68+/7qbzqo47PrzoZ9qMJ5cb6r50y6hM7e862Gn3vP88bvBAjpx0pr0IcOzWeLX4TGD32gge/3+ZCID274AIzz62L+Kq3xyhl7uZSxxAx81+BlEqAVmtcDztF72dCizix8kASgB1pIk9LP15P2sc5vpu1cPzyhDXzdmpcZg7XV7/WOdvZFrJdz/pYHnse8nn/N14oxT/6tgp65p0/L+eljPuzK6VZZ0I0M0CYZjkJ62p5+Y+HfPtjHWkk74V25bzYDaEy/uQHIEN9fQDMyiuzAd9ISoJ/+kgHN6BQfSO/f5hBfRSO88I8HNwLKHJQPOeSLYvirgc/qebfzrskxtNM/Lgs8017aSDxpCOkHdhvFug0E7/g4xDvUriXByZ915cMYB4JA56dpjehB9TlKaWCWKVmsKxVGaHKjQUiFjwBNMLcMGJqmQ0jWK6ipgeBKJ+Z5oT3Sj9ClxqGgRNAqustGwMrVOwSZkL6rNBDOq3hFCayeyCsP/qGdK2oBXoaAgcDYoRzkS+n3kNNZjo/9AC/n5ku0xjqMd0mbt9pAoNjU4aBsV3DeoUwoJ7KIIvEMvciDf2LN4MFHZigfBpd0VUCu4NONwH7uoM+ASbluQPhpg/RHYtAr+tIMgYwbH9h5n9L3W/HhW3GKUeW2guh+8j2DiLF7c8ujqygByVxFrkDx6UGTfIGzX/kJyu5+IY/KVSeE6TPT2equYPNgTwNd1AldCl4W817yMd3AXQN5PdDKNmdsA+B+vW4v6TwvZaYHmlLgvs68OZOv4qfzd6NVdBOch5RF4MM0XGI7zZi0swruBjs4xcgiqfEjgvLtSHa/MiJfjBa5rkwjTvzghP0Qj09zKIN4YHTGeo1vCf3Iy7nXz2XfqyQ0pHX7OY9+9GwG6c4P7WFaJyW3cw+8k9Ln0wd2ITjnwPRBBiRfc2DaIKNhssNagf1+ZwWQK3YzbCF4g6Befgy5EuKbb3PKe7D1EseZ354JR79zNMh7Nvl65eVXcv4znbPF4U2ieg0CJB2aC5468fiPjofTvzhtvybkxBiikBMPM5kfIWAOmACjmfeisMydKoUiFaTyPel1ZX5NR8g9t47JhVX1W5pYrkY8WwM4+bOT2Z8JrGKY0jotLuAe1oVYn6dnrcbunV4FNwa0XaNVkCeWrafwPoWeTUF15T28vvD6BeOXG480lOFx9Bfl0P58/SPWBa84HAVIbzwDkdfWZ7r3K4Bv0ML5Ev+oq2DgIty4sDZ07uw5E2Y3yOW2elobPPSuqj2bBYbf28DiI4uLKA4L1ce+e8zi2QFg7g498vJsAZD5vhk1gbMrvl5jMlS1yBqBXHJke6OwhWzojLSmgK4CryMfiWeB0n5NTMj7eYtxB25EvN8QECwYa1tDIpaRlFGOjjaFTQVCDXehBTY6IxTxuhmwEIngsmjqZ+cRbraooAdFu57w0RZGFEYR0uIFUMZ6hJa8tCtvm/JYSIxjFRiNoZkRDLpdUW0Uk3DyK9a5MIp/0McWGzzj+w9GXqOvxGeeKYv6ESyUgXMUpIVGjDejaFW+9YAyWNCbvzRvh6Dgv3tRbCdy7Nh4fp32m0yoLNpl3qbuGYHXw7v1Yu+X9tpABU794pSNsHhPnH3wkZjFVAwp2530CUrOiMzhO/pn6ptTdshuPTTRBvvZQfHSeLwB8CvV7BT6rpfDfrVcRopBJQULkRyawnAPEzdsIHIgXCkGCdoGBHEQ6IjUuPBMMMOE4MkYlY2P5UGhnN4SHeUy12AQ7bFOMKiMnAZHH1or0YfuHJRF+VGQuUeRH3n0ETPkAP6w1Vk4PTmoXRsEBsm9uTVw+kFVfU4//NkievY+vteOQOPVsWOBMUMJ2Z1xsEvEAMNuB4YahcNItO5pmfFmesX0dL0wvsuQ557TOkEfsdOGh5mCXQuM6Bt/eCPGZLBdua9OrfmzEFuNDRkIGg/WJdDbGddTtk8iyoquZ/a+ESSHu/6kc2XMlXIdKPQ/8PpQbgxdyfgZPkYes3YDXWyx2jkCGS9fA3Jw6I6/e8IWohunF773gnkVTD+2Yh6/XnBWByPL1NkgHmKs+KiQ9QcHRm3mrRnzdIYODMSWIFn9zlEVtxn0K494BVvBTdPE+A3B82w032ah+tbQn2Kd9LDzQDlcy9vS7AA5rC7g5W5VeynHw3qw0fRbDdXLzgC7GuwCDPsn3AZCtLDLwi4KYHeFfmMnIwU7FulPDg4TW2cg1oOtFoKtLO/jEtCtRmwHP4SKcGlEtGv7c/FsR9rOjbaZ9OVQY0vBtixnGNiKxYiX/z4Hz+W4YWJDBqIwwn2cQtKv3pSmqvfbCWVaq8KNQnntR34xEDG8+P3sD8Tkh7iE3Iuosa3A4bM9X9yzxkvAwBOf9uGwsflFyvWiPEe+EfQrY9B819+B69W/kbTDwCAepbSBinS+RsAuxsGvH8zXirgefe7o6oJWWs+gOkuw9YqNrj9dr/y0XRsp9xYH2/jsYLCgrCmF7ahwZVdmWFuaVbh5BmIYcOGKglW54LYBBbgV4Fuc6b3zxXcZUkVP028IG+Grp73N+mKzoG9sx0U88y1YDnfdTHyyDUSNGjWGiv4nKWvUqHHbozYQNWrU6IvaQNSoUaMvagNRo0aNvqgNRI0aNfqiNhA1atToi9pA1KhRoy/+IdxxR7B/d8SYGjVq1IioPYgaNWr0RW0gatSo0Qch/D9EttdvCe69YwAAAABJRU5ErkJggg==" style="width: 264px; height: 77px;" />
              </td>
            </tr>
            <tr>
              <td style="width: 205px;">&#160;</td>
              <td style="width: 165px;">&#160;</td>
              <td style="width: 277px;">&#160;</td>
            </tr>
            <tr>
              <td style="width: 205px;">
                <table align="left" border="0" cellpadding="0" cellspacing="0" id="customerPartyTable" style="border-top-color: rgb(0, 0, 0); border-bottom-color: rgb(0, 0, 0); border-top-width: 3px; border-bottom-width: 3px; border-top-style: solid; border-bottom-style: solid;" width="350">
                  <tbody>
                    <tr>
                      <td align="left" style="width: 469px;">
                        <span style="font-size: 12px;">
                          <span style="color: rgb(105, 105, 105);">
                            <strong>SAYIN</strong>
                          </span>
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <td align="left" style="width: 469px;">
                        <span style="font-size: 12px;">
                          <span style="color: rgb(105, 105, 105);">
                            <xsl:choose>
                              <xsl:when test="not(//n1:Invoice/cbc:ProfileID = &apos;YOLCUBERABERFATURA&apos; or //n1:Invoice/cbc:ProfileID = &apos;IHRACAT&apos;)">
                                <xsl:if test="n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyIdentification/cbc:ID/@schemeID = &apos;VKN&apos;">
                                  <xsl:value-of select="n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyName/cbc:Name" />
                                </xsl:if>
                                <xsl:if test="n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyIdentification/cbc:ID/@schemeID = &apos;TCKN&apos;">
                                  <xsl:value-of select="n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:Person/cbc:FirstName" />
                                  <xsl:text> </xsl:text>
                                  <xsl:value-of select="n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:Person/cbc:FamilyName" />
                                </xsl:if>
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:value-of select="n1:Invoice/cac:BuyerCustomerParty/cac:Party/cac:Person/cbc:FirstName" />
                                <xsl:text> </xsl:text>
                                <xsl:value-of select="n1:Invoice/cac:BuyerCustomerParty/cac:Party/cac:Person/cbc:FamilyName" />
                              </xsl:otherwise>
                            </xsl:choose>
                          </span>
                        </span>
                      </td>
                    </tr>
                    <tr align="left">
                      <td align="left" style="width: 469px;">
                        <span style="font-size: 12px;">
                          <span style="color: rgb(105, 105, 105);">
                            <xsl:choose>
                              <xsl:when test="not(//n1:Invoice/cbc:ProfileID = &apos;YOLCUBERABERFATURA&apos; or //n1:Invoice/cbc:ProfileID = &apos;IHRACAT&apos;)">
                                <xsl:value-of select="n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PostalAddress/cbc:StreetName" />
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:value-of select="n1:Invoice/cac:BuyerCustomerParty/cac:Party/cac:PostalAddress/cbc:StreetName" />
                              </xsl:otherwise>
                            </xsl:choose>&#160;<br />
                            <xsl:choose>
                              <xsl:when test="not(//n1:Invoice/cbc:ProfileID = &apos;YOLCUBERABERFATURA&apos; or //n1:Invoice/cbc:ProfileID = &apos;IHRACAT&apos;)">
                                <xsl:value-of select="n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PostalAddress/cbc:PostalZone" />
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:value-of select="n1:Invoice/cac:BuyerCustomerParty/cac:Party/cac:PostalAddress/cbc:PostalZone" />
                              </xsl:otherwise>
                            </xsl:choose>&#160;
                            <xsl:choose>
                              <xsl:when test="not(//n1:Invoice/cbc:ProfileID = &apos;YOLCUBERABERFATURA&apos; or //n1:Invoice/cbc:ProfileID = &apos;IHRACAT&apos;)">
                                <xsl:value-of select="n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PostalAddress/cbc:CitySubdivisionName" />
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:value-of select="n1:Invoice/cac:BuyerCustomerParty/cac:Party/cac:PostalAddress/cbc:CitySubdivisionName" />
                              </xsl:otherwise>
                            </xsl:choose> /
                            <xsl:choose>
                              <xsl:when test="not(//n1:Invoice/cbc:ProfileID = &apos;YOLCUBERABERFATURA&apos; or //n1:Invoice/cbc:ProfileID = &apos;IHRACAT&apos;)">
                                <xsl:value-of select="n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PostalAddress/cbc:CityName" />
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:value-of select="n1:Invoice/cac:BuyerCustomerParty/cac:Party/cac:PostalAddress/cbc:CityName" />
                              </xsl:otherwise>
                            </xsl:choose>
                          </span>
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <td align="left" style="width: 469px;">
                        <span style="font-size: 12px;">
                          <span style="color: rgb(105, 105, 105);">
                            <xsl:choose>
                              <xsl:when test="not(//n1:Invoice/cbc:ProfileID = &apos;YOLCUBERABERFATURA&apos; or //n1:Invoice/cbc:ProfileID = &apos;IHRACAT&apos;)">
                                <xsl:if test="n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:Contact/cbc:ElectronicMail !=&apos;&apos;">
                                  <xsl:text>E-Posta:</xsl:text>
                                  <xsl:value-of select="n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:Contact/cbc:ElectronicMail" />
                                </xsl:if>
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:if test="n1:Invoice/cac:BuyerCustomerParty/cac:Party/cac:Contact/cbc:ElectronicMail != &apos;&apos;">
                                  <xsl:text>E-Posta:</xsl:text>
                                  <xsl:value-of select="n1:Invoice/cac:BuyerCustomerParty/cac:Party/cac:Contact/cbc:ElectronicMail" />
                                </xsl:if>
                              </xsl:otherwise>
                            </xsl:choose>
                          </span>
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <td align="left" style="width: 469px;">
                        <span style="font-size: 12px;">
                          <span style="color: rgb(105, 105, 105);">
                            <xsl:choose>
                              <xsl:when test="not(//n1:Invoice/cbc:ProfileID = &apos;YOLCUBERABERFATURA&apos; or //n1:Invoice/cbc:ProfileID = &apos;IHRACAT&apos;)">
                                <xsl:if test="n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:Contact/cbc:Telephone !=&apos;&apos;">
                                  <xsl:text>Tel: </xsl:text>
                                  <xsl:value-of select="n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:Contact/cbc:Telephone" />
                                </xsl:if>
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:if test="n1:Invoice/cac:BuyerCustomerParty/cac:Party/cac:Contact/cbc:Telephone != &apos;&apos;">
                                  <xsl:text>Tel: </xsl:text>
                                  <xsl:value-of select="n1:Invoice/cac:BuyerCustomerParty/cac:Party/cac:Contact/cbc:Telephone" />
                                </xsl:if>
                              </xsl:otherwise>
                            </xsl:choose> Faks:&#160;
                            <xsl:choose>
                              <xsl:when test="not(//n1:Invoice/cbc:ProfileID = &apos;YOLCUBERABERFATURA&apos; or //n1:Invoice/cbc:ProfileID = &apos;IHRACAT&apos;)">
                                <xsl:if test="n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:Contact/cbc:Telefax !=&apos;&apos;">
                                  <xsl:text>Faks: </xsl:text>
                                  <xsl:value-of select="n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:Contact/cbc:Telefax" />
                                </xsl:if>
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:if test="n1:Invoice/cac:BuyerCustomerParty/cac:Party/cac:Contact/cbc:Telefax != &apos;&apos;">
                                  <xsl:text>Faks: </xsl:text>
                                  <xsl:value-of select="n1:Invoice/cac:BuyerCustomerParty/cac:Party/cac:Contact/cbc:Telefax" />
                                </xsl:if>
                              </xsl:otherwise>
                            </xsl:choose>
                          </span>
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <td align="left" style="width: 469px;">
                        <span style="font-size: 12px;">
                          <span style="color: rgb(105, 105, 105);">
                            <xsl:choose>
                              <xsl:when test="not(//n1:Invoice/cbc:ProfileID = &apos;YOLCUBERABERFATURA&apos; or //n1:Invoice/cbc:ProfileID = &apos;IHRACAT&apos;)">
                                <xsl:if test="n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyTaxScheme/cac:TaxScheme/cbc:Name !=&apos;&apos;">
                                  <xsl:text>Vergi Dairesi: </xsl:text>
                                  <xsl:value-of select="n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyTaxScheme/cac:TaxScheme/cbc:Name" />
                                </xsl:if>
                              </xsl:when>
                            </xsl:choose>
                          </span>
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <td align="left" style="width: 469px;">
                        <span style="font-size: 12px;">
                          <span style="color: rgb(105, 105, 105);">
                            <xsl:choose>
                              <xsl:when test="not(//n1:Invoice/cbc:ProfileID = &apos;YOLCUBERABERFATURA&apos; or //n1:Invoice/cbc:ProfileID = &apos;IHRACAT&apos;)">
                                <xsl:for-each select="//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyIdentification">
                                  <xsl:variable name="schemeID" select="cbc:ID/@schemeID" />
                                  <xsl:if test="$schemeID =&apos;TCKN&apos; or $schemeID =&apos;VKN&apos;" />
                                  <xsl:value-of select="$schemeID" /> :
                                  <xsl:value-of select="cbc:ID" />
                                </xsl:for-each>
                              </xsl:when>
                            </xsl:choose>
                          </span>
                        </span>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
              <td style="width: 165px;">&#160;</td>
              <td style="width: 277px;">
                <table style="width: 271px; border-collapse: collapse;">
                  <tbody>
                    <tr>
                      <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 116px;">
                        <span style="font-size: 12px;">
                          <span style="color: rgb(105, 105, 105);">
                            <span style="font-family: verdana,geneva,sans-serif;">
                              <span style="font-weight: bold;">Fatura Tipi:</span>
                            </span>
                          </span>
                        </span>
                      </td>
                      <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 148px;">
                        <span style="font-size: 12px;">
                          <span style="color: rgb(105, 105, 105);">
                            <span style="font-family: verdana,geneva,sans-serif;">
                              <xsl:value-of select="n1:Invoice/cbc:InvoiceTypeCode" />
                            </span>
                          </span>
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 116px;">
                        <span style="font-size: 12px;">
                          <span style="color: rgb(105, 105, 105);">
                            <span style="font-family: verdana,geneva,sans-serif;">
                              <span style="font-weight: bold;">Belge </span>
                              <span style="font-weight: bold;">No:</span>
                            </span>
                          </span>
                        </span>
                      </td>
                      <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 148px;">
                        <span style="font-size: 12px;">
                          <span style="color: rgb(105, 105, 105);">
                            <span style="font-family: verdana,geneva,sans-serif;">
                              <xsl:value-of select="n1:Invoice/cbc:ID" />
                            </span>
                          </span>
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 116px;">
                        <span style="font-size: 12px;">
                          <span style="color: rgb(105, 105, 105);">
                            <span style="font-family: verdana,geneva,sans-serif;">
                              <span style="font-weight: bold;">Fatura Tarihi:</span>
                            </span>
                          </span>
                        </span>
                      </td>
                      <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 148px;">
                        <span style="font-size: 12px;">
                          <span style="color: rgb(105, 105, 105);">
                            <span style="font-family: verdana,geneva,sans-serif;">
                              <xsl:for-each select="//n1:Invoice">
                                <xsl:for-each select="cbc:IssueDate">
                                  <xsl:value-of select="substring(.,9,2)" />-
                                  <xsl:value-of select="substring(.,6,2)" />-
                                  <xsl:value-of select="substring(.,1,4)" />
                                </xsl:for-each>
                              </xsl:for-each>
                            </span>
                          </span>
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 116px;">
                        <span style="font-size: 12px;">
                          <span style="color: rgb(105, 105, 105);">
                            <span style="font-family: verdana,geneva,sans-serif;">
                              <span style="font-weight: bold;">D&#252;zenleme Tarihi:</span>
                            </span>
                          </span>
                        </span>
                      </td>
                      <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 148px;">
                        <span style="font-size: 12px;">
                          <span style="color: rgb(105, 105, 105);">
                            <span style="font-family: verdana,geneva,sans-serif;">
                              <xsl:for-each select="//n1:Invoice">
                                <xsl:for-each select="cac:AdditionalDocumentReference">
                                  <xsl:if test="cbc:ID = &apos;duzenlemeTarihi&apos;">
                                    <xsl:for-each select="cbc:IssueDate">
                                      <xsl:value-of select="substring(.,9,2)" />-
                                      <xsl:value-of select="substring(.,6,2)" />-
                                      <xsl:value-of select="substring(.,1,4)" />
                                    </xsl:for-each>
                                  </xsl:if>
                                </xsl:for-each>
                                <xsl:text> </xsl:text>
                              </xsl:for-each>
                            </span>
                          </span>
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 116px;">
                        <span style="font-size: 12px;">
                          <span style="color: rgb(105, 105, 105);">
                            <span style="font-family: verdana,geneva,sans-serif;">
                              <span style="font-weight: bold;">D&#252;zenleme Zaman&#305;:</span>
                            </span>
                          </span>
                        </span>
                      </td>
                      <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 148px;">
                        <span style="font-size: 12px;">
                          <span style="color: rgb(105, 105, 105);">
                            <span style="font-family: verdana,geneva,sans-serif;">
                              <xsl:for-each select="//n1:Invoice">
                                <xsl:for-each select="cac:AdditionalDocumentReference">
                                  <xsl:if test="cbc:ID = &apos;duzenlemeTarihi&apos;">
                                    <xsl:value-of select="cbc:DocumentType" />
                                  </xsl:if>
                                </xsl:for-each>
                                <xsl:text> </xsl:text>
                              </xsl:for-each>
                            </span>
                          </span>
                        </span>
                      </td>
                    </tr>
                    <xsl:if test="//n1:Invoice/cac:DespatchDocumentReference">
                      <tr condition="//n1:Invoice/cac:DespatchDocumentReference">
                        <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 116px;">
                          <span style="font-size: 12px;">
                            <span style="color: rgb(105, 105, 105);">
                              <span style="font-family: verdana,geneva,sans-serif;">
                                <strong>&#304;rsaliye No:</strong>
                              </span>
                            </span>
                          </span>
                        </td>
                        <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 148px;">
                          <span style="font-size: 12px;">
                            <span style="color: rgb(105, 105, 105);">
                              <span style="font-family: verdana,geneva,sans-serif;">
                                <xsl:for-each select="//n1:Invoice/cac:DespatchDocumentReference">
                                  <xsl:value-of select="cbc:ID" />
                                  <br />
                                </xsl:for-each>
                              </span>
                            </span>
                          </span>
                        </td>
                      </tr>
                    </xsl:if>
                    <xsl:if test="//n1:Invoice/cac:DespatchDocumentReference">
                      <tr condition="//n1:Invoice/cac:DespatchDocumentReference">
                        <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 116px;">
                          <span style="font-size: 12px;">
                            <span style="color: rgb(105, 105, 105);">
                              <span style="font-family: verdana,geneva,sans-serif;">
                                <strong>&#304;rsaliye Tarihi:</strong>
                              </span>
                            </span>
                          </span>
                        </td>
                        <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 148px;">
                          <span style="font-size: 12px;">
                            <span style="color: rgb(105, 105, 105);">
                              <span style="font-family: verdana,geneva,sans-serif;">
                                <xsl:for-each select="//n1:Invoice/cac:DespatchDocumentReference">
                                  <xsl:variable name="ddt" select="cbc:IssueDate" />
                                  <xsl:value-of select="concat(substring($ddt, 9, 2), &apos;-&apos;, substring($ddt, 6, 2), &apos;-&apos;, substring($ddt, 1, 4))" />
                                  <br />
                                </xsl:for-each>
                              </span>
                            </span>
                          </span>
                        </td>
                      </tr>
                    </xsl:if>
                    <xsl:if test="//n1:Invoice/cac:OrderReference">
                      <tr condition="//n1:Invoice/cac:OrderReference">
                        <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 116px;">
                          <span style="font-size: 12px;">
                            <strong style="color: rgb(105, 105, 105); font-family: verdana, geneva, sans-serif; font-size: 10px;">Sipari&#351; No:</strong>
                          </span>
                        </td>
                        <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 148px;">
                          <span style="font-size: 12px;">
                            <span style="color: rgb(105, 105, 105);">
                              <span style="font-family: verdana,geneva,sans-serif;">
                                <xsl:value-of select="n1:Invoice/cac:OrderReference/cbc:ID" />
                              </span>
                            </span>
                          </span>
                        </td>
                      </tr>
                    </xsl:if>
                    <xsl:if test="//n1:Invoice/cac:OrderReference">
                      <tr condition="//n1:Invoice/cac:OrderReference">
                        <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 116px;">
                          <span style="font-size: 12px;">
                            <strong style="color: rgb(105, 105, 105); font-family: verdana, geneva, sans-serif; font-size: 10px;">Sipari&#351; Tarihi:</strong>
                          </span>
                        </td>
                        <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 148px;">
                          <span style="font-size: 12px;">
                            <span style="color: rgb(105, 105, 105);">
                              <span style="font-family: verdana,geneva,sans-serif;">
                                <xsl:variable name="spdt" select="n1:Invoice/cac:OrderReference/cbc:IssueDate" />
                                <xsl:value-of select="concat(substring($spdt, 9, 2), &apos;-&apos;, substring($spdt, 6, 2), &apos;-&apos;, substring($spdt, 1, 4))" />
                              </span>
                            </span>
                          </span>
                        </td>
                      </tr>
                    </xsl:if>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td style="width: 205px;">
                <span style="font-size: 12px;">
                  <span style="font-family: verdana,geneva,sans-serif;">
                    <span style="color: rgb(105, 105, 105);">
                      <strong>ETTN: </strong>
                      <xsl:value-of select="n1:Invoice/cbc:UUID" />
                    </span>
                  </span>
                </span>
              </td>
              <td style="width: 165px;">&#160;</td>
              <td style="width: 277px;">&#160;</td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>
    <html>
      <head></head>
      <body>
        <p>&#160;</p>
        <table style="border: 2px solid rgb(0, 0, 0); border-image: none; width: 800px; border-collapse: collapse;">
          <tbody>
            <tr>
              <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 3%; text-align: center;">
                <span style="font-size: 10px;">
                  <strong>
                    <span style="color: rgb(105, 105, 105);">S&#305;ra No</span>
                  </strong>
                </span>
              </td>
              <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 6%; text-align: center;">
                <span style="font-size: 10px;">
                  <strong>
                    <span style="color: rgb(105, 105, 105);">Malzeme / Hizmet</span>
                  </strong>
                </span>
              </td>
              <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 10%; text-align: center;">
                <span style="font-size: 10px;">
                  <strong>
                    <span style="color: rgb(105, 105, 105);">Malzeme / Hizmet Kodu</span>
                  </strong>
                </span>
              </td>
              <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 10%; text-align: center;">
                <span style="font-size: 10px;">
                  <strong>
                    <span style="color: rgb(105, 105, 105);">Malzeme / Hizmet A&#231;&#305;klamas&#305;</span>
                  </strong>
                </span>
              </td>
              <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 7%; text-align: center;">
                <span style="font-size: 10px;">
                  <strong>
                    <span style="color: rgb(105, 105, 105);">A&#231;&#305;klamalar</span>
                  </strong>
                </span>
              </td>
              <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 6%; text-align: center;">
                <span style="font-size: 10px;">
                  <strong>
                    <span style="color: rgb(105, 105, 105);">Miktar</span>
                  </strong>
                </span>
              </td>
              <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 13%; text-align: center;">
                <span style="font-size: 10px;">
                  <strong>
                    <span style="color: rgb(105, 105, 105);">Birim Fiyat</span>
                  </strong>
                </span>
              </td>
              <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 7%; text-align: center;">
                <span style="font-size: 10px;">
                  <strong>
                    <span style="color: rgb(105, 105, 105);">&#304;skonto Oran&#305;</span>
                  </strong>
                </span>
              </td>
              <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 11%; text-align: center;">
                <span style="font-size: 10px;">
                  <strong>
                    <span style="color: rgb(105, 105, 105);">&#304;skonto Tutar&#305;</span>
                  </strong>
                </span>
              </td>
              <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 8%; text-align: center;">
                <span style="font-size: 10px;">
                  <strong>
                    <span style="color: rgb(105, 105, 105);">KDV Oran&#305;</span>
                  </strong>
                </span>
              </td>
              <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 8%; text-align: center;">
                <span style="font-size: 10px;">
                  <strong>
                    <span style="color: rgb(105, 105, 105);">KDV Tutar&#305;</span>
                  </strong>
                </span>
              </td>
              <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 4%; text-align: center;">
                <span style="font-size: 10px;">
                  <strong>
                    <span style="color: rgb(105, 105, 105);">Di&#287;er Vergiler</span>
                  </strong>
                </span>
              </td>
              <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 10.6%; text-align: center;">
                <span style="font-size: 10px;">
                  <strong>
                    <span style="color: rgb(105, 105, 105);">Malzeme / Hizmet Tutar&#305;</span>
                  </strong>
                </span>
              </td>
            </tr>
            <xsl:for-each select="//n1:Invoice/cac:InvoiceLine">
              <tr>
                <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 3%; text-align: left;">
                  <span style="font-size: 10px;">
                    <span style="color: rgb(105, 105, 105);">
                      <xsl:value-of select="./cbc:ID" />
                    </span>
                  </span>
                </td>
                <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 6%; text-align: center;">
                  <span style="font-size: 10px;">
                    <span style="color: rgb(105, 105, 105);">
                      <xsl:value-of select="./cac:Item/cbc:Description" />
                    </span>
                  </span>
                </td>
                <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 10%; text-align: center;">
                  <span style="font-size: 10px;">
                    <span style="color: rgb(105, 105, 105);">
                      <xsl:value-of select="./cac:Item/cac:SellersItemIdentification/cbc:ID" />
                    </span>
                  </span>
                </td>
                <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 10%; text-align: left;">
                  <span style="font-size: 10px;">
                    <span style="color: rgb(105, 105, 105);">
                      <xsl:value-of select="./cac:Item/cbc:Name" />
                    </span>
                  </span>
                </td>
                <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 7%; text-align: center;">
                  <span style="font-size: 10px;">
                    <span style="color: rgb(105, 105, 105);">
                      <xsl:for-each select="./cbc:Note">
                        <span>
                          <xsl:text>&#160;</xsl:text>
                          <xsl:value-of select="." />
                        </span>
                        <br />
                      </xsl:for-each>
                    </span>
                  </span>
                </td>
                <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 6%; text-align: right;">
                  <span style="font-size: 10px;">
                    <span style="color: rgb(105, 105, 105);">
                      <xsl:value-of select="format-number(./cbc:InvoicedQuantity, &apos;###.###,##&apos;, &apos;european&apos;)" />
                      <xsl:if test="./cbc:InvoicedQuantity/@unitCode">
                        <xsl:for-each select="./cbc:InvoicedQuantity">
                          <xsl:text> </xsl:text>
                          <xsl:choose>
                            <xsl:when test="@unitCode  = &apos;26&apos;">
                              <span>
                                <xsl:text>Ton</xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;BX&apos;">
                              <span>
                                <xsl:text>Kutu</xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;LTR&apos;">
                              <span>
                                <xsl:text>
               LT
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;EA&apos;">
                              <span>
                                <xsl:text>
               Adet
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;NIU&apos;">
                              <span>
                                <xsl:text>
               Adet
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;C62&apos;">
                              <span>
                                <xsl:text>
               Adet
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;KGM&apos;">
                              <span>
                                <xsl:text>
               KG
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;KJO&apos;">
                              <span>
                                <xsl:text>
               kJ
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;GRM&apos;">
                              <span>
                                <xsl:text>
               GR
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;MGM&apos;">
                              <span>
                                <xsl:text>
               MG
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;NT&apos;">
                              <span>
                                <xsl:text>
               Net Ton
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;GT&apos;">
                              <span>
                                <xsl:text>
               GT
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;MTR&apos;">
                              <span>
                                <xsl:text>
               M
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;MMT&apos;">
                              <span>
                                <xsl:text>
               MM
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;KTM&apos;">
                              <span>
                                <xsl:text>
               KM
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;MLT&apos;">
                              <span>
                                <xsl:text>
               ML
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;MMQ&apos;">
                              <span>
                                <xsl:text>
               MM&#179;
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;CLT&apos;">
                              <span>
                                <xsl:text>
               CL
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;CMK&apos;">
                              <span>
                                <xsl:text>
               CM&#178;
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;CMQ&apos;">
                              <span>
                                <xsl:text>
               CM&#179;
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;CMT&apos;">
                              <span>
                                <xsl:text>
               CM
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;MTK&apos;">
                              <span>
                                <xsl:text>
               M&#178;
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;MTQ&apos;">
                              <span>
                                <xsl:text>
               M&#179;
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;DAY&apos;">
                              <span>
                                <xsl:text>
                G&#252;n
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;MON&apos;">
                              <span>
                                <xsl:text>
                Ay
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;PA&apos;">
                              <span>
                                <xsl:text>
                Paket
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;KWH&apos;">
                              <span>
                                <xsl:text>
                KWH
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;INH&apos;">
                              <span>
                                <xsl:text>
               IN
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;FOT&apos;">
                              <span>
                                <xsl:text>
               FT
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;MMK&apos;">
                              <span>
                                <xsl:text>
               MM&#178;
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;INK&apos;">
                              <span>
                                <xsl:text>
               IN&#178;
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;FTK&apos;">
                              <span>
                                <xsl:text>
               FT&#178;
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;INQ&apos;">
                              <span>
                                <xsl:text>
               IN&#179;
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;FTQ&apos;">
                              <span>
                                <xsl:text>
               FT&#179;
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;ONZ&apos;">
                              <span>
                                <xsl:text>
               OZ
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;LBR&apos;">
                              <span>
                                <xsl:text>
               LB
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:when test="@unitCode  = &apos;PR&apos;">
                              <span>
                                <xsl:text>
               &#199;ift
              </xsl:text>
                              </span>
                            </xsl:when>
                            <xsl:otherwise>
                              <span>
                                <xsl:value-of select="@unitCode" />
                              </span>
                            </xsl:otherwise>
                          </xsl:choose>
                        </xsl:for-each>
                      </xsl:if>
                    </span>
                  </span>
                </td>
                <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 13%; text-align: right;">
                  <span style="font-size: 10px;">
                    <span style="color: rgb(105, 105, 105);">
                      <xsl:value-of select="format-number(./cac:Price/cbc:PriceAmount, &apos;###.##0,0000&apos;, &apos;european&apos;)" />
                      <xsl:if test="./cac:Price/cbc:PriceAmount/@currencyID">
                        <xsl:text> </xsl:text>
                        <xsl:if test="./cac:Price/cbc:PriceAmount/@currencyID = &apos;TRY&apos; ">
                          <xsl:text>TL</xsl:text>
                        </xsl:if>
                        <xsl:if test="./cac:Price/cbc:PriceAmount/@currencyID != &apos;TRY&apos;">
                          <xsl:value-of select="./cac:Price/cbc:PriceAmount/@currencyID" />
                        </xsl:if>
                      </xsl:if>
                    </span>
                  </span>
                </td>
                <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 7%; text-align: right;">
                  <span style="font-size: 10px;">
                    <span style="color: rgb(105, 105, 105);">
                      <xsl:for-each select="./cac:AllowanceCharge">
                        <xsl:text> </xsl:text>
                        <table id="allowanceChargeRateTable" border="0" cellpadding="0" cellspacing="0" align="right" tagid="allowanceChargeRate" style="clear:both" width="100%">
                          <tbody>
                            <tr id="budgetContainerTr" align="right">
                              <td align="right">
                                <xsl:text>% </xsl:text>
                                <xsl:value-of select="format-number(cbc:MultiplierFactorNumeric*100, &apos;###.##0,00&apos;, &apos;european&apos;)" />
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </xsl:for-each>
                    </span>
                  </span>
                </td>
                <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 11%; text-align: right;">
                  <span style="font-size: 10px;">
                    <span style="color: rgb(105, 105, 105);">
                      <xsl:for-each select="./cac:AllowanceCharge">
                        <xsl:text></xsl:text>
                        <table id="allowanceChargeAmountTable" border="0" cellpadding="0" cellspacing="0" align="right" tagid="allowanceChargeAmount" style="clear:both" width="100%">
                          <tbody>
                            <tr id="budgetContainerTr" align="right">
                              <td align="right">
                                <xsl:value-of select="format-number(cbc:Amount, &apos;###.##0,00&apos;, &apos;european&apos;)" />
                                <xsl:if test="cbc:Amount/@currencyID">
                                  <xsl:text> </xsl:text>
                                  <xsl:if test="cbc:Amount/@currencyID = &apos;TRY&apos;">
                                    <xsl:text>TL</xsl:text>
                                  </xsl:if>
                                  <xsl:if test="cbc:Amount/@currencyID != &apos;TRY&apos;">
                                    <xsl:value-of select="cbc:Amount/@currencyID" />
                                  </xsl:if>
                                </xsl:if>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </xsl:for-each>
                    </span>
                  </span>
                </td>
                <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 8%; text-align: right;">
                  <span style="font-size: 10px;">
                    <span style="color: rgb(105, 105, 105);">
                      <xsl:for-each select="./cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cac:TaxScheme">
                        <xsl:if test="cbc:TaxTypeCode=&apos;0015&apos; ">
                          <xsl:if test="../../cbc:Percent">
                            <xsl:text>%</xsl:text>
                            <xsl:value-of select="format-number(../../cbc:Percent, &apos;###.##0,00&apos;, &apos;european&apos;)" />
                          </xsl:if>
                        </xsl:if>
                      </xsl:for-each>
                    </span>
                  </span>
                </td>
                <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 8%; text-align: right;">
                  <span style="font-size: 10px;">
                    <span style="color: rgb(105, 105, 105);">
                      <xsl:for-each select="./cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cac:TaxScheme">
                        <xsl:if test="cbc:TaxTypeCode=&apos;0015&apos; ">
                          <xsl:text> </xsl:text>
                          <xsl:value-of select="format-number(../../cbc:TaxAmount, &apos;###.##0,00&apos;, &apos;european&apos;)" />
                          <xsl:if test="../../cbc:TaxAmount/@currencyID">
                            <xsl:text> </xsl:text>
                            <xsl:if test="../../cbc:TaxAmount/@currencyID = &apos;TRY&apos;">
                              <xsl:text>TL</xsl:text>
                            </xsl:if>
                            <xsl:if test="../../cbc:TaxAmount/@currencyID != &apos;TRY&apos;">
                              <xsl:value-of select="../../cbc:TaxAmount/@currencyID" />
                            </xsl:if>
                          </xsl:if>
                        </xsl:if>
                      </xsl:for-each>
                    </span>
                  </span>
                </td>
                <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 4%; text-align: right;">
                  <span style="font-size: 10px;">
                    <span style="color: rgb(105, 105, 105);">
                      <xsl:for-each select="./cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cac:TaxScheme">
                        <xsl:if test="cbc:TaxTypeCode!=&apos;0015&apos; ">
                          <xsl:text> 
           </xsl:text>
                          <xsl:value-of select="cbc:Name" />
                          <xsl:if test="../../cbc:Percent">
                            <xsl:text>
              (%
            </xsl:text>
                            <xsl:value-of select="format-number(../../cbc:Percent, &apos;###.##0,00&apos;, &apos;european&apos;)" />
                            <xsl:text>
             )=
            </xsl:text>
                          </xsl:if>
                          <xsl:value-of select="format-number(../../cbc:TaxAmount, &apos;###.##0,00&apos;, &apos;european&apos;)" />
                          <xsl:if test="../../cbc:TaxAmount/@currencyID">
                            <xsl:text> 
            </xsl:text>
                            <xsl:if test="../../cbc:TaxAmount/@currencyID = &apos;TRY&apos;">
                              <xsl:text>
              TL
             </xsl:text>
                            </xsl:if>
                            <xsl:if test="../../cbc:TaxAmount/@currencyID != &apos;TRY&apos;">
                              <xsl:value-of select="../../cbc:TaxAmount/@currencyID" />
                            </xsl:if>
                          </xsl:if>
                        </xsl:if>
                      </xsl:for-each>
                      <xsl:for-each select="./cac:WithholdingTaxTotal/cac:TaxSubtotal">
                        <xsl:if test="not(cbc:TaxAmount = 0)">
                          <xsl:choose>
                            <xsl:when test="cac:TaxCategory/cac:TaxScheme/cbc:Name=&apos;&apos;">
                              <xsl:text>
              Di&#287;er Vergiler Toplam&#305; 
             </xsl:text>
                            </xsl:when>
                            <xsl:otherwise>
                              <xsl:text>
              Tevkifat (
             </xsl:text>
                              <xsl:value-of select="cac:TaxCategory/cac:TaxScheme/cbc:TaxTypeCode" />
                              <xsl:text>
              -
             </xsl:text>
                              <xsl:value-of select="cac:TaxCategory/cac:TaxScheme/cbc:Name" />
                              <xsl:text>
              )
             </xsl:text>
                            </xsl:otherwise>
                          </xsl:choose>
                          <xsl:if test="cbc:Percent !=&apos;&apos;">
                            <xsl:text>
             (%
            </xsl:text>
                            <xsl:value-of select=" format-number(cbc:Percent, &apos;###.##0,00&apos;, &apos;european&apos;)" />
                            <xsl:text>
             )= 
            </xsl:text>
                          </xsl:if>
                          <xsl:value-of select="format-number(../cbc:TaxAmount, &apos;###.##0,00&apos;, &apos;european&apos;)" />
                          <xsl:if test="../cbc:TaxAmount/@currencyID">
                            <xsl:if test="../cbc:TaxAmount/@currencyID = &apos;TRY&apos;">
                              <xsl:text>
              TL
             </xsl:text>
                            </xsl:if>
                            <xsl:if test="../cbc:TaxAmount/@currencyID != &apos;TRY&apos;">
                              <xsl:value-of select="../cbc:TaxAmount/@currencyID" />
                            </xsl:if>
                          </xsl:if>
                        </xsl:if>
                      </xsl:for-each>
                    </span>
                  </span>
                </td>
                <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 10.6%; text-align: right;">
                  <span style="font-size: 10px;">
                    <span style="color: rgb(105, 105, 105);">
                      <xsl:value-of select="format-number(./cbc:LineExtensionAmount, &apos;###.##0,00&apos;, &apos;european&apos;)" />
                      <xsl:if test="./cbc:LineExtensionAmount/@currencyID">
                        <xsl:text> 
          </xsl:text>
                        <xsl:if test="./cbc:LineExtensionAmount/@currencyID = &apos;TRY&apos; ">
                          <xsl:text>
            TL
           </xsl:text>
                        </xsl:if>
                        <xsl:if test="./cbc:LineExtensionAmount/@currencyID != &apos;TRY&apos; ">
                          <xsl:value-of select="./cbc:LineExtensionAmount/@currencyID">
                          </xsl:value-of>
                        </xsl:if>
                      </xsl:if>
                    </span>
                  </span>
                </td>
              </tr>
            </xsl:for-each>
            <tr>
              <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 3%; text-align: left;">&#160;</td>
              <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 6%; text-align: center;">&#160;</td>
              <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 10%; text-align: center;">&#160;</td>
              <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 10%; text-align: left;">
                <span style="font-size: 10px;">
                  <span style="color: rgb(105, 105, 105);">
                    <xsl:for-each select="//n1:Invoice/cac:AllowanceCharge">
                      <xsl:value-of select="./cbc:AllowanceChargeReason" />
                    </xsl:for-each>
                  </span>
                </span>
              </td>
              <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 7%; text-align: center;">&#160;</td>
              <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 6%; text-align: right;">&#160;</td>
              <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 13%; text-align: right;">&#160;</td>
              <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 7%; text-align: right;">
                <span style="font-size: 10px;">
                  <span style="color: rgb(105, 105, 105);">
                    <xsl:for-each select="//n1:Invoice/cac:AllowanceCharge">
                      <xsl:text></xsl:text>
                      <xsl:text>
          %
         </xsl:text>
                      <xsl:value-of select="format-number(cbc:MultiplierFactorNumeric*100, &apos;###.##0,00&apos;, &apos;european&apos;)" />
                    </xsl:for-each>
                  </span>
                </span>
              </td>
              <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 11%; text-align: right;">
                <span style="font-size: 10px;">
                  <span style="color: rgb(105, 105, 105);">
                    <xsl:for-each select="//n1:Invoice/cac:AllowanceCharge">
                      <xsl:value-of select="format-number(cbc:Amount, &apos;###.##0,00&apos;, &apos;european&apos;)" />
                      <xsl:if test="cbc:Amount/@currencyID">
                        <xsl:text></xsl:text>
                        <xsl:if test="cbc:Amount/@currencyID = &apos;TRY&apos;">
                          <xsl:text>
            TL
           </xsl:text>
                        </xsl:if>
                        <xsl:if test="cbc:Amount/@currencyID != &apos;TRY&apos;">
                          <xsl:value-of select="cbc:Amount/@currencyID" />
                        </xsl:if>
                      </xsl:if>
                    </xsl:for-each>
                  </span>
                </span>
              </td>
              <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 8%; text-align: right;">&#160;</td>
              <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 8%; text-align: right;">&#160;</td>
              <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none; width: 4%; text-align: right;">&#160;</td>
              <td style="border: 0.05em solid rgb(132, 132, 132); border-image: none;">&#160;</td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>
    <html>
      <head></head>
      <body>
        <table border="0" cellpadding="0" cellspacing="0" style="width: 800px;">
          <tbody>
            <tr>
              <td align="right">
                <table border="1" bordercolor="#a4a4a4" cellpadding="0" cellspacing="0" id="budgetContainerTable" style="width: 371px;" width="500">
                  <tbody>
                    <tr align="right" id="budgetContainerTr">
                      <td align="right" id="lineTableBudgetTd" style="width: 319px;" width="200">
                        <span style="font-size: 11px;">
                          <span style="color: rgb(105, 105, 105);">
                            <span style="font-weight: bold;">Mal / Hizmet Toplam Tutar&#305;</span>
                          </span>
                        </span>
                      </td>
                      <td align="right" id="lineTableBudgetTd" style="width: 120px;">
                        <span style="font-size: 11px;">
                          <span style="color: rgb(105, 105, 105);">
                            <xsl:value-of select="format-number(//n1:Invoice/cac:LegalMonetaryTotal/cbc:LineExtensionAmount, &apos;###.##0,00&apos;, &apos;european&apos;)" />
                            <xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:LineExtensionAmount/@currencyID">
                              <xsl:text> 
             </xsl:text>
                              <xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:LineExtensionAmount/@currencyID = &apos;TRY&apos;">
                                <xsl:text>
               TL
              </xsl:text>
                              </xsl:if>
                              <xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:LineExtensionAmount/@currencyID != &apos;TRY&apos;">
                                <xsl:value-of select="//n1:Invoice/cac:LegalMonetaryTotal/cbc:LineExtensionAmount/@currencyID" />
                              </xsl:if>
                            </xsl:if>
                          </span>
                        </span>
                      </td>
                    </tr>
                    <tr align="right" id="budgetContainerTr">
                      <td align="right" id="lineTableBudgetTd" style="width: 319px;" width="200">
                        <span style="font-size: 11px;">
                          <strong>
                            <span style="color: rgb(105, 105, 105);">
                              <xsl:text>
              Toplam &#304;skonto
             </xsl:text>
                              <xsl:if test="//n1:Invoice/cac:AllowanceCharge/cbc:MultiplierFactorNumeric">
                                <xsl:text>
               (%
              </xsl:text>
                                <xsl:value-of select="format-number((//n1:Invoice/cac:LegalMonetaryTotal/cbc:AllowanceTotalAmount*100) div //n1:Invoice/cac:LegalMonetaryTotal/cbc:LineExtensionAmount, &apos;###.##0,00&apos;, &apos;european&apos;)" />
                                <xsl:text>
               )
              </xsl:text>
                              </xsl:if>
                            </span>
                          </strong>
                        </span>
                      </td>
                      <td align="right" id="lineTableBudgetTd" style="width: 120px;">
                        <span style="font-size: 11px;">
                          <span style="color: rgb(105, 105, 105);">
                            <xsl:value-of select="format-number(//n1:Invoice/cac:LegalMonetaryTotal/cbc:AllowanceTotalAmount, &apos;###.##0,00&apos;, &apos;european&apos;)" />
                            <xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:AllowanceTotalAmount/@currencyID">
                              <xsl:text> 
             </xsl:text>
                              <xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:AllowanceTotalAmount/@currencyID = &apos;TRY&apos;">
                                <xsl:text>
               TL
              </xsl:text>
                              </xsl:if>
                              <xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:AllowanceTotalAmount/@currencyID != &apos;TRY&apos;">
                                <xsl:value-of select="//n1:Invoice/cac:LegalMonetaryTotal/cbc:AllowanceTotalAmount/@currencyID" />
                              </xsl:if>
                            </xsl:if>
                          </span>
                        </span>
                      </td>
                    </tr>
                    <tr align="right" id="budgetContainerTr">
                      <td align="right" id="lineTableBudgetTd" style="width: 319px;" width="200">
                        <span style="font-size: 11px;">
                          <strong>
                            <span style="color: rgb(105, 105, 105);">
                              <xsl:for-each select="//n1:Invoice/cac:TaxTotal/cac:TaxSubtotal">
                                <xsl:if test="not(cbc:Percent = 0.0000) or not(cbc:TaxAmount = 0)">
                                  <table id="taxSubtotalHeaderTable" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                      <tr align="right" style="border-bottom: 1px solid #000;">
                                        <td width="100%" align="right">
                                          <span style="font-weight:bold; ">
                                            <xsl:choose>
                                              <xsl:when test="cac:TaxCategory/cac:TaxScheme/cbc:Name=&apos;&apos;">
                                                <xsl:text>
                       Di&#65533;er Vergiler Toplam&#65533; 
                      </xsl:text>
                                              </xsl:when>
                                              <xsl:otherwise>
                                                <xsl:text>
                       Hesaplanan 
                      </xsl:text>
                                                <xsl:value-of select="cac:TaxCategory/cac:TaxScheme/cbc:Name" />
                                              </xsl:otherwise>
                                            </xsl:choose>
                                            <xsl:text>
                     (%
                    </xsl:text>
                                            <xsl:value-of select=" format-number(cbc:Percent, &apos;###.##0,00&apos;, &apos;european&apos;)" />
                                            <xsl:text>
                     )
                    </xsl:text>
                                          </span>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </xsl:if>
                              </xsl:for-each>
                            </span>
                          </strong>
                        </span>
                      </td>
                      <td align="right" id="lineTableBudgetTd" style="width: 120px;">
                        <span style="font-size: 11px;">
                          <span style="color: rgb(105, 105, 105);">
                            <xsl:for-each select="//n1:Invoice/cac:TaxTotal/cac:TaxSubtotal">
                              <xsl:if test="not(cbc:Percent = 0.0000) or not(cbc:TaxAmount = 0)">
                                <table id="taxSubtotalsTable" cellpadding="0" cellspacing="0" align="right" style="clear:both" width="100%">
                                  <tbody>
                                    <tr id="budgetContainerTr" align="right" style="border-bottom: 1px solid #000;">
                                      <td align="right">
                                        <xsl:for-each select="cac:TaxCategory/cac:TaxScheme">
                                          <xsl:value-of select="format-number(../../cbc:TaxAmount, &apos;###.##0,00&apos;, &apos;european&apos;)" />
                                          <xsl:if test="../../cbc:TaxAmount/@currencyID">
                                            <xsl:text> 
                    </xsl:text>
                                            <xsl:if test="../../cbc:TaxAmount/@currencyID = &apos;TRY&apos;">
                                              <xsl:text>
                      TL
                     </xsl:text>
                                            </xsl:if>
                                            <xsl:if test="../../cbc:TaxAmount/@currencyID != &apos;TRY&apos;">
                                              <xsl:value-of select="../../cbc:TaxAmount/@currencyID" />
                                            </xsl:if>
                                          </xsl:if>
                                        </xsl:for-each>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </xsl:if>
                            </xsl:for-each>
                          </span>
                        </span>
                      </td>
                    </tr>
                    <tr align="right" id="budgetContainerTr">
                      <td align="right" id="lineTableBudgetTd" style="width: 319px;" width="200">
                        <span style="font-size: 11px;">
                          <strong>
                            <span style="color: rgb(105, 105, 105);">
                              <xsl:for-each select="//n1:Invoice/cac:WithholdingTaxTotal/cac:TaxSubtotal">
                                <xsl:if test="not(cbc:Percent = 0.0000) or not(cbc:TaxAmount = 0)">
                                  <table id="taxSubtotalHeaderTable" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                      <tr align="right" style="width: 319px;" height="80">
                                        <td width="100%" align="right" style="border-bottom: 1px solid #a4a4a4;">
                                          <span style="font-weight:bold; ">
                                            <xsl:choose>
                                              <xsl:when test="cac:TaxCategory/cac:TaxScheme/cbc:Name=&apos;&apos;">
                                                <xsl:text>
                       Di&#287;er Vergiler Toplam&#305; 
                      </xsl:text>
                                              </xsl:when>
                                              <xsl:otherwise>
                                                <xsl:text>
                       Tevkifat 
                      </xsl:text>
                                                <xsl:value-of select="cac:TaxCategory/cac:TaxScheme/cbc:Name" />
                                              </xsl:otherwise>
                                            </xsl:choose>
                                            <xsl:text>
                     (%
                    </xsl:text>
                                            <xsl:value-of select=" format-number(cbc:Percent, &apos;###.##0,00&apos;, &apos;european&apos;)" />
                                            <xsl:text>
                     )
                    </xsl:text>
                                          </span>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </xsl:if>
                              </xsl:for-each>
                            </span>
                          </strong>
                        </span>
                      </td>
                      <td align="right" id="lineTableBudgetTd" style="width: 120px;">
                        <span style="font-size: 11px;">
                          <span style="color: rgb(105, 105, 105);">
                            <xsl:for-each select="//n1:Invoice/cac:WithholdingTaxTotal/cac:TaxSubtotal">
                              <xsl:if test="not(cbc:Percent = 0.0000) or not(cbc:TaxAmount = 0)">
                                <table id="taxSubtotalsTable" cellpadding="0" cellspacing="0" align="right" style="clear:both" width="100%">
                                  <tbody>
                                    <tr id="budgetContainerTr" align="right" height="80">
                                      <td align="right" style="border-bottom: 1px solid #a4a4a4;">
                                        <xsl:for-each select="cac:TaxCategory/cac:TaxScheme">
                                          <xsl:value-of select="format-number(../../cbc:TaxAmount, &apos;###.##0,00&apos;, &apos;european&apos;)" />
                                          <xsl:if test="../../cbc:TaxAmount/@currencyID">
                                            <xsl:text> 
                    </xsl:text>
                                            <xsl:if test="../../cbc:TaxAmount/@currencyID = &apos;TRY&apos;">
                                              <xsl:text>
                      TL
                     </xsl:text>
                                            </xsl:if>
                                            <xsl:if test="../../cbc:TaxAmount/@currencyID != &apos;TRY&apos;">
                                              <xsl:value-of select="../../cbc:TaxAmount/@currencyID" />
                                            </xsl:if>
                                          </xsl:if>
                                        </xsl:for-each>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </xsl:if>
                            </xsl:for-each>
                          </span>
                        </span>
                      </td>
                    </tr>
                    <tr align="right" id="budgetContainerTr">
                      <td align="right" id="lineTableBudgetTd" style="width: 319px;" width="200">
                        <span style="font-size: 11px;">
                          <span style="color: rgb(105, 105, 105);">
                            <span style="font-weight: bold;">Vergiler Dahil Toplam Tutar</span>
                          </span>
                        </span>
                      </td>
                      <td align="right" id="lineTableBudgetTd" style="width: 120px;">
                        <span style="font-size: 11px;">
                          <span style="color: rgb(105, 105, 105);">
                            <xsl:for-each select="//n1:Invoice">
                              <xsl:for-each select="cac:LegalMonetaryTotal">
                                <xsl:for-each select="cbc:TaxInclusiveAmount">
                                  <xsl:value-of select="format-number(., &apos;###.##0,00&apos;, &apos;european&apos;)" />
                                  <xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:TaxInclusiveAmount/@currencyID">
                                    <xsl:text> 
                </xsl:text>
                                    <xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:TaxInclusiveAmount/@currencyID = &apos;TRY&apos;">
                                      <xsl:text>
                  TL
                 </xsl:text>
                                    </xsl:if>
                                    <xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:TaxInclusiveAmount/@currencyID != &apos;TRY&apos;">
                                      <xsl:value-of select="//n1:Invoice/cac:LegalMonetaryTotal/cbc:TaxInclusiveAmount/@currencyID">
                                      </xsl:value-of>
                                    </xsl:if>
                                  </xsl:if>
                                </xsl:for-each>
                              </xsl:for-each>
                            </xsl:for-each>
                          </span>
                        </span>
                      </td>
                    </tr>
                    <tr align="right" id="budgetContainerTr">
                      <td align="right" id="lineTableBudgetTd" style="width: 319px;" width="200">
                        <span style="font-size: 11px;">
                          <span style="color: rgb(105, 105, 105);">
                            <span style="font-weight: bold;">&#214;denecek Tutar</span>
                          </span>
                        </span>
                      </td>
                      <td align="right" id="lineTableBudgetTd" style="width: 120px;">
                        <span style="font-size: 11px;">
                          <span style="color: rgb(105, 105, 105);">
                            <xsl:for-each select="//n1:Invoice">
                              <xsl:for-each select="cac:LegalMonetaryTotal">
                                <xsl:for-each select="cbc:PayableAmount">
                                  <xsl:value-of select="format-number(., &apos;###.##0,00&apos;, &apos;european&apos;)" />
                                  <xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:PayableAmount/@currencyID">
                                    <xsl:text> 
                </xsl:text>
                                    <xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:PayableAmount/@currencyID = &apos;TRY&apos;">
                                      <xsl:text>
                  TL
                 </xsl:text>
                                    </xsl:if>
                                    <xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:PayableAmount/@currencyID != &apos;TRY&apos;">
                                      <xsl:value-of select="//n1:Invoice/cac:LegalMonetaryTotal/cbc:PayableAmount/@currencyID" />
                                    </xsl:if>
                                  </xsl:if>
                                </xsl:for-each>
                              </xsl:for-each>
                            </xsl:for-each>
                          </span>
                        </span>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td align="right">
                <xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:LineExtensionAmount/@currencyID != &apos;TRY&apos;">
                  <table border="1" bordercolor="#a4a4a4" cellpadding="0" cellspacing="0" condition="//n1:Invoice/cac:LegalMonetaryTotal/cbc:LineExtensionAmount/@currencyID != &apos;TRY&apos;" id="budgetContainerTable" style="width: 371px;" width="500">
                    <tbody>
                      <tr align="right" id="budgetContainerTr">
                        <td align="right" id="lineTableBudgetTd" style="width: 319px;" width="200">
                          <span style="font-size: 11px;">
                            <span style="color: rgb(105, 105, 105);">
                              <span style="font-weight: bold;">D&#246;viz Kuru (</span>
                              <strong>
                                <xsl:value-of select="//n1:Invoice/cac:PricingExchangeRate/cbc:TargetCurrencyCode" />
                              </strong>
                              <span style="font-weight: bold;">)</span>
                            </span>
                          </span>
                        </td>
                        <td align="right" id="lineTableBudgetTd" style="width: 120px;">
                          <span style="font-size: 11px;">
                            <span style="color: rgb(105, 105, 105);">
                              <xsl:value-of select="format-number(//n1:Invoice/cac:PricingExchangeRate/cbc:CalculationRate, &apos;###.##0,0000&apos;, &apos;european&apos;)" />
                              <xsl:text> 
             </xsl:text>
                              <xsl:text>
              TL
             </xsl:text>
                            </span>
                          </span>
                        </td>
                      </tr>
                      <tr align="right" id="budgetContainerTr">
                        <td align="right" id="lineTableBudgetTd" style="width: 319px;" width="200">
                          <span style="font-size: 11px;">
                            <span style="color: rgb(105, 105, 105);">
                              <span style="font-weight: bold;">Mal / Hizmet Toplam Tutar&#305;</span>
                            </span>
                          </span>
                        </td>
                        <td align="right" id="lineTableBudgetTd" style="width: 120px;">
                          <span style="font-size: 11px;">
                            <span style="color: rgb(105, 105, 105);">
                              <xsl:for-each select="//n1:Invoice/cac:AdditionalDocumentReference">
                                <xsl:if test="cbc:DocumentType = &apos;LINEEXTENSIONAMOUNT&apos;">
                                  <xsl:value-of select="format-number(cbc:ID, &apos;###.##0,00&apos;, &apos;european&apos;)" />
                                </xsl:if>
                              </xsl:for-each>
                              <xsl:text> 
             </xsl:text>
                              <xsl:text>
              TL
             </xsl:text>
                            </span>
                          </span>
                        </td>
                      </tr>
                      <tr align="right" id="budgetContainerTr">
                        <td align="right" id="lineTableBudgetTd" style="width: 319px;" width="200">
                          <span style="font-size: 11px;">
                            <span style="color: rgb(105, 105, 105);">
                              <span style="font-weight: bold;">Toplam &#304;skonto</span>
                            </span>
                          </span>
                        </td>
                        <td align="right" id="lineTableBudgetTd" style="width: 120px;">
                          <span style="font-size: 11px;">
                            <span style="color: rgb(105, 105, 105);">
                              <xsl:for-each select="//n1:Invoice/cac:AdditionalDocumentReference">
                                <xsl:if test="cbc:DocumentType = &apos;ALLOWANCETOTALAMOUNT&apos;">
                                  <xsl:value-of select="format-number(cbc:ID, &apos;###.##0,00&apos;, &apos;european&apos;)" />
                                </xsl:if>
                              </xsl:for-each>
                              <xsl:text> 
             </xsl:text>
                              <xsl:text>
              TL
             </xsl:text>
                            </span>
                          </span>
                        </td>
                      </tr>
                      <tr align="right" id="budgetContainerTr">
                        <td align="right" id="lineTableBudgetTd" style="width: 319px;" width="200">
                          <span style="font-size: 11px;">
                            <span style="color: rgb(105, 105, 105);">
                              <strong>
                                <xsl:for-each select="//n1:Invoice/cac:TaxTotal/cac:TaxSubtotal">
                                  <xsl:if test="not(cbc:Percent = 0.0000) or not(cbc:TaxAmount = 0)">
                                    <table id="taxSubtotalHeaderTable" border="0" cellpadding="0" cellspacing="0" width="190">
                                      <tbody>
                                        <tr align="right">
                                          <td width="211px" align="right">
                                            <span style="font-weight:bold; ">
                                              <xsl:choose>
                                                <xsl:when test="cac:TaxCategory/cac:TaxScheme/cbc:Name=&apos;&apos;">
                                                  <xsl:text>
                        Di&#287;er Vergiler Toplam&#305; 
                       </xsl:text>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                  <xsl:text>
                        Hesaplanan 
                       </xsl:text>
                                                  <xsl:value-of select="cac:TaxCategory/cac:TaxScheme/cbc:Name" />
                                                </xsl:otherwise>
                                              </xsl:choose>
                                              <xsl:text>
                      (%
                     </xsl:text>
                                              <xsl:value-of select=" format-number(cbc:Percent, &apos;###.##0,00&apos;, &apos;european&apos;)" />
                                              <xsl:text>
                      )
                     </xsl:text>
                                            </span>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </xsl:if>
                                </xsl:for-each>
                                <xsl:for-each select="//n1:Invoice/cac:WithholdingTaxTotal/cac:TaxSubtotal">
                                  <xsl:if test="not(cbc:Percent = 0.0000) or not(cbc:TaxAmount = 0)" >
                                  </xsl:if>
                                  <tbody>
                                    <tr align="right">
                                      <td width="211px" align="right">
                                        <span style="font-weight:bold; ">
                                          <xsl:choose>
                                            <xsl:when test="cac:TaxCategory/cac:TaxScheme/cbc:Name=&apos;&apos;">
                                              <xsl:text>
                      Di&#287;er Vergiler Toplam&#305; 
                     </xsl:text>
                                            </xsl:when>
                                            <xsl:otherwise>
                                              <xsl:text>
                      Tevkifat 
                     </xsl:text>
                                              <xsl:value-of select="cac:TaxCategory/cac:TaxScheme/cbc:Name" />
                                            </xsl:otherwise>
                                          </xsl:choose>
                                          <xsl:text>
                    (%
                   </xsl:text>
                                          <xsl:value-of select=" format-number(cbc:Percent, &apos;###.##0,00&apos;, &apos;european&apos;)" />
                                          <xsl:text>
                    )
                   </xsl:text>
                                        </span>
                                      </td>
                                    </tr>
                                  </tbody>
                                </xsl:for-each>
                                <table id="taxSubtotalHeaderTable" border="0" cellpadding="0" cellspacing="0" width="190" tagid="taxtable">
                                </table>
                              </strong>
                            </span>
                          </span>
                        </td>
                        <td align="right" id="lineTableBudgetTd" style="width: 120px;">
                          <span style="font-size: 11px;">
                            <span style="color: rgb(105, 105, 105);">
                              <xsl:for-each select="//n1:Invoice/cac:TaxTotal/cac:TaxSubtotal">
                                <xsl:variable name="var_Percent" select="cbc:Percent"></xsl:variable>
                                <xsl:variable name="var_TaxTypeCode" select="cac:TaxCategory/cac:TaxScheme/cbc:TaxTypeCode"></xsl:variable>
                                <xsl:text> 
              </xsl:text>
                                <xsl:for-each select="//n1:Invoice/cac:AdditionalDocumentReference">
                                  <xsl:variable name="var_DocumentType" select="cbc:DocumentType"></xsl:variable>
                                  <xsl:variable name="var_DocumentTypeCode" select="cbc:DocumentTypeCode"></xsl:variable>
                                  <xsl:if test="$var_DocumentType = $var_Percent and $var_DocumentTypeCode = $var_TaxTypeCode and not($var_Percent = &apos;0.0000&apos;) ">
                                    <table id="taxSubtotalsTable" border="0" cellpadding="0" cellspacing="0" align="right" tagid="taxtable" style="clear:both" width="100%">
                                      <tbody>
                                        <tr id="budgetContainerTr" align="right">
                                          <td align="right">
                                            <xsl:value-of select="format-number(cbc:ID, &apos;###.##0,00&apos;, &apos;european&apos;)" />
                                            <xsl:text> 
                    </xsl:text>
                                            <xsl:text>
                      TL 
                    </xsl:text>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </xsl:if>
                                </xsl:for-each>
                              </xsl:for-each>
                            </span>
                          </span>
                        </td>
                      </tr>
                      <tr align="right" id="budgetContainerTr">
                        <td align="right" id="lineTableBudgetTd" style="width: 319px;" width="200">
                          <span style="font-size: 11px;">
                            <span style="color: rgb(105, 105, 105);">
                              <strong>
                                <xsl:for-each select="//n1:Invoice">
                                  <xsl:for-each select="cac:LegalMonetaryTotal">
                                    <xsl:for-each select="cbc:TaxExclusiveAmount">
                                      <xsl:value-of select="format-number(.*//n1:Invoice/cac:PricingExchangeRate/cbc:CalculationRate, &apos;###.##0,00&apos;, &apos;european&apos;)" />
                                      <xsl:text> 
                 </xsl:text>
                                      <xsl:text>
                  TL
                 </xsl:text>
                                    </xsl:for-each>
                                  </xsl:for-each>
                                </xsl:for-each>
                              </strong>
                            </span>
                          </span>
                        </td>
                        <td align="right" id="lineTableBudgetTd" style="width: 120px;">
                          <span style="font-size: 11px;">
                            <span style="color: rgb(105, 105, 105);">
                              <xsl:for-each select="//n1:Invoice/cac:WithholdingTaxTotal/cac:TaxSubtotal">
                                <xsl:variable name="var_Percent" select="cbc:Percent"></xsl:variable>
                                <xsl:variable name="var_TaxTypeCode" select="cac:TaxCategory/cac:TaxScheme/cbc:TaxTypeCode"></xsl:variable>
                                <xsl:text> 
              </xsl:text>
                                <xsl:for-each select="//n1:Invoice/cac:AdditionalDocumentReference">
                                  <xsl:variable name="var_DocumentType" select="cbc:DocumentType"></xsl:variable>
                                  <xsl:variable name="var_DocumentTypeCode" select="cbc:DocumentTypeCode"></xsl:variable>
                                  <xsl:choose>
                                    <xsl:when test="string(number($var_Percent)) != &apos;NaN&apos; and string(number($var_DocumentType)) != &apos;NaN&apos;">
                                      <xsl:if test="(number($var_DocumentType) = number($var_Percent)) and $var_DocumentTypeCode = $var_TaxTypeCode and not(number($var_Percent) = 0) ">
                                        <table id="taxSubtotalsTable" border="0" cellpadding="0" cellspacing="0" align="right" tagid="taxtable" style="clear:both" width="100%">
                                          <tbody>
                                            <tr id="budgetContainerTr" align="right" height="80">
                                              <td align="right" style="border-bottom: 1px solid #a4a4a4;">
                                                <xsl:value-of select="format-number(cbc:ID, &apos;###.##0,00&apos;, &apos;european&apos;)" />
                                                <xsl:text> 
                      </xsl:text>
                                                <xsl:text>
                        TL 
                      </xsl:text>
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </xsl:if>
                                    </xsl:when>
                                    <xsl:otherwise>
                                      <xsl:if test="$var_DocumentType = $var_Percent and $var_DocumentTypeCode = $var_TaxTypeCode and not($var_Percent = &apos;0.0000&apos;) ">
                                        <table id="taxSubtotalsTable" border="0" cellpadding="0" cellspacing="0" align="right" tagid="taxtable" style="clear:both" width="100%">
                                          <tbody>
                                            <tr id="budgetContainerTr" align="right" height="80">
                                              <td align="right" style="border-bottom: 1px solid #a4a4a4;">
                                                <xsl:value-of select="format-number(cbc:ID, &apos;###.##0,00&apos;, &apos;european&apos;)" />
                                                <xsl:text> 
                      </xsl:text>
                                                <xsl:text>
                        TL 
                      </xsl:text>
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </xsl:if>
                                    </xsl:otherwise>
                                  </xsl:choose>
                                </xsl:for-each>
                              </xsl:for-each>
                            </span>
                          </span>
                        </td>
                      </tr>
                      <tr align="right" id="budgetContainerTr">
                        <td align="right" id="lineTableBudgetTd" style="width: 319px;" width="200">
                          <span style="font-size: 11px;">
                            <span style="color: rgb(105, 105, 105);">
                              <span style="font-weight: bold;">Vergiler Dahil Toplam Tutar</span>
                            </span>
                          </span>
                        </td>
                        <td align="right" id="lineTableBudgetTd" style="width: 120px;">
                          <span style="font-size: 11px;">
                            <span style="color: rgb(105, 105, 105);">
                              <xsl:for-each select="//n1:Invoice/cac:AdditionalDocumentReference">
                                <xsl:if test="cbc:DocumentType = &apos;TAXINCLUSIVEAMOUNT&apos;">
                                  <xsl:value-of select="format-number(cbc:ID, &apos;###.##0,00&apos;, &apos;european&apos;)" />
                                </xsl:if>
                              </xsl:for-each>
                              <xsl:text> 
             </xsl:text>
                              <xsl:text>
              TL
             </xsl:text>
                            </span>
                          </span>
                        </td>
                      </tr>
                      <tr align="right" id="budgetContainerTr">
                        <td align="right" id="lineTableBudgetTd" style="width: 319px;" width="200">
                          <span style="font-size: 11px;">
                            <span style="color: rgb(105, 105, 105);">
                              <span style="font-weight: bold;">&#214;denecek Tutar</span>
                            </span>
                          </span>
                        </td>
                        <td align="right" id="lineTableBudgetTd" style="width: 120px;">
                          <span style="font-size: 11px;">
                            <span style="color: rgb(105, 105, 105);">
                              <xsl:for-each select="//n1:Invoice/cac:AdditionalDocumentReference">
                                <xsl:if test="cbc:DocumentType = &apos;PAYABLEAMOUNT&apos;">
                                  <xsl:value-of select="format-number(cbc:ID, &apos;###.##0,00&apos;, &apos;european&apos;)" />
                                </xsl:if>
                              </xsl:for-each>
                              <xsl:text> 
             </xsl:text>
                              <xsl:text>
              TL
             </xsl:text>
                            </span>
                          </span>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </xsl:if>
              </td>
            </tr>
          </tbody>
        </table>
        <p>&#160;</p>
        <table border="1" cellpadding="0" cellspacing="0" style="width: 800px; margin-top: 6px;">
          <tbody>
            <tr>
              <td align="left">
                <p style="margin-left: 40px;">
                  <br />
                  <span style="font-size: 11px;">
                    <span style="color: rgb(105, 105, 105);">
                      <span style="font-family: tahoma,geneva,sans-serif;">
                        <xsl:if test="//n1:Invoice/cac:PaymentMeans/cbc:InstructionNote">
                          <b>&#214;deme Notu:</b>
                          <xsl:value-of select="//n1:Invoice/cac:PaymentMeans/cbc:InstructionNote" />
                          <br />
                        </xsl:if>
                        <xsl:if test="//n1:Invoice/cac:PaymentMeans/cac:PayeeFinancialAccount/cbc:PaymentNote">
                          <b>Hesap A&#231;&#305;klamas&#305;:</b>
                          <xsl:value-of select="//n1:Invoice/cac:PaymentMeans/cac:PayeeFinancialAccount/cbc:PaymentNote" />
                          <br />
                        </xsl:if>
                        <xsl:if test="//n1:Invoice/cac:PaymentTerms/cbc:Note">
                          <b>&#214;deme Ko&#351;ulu:</b>
                          <xsl:value-of select="//n1:Invoice/cac:PaymentTerms/cbc:Note" />
                          <br />
                        </xsl:if>
                        <xsl:if test="//n1:Invoice/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cbc:TaxExemptionReason">
                          <b>Vergi Muafiyet Sebebi</b>
                          <xsl:value-of select="//n1:Invoice/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cbc:TaxExemptionReason" />
                          <br />
                        </xsl:if>
                        <br />
                        <br />
                        <xsl:if test="//n1:Invoice/cbc:Note">
                          <b>Genel A&#231;&#305;klamalar</b>
                          <br />
                          <xsl:for-each select="//n1:Invoice/cbc:Note">
                            <xsl:value-of select="." />
                            <br />
                          </xsl:for-each>
                        </xsl:if>
                        <br />
                        <br />
                        <xsl:if test="//n1:Invoice/cac:BillingReference/cac:AdditionalDocumentReference/cbc:DocumentTypeCode  = &apos;OKCBF&apos;">
                          <b>&#214;KC Bilgi Fisi Bilgileri:</b>
                          <br />
                          <b>Fis No: </b>
                          <xsl:value-of select="//n1:Invoice/cac:BillingReference/cac:AdditionalDocumentReference/cbc:ID" />,
                          <b>Fis Tarihi: </b>
                          <xsl:value-of select="//n1:Invoice/cac:BillingReference/cac:AdditionalDocumentReference/cbc:IssueDate" />,
                          <b>Fis Saati: </b>
                          <xsl:value-of select="//n1:Invoice/cac:BillingReference/cac:AdditionalDocumentReference/cac:ValidityPeriod/cbc:StartTime" />,
                          <b>Fis Tipi: </b>
                          <xsl:value-of select="//n1:Invoice/cac:BillingReference/cac:AdditionalDocumentReference/cbc:DocumentDescription" />,
                          <b>Z Rapor No: </b>
                          <xsl:value-of select="//n1:Invoice/cac:BillingReference/cac:AdditionalDocumentReference/cac:Attachment/cac:ExternalReference/cbc:URI" />,
                          <b>&#214;KC Seri No: </b>
                          <xsl:value-of select="//n1:Invoice/cac:BillingReference/cac:AdditionalDocumentReference/cac:IssuerParty/cbc:EndpointID" />
                          <br />
                        </xsl:if>
                      </span>
                    </span>
                  </span>
                </p>
              </td>
            </tr>
          </tbody>
        </table>
        <xsl:if test="//n1:Invoice/cac:AdditionalDocumentReference[cbc:ID = &apos;EINVOICE&apos;]/cbc:DocumentType=3">
          <table border="1" cellpadding="0" cellspacing="0" condition="//n1:Invoice/cac:AdditionalDocumentReference[cbc:ID = &apos;EINVOICE&apos;]/cbc:DocumentType=3" style="width: 800px; margin-top: 4px;">
            <tbody>
              <tr>
                <td>
                  <p>
                    <span style="font-size: 11px;">
                      <span style="color: rgb(105, 105, 105);">
                        <span style="font-family: tahoma,geneva,sans-serif;">
                          <strong>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#304;nternet Sat&#305;&#351; Bilgileri:</strong>
                        </span>
                      </span>
                    </span>
                    <br />
                    <span style="font-size: 11px;">
                      <span style="color: rgb(105, 105, 105);">
                        <span style="font-family: tahoma,geneva,sans-serif;">&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Bu sat&#305;&#351; internet &#252;zerinden yap&#305;lm&#305;&#351;t&#305;r.</span>
                      </span>
                    </span>
                    <br />
                    <br />
                    <span condition="normalize-space(//n1:Invoice/cac:AdditionalDocumentReference[cbc:ID = &apos;internetSatisBilgi/webAdresi&apos;]/cbc:DocumentType)" style="margin-left: 40px;">
                      <span style="font-size: 11px;">
                        <span style="color: rgb(105, 105, 105);">
                          <span style="font-family: tahoma,geneva,sans-serif;">
                            <strong>Sat&#305;&#351; &#304;&#351;leminin Yap&#305;ld&#305;&#287;&#305; Web Adresi: </strong>
                            <xsl:value-of select="//n1:Invoice/cac:AdditionalDocumentReference[cbc:ID = &apos;internetSatisBilgi/webAdresi&apos;]/cbc:DocumentType" />
                          </span>
                        </span>
                      </span>
                    </span>
                    <br />
                    <span condition="normalize-space(//n1:Invoice/cac:AdditionalDocumentReference[cbc:ID = &apos;internetSatisBilgi/odemeAracisiAdi&apos;]/cbc:DocumentType)" style="margin-left: 40px;">
                      <span style="font-size: 11px;">
                        <span style="color: rgb(105, 105, 105);">
                          <span style="font-family: tahoma,geneva,sans-serif;">
                            <strong>&#214;deme Arac&#305;s&#305;: </strong>
                            <xsl:value-of select="//n1:Invoice/cac:AdditionalDocumentReference[cbc:ID = &apos;internetSatisBilgi/odemeAracisiAdi&apos;]/cbc:DocumentType" />
                          </span>
                        </span>
                      </span>
                    </span>
                    <br />
                    <span condition="normalize-space(//n1:Invoice/cac:AdditionalDocumentReference[cbc:ID = &apos;internetSatisBilgi/odemeSekli&apos;]/cbc:DocumentType)" style="line-height: 1.6em; margin-left: 40px;">
                      <span style="font-size: 11px;">
                        <span style="color: rgb(105, 105, 105);">
                          <span style="font-family: tahoma,geneva,sans-serif;">
                            <strong>&#214;deme &#351;ekli: </strong>
                            <xsl:value-of select="//n1:Invoice/cac:AdditionalDocumentReference[cbc:ID = &apos;internetSatisBilgi/odemeSekli&apos;]/cbc:DocumentType" />
                          </span>
                        </span>
                      </span>
                    </span>
                    <br />
                    <span condition="normalize-space(//n1:Invoice/cac:AdditionalDocumentReference[cbc:ID = &apos;internetSatisBilgi/odemeTarihi&apos;]/cbc:DocumentType)" style="line-height: 1.6em; margin-left: 40px;">
                      <span style="font-size: 11px;">
                        <span style="color: rgb(105, 105, 105);">
                          <span style="font-family: tahoma,geneva,sans-serif;">
                            <strong>&#214;deme Tarihi: </strong>
                            <xsl:value-of select="//n1:Invoice/cac:AdditionalDocumentReference[cbc:ID = &apos;internetSatisBilgi/odemeTarihi&apos;]/cbc:DocumentType" />
                          </span>
                        </span>
                      </span>
                    </span>
                    <br />
                    <span condition="normalize-space(//n1:Invoice/cac:AdditionalDocumentReference[cbc:ID = &apos;internetSatisBilgi/gonderiBilgileri/gonderimTarihi&apos;]/cbc:DocumentType)" style="line-height: 1.6em; margin-left: 40px;">
                      <span style="font-size: 11px;">
                        <span style="color: rgb(105, 105, 105);">
                          <span style="font-family: tahoma,geneva,sans-serif;">
                            <strong>G&#246;nderim Tarihi: </strong>
                            <xsl:value-of select="//n1:Invoice/cac:AdditionalDocumentReference[cbc:ID = &apos;internetSatisBilgi/gonderiBilgileri/gonderimTarihi&apos;]/cbc:DocumentType" />
                          </span>
                        </span>
                      </span>
                    </span>
                    <br />
                    <span condition="normalize-space(//n1:Invoice/cac:AdditionalDocumentReference[cbc:ID = &apos;internetSatisBilgi/gonderiBilgileri/gonderiTasiyan/gercekKisi/tckn&apos;]/cbc:DocumentType)" style="line-height: 1.6em; margin-left: 40px;">
                      <span style="font-size: 11px;">
                        <span style="color: rgb(105, 105, 105);">
                          <span style="font-family: tahoma,geneva,sans-serif;">
                            <strong>G&#246;nderiyi ta&#351;&#305;yan TCKN: </strong>
                            <xsl:value-of select="//n1:Invoice/cac:AdditionalDocumentReference[cbc:ID = &apos;internetSatisBilgi/gonderiBilgileri/gonderiTasiyan/gercekKisi/tckn&apos;]/cbc:DocumentType" />
                          </span>
                        </span>
                      </span>
                    </span>
                    <br />
                    <span condition="normalize-space(//n1:Invoice/cac:AdditionalDocumentReference[cbc:ID = &apos;internetSatisBilgi/gonderiBilgileri/gonderiTasiyan/tuzelKisi/vkn&apos;]/cbc:DocumentType)" style="line-height: 1.6em; margin-left: 40px;">
                      <span style="font-size: 11px;">
                        <span style="color: rgb(105, 105, 105);">
                          <span style="font-family: tahoma,geneva,sans-serif;">
                            <strong>G&#246;nderiyi ta&#351;&#305;yan VKN: </strong>
                            <xsl:value-of select="//n1:Invoice/cac:AdditionalDocumentReference[cbc:ID = &apos;internetSatisBilgi/gonderiBilgileri/gonderiTasiyan/tuzelKisi/vkn&apos;]/cbc:DocumentType" />
                          </span>
                        </span>
                      </span>
                    </span>
                    <br />
                    <span condition="normalize-space(//n1:Invoice/cac:AdditionalDocumentReference[cbc:ID = &apos;internetSatisBilgi/gonderiBilgileri/gonderiTasiyan/gercekKisi/adiSoyadi&apos;]/cbc:DocumentType)" style="line-height: 1.6em; margin-left: 40px;">
                      <span style="font-size: 11px;">
                        <span style="color: rgb(105, 105, 105);">
                          <span style="font-family: tahoma,geneva,sans-serif;">
                            <strong>G&#246;nderiyi ta&#351;&#305;yan ad&#305; soyad&#305;: </strong>
                            <xsl:value-of select="//n1:Invoice/cac:AdditionalDocumentReference[cbc:ID = &apos;internetSatisBilgi/gonderiBilgileri/gonderiTasiyan/gercekKisi/adiSoyadi&apos;]/cbc:DocumentType" />
                          </span>
                        </span>
                      </span>
                    </span>
                    <br />
                    <span condition="normalize-space(//n1:Invoice/cac:AdditionalDocumentReference[cbc:ID = &apos;internetSatisBilgi/gonderiBilgileri/gonderiTasiyan/tuzelKisi/unvan&apos;]/cbc:DocumentType)" style="margin-left: 40px;">
                      <span style="font-size: 11px;">
                        <span style="color: rgb(105, 105, 105);">
                          <span style="font-family: tahoma,geneva,sans-serif;">
                            <strong>G&#246;nderiyi ta&#351;&#305;yan unvan&#305;: </strong>
                            <xsl:value-of select="//n1:Invoice/cac:AdditionalDocumentReference[cbc:ID = &apos;internetSatisBilgi/gonderiBilgileri/gonderiTasiyan/tuzelKisi/unvan&apos;]/cbc:DocumentType" />
                          </span>
                        </span>
                      </span>
                    </span>
                  </p>
                  <table border="0" cellpadding="0" cellspacing="0" condition="//n1:Invoice/cac:AdditionalDocumentReference/cbc:ID =&apos;EINVOICE&apos; and //n1:Invoice/cac:AdditionalDocumentReference/cbc:DocumentType =3" style="margin-top: 30px;">
                    <tbody>
                      <tr>
                        <td>
                          <span style="margin-left: 40px;">
                            <span style="font-size: 11px;">
                              <span style="color: rgb(105, 105, 105);">
                                <span style="font-family: tahoma,geneva,sans-serif;">
                                  <strong>&#160;&#160;&#160;&#160;&#160;&#304;ade B&#246;l&#252;m&#252;</strong>
                                </span>
                              </span>
                            </span>
                          </span>
                          <table border="1" bordercolor="#a4a4a4" cellpadding="0" cellspacing="0" style="margin-left: 40px;">
                            <tbody>
                              <tr id="budgetContainerTr">
                                <td id="lineTableBudgetTd" style="width: 290px;">
                                  <span style="margin-left: 40px;">
                                    <span style="font-size: 11px;">
                                      <span style="color: rgb(105, 105, 105);">
                                        <span style="font-family: tahoma,geneva,sans-serif;">
                                          <span style="font-weight: bold;">Mal&#305;n Cinsi</span>
                                        </span>
                                      </span>
                                    </span>
                                  </span>
                                </td>
                                <td id="lineTableBudgetTd" style="width: 100px;">
                                  <span style="margin-left: 40px;">
                                    <span style="font-size: 11px;">
                                      <span style="color: rgb(105, 105, 105);">
                                        <span style="font-family: tahoma,geneva,sans-serif;">
                                          <span style="font-weight: bold;">Mal&#305;n Miktar&#305;</span>
                                        </span>
                                      </span>
                                    </span>
                                  </span>
                                </td>
                                <td id="lineTableBudgetTd" style="width: 150px;">
                                  <span style="margin-left: 40px;">
                                    <span style="font-size: 11px;">
                                      <span style="color: rgb(105, 105, 105);">
                                        <span style="font-family: tahoma,geneva,sans-serif;">
                                          <span style="font-weight: bold;">Birim Fiyat&#305;</span>
                                        </span>
                                      </span>
                                    </span>
                                  </span>
                                </td>
                                <td id="lineTableBudgetTd" style="width: 150px;">
                                  <span style="margin-left: 40px;">
                                    <span style="font-size: 11px;">
                                      <span style="color: rgb(105, 105, 105);">
                                        <span style="font-family: tahoma,geneva,sans-serif;">
                                          <span style="font-weight: bold;">Tutar</span>
                                        </span>
                                      </span>
                                    </span>
                                  </span>
                                </td>
                              </tr>
                              <tr id="budgetContainerTr">
                                <td id="lineTableBudgetTd">
                                  <span style="margin-left: 40px;">&#160;</span>
                                </td>
                                <td id="lineTableBudgetTd">
                                  <span style="margin-left: 40px;">&#160;</span>
                                </td>
                                <td id="lineTableBudgetTd">
                                  <span style="margin-left: 40px;">&#160;</span>
                                </td>
                                <td id="lineTableBudgetTd">
                                  <span style="margin-left: 40px;">&#160;</span>
                                </td>
                              </tr>
                              <tr id="budgetContainerTr">
                                <td id="lineTableBudgetTd">
                                  <span style="margin-left: 40px;">&#160;</span>
                                </td>
                                <td id="lineTableBudgetTd">
                                  <span style="margin-left: 40px;">&#160;</span>
                                </td>
                                <td id="lineTableBudgetTd">
                                  <span style="margin-left: 40px;">&#160;</span>
                                </td>
                                <td id="lineTableBudgetTd">
                                  <span style="margin-left: 40px;">&#160;</span>
                                </td>
                              </tr>
                              <tr id="budgetContainerTr">
                                <td id="lineTableBudgetTd">
                                  <span style="margin-left: 40px;">&#160;</span>
                                </td>
                                <td id="lineTableBudgetTd">
                                  <span style="margin-left: 40px;">&#160;</span>
                                </td>
                                <td id="lineTableBudgetTd">
                                  <span style="margin-left: 40px;">&#160;</span>
                                </td>
                                <td id="lineTableBudgetTd">
                                  <span style="margin-left: 40px;">&#160;</span>
                                </td>
                              </tr>
                              <tr id="budgetContainerTr">
                                <td id="lineTableBudgetTd">
                                  <span style="margin-left: 40px;">&#160;</span>
                                </td>
                                <td id="lineTableBudgetTd">
                                  <span style="margin-left: 40px;">&#160;</span>
                                </td>
                                <td id="lineTableBudgetTd">
                                  <span style="margin-left: 40px;">&#160;</span>
                                </td>
                                <td id="lineTableBudgetTd">
                                  <span style="margin-left: 40px;">&#160;</span>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <br />
                          <br />
                          <span style="margin-left: 40px;">
                            <span style="font-size: 11px;">
                              <span style="color: rgb(105, 105, 105);">
                                <span style="font-family: tahoma,geneva,sans-serif;">
                                  <span style="font-weight: bold;">&#160;&#160;&#160;&#160;&#160;Mal&#305; &#304;ade Edenin</span>
                                </span>
                              </span>
                            </span><br /> <span style="font-size: 11px;">
                              <span style="color: rgb(105, 105, 105);">
                                <span style="font-family: tahoma,geneva,sans-serif;">
                                  <span style="font-weight: bold;">&#160;&#160;&#160;&#160;&#160;Ad&#305; Soyad&#305;</span>
                                </span>
                              </span>
                            </span><br /> <span style="font-size: 11px;">
                              <span style="color: rgb(105, 105, 105);">
                                <span style="font-family: tahoma,geneva,sans-serif;">
                                  <span style="font-weight: bold;">&#160;&#160;&#160;&#160;&#160;Adresi</span>
                                </span>
                              </span>
                            </span><br /> <br /> <br /> <span style="font-size: 11px;">
                              <span style="color: rgb(105, 105, 105);">
                                <span style="font-family: tahoma,geneva,sans-serif;">
                                  <span style="font-weight: bold;">&#160;&#160;&#160;&#160;&#160;&#304;mzas&#305;</span>
                                </span>
                              </span>
                            </span><br /> <br /> <br /> &#160;
                          </span>
                          <span style="margin-left: 40px;"> </span>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </xsl:if>
        <!--<table border="0" cellpadding="0" cellspacing="0" style="width: 800px;">
          <tbody>
            <tr>
              <td>
                <xsl:if test="//n1:Invoice/cac:AdditionalDocumentReference[cbc:ID =&apos;gonderimSekli&apos;]/cbc:DocumentType =&apos;ELEKTRONIK&apos;">
                  <div condition="//n1:Invoice/cac:AdditionalDocumentReference[cbc:ID =&apos;gonderimSekli&apos;]/cbc:DocumentType =&apos;ELEKTRONIK&apos;" style="text-align: center;">
                    <span style="margin-left: 40px;">
                      <span style="font-size: 11px;">
                        <span style="color: rgb(105, 105, 105);">
                          <span style="font-family: tahoma,geneva,sans-serif;">e-Ar&#351;iv izni kapsam&#305;nda elektronik ortamda iletilmi&#351;tir.</span>
                        </span>
                      </span>
                    </span>
                  </div>
                </xsl:if>
                <div style="text-align: center;">
                  <span style="margin-left: 40px;">
                    <span style="font-size: 11px;">
                      <span style="color: rgb(105, 105, 105);">
                        <span style="font-family: tahoma,geneva,sans-serif;">e-Ar&#351;iv izni kapsam&#305;nda olu&#351;turulmu&#351;tur.</span>
                      </span>
                    </span>
                  </span>
                </div>
                <xsl:if test="not(//n1:Invoice/cac:DespatchDocumentReference)">
                  <div condition="not(//n1:Invoice/cac:DespatchDocumentReference)" style="text-align: center;">
                    <span style="margin-left: 40px;">
                      <span style="font-size: 11px;">
                        <span style="color: rgb(105, 105, 105);">
                          <span style="font-family: tahoma,geneva,sans-serif;">&#304;rsaliye yerine ge&#231;er.</span>
                        </span>
                      </span>
                    </span>
                  </div>
                </xsl:if>
              </td>
            </tr>
          </tbody>
        </table>-->
        <table border="0" cellpadding="0" cellspacing="0" style="width: 800px;">
          <tbody>
            <xsl:choose>
              <xsl:when test="//n1:Invoice/cbc:ProfileID = 'EARSIVFATURA'">
                <tr>
                  <td>
                    <div style="text-align: center;">
                      <span style="margin-left: 40px;font-size: 11px;color: rgb(105, 105, 105);font-family: tahoma,geneva,sans-serif;">
                        e-Ar&#351;iv izni kapsam&#305;nda elektronik ortamda iletilmi&#351;tir.
                      </span>
                      <br/>
                      <span style="text-align:center;font-size: 11px;color: rgb(105, 105, 105);font-family: tahoma,geneva,sans-serif;">
                        e-Ar&#351;iv izni kapsam&#305;nda olu&#351;turulmu&#351;tur.
                      </span>
                      <br/>
                      <span style="text-align:center;font-size: 11px;color: rgb(105, 105, 105);font-family: tahoma,geneva,sans-serif;">
                        &#304;rsaliye yerine ge&#231;er.
                      </span>
                      <br/><br/>
                      <span style="text-align:center;font-size: 11px;color: rgb(105, 105, 105);font-family: tahoma,geneva,sans-serif;">
                        Faturaniza ait detaylara <i><b>https://partner.trendyol.com</b></i> adresi uzerinden erisebilirsiniz.
                      </span>
                    </div>
                  </td>
                </tr>
              </xsl:when>
              <xsl:otherwise>

                <tr>
                  <td>
                    <div style="text-align: center;">
                      <span style="margin-left: 40px;font-size: 11px;color: rgb(105, 105, 105);font-family: tahoma,geneva,sans-serif;">
                        &#304;rsaliye yerine ge&#231;er.
                      </span>
                      <br/><br/>
                      <span style="text-align:center;font-size: 11px;color: rgb(105, 105, 105);font-family: tahoma,geneva,sans-serif;">
                        Faturaniza ait detaylara <i><b>https://partner.trendyol.com</b></i> adresi uzerinden erisebilirsiniz.
                      </span>
                    </div>
                  </td>
                </tr>
              </xsl:otherwise>
            </xsl:choose>

          </tbody>
        </table>
      </body>
    </html>
  </xsl:template>
  <xsl:template name="geneliskonto">
    <tr id="lineTableTr" >
      <td id="lineTableTd" style="border-top:thick double black; border-top-width:3px;">
        <span>
          <xsl:text>&#160;</xsl:text>
        </span>
      </td>
      <td id="lineTableTd" style="border-top:thick double black; border-top-width:3px;" align="right">
        <span>
          <xsl:text>&#160;</xsl:text>
        </span>
      </td>
      <td id="lineTableTd" style="border-top:thick double black; border-top-width:3px;" align="left">
        <span>
          <table border="0">
            <tbody>
              <xsl:for-each select="//n1:Invoice/cac:AllowanceCharge">
                <tr>
                  <td>
                    <xsl:text>&#160;&#160;&#160;&#160;&#160;</xsl:text>
                    <xsl:text>İndirim</xsl:text>
                  </td>
                </tr>
              </xsl:for-each>
            </tbody>
          </table>
        </span>
      </td>
      <td id="lineTableTd" style="border-top:thick double black; border-top-width:3px;" align="right">
        <span>
          <xsl:text>&#160;</xsl:text>
        </span>
      </td>
      <td id="lineTableTd" style="border-top:thick double black; border-top-width:3px;" align="right">
        <span>
          <xsl:text>&#160;</xsl:text>
        </span>
      </td>
      <td id="lineTableTd" style="border-top:thick double black; border-top-width:3px;" align="right">
        <span>
          <xsl:text>&#160;</xsl:text>
        </span>
      </td>
      <td id="lineTableTd" style="border-top:thick double black; border-top-width:3px;" align="right">
        <span>
          <table border="0">
            <tbody>
              <xsl:for-each select="//n1:Invoice/cac:AllowanceCharge">
                <tr>
                  <td>
                    <xsl:choose>
                      <xsl:when test="normalize-space(cbc:AllowanceChargeReason)">
                        <xsl:value-of select="cbc:AllowanceChargeReason"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:if test ="normalize-space(cbc:MultiplierFactorNumeric)">
                          <xsl:text>%</xsl:text>
                          <xsl:value-of select="format-number(cbc:MultiplierFactorNumeric*100, '###.##0,00', 'european')"/>
                        </xsl:if>
                      </xsl:otherwise>
                    </xsl:choose>
                  </td>
                </tr>
              </xsl:for-each>
            </tbody>
          </table>
        </span>
      </td>
      <td id="lineTableTd" style="border-top:thick double black; border-top-width:3px;" align="right">
        <span>
          <table border="0">
            <tbody>
              <xsl:for-each select="//n1:Invoice/cac:AllowanceCharge">
                <tr>
                  <td>
                    <xsl:value-of select="format-number(cbc:Amount, '###.##0,00', 'european')"/>
                    <xsl:if test="cbc:Amount/@currencyID">
                      <xsl:text> </xsl:text>
                      <xsl:if test="cbc:Amount/@currencyID = 'TRY'">
                        <xsl:text>TL</xsl:text>
                      </xsl:if>
                      <xsl:if test="cbc:Amount/@currencyID != 'TRY'">
                        <xsl:value-of select="cbc:Amount/@currencyID"/>
                      </xsl:if>
                    </xsl:if>
                  </td>
                </tr>
              </xsl:for-each>
            </tbody>
          </table>
        </span>
      </td>
      <td id="lineTableTd" style="border-top:thick double black; border-top-width:3px;" align="right">
        <span>
          <xsl:text>&#160;</xsl:text>
        </span>
      </td>
      <td id="lineTableTd" style="border-top:thick double black; border-top-width:3px;" align="right">
        <span>
          <xsl:text>&#160;</xsl:text>
        </span>
      </td>
      <td id="lineTableTd" style="border-top:thick double black; border-top-width:3px;" align="right">
        <span>
          <xsl:text>&#160;</xsl:text>
        </span>
      </td>
      <td id="lineTableTd" style="border-top:thick double black; border-top-width:3px;" align="right">
        <span>
          <xsl:text>&#160;</xsl:text>
        </span>
      </td>
      <td id="lineTableTd" style="border-top:thick double black; border-top-width:3px;" align="right">
        <span>
          <xsl:text>&#160;</xsl:text>
        </span>
      </td>
    </tr>
  </xsl:template>
  <xsl:template name="Country">
    <xsl:param name="CountryType" />
    <xsl:choose>
      <xsl:when test="$CountryType='AF'">Afganistan</xsl:when>
      <xsl:when test="$CountryType='DE'">Almanya</xsl:when>
      <xsl:when test="$CountryType='AD'">Andorra</xsl:when>
      <xsl:when test="$CountryType='AO'">Angola</xsl:when>
      <xsl:when test="$CountryType='AG'">Antigua ve Barbuda</xsl:when>
      <xsl:when test="$CountryType='AR'">Arjantin</xsl:when>
      <xsl:when test="$CountryType='AL'">Arnavutluk</xsl:when>
      <xsl:when test="$CountryType='AW'">Aruba</xsl:when>
      <xsl:when test="$CountryType='AU'">Avustralya</xsl:when>
      <xsl:when test="$CountryType='AT'">Avusturya</xsl:when>
      <xsl:when test="$CountryType='AZ'">Azerbaycan</xsl:when>
      <xsl:when test="$CountryType='BS'">Bahamalar</xsl:when>
      <xsl:when test="$CountryType='BH'">Bahreyn</xsl:when>
      <xsl:when test="$CountryType='BD'">Bangladeş</xsl:when>
      <xsl:when test="$CountryType='BB'">Barbados</xsl:when>
      <xsl:when test="$CountryType='EH'">Batı Sahra (MA)</xsl:when>
      <xsl:when test="$CountryType='BE'">Belçika</xsl:when>
      <xsl:when test="$CountryType='BZ'">Belize</xsl:when>
      <xsl:when test="$CountryType='BJ'">Benin</xsl:when>
      <xsl:when test="$CountryType='BM'">Bermuda</xsl:when>
      <xsl:when test="$CountryType='BY'">Beyaz Rusya</xsl:when>
      <xsl:when test="$CountryType='BT'">Bhutan</xsl:when>
      <xsl:when test="$CountryType='AE'">Birleşik Arap Emirlikleri</xsl:when>
      <xsl:when test="$CountryType='US'">Birleşik Devletler</xsl:when>
      <xsl:when test="$CountryType='GB'">Birleşik Krallık</xsl:when>
      <xsl:when test="$CountryType='BO'">Bolivya</xsl:when>
      <xsl:when test="$CountryType='BA'">Bosna-Hersek</xsl:when>
      <xsl:when test="$CountryType='BW'">Botsvana</xsl:when>
      <xsl:when test="$CountryType='BR'">Brezilya</xsl:when>
      <xsl:when test="$CountryType='BN'">Bruney</xsl:when>
      <xsl:when test="$CountryType='BG'">Bulgaristan</xsl:when>
      <xsl:when test="$CountryType='BF'">Burkina Faso</xsl:when>
      <xsl:when test="$CountryType='BI'">Burundi</xsl:when>
      <xsl:when test="$CountryType='TD'">Çad</xsl:when>
      <xsl:when test="$CountryType='KY'">Cayman Adaları</xsl:when>
      <xsl:when test="$CountryType='GI'">Cebelitarık (GB)</xsl:when>
      <xsl:when test="$CountryType='CZ'">Çek Cumhuriyeti</xsl:when>
      <xsl:when test="$CountryType='DZ'">Cezayir</xsl:when>
      <xsl:when test="$CountryType='DJ'">Cibuti</xsl:when>
      <xsl:when test="$CountryType='CN'">Çin</xsl:when>
      <xsl:when test="$CountryType='DK'">Danimarka</xsl:when>
      <xsl:when test="$CountryType='CD'">Demokratik Kongo Cumhuriyeti</xsl:when>
      <xsl:when test="$CountryType='TL'">Doğu Timor</xsl:when>
      <xsl:when test="$CountryType='DO'">Dominik Cumhuriyeti</xsl:when>
      <xsl:when test="$CountryType='DM'">Dominika</xsl:when>
      <xsl:when test="$CountryType='EC'">Ekvador</xsl:when>
      <xsl:when test="$CountryType='GQ'">Ekvator Ginesi</xsl:when>
      <xsl:when test="$CountryType='SV'">El Salvador</xsl:when>
      <xsl:when test="$CountryType='ID'">Endonezya</xsl:when>
      <xsl:when test="$CountryType='ER'">Eritre</xsl:when>
      <xsl:when test="$CountryType='AM'">Ermenistan</xsl:when>
      <xsl:when test="$CountryType='MF'">Ermiş Martin (FR)</xsl:when>
      <xsl:when test="$CountryType='EE'">Estonya</xsl:when>
      <xsl:when test="$CountryType='ET'">Etiyopya</xsl:when>
      <xsl:when test="$CountryType='FK'">Falkland Adaları</xsl:when>
      <xsl:when test="$CountryType='FO'">Faroe Adaları (DK)</xsl:when>
      <xsl:when test="$CountryType='MA'">Fas</xsl:when>
      <xsl:when test="$CountryType='FJ'">Fiji</xsl:when>
      <xsl:when test="$CountryType='CI'">Fildişi Sahili</xsl:when>
      <xsl:when test="$CountryType='PH'">Filipinler</xsl:when>
      <xsl:when test="$CountryType='FI'">Finlandiya</xsl:when>
      <xsl:when test="$CountryType='FR'">Fransa</xsl:when>
      <xsl:when test="$CountryType='GF'">Fransız Guyanası (FR)</xsl:when>
      <xsl:when test="$CountryType='PF'">Fransız Polinezyası (FR)</xsl:when>
      <xsl:when test="$CountryType='GA'">Gabon</xsl:when>
      <xsl:when test="$CountryType='GM'">Gambiya</xsl:when>
      <xsl:when test="$CountryType='GH'">Gana</xsl:when>
      <xsl:when test="$CountryType='GN'">Gine</xsl:when>
      <xsl:when test="$CountryType='GW'">Gine Bissau</xsl:when>
      <xsl:when test="$CountryType='GD'">Grenada</xsl:when>
      <xsl:when test="$CountryType='GL'">Grönland (DK)</xsl:when>
      <xsl:when test="$CountryType='GP'">Guadeloupe (FR)</xsl:when>
      <xsl:when test="$CountryType='GT'">Guatemala</xsl:when>
      <xsl:when test="$CountryType='GG'">Guernsey (GB)</xsl:when>
      <xsl:when test="$CountryType='ZA'">Güney Afrika</xsl:when>
      <xsl:when test="$CountryType='KR'">Güney Kore</xsl:when>
      <xsl:when test="$CountryType='GE'">Gürcistan</xsl:when>
      <xsl:when test="$CountryType='GY'">Guyana</xsl:when>
      <xsl:when test="$CountryType='HT'">Haiti</xsl:when>
      <xsl:when test="$CountryType='IN'">Hindistan</xsl:when>
      <xsl:when test="$CountryType='HR'">Hırvatistan</xsl:when>
      <xsl:when test="$CountryType='NL'">Hollanda</xsl:when>
      <xsl:when test="$CountryType='HN'">Honduras</xsl:when>
      <xsl:when test="$CountryType='HK'">Hong Kong (CN)</xsl:when>
      <xsl:when test="$CountryType='VG'">İngiliz Virjin Adaları</xsl:when>
      <xsl:when test="$CountryType='IQ'">Irak</xsl:when>
      <xsl:when test="$CountryType='IR'">İran</xsl:when>
      <xsl:when test="$CountryType='IE'">İrlanda</xsl:when>
      <xsl:when test="$CountryType='ES'">İspanya</xsl:when>
      <xsl:when test="$CountryType='IL'">İsrail</xsl:when>
      <xsl:when test="$CountryType='SE'">İsveç</xsl:when>
      <xsl:when test="$CountryType='CH'">İsviçre</xsl:when>
      <xsl:when test="$CountryType='IT'">İtalya</xsl:when>
      <xsl:when test="$CountryType='IS'">İzlanda</xsl:when>
      <xsl:when test="$CountryType='JM'">Jamaika</xsl:when>
      <xsl:when test="$CountryType='JP'">Japonya</xsl:when>
      <xsl:when test="$CountryType='JE'">Jersey (GB)</xsl:when>
      <xsl:when test="$CountryType='KH'">Kamboçya</xsl:when>
      <xsl:when test="$CountryType='CM'">Kamerun</xsl:when>
      <xsl:when test="$CountryType='CA'">Kanada</xsl:when>
      <xsl:when test="$CountryType='ME'">Karadağ</xsl:when>
      <xsl:when test="$CountryType='QA'">Katar</xsl:when>
      <xsl:when test="$CountryType='KZ'">Kazakistan</xsl:when>
      <xsl:when test="$CountryType='KE'">Kenya</xsl:when>
      <xsl:when test="$CountryType='CY'">Kıbrıs</xsl:when>
      <xsl:when test="$CountryType='KG'">Kırgızistan</xsl:when>
      <xsl:when test="$CountryType='KI'">Kiribati</xsl:when>
      <xsl:when test="$CountryType='CO'">Kolombiya</xsl:when>
      <xsl:when test="$CountryType='KM'">Komorlar</xsl:when>
      <xsl:when test="$CountryType='CG'">Kongo Cumhuriyeti</xsl:when>
      <xsl:when test="$CountryType='KV'">Kosova (RS)</xsl:when>
      <xsl:when test="$CountryType='CR'">Kosta Rika</xsl:when>
      <xsl:when test="$CountryType='CU'">Küba</xsl:when>
      <xsl:when test="$CountryType='KW'">Kuveyt</xsl:when>
      <xsl:when test="$CountryType='KP'">Kuzey Kore</xsl:when>
      <xsl:when test="$CountryType='LA'">Laos</xsl:when>
      <xsl:when test="$CountryType='LS'">Lesoto</xsl:when>
      <xsl:when test="$CountryType='LV'">Letonya</xsl:when>
      <xsl:when test="$CountryType='LR'">Liberya</xsl:when>
      <xsl:when test="$CountryType='LY'">Libya</xsl:when>
      <xsl:when test="$CountryType='LI'">Lihtenştayn</xsl:when>
      <xsl:when test="$CountryType='LT'">Litvanya</xsl:when>
      <xsl:when test="$CountryType='LB'">Lübnan</xsl:when>
      <xsl:when test="$CountryType='LU'">Lüksemburg</xsl:when>
      <xsl:when test="$CountryType='HU'">Macaristan</xsl:when>
      <xsl:when test="$CountryType='MG'">Madagaskar</xsl:when>
      <xsl:when test="$CountryType='MO'">Makao (CN)</xsl:when>
      <xsl:when test="$CountryType='MK'">Makedonya</xsl:when>
      <xsl:when test="$CountryType='MW'">Malavi</xsl:when>
      <xsl:when test="$CountryType='MV'">Maldivler</xsl:when>
      <xsl:when test="$CountryType='MY'">Malezya</xsl:when>
      <xsl:when test="$CountryType='ML'">Mali</xsl:when>
      <xsl:when test="$CountryType='MT'">Malta</xsl:when>
      <xsl:when test="$CountryType='IM'">Man Adası (GB)</xsl:when>
      <xsl:when test="$CountryType='MH'">Marshall Adaları</xsl:when>
      <xsl:when test="$CountryType='MQ'">Martinique (FR)</xsl:when>
      <xsl:when test="$CountryType='MU'">Mauritius</xsl:when>
      <xsl:when test="$CountryType='YT'">Mayotte (FR)</xsl:when>
      <xsl:when test="$CountryType='MX'">Meksika</xsl:when>
      <xsl:when test="$CountryType='FM'">Mikronezya</xsl:when>
      <xsl:when test="$CountryType='EG'">Mısır</xsl:when>
      <xsl:when test="$CountryType='MN'">Moğolistan</xsl:when>
      <xsl:when test="$CountryType='MD'">Moldova</xsl:when>
      <xsl:when test="$CountryType='MC'">Monako</xsl:when>
      <xsl:when test="$CountryType='MR'">Moritanya</xsl:when>
      <xsl:when test="$CountryType='MZ'">Mozambik</xsl:when>
      <xsl:when test="$CountryType='MM'">Myanmar</xsl:when>
      <xsl:when test="$CountryType='NA'">Namibya</xsl:when>
      <xsl:when test="$CountryType='NR'">Nauru</xsl:when>
      <xsl:when test="$CountryType='NP'">Nepal</xsl:when>
      <xsl:when test="$CountryType='NE'">Nijer</xsl:when>
      <xsl:when test="$CountryType='NG'">Nijerya</xsl:when>
      <xsl:when test="$CountryType='NI'">Nikaragua</xsl:when>
      <xsl:when test="$CountryType='NO'">Norveç</xsl:when>
      <xsl:when test="$CountryType='CF'">Orta Afrika Cumhuriyeti</xsl:when>
      <xsl:when test="$CountryType='UZ'">Özbekistan</xsl:when>
      <xsl:when test="$CountryType='PK'">Pakistan</xsl:when>
      <xsl:when test="$CountryType='PW'">Palau</xsl:when>
      <xsl:when test="$CountryType='PA'">Panama</xsl:when>
      <xsl:when test="$CountryType='PG'">Papua Yeni Gine</xsl:when>
      <xsl:when test="$CountryType='PY'">Paraguay</xsl:when>
      <xsl:when test="$CountryType='PE'">Peru</xsl:when>
      <xsl:when test="$CountryType='PL'">Polonya</xsl:when>
      <xsl:when test="$CountryType='PT'">Portekiz</xsl:when>
      <xsl:when test="$CountryType='PR'">Porto Riko (US)</xsl:when>
      <xsl:when test="$CountryType='RE'">Réunion (FR)</xsl:when>
      <xsl:when test="$CountryType='RO'">Romanya</xsl:when>
      <xsl:when test="$CountryType='RW'">Ruanda</xsl:when>
      <xsl:when test="$CountryType='RU'">Rusya</xsl:when>
      <xsl:when test="$CountryType='BL'">Saint Barthélemy (FR)</xsl:when>
      <xsl:when test="$CountryType='KN'">Saint Kitts ve Nevis</xsl:when>
      <xsl:when test="$CountryType='LC'">Saint Lucia</xsl:when>
      <xsl:when test="$CountryType='PM'">Saint Pierre ve Miquelon (FR)</xsl:when>
      <xsl:when test="$CountryType='VC'">Saint Vincent ve Grenadinler</xsl:when>
      <xsl:when test="$CountryType='WS'">Samoa</xsl:when>
      <xsl:when test="$CountryType='SM'">San Marino</xsl:when>
      <xsl:when test="$CountryType='ST'">São Tomé ve Príncipe</xsl:when>
      <xsl:when test="$CountryType='SN'">Senegal</xsl:when>
      <xsl:when test="$CountryType='SC'">Seyşeller</xsl:when>
      <xsl:when test="$CountryType='SL'">Sierra Leone</xsl:when>
      <xsl:when test="$CountryType='CL'">Şili</xsl:when>
      <xsl:when test="$CountryType='SG'">Singapur</xsl:when>
      <xsl:when test="$CountryType='RS'">Sırbistan</xsl:when>
      <xsl:when test="$CountryType='SK'">Slovakya Cumhuriyeti</xsl:when>
      <xsl:when test="$CountryType='SI'">Slovenya</xsl:when>
      <xsl:when test="$CountryType='SB'">Solomon Adaları</xsl:when>
      <xsl:when test="$CountryType='SO'">Somali</xsl:when>
      <xsl:when test="$CountryType='SS'">South Sudan</xsl:when>
      <xsl:when test="$CountryType='SJ'">Spitsbergen (NO)</xsl:when>
      <xsl:when test="$CountryType='LK'">Sri Lanka</xsl:when>
      <xsl:when test="$CountryType='SD'">Sudan</xsl:when>
      <xsl:when test="$CountryType='SR'">Surinam</xsl:when>
      <xsl:when test="$CountryType='SY'">Suriye</xsl:when>
      <xsl:when test="$CountryType='SA'">Suudi Arabistan</xsl:when>
      <xsl:when test="$CountryType='SZ'">Svaziland</xsl:when>
      <xsl:when test="$CountryType='TJ'">Tacikistan</xsl:when>
      <xsl:when test="$CountryType='TZ'">Tanzanya</xsl:when>
      <xsl:when test="$CountryType='TH'">Tayland</xsl:when>
      <xsl:when test="$CountryType='TW'">Tayvan</xsl:when>
      <xsl:when test="$CountryType='TG'">Togo</xsl:when>
      <xsl:when test="$CountryType='TO'">Tonga</xsl:when>
      <xsl:when test="$CountryType='TT'">Trinidad ve Tobago</xsl:when>
      <xsl:when test="$CountryType='TN'">Tunus</xsl:when>
      <xsl:when test="$CountryType='TR'">Türkiye</xsl:when>
      <xsl:when test="$CountryType='TM'">Türkmenistan</xsl:when>
      <xsl:when test="$CountryType='TC'">Turks ve Caicos</xsl:when>
      <xsl:when test="$CountryType='TV'">Tuvalu</xsl:when>
      <xsl:when test="$CountryType='UG'">Uganda</xsl:when>
      <xsl:when test="$CountryType='UA'">Ukrayna</xsl:when>
      <xsl:when test="$CountryType='OM'">Umman</xsl:when>
      <xsl:when test="$CountryType='JO'">Ürdün</xsl:when>
      <xsl:when test="$CountryType='UY'">Uruguay</xsl:when>
      <xsl:when test="$CountryType='VU'">Vanuatu</xsl:when>
      <xsl:when test="$CountryType='VA'">Vatikan</xsl:when>
      <xsl:when test="$CountryType='VE'">Venezuela</xsl:when>
      <xsl:when test="$CountryType='VN'">Vietnam</xsl:when>
      <xsl:when test="$CountryType='WF'">Wallis ve Futuna (FR)</xsl:when>
      <xsl:when test="$CountryType='YE'">Yemen</xsl:when>
      <xsl:when test="$CountryType='NC'">Yeni Kaledonya (FR)</xsl:when>
      <xsl:when test="$CountryType='NZ'">Yeni Zelanda</xsl:when>
      <xsl:when test="$CountryType='CV'">Yeşil Burun Adaları</xsl:when>
      <xsl:when test="$CountryType='GR'">Yunanistan</xsl:when>
      <xsl:when test="$CountryType='ZM'">Zambiya</xsl:when>
      <xsl:when test="$CountryType='ZW'">Zimbabve</xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$CountryType"/>
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>

</xsl:stylesheet>
