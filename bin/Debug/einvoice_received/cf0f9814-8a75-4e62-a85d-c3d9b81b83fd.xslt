<?xml version="1.0" encoding="UTF-8"?><!-- DWXMLSource="1.xml" -->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" exclude-result-prefixes="cac cbc ccts clm54217 clm5639 clm66411 clmIANAMIMEMediaType fn link n1 qdt udt xbrldi xbrli xdt xlink xs xsd xsi" xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:ccts="urn:un:unece:uncefact:documentation:2" xmlns:clm54217="urn:un:unece:uncefact:codelist:specification:54217:2001" xmlns:clm5639="urn:un:unece:uncefact:codelist:specification:5639:1988" xmlns:clm66411="urn:un:unece:uncefact:codelist:specification:66411:2001" xmlns:clmIANAMIMEMediaType="urn:un:unece:uncefact:codelist:specification:IANAMIMEMediaType:2003" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:link="http://www.xbrl.org/2003/linkbase" xmlns:n1="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2" xmlns:qdt="urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2" xmlns:udt="urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2" xmlns:xbrldi="http://xbrl.org/2006/xbrldi" xmlns:xbrli="http://www.xbrl.org/2003/instance" xmlns:xdt="http://www.w3.org/2005/xpath-datatypes" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2" xmlns:ext="urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <xsl:decimal-format name="european" decimal-separator="," grouping-separator="." NaN=""/>
  <xsl:output version="4.0" method="html" indent="no" encoding="UTF-8" doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN" doctype-system="http://www.w3.org/TR/html4/loose.dtd"/>
  <xsl:param name="SV_OutputFormat" select="'HTML'"/>
  <xsl:variable name="XML" select="/"/>
  <xsl:template match="/">
    <html>
      <head>
        
        <title></title>
        <style type="text/css">

          body {
          background-color: #FFFFFF;
          font-family: 'Tahoma', "Times New Roman", Times, serif;
          font-size: 11px;
          color: #666666;
          }

          h1, h2 {
          padding-bottom: 3px;
          padding-top: 3px;
          margin-bottom: 5px;
          text-transform: uppercase;
          font-family: Arial, Helvetica, sans-serif;
          }

          h1 {
          font-size: 1.4em;
          text-transform:none;
          }

          h2 {
          font-size: 1em;
          color: brown;
          }

          h3 {
          font-size: 1em;
          color: #333333;
          text-align: justify;
          margin: 0;
          padding: 0;
          }

          h4 {
          font-size: 1.1em;
          font-style: bold;
          font-family: Arial, Helvetica, sans-serif;
          color: #000000;
          margin: 0;
          padding: 0;
          }

          hr{
          height:2px;
          color: #000000;
          background-color: #000000;
          border-bottom: 1px solid #000000;
          }

          p, ul, ol {
          margin-top: 1.5em;
          }

          ul, ol {
          margin-left: 3em;
          }

          blockquote {
          margin-left: 3em;
          margin-right: 3em;
          font-style: italic;
          }

          a {
          text-decoration: none;
          color: #70A300;
          }

          a:hover {
          border: none;
          color: #70A300;
          }

          #despatchTable{
          border-collapse:collapse;
          font-size:11px;
          float:left;
          border-color:gray;

          }

          #ettnTable{
          border-collapse:collapse;
          font-size:11px;
          border-color:gray;
          }

          #customerPartyTable{
          border-width: 0px;
          border-spacing: ;
          border-style: inset;
          border-color: gray;
          border-collapse: collapse;
          background-color:
          }

          #customerIDTable{
          border-width: 2px;
          border-spacing: ;
          border-style: inset;
          border-color: gray;
          border-collapse: collapse;
          background-color:
          }

          #customerIDTableTd{
          border-width: 2px;
          border-spacing: ;
          border-style: inset;
          border-color: gray;
          border-collapse: collapse;
          background-color:
          }

          #lineTable{
          border-width:2px;
          border-spacing: ;
          border-style: inset;
          border-color: black;
          border-collapse: collapse;
          background-color: ;
          }

          #lineTableTd{
          border-width: 1px;
          padding: 1px;
          border-style: inset;
          border-color: black;
          background-color: white;
          }

          #lineTableTr{
          border-width: 1px;
          padding: 0px;
          border-style: inset;
          border-color: black;
          background-color: white;
          -moz-border-radius: ;
          }

          #lineTableDummyTd {
          border-width: 1px;
          border-color:white;
          padding: 1px;
          border-style: inset;
          border-color: black;
          background-color: white;
          }

          #lineTableBudgetTd{
          border-width: 2px;
          border-spacing:0px;
          padding: 1px;
          border-style: inset;
          border-color: black;
          background-color: white;
          -moz-border-radius: ;
          }

          #notesTable{

          border-width: 2px;
          border-spacing: ;
          border-style: inset;
          border-color: black;
          border-collapse: collapse;
          background-color:
		  
		  }

          #notesTableTd{


          border-width: 0px;
          border-spacing: ;
          border-style: inset;
          border-color: black;
          border-collapse: collapse;
          background-color:
          }

          table{

          border-spacing:0px;

          }

          #budgetContainerTable{

          border-width: 0px;
          border-spacing: 0px;
          border-style: inset;
          border-color: black;
          border-collapse: collapse;
          background-color: ;

          }

          td{
          border-color:gray;
          }


          .header {font-family:Tahoma, sans-serif; font-size: 12px; COLOR:#200000; padding-left:10; padding-right:5; font-weight:900 }
          .mheader {font-family:Tahoma, sans-serif; font-size: 10px; COLOR:#200000; padding-left:10; padding-right:5; font-weight:900 }
          .text {font-family:Tahoma,sans-serif; font-size: 11px; color:#000000; padding-left:20; padding-right:10 }
          .text2 {font-family:Verdana,sans-serif; font-size: 10px; color:#000000; padding-left:20; padding-right:10 }
          .news {font-family:Arial, sans-serif; font-size: 9px; color:#00000; padding-left:10; padding-right:5; font-weight:900; }
          a:link{text-decoration: none; color:#004FDF}
          a:visited{text-decoration: none; color: #004FDF}
          a:hover{text-decoration: underline; color: #004FDF}
          a:active{text-decoration: none; color: #004FDF}
          li {
          list-style : url(images/pic.jpg);
          }

        </style>
        <title>e-Fatura</title>
      </head>
      <body style="margin-left=0.6in; margin-right=0.6in; margin-top=0.79in; margin-bottom=0.79in">
        <xsl:for-each select="$XML">
          <table style="border-color:blue; " border="0" cellspacing="0px" width="950" cellpadding="0px">
            <tbody>
              <tr>
                <td colspan="3">
                   <!--üst banner-->
                  <img width="260px" align="right" alt="TurkTelekom" src="data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAB2MAAAKNCAIAAAHltGcMAAAACXBIWXMAAGunAABrpwHCCSECAAAA
GXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAS4BJREFUeNrsnU1u1EAQRp3R3ABx
ATYkeySQCJvAOVigHAtxEmAVJCJlT1ZcIMqCC2CMLE1GtrunurvK7p/3hCI0Qyq45/mb6p9Rzvq+
7wAKZ8cQAB4D4DEAHkPdPH54g8fQlsp4DLmrLLEZj6EGm/EYarAZj0GN+99vt2qa94w+uHh/82v4
+u3yQi5x0LdE8Ozrz8XH8RhSbZ7HsIXNLoPxGFJt9jcSijb7JaY/hhibR4OF3fDhW6INPikxeQwx
Kt/d726vrPpseQbjMUQyGDz+5fX3L8PX26tP1jYLoa+AYIkPDDaPQgfZnNhp4DHEGzyX+Njm6D5b
C/oKCMtgj8obthnkcYs8v7zOZ9aoks143K7KEptfnf8d/kgKusLY35NotRn0FQRz93Dz+aTNHhc9
Bq/WZuAx/LdZovJcTRWDVWzGYwgL5lFTz5QuWuIUm+mPIZg/1y8XH3/38SJd4rimmTyGTpLEI4eM
PH/xozs6JzQYbLSaIQxmPAaRxIsyDTavsIQnsRmPMbiLk7hbdx3abzP9cdLEKLdSQQYLY3hRHeHy
s5HNojyW//+Ed7PpDDpdjogfofUSbrWvlhjDGZJpX5HPxmllMax7J6x8CZ77aq/+ksyv0DS2oRGb
g+d5E+0KNbKOqyhlqE1VZt2tlfnl5nemUTDLG/S94mUIR3YSjUFJaRerWn64Xkt5qUmF4290FV+c
E3vGeVLZ81SoBuOn99Y0eGSXYbqsPDHyLCGF/kTPvxeWckksWecK/eljTddT0eMpP+qpuE6yVxTR
tCX1pJTpjSQ5C6ZVyugaN8F/1FPR4JL643VeYNc7uFxlxVKedsKTF576koKJVz1XuRMff0tcq97e
4xRF7N5MQufg6aWEb1+LU5GUNuC4oMXKw8keQ2W3pYB96RaWzBLbs5NzwWzHTWvLcOM83nCBwm4S
WfRGXXEGF5PH+FGfweonN9gH0Q/7xFLpE6x2YrjIPG4zkiX7IM3GcEkee+Y91fTHk9PAKfsvGWa5
qcE6HnsWShUlXmHBuJvtmXm2u0xL+QdwsXKbGWzYH9uN6fHSpmIHOV8xlV/C5FRDSilXhdDKWYXx
msfwdyoqrD+mijeM4tpf+iW73t8klfOReLUYVu6P5x/2shhTu0bZ9WG18fGgS/OXSrldrW+hQg0e
Oev7voMCqXt5Do+hRfjcP+AxAB7Dybkd4DEq4zFkoDI24zE24zHQZuAxZILRbw7NE87R507ipp3p
b3XGYzA3uCmb8bhygyc216oy5ytq7o9dT9VnM/M8poB4DJUGNh4DwYzHgM14DM3ajMfwRLnrGHgM
xUvcsQ8CXRXLyXiMwTWAxxhcA/THSEweAwbjMWAwHgMG0x8jcV1w/hjIYwA8BsBjgCf+CcDe2aRY
DURR+NH03IE7cGI7b1AQJ+I6RKSX1bgScSYouIB24g564NRBY9pgCJXkvlt1b1WqKt/hge37qZeq
fDk59ZM88jHCjBECYoSAGAExQkCMUA7dv3s1PIAYHQJlIEZtoAzEqGdLBmK0roQbVhS4x8Uqx6ww
Rmeg1KztvPv1+t+/t1lvpfz08zecGPlb8oDvf4LzWvIWwTgxMlnyEt8EF7fgC8QoHWUBX0eUz+JL
nECJKD+5/Rn7kYSAoSQYiFGcftxdDI/hj5dfPvlma4uIE0iLb/DMyPH3tx/t2RqIUWl8a0OZKzuQ
hJpMcKDrq4fYb3FBmUx8RCl/dzqK4LRyXIIyEMOx5KxKcx3ixGqimDqCsuUbUSYTH53jsz9+OnK8
xaKQhqOM3BKUcWJQvlG6sp5gjQE7pgucGGldeW7JAr4uHcooSwZiFKffN89Pj9PO4fNv3r/488Ht
xB6FMhCjkyYZj5qounr2db58YiA4x1YpUQZi8I3Dd9LA8Uk9WmdEWeYYiME3Gt95jC4j2ZKrhnho
JmVDoxwE14CvBuVLyyZmJczSUlmroBxeLVxUMYLHj+/FcTNOXFUDZdqwausYdRgUrkXGODGvybJu
5IG+M0kZjqM7dgF2LRLZQRWwZA2+jE40qSUuNRyW4zbEXrbkQrAPxMtGlJt12g3Byejszgj2n+PO
2zKS5G5fcjlCHbc2cqt3OG9noWTHo+L66sFr9WbUtPNFow5UgOCEs6TAWcIJV0Ow8g3LV8dnVj9l
yQb61ZsCvrFr2S4dCYhNosmNVYbg6Q3Kr7MXtdViyobaKl+2cGOtt1A+xS8GSr7Ko41MnKlnlnDu
zleU8m1CbJDh0+QH3/5ZFMqW65T2jxNDC44Pr3O6i7tbDpWEopTHwGpRmvLn73GsqR5l3/xQF8SV
dOb0Z/ncRe072mA5FyVwbMe3pTiRe+/uOxWnPAbkl9qaAvS99QSXJ6G2CW7MifMtanMs1liUUEf5
pWPie3QnzpT/7EW1vjYoa/wtCrHXnggGLurcwXYjtwxF1w99Pnz9IZ5a071ZC3AcTKqN/00b/dUU
tSxWOawWlJNv9KZ+A3bLxEG/uIwreIXjZad+dXpWTqITgmeLitqe+ffu0siVu2+TmbiDLp1lFjB2
RqN793UenRAWwvt6xpZRecGnWc8lj8huLa7Vb6pQvvzSAQ14Erd2bVJ7XZxXp4AYNS9m7BAQIwTE
CAFxh522LmeegRiUERA3izKNAMRYMhCjCpQ2qWH/VaJWxB2AOsQ3QHmXqWCcGPkQfBBLxol7xjdA
+bTTAp3cYu1Et9py3/44Jk4cEe7O0gUQgzIQI1AGYlRtegZihCUDMUI6MU6MHtX0uBsQg2/zw8bE
CQgmTiDwBWIEvkCMjosvmRiCcWIEvkCMwNdFrCdGZGKEgBghIEZH118B2LubXLmJKAzDV9HdAWID
jLKASCARJsA6UISYMWY3EStBjBIJJObJiCGTiA0wQDRqYe5fV5fL9XPKfl7dRD1odx/b5defy+Vq
mRgABAoAABcDABcDAP7FQCAAWMGfX392fvHRT79wMQDsSspcDAB1pLzFy1wMAOPDMhcDwPiwzMUA
1rHM0N5oloj3v39++v/7P143/ZY+5KdjLgawVcrbjXn2b+tvCahgLgYwPiwnFDwkknf2LxcDGBaW
1/o3uJRrjTLmYgA9wnIVBReoP7J/72KeNgDl+rvKb+/vTbTw65ffZi643LsrYMbbfXIxgPo8UPDC
pz//WODlKcIyFwMI7d9LLF5uJ+Wb2Lf7uBjAMAULy0+ivxjAaqlt9+9VXjz/u8O6xJEyFwOH4OOX
3y2vP7x5veWj7gbMFlI+R+P8e3dLDRv1PdbL+iiA43p5o5Tv6m+jlNd2TbQ4B4ztWeZigJQrePlu
Js0XZQQFp4N/Ny9zMYCuYbng7lwfBY8Ny1wMoIeUY0bgOGGZiwHcY7uCn3DWy9O/tzcZk1F88c1/
i7yKu4lahGUuBtBMwY94/snb5fXi5f/9OxsVwzIXA/zb1r/XI/Au2BiWuRig4H4KvjtyY6+USTnr
WY8Wm69ia+hPxZsbw9txt1UYtdFmrGp/FDTdv179sHaRPo/qNTpjycVO6YfbaPbj2CR+nO2/Khff
Bj88cnZhn0TjAN7HFrMf43h5l/ui+A6eXAxAWB6m4HUuTgfPsXH1sG3Xjpj9QsF+mT0s133i4zba
ntj4nnxJPfi0FsdJolVFPg7TB8OoygdWVfbVV53y+A2Za5GzYE6bj9lWl6/rMzPncAXro2h7Bp7O
wvmbouJsMlNUtXZXbmlUxYM6TgueF0l/++PPz6l24FCTWpPARVZwFBeXXVZXuRgn4lruaLFSEaoq
2I+1GlXBKqz66rO711bbbnfnS7mzl3tOnuneHQvnXn1fev8SyvZUVfU4XLBU8SrkbKJ0R0eisLq7
O2ZYNn/xSNrFqOk6JS4VnLgKrnV8RqiqbhxOF3C1r2DtKjz55qvdF4+XyunxGE5dKQ//saWju/jI
9+jKCr50bbtdx1tOZgVX3B1OqPlLVTmjVC+y1lbt2YMxnYK5uLmIJx0psbPH5PJFVrwfKzaAWdw3
vJti9gjMxf00Yejowc/WGgAFc7HggMrn2rJOZw2Af7l47sN4xhi476q6idVcGQdU8MKz6SoO2F5P
B2rrAad97Da22ghVJWpY2x9VNnp3lhNkTP8ufzPWH9TFiSE+Yb1WcBjHFF/Z025Bqire2ssJteBB
4Y1lx2wh0yl49hW5HW6EibR79SBMr060gFNl4yfWKzH6rXgb9ulVSI8wu7kwILd6S5aI99QFMUEu
Lhv6E9nOE/VXJKrdfhEze1VrA3KVson4CBE4Yi5+0Oxyjp8p2mg6EgYMyFv81ehhgXZV1QrIZQ+5
UbAU/CRZv3cHoGlCp2BwMQCM55lNAABcDADgYgDgYiA+nsVAB8xHAeQaeXlt2AO4GAjkZVIGFwPC
MvaD/mKgvpeb8tWbd6c/G1wuBnCPIYl40fFBHhHmYgBR/JuWMi9zMUDBsbxMytNhPgpgMtZ2FvOy
XAxAWAYXA7gQqHmZiwEIy+BiAMIyFwMQlsHFAEiZiwGEh3+5GAAFczEA/gUXA6BgLgZAweBiAPzL
xQAoGFwMgH+5GAAFg4sB8O/BMZc8AIzH70ADABcDALgYACLwjwDs3c1uG1UYgGFHyr4LxIYlq7JH
AomyoVwHQohbYldxJcCqlUBi33bDDSBuAeNkpOA6sT1z5vx8Z87zqLvG8WQ8fv35ZOKxXgxgLgZA
iwG0GAAtBtBiALQYQIsB0GIALQbo0j/ffnn4p8UAGyyyFgO0L7IWA6wtshYDbGFA1mKA9kXWYoD2
RdZigCJF1mKAzgZkLQaWefn6bem7ePfXVxXuJZRbBxaQluNfX3xWosIV7qWaj3753VwM9DQgHyp8
HOKaY3ihCs8PsbkYCDEgP1nhTgfkRQnWYiBEkS9XuLsip4VYi4FmRZ5f4S6KnFzhifViIH+Rr1Y4
LcSL7qVmhVeG2FwM1B6QV1Y41IC8PsFaDNQucq4KBylyxhDvrFEAFYq8flFizr3UrHDeEGsxUNyz
V++/+O3nCtGvU+TsFZ5YowBK+fPd/9PelOM/vvmhwhje45/qmYuB4iE+LvKWZmQtBkJX+MkQn8zI
FYrc0U6zRgGUnYUv5NiShbkYaBbi4yJbsjAXA80qbEY2FwM5K7wyxCdFrjAjm4sBs7AB+Wk3+/3e
UQUsalmJEJ/4/Pm/FX6WOEW2RgFD+PjFjx2FculonLxUEmfJwhoFjJXjv1+/ypXjEtNxQoXXj/kR
BmQtBkVeNR3nKnLCMnHGF4PmRdZiUOT2Ra4/DkcrsvViGL3IWRyKnLaIfKjwohBnPIvucpHNxUCX
A/Ju4SJykFk4yICsxUDtJYu2S8Mxi2yNAvigyLkG5HNLFtXOV8tY5AqrFuZioMiAvHu0ZBF5UWJO
kYsOyFoMFCzyNB3/9MniiTtUiB9yvCu2ZKHFwKlcc/HkPl5v5l979Ovv7mP3fdCdU6jIWgyUCvFx
sJ5/+mZ3f0Ho6xXuQfYiazFQtsLHzhW5owqfFDlXjrUYqFHhkyIf57jTEOcdkLUYVDib+UmaBuS8
nx7XdZG1GIS4aoUnm6nwSZGTc6zFoMIq3H5A1mJQ4UoV3nyI1xTZ30CnG+So2vxOG+dxPFS47Tg8
2lNm0R9P3zY5WPP+umCQZ2+JO635QKiwcXhMMxeRrVE4pAbdaR7K+q8EA+7z+a9bWuypq8LUK/I4
O3/puwfrxZ69QkzVHPe+RFkixFHm4nNPj5PH7MkvG+Fxhe0Veauvi8nnF5uLgZZF3lKF/d0dYEDu
bxZe3OILL18XdqLVg5pDxMx1HlDkgCE2F0Mlj0PjZbL3Iuf9OHkthqoJfvxfonxS5Pg5LnGZpdsg
ez/jlyW8eT/5mizPjcvHU9in39WnQZMtb7tVaQtx84OyNMpXR+yEBatQi43TPT579X6cEJuLS70h
6jHEM3dF3usEx9+qhEglH1SHG678ES7f9ZPf/+rWVn7EH0wXLQ11BdKi14Ee+py2Jm+FAoY44UNb
KnzOS9pW9RviNTefbjXntid7NWF4r1/kKcrNK1w0xEO3uNyx1de5JWv2Q5N9WO1FIuH7ZLnrNTku
epOGy7gNc1yhwu3XKNJOwwp+8tY4Ic71tjrUc37Nm5uMm11ir/a1h4MsWdSpsDUKId7s50VkeY1p
FeLNPzpdLFlUG4dDzMV0NPpdPjer2itNna3K/pqatric/CNcOEco+622NyNXTrAWG4qXbW2d8/Aj
bFX239ddePSzb//j+5pzuu6iW9V86b1a5Lw5blVhaxQfHHnH/zYf4rRw7IqdPFBoq2puw5pbZTlJ
Ofve6+K4zbhk0TbEWpznmOsuxH2tRZZYaqhzk/lbnuU4Gfbv91YWuf7SsBbXPny7e26sHOJ6fxzz
vqaG2lEjZDohx0EqrMVln8A+ZKAvzd/cNDlghj1KQ1V44nd3QrzqHXqFu267olJumXj+nvG6nj3E
Abdq3BY7vvHmRoW1WOVHHH7HiTgqvJTzKDw/8VAKsRZ7DhvbvQeiZIW7CPHOGkXRHG/ynLbRtsqv
zszC5mIMmL2+yyn6Z81sZhbW4rjPYRwtDDUOh2jxZl7wS3+ewOA5qLZVFz6NpPRoLMTDjsOh5+K+
jsvp2buZDwBquPPb7sOr9573gp6jzSsq3GWLO8r0nKdKdyPP/Mt9VvvR5t/X+q3K29Y5V/Y0FA9e
4dAtfvLoDH7I9rVScfVCVucegod/JVrfaqtO7nfpNJq22bs+rxceKsRb+nFumxch7TANm+P4n8Cd
d0BO+7kevvOa22bfqpn3nv0Trhm8wl2uUVBtNB58q5a+y8m+2YbizS9KhGvxxo65La1UrJz4kr95
/ByX3mwhHqrCgebiRUde/MO0uxwH3KXrt6rori76uW5CPMiiRNA1ipnHXy8nkI3z18+Xo7lyPxTa
qqIvq61+ZONw726j5WDznyQb9pd4D1s1Z6JcdCW3NSNqoa1atAFLfx+bdnVnFR5wFj52s9/vPepQ
eTVDhdFiaN9l/UWLAcJxfjGAFgOgxQBaDIAWA2gxAFoMoMXQCZfeQItBkdFi4FGR7QS0GAzIaDGg
yGgxKDJaDJwtsp2AFoMBGS0G6nr5+q2dsD23dgGsV/k6HVOOR7sinBYDUSqsyFtljQK6DPFxka1a
mItBhUM45NiArMWgwiFyvLNkocWgwopMMuvFsJ0QnxSZjtzs93t7AbbaWQOyuRgIEW4zshYDiowW
Ax8W2U7QYsCAjBYDiqzFgCKjxcDZItsJWgwYkNFiQJG1GFBktBg4W2Q7QYsBOdZiAB8qVJ3PLwZU
WIsBFUaLARWOwHoxCLEQm4sBFUaLQYXRYkCFuWO9GIQYczGgwmgxqDARWKMAIcZcDKgwWgwqjBYD
Kswd68UgxJiLARVGi0GF0WJAhbljvRiEmPZu9vu9vQBgLgbQYgC0GAAtBtBiALQYQIsBuPOfAOzd
v44bRRzAcd8pb4DyAjQkfSQicTSE50AIXccrRTwJoQoSSOlzVUoaREFLEXPIsPj+eG3vzp/fzHw+
QshCydk7nt397rBnu78YAAAsUQAAgCwGAABZDAAAshgAAGQxAADIYgAAkMUAAPCfJ4YAAIAc/vj6
5e7BJz/+IosBANDHL6fHYRNZFgMAUCGRo/WxLAYAoGYfB0lkWQwAQJRErtjHshgAAH0siwEACNzH
xRJZFgMA0EYiZ+1jWQwAQGN9nCORfcsdAJDRq7fvDQKZEnn3T6ofaLUYAChUxm+unjf34m8+fDE9
/v6319PjFrelP2kXjGUxAKCP52q4v9aXwrIYAIjVx9Gy8mgNN7QtalgWAwBNJnKtplyQwmG3RQrL
YgCgnz4uk5UJa7j6tqhhWQwAdJ7IaZsyawoX3hYpLIsBgBH7eE1WVqnhTNuihmUxACCRz1h2DZLC
SbZFCstiAIC5pnyYlcFr+KxtUcOFXWy3W3sXAFAg+/J5d3P561ff5X6W/a/zyMotFlVYLQYAWnVb
w9Pjz3/6YXpcIJGLXUtIZFkMAHC8hh81JXLrfbzxQRayGADgrBSe7+ONJWRkMQAwWg3PJ7IlZGQx
ADBWCs/38aajJWR9LIsBADWcIJHdYoEsBgDGSuG++3hjCVkWAwBqOGEfbywhy2IAgKFS+GgiW0Ie
gW+5A4BOPL26vv33729fh3pVuxprsYkfevHsY4GLhwLPMpHI+6wWA0CHcRytj6fUa66P99eJM335
c8UxsYQsiwFAH1fu4+CJXOCuiVCb7y5kWQwAA/XxxhJy7RpuYrF82CVkWQwAIyayJeRiKbxp9tbq
0ZaQZTEADN3Hm1GXkC0ML0vkjvtYFgOARB5lCdnCcMI+7i+RZTEAcL+PN30tIVsYzp3IffSxLAYA
5hK50SVkC8P6WBYDAFn6eNPCErKF4SB93GIiy2IAoG1/Xn92+++bD1/ke4ovv/mn8P76VhMvSeRW
+lgWAwBzon2b9ORebD379OfpcZJE3qUwCfs4eCLLYgCgmRo+MaqmRF7Qx2q4TCIH7GNZDACETuE1
CXX6ErIartXHcRJZFgOAGm54YXhBIk99LIWjJXLdPpbFAKCGe67hh9Rw/D6uksiyGACkcP81vP8x
c7SVyMX6WBYDgBruM4XVcGd9nHvOyGLquD1OhV2xAJDCTdewFB4hkXPMIllM6Ro2CBSeaS7ADJQa
HiGFnWKG7eOEs+vJUHPRQV8K2+SO9whnRAPFsEE8bb7pPZq0s8tqMU69mGkGCnq7MDDh1bAsxnkX
Mw0DBXcWzu0IUlgWZz9/zPy/qrN+lFs7QOoBBRLZ0UMNy2IAAEvIalgWAwAcSGR9PHgKy2IAgDt9
LJGHreH0Wbz4BtkFU9DNuDR0kM29R9gdAJIfvfXxOCmcJYsBoIo1BePCkqMTQyJ3X8OyOO/B0XGW
kePD/DdWkTt4/kfFf0dmtj3hiz86wkNNXUvIfaewLB76DPHwWNbW+XvxUUl/FBvqR/9ix+O/5kz5
8O82N1DFdskyRTI9S8nEnH/G0zd8ceIvGNvmLidS9fH0hcPvbi6dJrqpYVlMe1e9grjpWZGjNroc
q/0f2/TKZXNBHG3Grt/qo5uQY9F9nOPti2cf9XEfKSyLpc/1IK9ZEAecEq2fPo1VscvUIEeq3cso
8BbcPtHuWZJv+MNNyH0BPNSxVx93UMOyWAD1/4I1cfD50Nzp01gVu0wNeJgq8xZk3fBddpe8F2W0
g/DUx4MkcjcpLIuXHH185FaXZ19jG+Fl9HqrQJdjVWCXjHzp3nrtFR7bJvbu3IncXx93WcOyGEFs
eKVGe4lWsTMauo145kmT/PyRa89YLe7jphO57xSWxWhiY7vJOsLLfrE92jsYc6yqDFTwReLTn+je
n1zzcSvF3oWjT5Tq4q3YE0nkVvp4nBqWxQTtS0Hcx/Bm+s0hY1W+yYIvEq8chzWfR1vgXTjx56/f
3bI+kQXjmT6OmcgD1rAsJmJW+ibwzibGuSfROKfP8lPx3N+FKjNWkXfJtE8ULfiWXV+VuYYp9nt7
oyVy3T4eOYVlMT0EsSbOPciphvesk2ijC0tJXnOo2ii5S8b5peQFb0HrS6HLXrwyztrHalgWM24Q
bywSd93ExqrFS4iSu2S0mRkh+BZvo1pFCstiGm5iQYxdqelLgsIjUOa5Yt7Q0vTsdXuxGpbFOBRq
4rHyaMwN72mg7JKLyxiksCyG4VJe40LCXfKsmekgAGpYFsO/p08nRbBLAlI4n0tDQEOnYYMAdklg
v4Z3/xgKWcyIp2Fn4gWs6hFzlyz2NXjQZQqr4eTcREGTZ2Kdp6TzbbgPs7NLSnBi1rBBkMVw8KQl
R4QOQ+2SMb/bz56CFO7GuDdROJB18I5Y0THhI4xVr/OwzC4ZbbQdVQhSw+6RqMJq8RnHyjLfazpy
hSz75lXNl7UPjO2AY1V4l1zw9RmZRtsnN1M3hQ2CLB7iZMnpZ5Tdf1lwJnZySh4fI8fxsGPV0C6Z
drSXHasddlDDsnj0xj3xOCiI15xRlq1ROUVlqr39+bxmkB8+dZm3bNncqDhWjz5vgbGquEvWGu1i
H6MBalgW95kI80dhNVzx3fE//U8/ly+eqGlneLFgPfTn55995TcARxirhnbJtDMz9/HZcQYpLIu1
lwJuIOAsG+ee+WkFf79WtlpnY1Vgl0w1M/O9ZQ4vqOGO+ToPx7gO3yDf+nHiwAaZ/PHfLGNVcpeM
M9rOF6xJYZ8mIYv1gYNpoDdIGTeUIE18UmyjY9XoLhktjiPHOgFr2FA0yq/c3T/RJj/lrPmoI+6N
pLuNC1y2VZmlJX8lLtVLrbU7x5nPBXbJCDexOIBwNIUNgizuvw9WHogdSYOchjfuNg6ZyEnekbrN
VOxaIvi91wV2ySqXIg4aqOEBXWy3W6Nw1tHcARR7QeSwOPrySr6q4GM1wiSUwoAsBkAo62BAFgMA
wDl8EgUAAMhiAACQxQAAIIsBAEAWAwCALAYAAFkMAACyGIAUnl5d1/oKboCEnhgCAJLE8fTYt8cB
shgA/k9kfQzIYgCwhAzIYgA4kMj6GJDFAGAJGZDFAHAgkfUxIIsBwBIyIIsB4EAi99HHr96+f3P1
3NsKshgAztDlUvFtGU+PJTLIYgAYq4bnE1kfgywGgLFSeL6PJTLIYgDUMHcSWR+DLAZACmMJGWQx
AGqYA4msj0EWA6CGsYQMshgAKcyBRNbHkNzFdrs1CgCQO2TzkciQhNViAOikvPUxyGIAwF3IIIsB
gAOJrI9BFgMA+hhkMQDwWB9LZJDFAMCdRNbHIIsBAEvIIIsBgAOJrI+RxQAAlpCRxQAABxJZHyOL
AQAsISOLAQAEMbIYAEANI4sBADUMshgAkMIgiwEANQyyGACQwiCLAQA1DLIYAJDCIIsBADUMshgA
kMIgiwEANQyyGACQwiCLAQA1DLIYAJDCIIsBADUMshgAUMMgiwEAKQyyGABQwyCLAQApDLIYAFDD
MKaL7XZrFAAAGNylIQAAAFkMAACyGAAAZDEAAMhiAACQxQAAIIsBAGDnbwHYu58cN7I6gOOVUW6A
cgFWyYZVJJCm2cxwDhYoN2DDEdhwg4iTMKyCxEhZsZmscoERV0hjxlKP25m0y+WqV78/n4+iCKSM
/zy/qvr69Wvb5xYDANCdpWIAADQxAABoYgAA0MQAAKCJAQBAEwMAgCYGAABNDAAAmhgAADQxAABo
YgAA0MQAAKCJAQBAEwMAgCYGAABNDAAAS/z3D787/NHEAAAo4xxlrIkBANi8jDUxAACyOPSCsSYG
AKB7GWtiAAC6l7EmBgBgnzLWxAAAyOIoC8aaGACA7mWsiQEACFHGmhgAAFm824KxJgYAoHsZa2IA
ALqXsSYGACBuGWtiAABk8YgFY00MAED3MtbEAAB0L2NNDABAsjLWxAAAyOKVF4w1MQDAZd+++8Eg
FC5jTQwAbJuSBWryw8evyzyXYn71j3+vcjvPDSUAMKCMD39/d/cqaQ2fPZeMT0QNa2IAIEoZJ6rJ
z2u4QOKr4S+xdwIAGJrFKbYfPBHE6Z6LIJ7DOjEAsEMZT1HXWefUcJbnoobns04MAOxZxqFq+Nog
Dvtc6tXwpkE8WScGAHbP4t0XWRencMDnUjKIB9yLJgYAWpfxKkGsjPPWsCYGAFqX8bo1rIyT1vCR
/cQAQLgyHlDD2wXx4OdSr4bHB/FknRgAiJnFGy2yDkjhYc+lZBDvddeaGADoUsaDg1gZp6hhTQwA
dCnjvWr47LnI4oA1rIkBgDRlvLgmI9Tw6omvhjUxANA0i6+tyVA1rIwjB7EmBgBqlnHYIFbG0Wr4
yGexAQApy/iJGo4fxDOfS70ajhnEk3ViACBvFp8tsiZK4YvPpV4NB3+E1okBgMRl/LDOmjSIf/G5
COLxrBMDAOnL+P2Hr77/pshzmQqtGaeo4SPrxABAYocaPvw5/I/f/vPvlSpfEGtiAIC5QXz6fw9Z
XKaMC2+liMneCQAgfQ2flfHh7++/+VONMp58x4cmBgCYX8PKmMXsnQAA0tTwzCA+K+MabKXQxACA
IF4YLTYZM4e9EwBAzRo+K+PJVgq+zDoxABC3hlcJ4tMyrrRmbIZoYgCgfhBvdMu2UvA5eycAgC41
fJbFtlKgiQGAjjWsjPlF9k4AACFqeHAQn5VxDbZSaGIAIHEQ7/sAfF4b9k4AAH1r+KyMJ1spNDEA
QM8aVsbYOwEAjK7hsEF8VsY12EqhiQGAcEGc5aHaZNyKvRMAgBp+qownWykaeHZ/f28UAIDtOixp
DZ95/fJTpddFGZ+xdwIAKnhx9+bwxzikNnKnta0UmhgAlPFQr19+yr7IuvXeiV1+79Am41P2EwNA
wTL+8d3bgGU8JdxVPGAn8b5jYpOxJgaAyll8+FsZq+H5Zdw8i+2dAIDKZRxzk3H8rRQlN0tczOLO
WymsEwNA/TKe4q0Zh10wHlDDkWdL260UmhgAupSxrRT71vCUZzt1wzLWxADQKIsnm4zV8DVl3CeL
7ScGgHZlbJPxyCAOuHX4qixussnYOjEANC3jqf0m4+Zbh68q46n6VgpNDACty7jnVgqbJZSxJgYA
HmXxFHWT8RZZqYZvL+OSWayJAYAuWylsllgri6dyC8aaGACoX8aWh5WxJgYArivjSpuM1bAy1sQA
wMIsnvJvMlbDI8s4exZrYgAgUxnPXDC2dXh8Fk+ZF4w1MQBwoYxzbaWwPKyMNTEAsEkWTxm+FFoN
xynjdFnsdQUA5pZx5C+F3jqI//OXvwniq7I415dCWycGAGYJuE589P8lybvpw8etbv/3f3zl1V9c
xlOSrRSaGADIGsSnsfXy1//68PFrQayMNTEA0LGGT7P48PdaZayGVy/jyFmsiQGATDU8XVpxvL2M
1fB2WTxFXTDWxABAkRq+vYzVcNsy1sQAQIIgXpZQV20yFsSdy1gTAwAFa/g0i6dLC8ZqeMcyDpLF
mhgAqLBZYlkZq+EIWTwFWDDWxAAgiAsuD88pY0GsjDUxANCohs/KOOYX8rHjVgpNDABquEsNTz99
Q7VXP3gWT3ssGGtiAFDDLYJYDStjTQwAxA1iy8PsXsaaGADUsBomdBkPmCeaGADUcM0gVsOVsnjr
CaOJAUANq2G6l/FXxpddOFthphkoegbxIWgEMTeW8TGO12WdGJdezDQDhRoeVMPmMyuW8bozShPj
0ouZZqBQw+mD2HzumcUrTq3nrSZi2DOFq68n63Aw0wwUahiWlfEqc8w6MS69mGkGCiq8JTC3G1rx
HZcmxqUXM81AQZEsNs/VsCbGpRczDQMFylgNa2JcfTHNMFagjAWxJsalFzMNAwWnZewoUMOaGFdf
zDQDBbLYgrEa1sRbXjye+MibFW8KAFDGgngA3+0MADQqYxLV8MgPt7ZODAD0ymILxvFrePydamIA
QBnTt4aP7J0AAJqWsd0UglgTAwDYZBylhvcN4sneCQBAFk+2UuxXw0EeyfMVJ9MyPrmMqqdXhwOA
MiZ+DR/ZOwEA8HMZW25oGMSTvRMAVHJxkU/uMLOMLRj3qWFNDED9CH7i3+tjLr59UsYdalgTA9Co
hp+4BXGMMu5cw5r4FyZ9tJuCjAniEDBWwVP4S7cZ/+V4+rmv9fjnjHC3qauMywexJu54eXjiRDbm
bLvX5VB8DBvwtj+bvmWs8g7UyENy6yLZqIxvT8yZT/zG6XTV8PZcYv/NX//8/oPLQsEa1sQMuszE
eYvP+FmRZflt97HqVhgBgzjmpF32rA//1VWP/5ax7XCMn3r98tPh7/cffGxXqRrWxOSrYT+3SjrI
Va+axmrMIbnLgX9tVm5xdwNS1btfZSyINTGNgtgicZApUemqaayGHZI7nqkGZ/FGqbp4v1yuEdur
jGVxjRo+8lqqH0HM0ClRYLHfWHUI4ocHMP4xrHuPX7q1jZ5Xt5/mHbL4uGbMoYZTB7EmVj+CmB2m
RN6r5vhCKlYYuYJ4l0ey3YacYc+o4Sa35mVcoIY1cUetvphHEMeMvLxXzb0ec8CxWvaQkgbxsMez
6cF4estjnkvD02nPMq5Rw5qYyiEiiONft3JdNY1V5yCu1Hl+U3lAGfep4UpBPHX+HbsVP9Z33fva
NOaa7JoQxFkmQ5bfyDFWgtghX/Xo3i6LC//6XbEUfmCdGGdwvMo5HmGEXzhrPh+clLiqjOutGddb
G9bEVGaR2KXdQDkkTQljpYy3COLaL5bPJ6bUyVQQZ7xQdf4Za6KxSrRrYuQn8sLMMk69laJ8DWti
BLGxnVYf5zFfRZtirLYYqMKH5NYz8+zf3PgVcYOn65fubvVZtPodecd7msVTwk3GTWpYEyOIWX+E
j/+s+Wrc/DhbMFDjIyP+Ibngvm6cqMNehafvZcXDbdgdKeMsZdyqho/sJ6ZRrjHs208WvCI1Po/2
8MSveu7X/vuqh+QtM/OWAQw++Fe9EU1xRzyUcfBNxg2DeLJOTLSznnWIMlOi1RfE3HjsXDtWI5eK
I7+IqwzC4om69asw7CW+9l2cs/SKZRxwwbhnDR9ZJ+bnM93uS1Z2TRQb3rwvzYKxKjkPx8yZ3Q/8
gK/dgJ/MECSL4ywY1/6cNU1MpvOpIDYxOq8/lfkVwwi7bJ1JNn3iwzbGKOPBQeyF0MSiJ/F2RkEc
f3gzvkYpFoljdkbSIF58s6FeBefDAmW8Vw0LYk1MoHOoH1X3TCjHUdgpPeyQlJU7Pgxn0YBZPLKM
1fAZv2PnQi6IMcKkOSTNTDqU8YAaNs6fs06Mqy8ihpsOyc7T1U9gSEcQa2JkCoJs/QfgUJosEkOe
GhbEmpg6AeSiqPPEZZlDsvNvCoIajsZ+YtddQUyOUjFWzQ/JJoe/sxyr17BBmMk6MS4VIMcdkiCI
NTEIYsAh6YRDrRoWxJqYgud61ydwSIZlrxFqWBODSw44JIFAQWwQNDGuwYBDEvrWsCC+kc+dAABI
XMMGYRXWiUnDuhQ4JAOyu5oda1gQa2Jcg0GKvXVIel50DmKDsC57JwAA1LAmhlRe3L3xk8qtGWFj
5ZAENdyNvROkvAYbBCNMtwljWtK8hgWxJgZXx1tZxiPmIRlzZjq9EDCIDYImBvD2DKNN3xoWxJrY
SRwDyw6sqTc/JK99FiYMalgTw/4hIov1jbHqNhRGGzWMJkYWM3R4xUfzCTPmkAw1M815IgSxQdDE
yDXdhvcPDsndLHjk3smzbg0LYk1c9nSp2BZcVORIgWlvrByS6d6EmOqoYTSxa2Sgq6+hDjjaG41t
8Jes7VglOiRXvAsnEPYNYoOgifNd85w3x7wEfhwZ8N3duv1R+FBKPVbpDslVxmfxLThTcXsNC2JN
3CUjDIJ3LAUGdq3+OP7np7dQcqm46lhtfUjeONrenqGGud1zQ3DtOfTiudtJNvKrw1pv9uYMdZBj
4fgwxs+N+WNV9aRx1SF5+Je31O1VL/HtA+5Uw+IaNgiaOKJlp+AnTr5qePdXh2EDu9aNDGuLzx/w
xbtuO1ZJD8nTh7fpKVoQI4g1MfI3+jXYUrH3G8Yq7yG54mh71VDDXKv7fmL95O2Kme/1MrfjPOz4
52RXDa6tYUGsiWG3y4/Mqn1pH/zINZBD0mRADWtip3i8QMZ2c/GDySQcORphR9s04KogNgiaGEKw
VOwy7/1D3kMy4LR0pDC/hgWxJna1czKNNYCyONH8HPBtwKsMlGN52CEZaqi97qhhTewUv9otO6W6
JhleY7XL+4e84xBkWjo6UMOauOOlbvVzn5Opeug8/zc9UvZ9tLnGKu8hufuTdQ5nThAbBE1ctgyc
TCu9NLI4eO3dco+7f4fFyAdwvLsCOwoWZPFez9o5nIs1LIg1sSxYcgtOr7JYGa9eeBHWEceMVfND
MtFbNdQwSfkeuwun4HQ/6YPVcyf4N+JG+Kq5LGOVevyXnZNrDz57BbFBKOnZ/f29UZjpiXOxcygO
hAjHxcXHNvghzRwrJ5DtJqEaBjQxAMpYCgOaGABxLIUBTQyARBbBgCYGAIALfBYbAACaGAAANDEA
AGhiAADQxAAAoIkBAEATAwCAJgYAAE0MAACaGAAANDEAAGhiAADQxAAAoIkBAEATAwCAJgaAC17c
vTn8MQ6AJgZAGStjQBMDwE9lbBAATQyALLZgDGhiAFDGgCYGAGUMaGIAeFTGBgHQxADIYgvGgCYG
AGUMaGIAUMaAJgaAR2VsEABNDIAstmAMaGIAUMaAJgaAhzI2CIAmBkAWWzAGNDEAKGNAEwPAQxmX
eS7fvvvBCwqaGACu9uO7t5WeziGLlTEE99wQAKCGx5Txd3evvMSgiQGgYw2fZvHhb2UMmhgAOtaw
Mobg7CcGQBDvU8Y2GUMc1okBUMN7lrEFY9DEAKhhWWwrBWhiANQwyhj2Zj8xAII4UBnbZAy7sE4M
gBoOV8YWjEETA6CGZbGtFDCUvRMACOK4ZWwrBYxhnRgANRy9jC0YgyYGQA3LYlspQBMDoIZRxrAl
+4kBEMTJytgmY1jds/v7e6MAANsl7Ea3bMEYVmSdGACy1rYFY9DEAIAyBk0MAChj0MQAwGkZGwTQ
xAAgiy0YgyYGAJQxaGIAQBmDJgYAHpWxQQBNDACy2IIxaGIAQBmDJgYAHsrYIIAmBgBZbMEYNDEA
oIxBEwMAyhg0MQDwqIwNApoYAJDFFozRxAAAyhhNDACgjNHEAACPytggoIkBAFlswRhNDACgjNHE
AADKGE0MAPCojA0CmhgAkMWymIKeGwIAYKbv7l4ZBDQxAKCGoSB7JwAAQUx31okBADWMJgYAUMNo
YgAANUxn9hMDAIKY7qwTAwBqGE0MAKhh0MQAgBqGzuwnBgBBDN1ZJwYANQyaGABQw6CJAQA1DJ3Z
TwwAghi6s04MAGoYNDEAoIahN3snAEAQQ3fWiQFADYMmBgDUMGhiAEANQ2f2EwOAIIburBMDgBoG
TQwAqGHQxACAGobO7CcGAEEM3VknBgA1DJoYAFDDoIkBADUMndlPDACCGLqzTgwAahg0MQCghqE3
eycAQBBDd9aJAUANgyYGANQwaGIAQA1DZ/YTA4Aghu6sEwOAGgZNDACoYdDEAIAahs6e3d/fGwUA
ADrzO3YAAGhiAADQxAAAoIkBAEATAwCAJgYAAE0MAACaGAAANDEAAGhiAADQxAAAoIkBAEATAwCA
JgYAAE0MAAAd/E+A9u5e17HrPMAwZ6A7CNS4TKVpUglIAE0aO9cRBIY6lUHuIU1Kd4KvJJNKAWLA
vadSqcbILehkDDoc6hwecm9y77W+n+eBYIwB/Rxurv2zXn7DefP09OQoAAAAAAB0ZnoCAAAAAKA7
pRgAAAAAoDulGAAAAACgO6UYAAAAAKA7pRgAAAAAoDulGAAAAACgO6UYAAAAAKA7pRgAAAAAoDul
GAAAAACgO6UYAAAAAKA7pRgAAAAAoDulGAAAAACgO6UYAAAAAKA7pRgAAAAAoDulGAAAAADI4X//
6R8+/eU47OELhwAAAAAASOQUi//mP//H0diKUgwAAAAApCQZb0gpBgAAAAByO/9KCtX4PkoxAAAA
AFCHQeP7KMUAAAAAQEEGjVdRigEAAACA4gwa36QUAwAAAABdGDR+jVIMAAAAAHRk0PicUgwAAAAA
tGbQ+KAUAwAAAACctB00VooBAAAAAJ7rNmisFAMAAAAAXNNh0FgpBgAAAABYpPCgsVIMAAAAALBa
sUFjpRgAAAAA4H41Bo2VYgAAAACAbeQdNFaKAQAAAAA2li4ZK8UAAAAAAHvJ8t0USjEAAAAAwAiR
B42VYgAAACCl3/zwp0//++H9O4ciiI8/fvPdT98ff+19geuOyThUL1aKAQAAgMSOvfhInRzv44/f
eF9grZjfQaEUAwAAAEWc6qQ0ubfXArH3BV4T/8+1U4oBAACAagy07mFVHfa+wFH8QHyiFAMAAACV
GWh90OOB2PtCN4nq8DmlGAAAAGjBQOtyO9Xh6++LN4XskgbiE6UYAAAAaEedvGhkIL7ypnhfSCR7
HT6nFAMAAAB9qZNz6/DN90UyJqZKgfhEKQYAAAD4i1Z1MmYgvvKmHFRjZitZh88pxQAAAAC/ULVO
ZqnDN98XyZiRygfiE6UYAAAA4FUF6mT2QHzlTTmoxuyjTx0+pxQDAAAA3JYuGdcLxDXeFyLrGYhP
lGIAAACAFSIPtDapw+neFyJrXofPKcUAAAAAdwoy0No5EEd+X4hMIH5JKQYAAAB41PiBVnU45vtC
cALxFUoxAAAAwJZ2HWgViGO+L0SmDi+kFAMAAADsYquBVnU45vtCcALxWkoxAAAAwO7uGGgViGO+
L0SmDj9CKQYAAAAY5/pAqzoc830hOIF4E0oxAAAAwBynOvm7X33raAR8XyTjyNThzb15enpyFAAA
AIB0zodAU/vjx7fn//cPv/5t3tfy3U/fl1xskjEdmCkGAAAAmOBZID75+//6/fEXqZNxMb6bgg6U
YgAAAIBBXqvDF52S8UE1jsR3U1CVUgwAAACwr1WB+CKDxgEZNKYYpRgAAABge4/X4Ysk45gMGlOA
UgwAAACwmZ0C8Uu+myImg8bkpRQDAAAAPGRYHX6NQeOYDBqTi1IMAAAAcI/pgfglg8YxGTQmBaUY
AAAAYKmAdfg1Bo1jMmhMWEoxAAAAwA2JAvFLBo1jMmhMNEoxAAAAwAWp6/BrDBrHZNCYCJRiAAAA
gM9KBuKXDBrHZNCYiZRiAAAAgC6B+CKDxjEZNGYwpRgAAABoqnMdvsigcUySMWMoxQAAAEAvAvES
Bo0D8t0U7EopBgAAAOpTh+9m0Dgmg8ZsTikGAAAAyhKIt2XQOCCDxmxFKQYAAABKUYcHMGgck0Fj
HqEUAwAAABUIxLMYNA7IoDF3ePP09OQoAAAAQARfvv/20//++YfvHYolji1MIA7l669+rvFCjuuq
zMs5koy5zkwxAAAAxHLsxQfJeJlTy5OMp/vDr3/73U+5F23tVWTQmOuUYgAAAAjqlIwPqvEC5+Of
qvEwBb5xoudq8Y3GvKQUAwAAQAIGjVcxaLw3gbgMg8acKMUAAACQiUHjVSTjDanD5Rk0bk4pBgAA
gKwMGi/nuynuJhA3ZNC4J6UYAAAA0jNovIpB45sK1GHv71YMGvehFAMAAEApBo2XM2j8jPFhrjBo
XJ5SDAAAADUZNF6l7aCx8WHuYNC4JKUYAAAA6jNovFyTQWPjw2xCMq5EKQYAAIBGJONVig0aGx9m
P76bogClGAAAADry3RSrpB40Nj7MYAaNk1KKAQAAoDuDxqtkGTQWiJnOoHEuSjEAAADwVwaNVwk4
aKwOE5ZB4/iUYgAAAOACg8arzB00FohJxKBxWEoxAAAAcI1B41WGDRqrwxRg0DgUpRgAAABYyqDx
KnsMGgvElGTQOAKlGAAAAFjNoPEqDw4aq8O0YtB4FqUYAAAAeIhB41WWDxoLxDRn0HgwpRgAAADY
hkHjVS4mY3UYLjJoPIBSDAAAADDT11/9/LtffZv9VfzjPx/73X8c/uXfvKfsx6DxfpRiAAAAYDNG
iZf7ZeT679OvPv74TaJX8f+BGCYwaLwtpRgAAAB4iDq8ys2k9dXf/rUah03G6jDRSMabUIoBAACA
ewjEy91Xr07J+BCjGgvExOe7KR6hFAMAAABLqcOrbBiqZg0aq8PkZdB4LaUYAAAAuEEgXm7vJjUm
GQvEVGLQeCGlGAAAALhAHV5lfH7a/Lsp1GE6MGh8hVIMAAAAfCYQrxIkNj0yaCwQ05NB45eUYgAA
AOhOHV4lclRaOGisDsM5g8ZHSjEAAAA0JRCvki4hvRw0FojhuuaDxkoxAAAANKIOr1IgFX35/tvD
QSCG1RoOGivFAAAAUJ9AvEqVQAxsoM+gsVIMAAAANanDq6jDwE21B42VYgAAAChFIF5FIAbuUHLQ
WCkGAACACgTi5WpkHYEYgigzaKwUAwAAQFbq8CrGh4FdZR80VooBAAAgGYF4OePDwBQZB42VYqCL
06OVp2rANQ2sKMjIGbqK8WEgiETJWCkGWux7AVzTwIqCpATi5YwPA5HF/26KWKXY1dDDBLiY4D11
g8D6x4oC3K9XMT4MpBNz0NhMMWDfC+CahhUFRKERr1JjiPj8fXfFBlewiZRiwL4XwDUNiwqAECRj
KC/yR1xKMWDfC+CahhUFQCySMVSS5TdAKMWAfS+AaxoWFQBBnX8hias95JLuG3KUYsC+F8A1DSsK
gAQMGkN8qb8/XSkGbH0BXNCwqADIxKAxRFPjD9hUihm997jvT/Ld9afyhwsDAACQlEFjmKVGHT6n
FAMAAACkZ9AYxqgXiE+UYgAAAIBSDBrDtgrX4XNKMQAAAEBNBo3hEU0C8YlSDAAAAFCfQWNYolsd
PqcUAwAAADRi0Bhe6hyIT5RiAAAAgKYMGtOZOvxMrFJ8/qHWMAMuhVNeF0DJ59dKNwh3BwAAYj5y
q8bUJhC/xkwxAAAAAJ8ZNKYkgfgmpRgAAACACyRjslOHV1GKAQAA5vOtR0BkvpuCXATi+yjFAAAA
Q02JLBf/o/IxcAeDxsSkDj9OKQYAxnltOyFVYF3Rc5VG+8GcNYOXQeoD7rMHDgaNiUEg3pBSTI5b
uweORBsbbxa4Vuz6j7vIWFSb/xssKsKu4bk/s1NjwJt+5e+fe/zvXr03/0HrqkNMkIwZQB3eiVIM
xXcRyx/F3M5rbBE9fFNyYV//r1v21pVF5U0PdZcs80x1eiGRz4gNj/Z9L3O/t3vwuPewdWuMvYO/
+/d/PRx+Pv76jx/fOiBsSCDem1IMdmgUObyes2l7uTABZ1Ht/bNZV+6SzR+rUiTj8pevPd6FCIvW
Tby8r7+SjHmUOjySUgyebqlwhD1Y41phw2lRDfixLSp3yeaPVbXPhU+v7vrrCvJeP/4uxP/WbBfb
kk7J+KAas4xAPIVSDPZm5D7InqRxobDhtKgsKothyl2y82PV8bXXOxFei8Ux3+s73oUsi7bqAuPE
oDGvUYenU4rBxgy7Xyh+lbDhtKgsKndJC3vXQ1H4XIj/di+8HOX9fUKutLUZNOZIII5DKYbK/OGz
NsBQYA9sw2ldxX+Z1lWTu6QHqg4X2NNYca63+8pXZ2Rft27ffRg07kYdjkkpBhsz7H6xdNu9aueR
dWVduUta2E6EYu/4y+Nfad3e/BZpKjFoXJtAHJxS7GE9x9P53NeV96HErqbYQfZ8jIvDfttprKtN
joBFVfIuaW13OxFSv+NJZ6Ldu3mNQeMyBOIslGKAHLsIj8XY/e63nca62vZQWFcHo8TefSeCa7J7
N5syaJyROpyRUgwQ/RHc0zC2jnsfE2eZRbX5YWm+qIwS40TA0mI/Bo2DE4hTU4oBOu5+sW6x57Su
BhychutKI571qmP+zD6NA3YlGcehDpehFANE3DraU5Fx3W61sKf8wGJx0kW1cF3N+oG7rSuZeOIL
jFyQ+5wIN19mxg/Yrr8oV1eC8N0UswjE9SjFAI12v1i6KZbxs3922Euw7Qy+qB5ZVy//QevKXbLM
8o55RW1yIjwS9MNW41UvauICc9fmNQaN96YO16YUAwTaN3reJenS3XUBn/61A16ObWe0RWVdWRUj
75JTyt3gtTFy5dc+ER5/LRPfi/2W4vFf4ruJCMKg8bYE4iaUYiD6lsbuF4Kv3mELeMz+UyxutagO
uoa75IzlHeG+PyVT1rjAbv4Spl+F8r4it2yWM2h8H3W4IaUYiLuHsQEGq3fK/tPOs+Elce91VXJR
ycQ1bvqDM2Xqc2HXn/zTv7zYVPusVwTXGTReQiDuTCkG4m5d7H4h8gKeu3rtPy2qdOuqUiwudpd0
0z+M7cVJz4UBP/PgW1uNV+TDXR5h0PicOsyRUgyIlXN2jA47eRdwkNW7a9ew82x7M/IhRLe7pJv+
sOtq6mvsyO/DKbYmXVRJofOgsUDMM0ox2JYzervoyJN6DVu91lX5dbVf18j+CYS7ZJOXI+258VlR
dNZh0Fgd5gqlGDzyYvcLiRewqFfg8ug4WwmzFsPeLyr7F/L6Rnh3c+jsfNC4BoGYJZRi8GiI33mK
lQzuSs9/Kp9AlL9LysQTT4REp8OUP7jVDR3YijrMWr60G2zIW/v0IC4TQ/ZlvN8PZq8upeEu2fwV
OUndzV2mIKMP798d/3IoWMtMMXgutPt12MFKBtwlvahXX44SB5CCNMzjlGLoQuWx+8Wqjn/WYN3S
562Ze5e09uKstMjPS57lgPjUYbalFAN2v7YWANOulj0PbOR7hLukl3bxRbkgAIQiELMTpRhaUC0d
bQBwlwSAvNRhBlCKAex+AcBd0mu88dKMFQNMIRAzklIMNmY4zqRn9w64S7pm5r2FeYgCeEYdZhal
GGDLbaetDgC4SwLAHQRiplOKATbeCdsGA4C7JAAsoQ4TilIMsP02+GBsCgDcJQHgFQIxMb11CAD2
2wnDGJoLOGHdJb0FzgiA4D68f3f6y9EgJjPFAPtug+1/AKDJXbLw12v4CBzgbrowiSjFADaNAOAu
CQCbUYdJSikGsA2G26zhnm/63lOE1hX17pIlb/oGigEWEojJzvcUAwzaYtllkX0NOwhAmbvkrjG3
2AVz15fj4yKgBt8+TBlKMUCRvRbN2Wzjqoj1AADD+OPpKEkpBhi9DbYTJu/qdRC68QkEhe+Sxoqn
vxBXGCAjdZjalGI8wsKETYtFS9LVa+liUbnOWBXLX0XqF+KzbYAT48P0oRR7vgeenyDDtsE2YGRk
3XpysKjcJcvcJS3viT+2XQkQnzpMQ0ox6Z+GbS/ZartyvmMZtnuxgMm48bZu2WNRWVe57pKuNoVf
hZMR6Mz4MM0pxXjyxu738nbXNpi8S3rMurV0LSrXw853yUOVj6ZcM6f8nAaKgWjUYTj6wiFgj0dM
fY0su98lf8+AlXb8T9g1seHaHnOFtHQtqv1u7taVu+SwS41r5uDnamc3EIQuDC8pxTZ7ez1r7v0I
KBMzbIty/JvH7ITtnUh0qX95QY68gM+PhhMt/qI6JPkcomrXbniXbHvNHP9E7QoMTCcQwxVKMfme
5jVipmxRDBeTcakPvmBmyR/FPpUZ+XIsqg6Lqu1dctbynrVypjxRe8IBZlGHYSGlWD7I9PirEdPh
xDkYLibhor1+uQ74u0yK1dXasdiicsEZdqinLO+Rv+Nh7rO0ZxtgPIEY1lKKGff4e/fToUBMqG3w
YdTvsbWnYqtFO/0qeuUHuL7Id/3J600W3zyqW83nDrsS7rGodv3JxeICd8m518zNPxGJ8xTtkQYY
Rh2GRyjF2sHMTd3FR0ZdGKfP+elgZ8VW+/OYV9fptbHkKTbmqEb4EMKicpfcKRYHuWbWeCp2RgBj
CMSwibcOgXYw9/H35V/eF5w+9XaJuOaH5RR7cFFZVxZV1bukte0wAvF9eP/u9JejAZtQij29eXqD
+0+fMWeQD1FIt2hzGXB+1T7sFtWUReWCM+AuaW07gEBM6jDsRynGMxzkOIN0BzZctK78zi+LyqJy
l7S2XROAXARiGMD3FPP5Id6WBoKfQf6YO1z5Ux/w8kfbonp50XbFPlT5Y+6s7VXHCmArujAMZqaY
Ug92nk3psPzsVNl23bpyOrP2WFTWlXVV7y5pYTs+wDDGh2EWpZgiT3inn9wTKk22weoDm19CXT/3
Pq26HWGLasy6cpccfJe0sB0TYCf+eDqIwLdP8OpzfJaNjQdT2p4+fl8z+11Rta39jnC3Y2tRUfIu
6fsoPIcDW9GFIRSlmMRbu9eeTRvuwwm4OMVi3ALSvdiR/7mG96nz42xducLUuEu2/SDE8gYepA5D
WEox+R6ClzybisW02gbbszHsklvj0hrhfGk+kFiyGrsOd75L9jmjrXPgEQIxxKcUk2Nf56mUvKeM
4WIKN4IUWSTyqeE3sFtU3vpKd8mqg/MWPPAIdRhyefP09OQosNUjuEdSAKb0kTJ3jQFHL+OxmhXd
PI2Qd/Va/wDAHZRiAACA3cWvxuowADSnFAMAAEwwtx3rwgDAM0oxAABAFHvkY1EYAFhCKQYAAAAA
6O6tQwAAAAAA0JxSDAAAAADQnVIMAAAAANCdUgwAAAAA0J1SDAAAAADQnVIMAAAAANCdUgwAAAAA
0J1SDAAAAADQnVIMAAAAANCdUgwAAAAA0J1SDAAAAADQnVIMAAAAANCdUgwAAAAA0J1SDAAAAADQ
nVIMAAAAANDdFw4BAAAwy5fvvz3+4s8/fO9oAABMpBQDAADzScYAAHMpxQAAQCCnZHxQjQEABlKK
AQCAoAwaAwAMoxQDAADRGTQGANibUgwAAGRi0BgAYA9KMQAAkJJBYwCADSnFAABAegaNAQAepBQD
AAB1SMYAAPdRigEAgIJ8NwUAwCpKMQAAUJxBYwCAm5RiAACgC4PGAACvUYoBAICODBoDAJxTigEA
gNYMGgMAHJRiAACAE4PGAEBbSjEAAMBzBo0BgG6UYgAAgGsMGgMAHSjFAAAAt8nEAf3mhz+dfv3h
/TsHBAAeoRQDAAC8SiDOQjUGgAcpxQAAAL+gDmd3qsaSMQAspxQDAAD8hUBcj0FjAFhOKQYAAPpS
h/swaAwA1ynFAABAOwJxZ5IxAFykFAMAAF0IxJzz3RQAcE4pBgAAKlOHWcKgMQAoxQAAQEECMfcx
aAxAW0oxAABQhDrMtgwaA9CKUgwAAOQmELM3g8YAdKAUAwAA+ajDzGLQGICqlGIAACANgZg4DBoD
UIxSDAAAhKYOE59BYwAKUIoBAICIBGIyMmgMQF5KMQAAEIU6TCUGjQHIRSkGAAAmE4ipzaAxACm8
eXp6chQAAABI57zApiMZAxCNmWIAAAAYzaAxANEoxQAAADCTbzQGIAKlGAAAAEKQjAGYSCkGAACA
WHw3BQDjKcUAAAAQl0FjAMZQigEAACABg8YA7EopBgAAgGQMGgOwOaUYAAAAsjJoDMBWlGIAAACo
wKAxAI9QigEAAKAUg8YA3EEpBgAAgLIMGgOwkFIMAAAA9Rk0BuA6pRgAAAB6MWgMwEtKMQAAADRl
0BiAE6UYAAAAMGgM0J1SDAAAAHwmGQP0pBQDAAAAF/huCoBWlGIAAADgBoPGAOUpxQAAAMBSBo0B
qlKKAQAAgHsYNAaoRCkGAAAAHmLQGKAApRgAAADYjEFjgKSUYgAAAGB7kjFALkoxAAAAsAuNGCAR
pRgAAADYkkAMkJFSDAAAADxKHQbITikGAAAA7iQQA5ShFAMAAAArqMMAJSnFAAAAwG0CMUBtSjEA
AABwmToM0IdSDAAAAPyCQAzQkFIMAAAAqMMA3SnFAAAA0JdADMCRUgwAAADtCMQAPKMUAwAAQAvq
MABXKMUAAABQmUAMwBJKMQAAAFSjDgOwllIMAAAARQjEANxNKQYAAIDE1GEANqEUAwAAQD4CMQDb
UooBAAAgB3UYgP0oxQAAABCaQAzAAEoxAAAAhKMOAzCYUgwAAABRCMQAzKIUAwAAwEzqMAARKMUA
AAAwgUAMQChKMQAAAAyiDgMQllIMAAAA+xKIAYhPKQYAAIBdCMQAJKIUAwAAwGbUYQCSUooBAADg
UQIxANkpxQAAAHAPdRiASpRiAAAAWEEgBqAkpRgAAABuUIcBKE8pBgAAgMsEYgD6UIoBAADgM3UY
gJ6UYgAAABCIAehOKQYAAKApdRgATpRiAAAAehGIAeAlpRgAAID61GEAuE4pBgAAoCyBGAAWUooB
AACoRiAGgLWUYgAAACpQhwHgEUoxAAAAiQnEALCJN09PT44CAAAAAEBnbx0CAAAAAIDmlGIAAAAA
gO6UYgAAAACA7pRiAAAAAIDulGIAAAAAgO6UYgAAAACA7pRiAAAAAIDulGIAAAAAgO6UYgAAAACA
7pRiAAAAAIDulGIAAAAAgO6UYgAAAACA7pRiAAAAAIDulGIAAAAAgO7+DymzsW4BH5izAAAAAElF
TkSuQmCC" />

                </td>

              </tr>
              <tr>
                <td>
                  <img width="380" height="1" src="data:image/jpg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/4QBYRXhpZgAATU0AKgAAAAgABVEAAAQAAAABAAAAAFEBAAMAAAABAAEAAFECAAEAAAAGAAAASlEDAAEAAAABAAAAAFEEAAEAAAABAAAAAAAAAAAAAAAAAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAABAAEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9/KKKKAP/2Q==" align="middle"/>
                </td>
                <td>
                  <img width="190" height="1" src="data:image/jpg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/4QBYRXhpZgAATU0AKgAAAAgABVEAAAQAAAABAAAAAFEBAAMAAAABAAEAAFECAAEAAAAGAAAASlEDAAEAAAABAAAAAFEEAAEAAAABAAAAAAAAAAAAAAAAAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAABAAEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9/KKKKAP/2Q==" align="middle"/>
                </td>
                <td>
                  <img width="380" height="1" src="data:image/jpg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/4QBYRXhpZgAATU0AKgAAAAgABVEAAAQAAAABAAAAAFEBAAMAAAABAAEAAFECAAEAAAAGAAAASlEDAAEAAAABAAAAAFEEAAEAAAABAAAAAAAAAAAAAAAAAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAABAAEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9/KKKKAP/2Q==" align="middle"/>
                </td>
              </tr>
              <tr valign="top">
                <td width="320px">
                  <br/>
                  <table align="center" border="0" width="420px">
                    <tbody>
                      <tr align="left">
                        <xsl:for-each select="n1:Invoice">
                          <xsl:for-each select="cac:AccountingSupplierParty">
                            <xsl:for-each select="cac:Party">
                              <td align="left">
                                <xsl:if test="cac:PartyName">
                                  <xsl:value-of select="cac:PartyName/cbc:Name"/>
                                  <br/>
                                </xsl:if>
                                <!--xsl:for-each select="cac:Person">
                                  <xsl:for-each select="cbc:Title">
                                    <xsl:apply-templates/>
                                    <span>
                                      <xsl:text>&#160;</xsl:text>
                                    </span>
                                  </xsl:for-each>
                                  <xsl:for-each select="cbc:FirstName">
                                    <xsl:apply-templates/>
                                    <span>
                                      <xsl:text>&#160;</xsl:text>
                                    </span>
                                  </xsl:for-each>
                                  <xsl:for-each select="cbc:MiddleName">
                                    <xsl:apply-templates/>
                                    <span>
                                      <xsl:text>&#160;</xsl:text>
                                    </span>
                                  </xsl:for-each>
                                  <xsl:for-each select="cbc:FamilyName">
                                    <xsl:apply-templates/>
                                    <span>
                                      <xsl:text>&#160;</xsl:text>
                                    </span>
                                  </xsl:for-each>
                                  <xsl:for-each select="cbc:NameSuffix">
                                    <xsl:apply-templates/>
                                  </xsl:for-each>
                                </xsl:for-each-->
                              </td>

                            </xsl:for-each>
                          </xsl:for-each>
                        </xsl:for-each>
                      
                      
                        
                      </tr>
                      <tr align="left">
                        <xsl:for-each select="n1:Invoice">
                          <xsl:for-each select="cac:AccountingSupplierParty">
                            <xsl:for-each select="cac:Party">
                              <td align="left">
                                <xsl:for-each select="cac:PostalAddress">
                                  <xsl:for-each select="cbc:StreetName">
                                    <xsl:apply-templates/>
                                    <span>
                                      <xsl:text>&#160;</xsl:text>
                                    </span>
                                  </xsl:for-each>
                                  <xsl:for-each select="cbc:BuildingName">
                                   <xsl:apply-templates/>
                                  </xsl:for-each>
                                  <xsl:if test="cbc:BuildingNumber">
                                   <span>
										<br/>
								   </span>
                                    <xsl:for-each select="cbc:BuildingNumber">
                                      <xsl:apply-templates/>
                                    </xsl:for-each>
                                    <span>
                                      <xsl:text>&#160;</xsl:text>
                                    </span>
									
                                  </xsl:if>
								   
                                  <xsl:for-each select="cbc:PostalZone">
								    <xsl:apply-templates/>
                                    <span>
                                      <xsl:text>&#160;</xsl:text>
                                    </span>
                                  </xsl:for-each>
                                  <xsl:for-each select="cbc:CitySubdivisionName">
                                    <xsl:apply-templates/>
                                  </xsl:for-each>
                                  <span>
                                    <xsl:text> </xsl:text>
                                  </span>
                                  <xsl:for-each select="cbc:CityName">
                                    <xsl:apply-templates/>
                                    <span>
                                      <xsl:text>&#160;</xsl:text>
                                    </span>
                                  </xsl:for-each>
                                </xsl:for-each>
                              </td>
                            </xsl:for-each>
                          </xsl:for-each>
                        </xsl:for-each>
                      </tr>
					   <tr align="left">
                        <td align="left">
                          <xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cac:AccountingSupplierParty">
                              <xsl:for-each select="cac:Party">
                                <xsl:for-each select="cac:PartyTaxScheme">
                                  <xsl:for-each select="cac:TaxScheme">
                                    <xsl:for-each select="cbc:Name">
									<xsl:apply-templates/>
                                    </xsl:for-each>
                                  </xsl:for-each>

                                </xsl:for-each>
                                <span>
                                  
                                </span>
                              </xsl:for-each>
                            </xsl:for-each>
                          </xsl:for-each>
                          <xsl:for-each select="//n1:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PartyIdentification">
      <xsl:if test="cbc:ID/@schemeID = 'VKN'">
                            
                              <xsl:text> Vergi No: </xsl:text>
                            
                            <xsl:value-of select="cbc:ID"/>
</xsl:if>
                          </xsl:for-each>
                          
                        </td>
                      </tr>
					  <tr>
                        <td> <xsl:for-each select="//n1:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PartyIdentification">
 <xsl:if test="cbc:ID/@schemeID = 'MERSISNO'">

                          
                              <xsl:text>Mersis No: </xsl:text>
                           
                            <xsl:value-of select="cbc:ID"/>
   </xsl:if>
                          </xsl:for-each>
                        
                       </td>
                      </tr>
                       <tr align="left">
					    <xsl:if test="//n1:Invoice/cac:AccountingSupplierParty/cac:Party/cac:Contact/cbc:Telephone or //n1:Invoice/cac:AccountingSupplierParty/cac:Party/cac:Contact/cbc:Telefax">
						<xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cac:AccountingSupplierParty">
                              <xsl:for-each select="cac:Party">
                                <td align="left">
                                  <xsl:for-each select="cac:Contact">
                                                                     
                                    <xsl:if test="cbc:Telefax">
                                      <span>                                      
                                          <xsl:text> Fax: </xsl:text>
                                      </span>
                                      <xsl:for-each select="cbc:Telefax">
                                        <xsl:apply-templates/>
                                      </xsl:for-each>
                                    </xsl:if>
                                    <span>
                                      <xsl:text>&#160;</xsl:text>
                                    </span>
                                  </xsl:for-each><xsl:for-each select="//n1:Invoice/cac:AccountingSupplierParty/cac:Party/cbc:WebsiteURI">
                            <span><xsl:value-of select="."/></span>
                          </xsl:for-each>
                                </td>
                              </xsl:for-each>
                            </xsl:for-each>
                          </xsl:for-each>
						</xsl:if>                      
					   
                          <br/>
                        
					  
					  
					  </tr>
                      
                      <tr align="left">
                        <td>
                         
                        </td>
                      </tr>
           
                      
                       <tr>
                        <td height="30"></td>
                      </tr>
                     <!--  <tr class="header">
                        <td>
                          <span style="font-weight:bold; ">
                            <xsl:text>SAYIN </xsl:text>
                          </span>
                        </td>
                      </tr> -->
                      <tr class="header">
                        <xsl:for-each select="n1:Invoice">
                          <xsl:for-each select="cac:AccountingCustomerParty">
                            <xsl:for-each select="cac:Party">
                              <td style="width:469px; " align="left">

                                <xsl:if test="cac:PartyName">
                                  <xsl:value-of select="cac:PartyName/cbc:Name"/>
                                  <br/>
                                </xsl:if>
                               <!-- <xsl:for-each select="cac:Person">
                                  <xsl:for-each select="cbc:Title">
                                    <xsl:apply-templates/>
                                    <span>
                                      <xsl:text>&#160;</xsl:text>
                                    </span>
                                  </xsl:for-each>
                                  <xsl:for-each select="cbc:FirstName">
                                    <xsl:apply-templates/>
                                    <span>
                                      <xsl:text>&#160;</xsl:text>
                                    </span>
                                  </xsl:for-each>
                                  <xsl:for-each select="cbc:MiddleName">
                                    <xsl:apply-templates/>
                                    <span>
                                      <xsl:text>&#160; </xsl:text>
                                    </span>
                                  </xsl:for-each>
                                  xsl:for-each select="cbc:FamilyName">
                                    <xsl:apply-templates/>
                                    <span>
                                      <xsl:text>&#160;</xsl:text>
                                    </span>
                                  </xsl:for-each
                                  <xsl:for-each select="cbc:NameSuffix">
                                    <xsl:apply-templates/>
                                  </xsl:for-each>
                                </xsl:for-each>-->
                              </td>
                            </xsl:for-each>
                          </xsl:for-each>
                        </xsl:for-each>
                      </tr>
					  <tr>
					  <xsl:for-each select="n1:Invoice">
                        <xsl:for-each select="cbc:Note">
                          <xsl:if test="substring(.,1,9) = 'AltUnvan-'">
                              <td >
                                <xsl:value-of select="substring(.,10,string-length(.)-1)"/>
                              </td>
                          </xsl:if>
                        </xsl:for-each>
						</xsl:for-each>
						</tr>
                     
                      <tr class="header">
                        <xsl:for-each select="n1:Invoice">
                          <xsl:for-each select="cac:AccountingCustomerParty">
                            <xsl:for-each select="cac:Party">
                              <td style="width:469px; " align="left">
                                <xsl:for-each select="cac:PostalAddress">
                                  <xsl:for-each select="cbc:StreetName">
                                    <xsl:value-of select="."/>
                                    <br/>
                                  </xsl:for-each>
                                  <!-- <xsl:for-each select="cbc:CityName">
                                    <xsl:value-of select="."/>
                                  </xsl:for-each>
                                  <xsl:text> </xsl:text>
                                  <xsl:for-each select="cbc:PostalZone">
                                    <xsl:value-of select="."/>
                                  </xsl:for-each> -->
                                </xsl:for-each>
                              </td>
                            </xsl:for-each>
                          </xsl:for-each>
                        </xsl:for-each>
                      </tr>
					  <tr>							
											   <xsl:for-each select="n1:Invoice">
													<xsl:for-each select="cbc:Note">
													  <xsl:if test="substring(.,1,14) = 'BolgeBayiSube-'">
														  <td >
															<b>Bölge/Bayi/Şube Kodu : </b>
															<xsl:value-of select="substring(.,15,string-length(.)-1)"/>
														  </td>
													  </xsl:if>
													</xsl:for-each>
												</xsl:for-each>
											</tr>
                      <tr style="height:13px;">
                        <td align="left" valign="top">
                          <span style="font-weight:bold; ">
                            <xsl:text>ETTN: </xsl:text>
                            <xsl:for-each select="n1:Invoice">
                              <xsl:for-each select="cbc:UUID">
                                <xsl:apply-templates/>
                              </xsl:for-each>
                            </xsl:for-each>
                          </span>
                        </td>
                      </tr>
                      <tr>
                        <td height="15">
                          <br/>
                        </td>
                      </tr>
                    </tbody>
                  </table>

                </td>
                <td align="left" valign="top">
                  <br/>
                        <img style="width:91px;" align="middle" alt="E-Fatura Logo" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/4QBoRXhpZgAASUkqAAgAAAADABIBAwABAAAAAQAAADEBAgAQAAAAMgAAAGmHBAABAAAAQgAAAAAAAABTaG90d2VsbCAwLjIyLjAAAgACoAkAAQAAAKYBAAADoAkAAQAAAKYBAAAAAAAA/+EJ9Gh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8APD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iWE1QIENvcmUgNC40LjAtRXhpdjIiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczpleGlmPSJodHRwOi8vbnMuYWRvYmUuY29tL2V4aWYvMS4wLyIgeG1sbnM6dGlmZj0iaHR0cDovL25zLmFkb2JlLmNvbS90aWZmLzEuMC8iIGV4aWY6UGl4ZWxYRGltZW5zaW9uPSI0MjIiIGV4aWY6UGl4ZWxZRGltZW5zaW9uPSI0MjIiIHRpZmY6SW1hZ2VXaWR0aD0iNDIyIiB0aWZmOkltYWdlSGVpZ2h0PSI0MjIiIHRpZmY6T3JpZW50YXRpb249IjEiLz4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8P3hwYWNrZXQgZW5kPSJ3Ij8+/9sAQwADAgIDAgIDAwMDBAMDBAUIBQUEBAUKBwcGCAwKDAwLCgsLDQ4SEA0OEQ4LCxAWEBETFBUVFQwPFxgWFBgSFBUU/9sAQwEDBAQFBAUJBQUJFA0LDRQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQU/8AAEQgAaQBpAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A/VOiioL6+ttMsp7y8njtbSBGlmnmcIkaKMszMeAABkk0bgT1458QP2nfDvhbxDJ4W8N2F/8AEHxsvB0Hw6gla3PTNzMf3cC567jkelcJqHjHxT+1FJeL4Z1a48B/Bq03i88Vg+Tfa0qZ8wWpb/UwDBzMeTjj+IVTl+JHhz4QeArPT/gf4dtJ7SG/FtqEj6dcuVLQmSGaX7ssiT4wtyPMU/wiQkLXuUcCoO1Vc0/5dkv8T6P+6tel09DzqmIurwdl36v0X6/mdDdaJ8c/HdpJfeJ/GWh/B7QgNz2OhwpfXqIf4ZbubEaN/tRrisTSv2evhJ4v8XXnhrxD4w8W/EHxDaq7Twa9r94UOzZ5gTyzHG2wyR7lTOzeoYDIr1P4l/CeL41aDod415eeGNUjETuypuZ7dmjkmtJoyQGB2Lz1VlBHcHW0D4L+GfDPxC1Xxlp0E9vq2pl3uFWUiFncIHfb3J8tepIB3FQCzZFjeSD5ZcktdIpKz0teW7W/VsHQ5parmXdu/wCGy+4+KPi34e+Cvwt8W+NPDSfBfSr+60p7VNLaTUrkG/zBHcXhY7iV8qKRW4znPOK9b1f4H/Anwn4p1LQNHvPFPgTXtOsZdSdtB1bULYeVFGskjRu7NExVWUkD1I6g4+gfEHwW8EeK9VudS1bw5aX1/cGQy3Eu7e3mQJA/IPG6KKNDjsorD1/9m7wVr2peItQa3vbO/wBes7yyvZ7a8flLpY1nZEYsiMwhQZC9j611vNIzjCLqTTS195u706N7aN7dTH6m4tvli9dNLaa+W/8AkeYeFtE+Lek28M/gP4lP4th+wWuonw98RNM/exxTqWRDf24GZcKQV+bbwTwwJ6rw/wDtT2mka3beHfin4cvfhdr87eXBNqMizaVeN6Q3q/Jnvh9pGQOTVHx/8NvF1l4ss4fBPnpqOq+IV1m8164RFstPtY7B7RINgk3SMn7t1j27WYnJA3Yk8G+L734o+MvEnw08V+FYtY8L6bFNaTXWq+XLPN5TJHHLcIMAGf8AeSJhFwqBlLZ+XOfsq8eecVJWu2rRkvu0evdXfdFR56cuWLad+uqf6r5Ox7+jrKiujBkYZDA5BFOr5QdtX/Za8SX9p4K1R/Hfw/05EuNX8Dtci41bw9A+SJ7XJ3vDgE+U3IAyDySPpTwX400X4h+GLDxD4e1CHVNHvoxLBcwHIYdwR1BByCpwQQQRkV5NfCuilUi+aD2f6NdH+fRtHbSrKb5XpJdP8u/9XNuiiiuI6Ar5m8X3M37U/wARNR8IW9y9t8I/CtwE8R3sTlBrV6mG+wq4/wCWMfBlIPJwPQ13X7TfxD1Twd4FtdE8MMP+E18W3iaFovPMUsv37g+ixR7nz0BC5615L9v8P+GPDKfBnw7pZ8XeE7SyxfX3htxeX9ldQXCec9/aEDzElmOSiszOvmDYV5HuYGhKEPbr4nt5Jby9VtHzvbVI87EVE37N7Lfz7L/Py9To/EfirUNS+KZ8F6PpNv4T1rS7SCTw3GYhPb6rp5a4juIrpIgwhtD9nQKRypeFiMkR17N8P/hZoXw6tIYtMt2MsMBtIZ5yHlitfNeSO2V8AmKMyFUByQuBmsr4HfCWP4R+CLPSJboajfRhla4HmbIkLErDCJHdkiXsm4jJYjGcV6LXHia6b9lRfur8fPv52u92b0aTXvz3/IK+Zf2vv2s4/gnYL4e8NyQ3PjS6QPl1DpYRHo7joXP8Kn6njAPo/wC0d8cLH4D/AA5utcmEc+qz5t9Ns2P+unI4JHXav3mPoMdSK/IfxL4k1Hxdr1/rOr3cl9qV9M09xcSHJdiefoPQdAOBXw2dZo8JH2FF++/wX+Z/QfhlwLHiCs80zGN8NTdkn9uS6f4V17vTue6f8N7/ABl/6GC0/wDBbB/8RR/w3t8Zf+hgtP8AwWwf/EV88gV9ifsa/sejx6bXxx41tSPDiMH0/TZRj7eQf9Y4/wCeQPQfxf7v3vksJWzHGVVSpVZX9Xp5s/oTiDLeDuGsDLH47A0lFaJKEbyfSKVt3+C1eh6x+zL4o+P/AMaGg13X/EMWheDshll/suAT3w9IgU4X/bIx6A84+vJ45HtpEjlMUrIVWXaDtOODjoa8y8Y/tIfDH4XeILfwzrXiW00zUFCJ9kiid1twQNocopWMYxwxGBg9K9NtrmK8t4p4JEmhlUOkkbBldSMggjqCK/RsHGNKLpqpzyW93d39Oh/GHEdevjq8ca8EsNRn/DUYcsXHunZc77v7rI+PtE8Az/AL4gQeJ/HGpy3K27XN3ay2d0ss+vag8TrPcSeZGv2aPyNm6NphCrxxnICiti51K1+AOqad8WPBwkl+DfjDybrX9KjjIXS5JwPL1KGP+FTuUSoB3BweNv0Z478B6L8RNBfS9c0201S3DrNFHexeZGsqnKkgEEjPBGRuUsp4JFeA/DbT00Dxj4p0/wCKfivStd1TXZW0aHR5rZlmisnfy4FMccrxW9vMVbYpRSTJEGkZ2Ar7WniliIudTV2tKP8AMvJdGt79H5Oy/O5UXSkox23T7Pz/ACt1Ppu2uYb22iuLeVJoJUEkcsbBldSMggjqCO9S18//ALM+o3nw/wBd8T/BbWbmS5n8LFLvQbmc5e60aUnyee5hYGInpwor6Arw8RR9hUcL3W6fdPVP7j0aVT2kFLZ9fXqfPujIPib+2HrmoS/vdL+HOjxadaKeVGoXo8yaRT6rCqIfTdXp9z4K8I6t8RYtZ/s5I/F2mQpI1/brJBI8UgkRUkdcLMvyP8jFgCAcDg185fCLwrrvjv4f6x400S2g1W5vviPqHiGXSrq9e0j1G3haS3hhMqq2PLZI5FDAqWiAPByPoL4O2fiCHSdcvfEMipPqOrz3dvpyagb4adGQim387AziRJX2jhPM2DhRXp42PsnaM7ciUbX+/wA9Xd7W13vocdB8+8d3e/5fojvqQkAEk4Apa8a/a6+I7/DL4DeI7+3l8rUL2MabaMDgiSX5SR7qm9h/u187WqxoU5VZbJXPosuwNXM8ZRwVH4qklFerdvwPz3/a++Nknxm+Ld9Lazl/D+kFrHTUB+VlU/PKPd2Gc/3Qo7V4dSk5NPghe5mjiiRpJXYKiKMliTgAe9fjVetPE1ZVZ7tn+leV5bh8mwNLA4ZWhTikvlu35t6vzPe/2Pf2eG+OPj/7RqcLf8Ino5Wa/PIFwx+5AD/tYy2Oig9CRX6ZfEfxEnw3+F/iLWrSCONdG0ue4t4FXCAxxkogA6DIAxXP/s6/Ci2+C3wm0Tw8FRdQ8sXOoSDGZLlwC/PcDhB7IK7Lxn4bs/G3hHWvD95JttdUs5bOVlIyqyIVJHuM5r9Oy7A/UsLyx+OS19ei+R/DHGfFS4mz5VarbwtKXLFd4p+9L1la/pZdD8SNV1S71vU7rUL+eS6vbqVp555TlpHY5ZifUkmv1v8A2P7m9u/2bfAz6gzNOLNkUuefKWV1i/DYFr4y8N/8E8fH9547GnaxPYWXhuKb95q8NwrmaIH/AJZx/eDEdmAA9T3/AEg8PaDZeFtC0/R9NhFvp9hbpbW8Q/gjRQqj8hXkZDgsRQq1KtZNaW1667n6L4s8T5RmmBwuX5ZUjUafPeO0VytJeTd9ultbaGhXgP7Q/g/RdD1jSvHz6fpE+p200apJrt9cR2cdwvMMwtoI3a5nGAqjggKMHgY9+rmPiWryeCNVSK6ns7howIZLW+SylaTcNqJM4IQscLnH8XHNffYWo6VVNddHrbRn8v1oKcGjwb4n63eaTqXwP+M11YTaPe/aYdD1+2miaFktL9Qp8xW+ZVjnCMFbkbuea+ntwr4+8T6HonjP9lH4ry2N5p93qklnLcmWx8XzeIpGazUXCb5pMbJAwJ2IMAFTnnjiP+Hg8v8Aeh/Svell9bG00qEbuDcflo136trfZHnRxMKEm5v4rP57P8kdx+y7oHj/AFX4LfCu78Ha5ZaJYQ2niBdSfU7R7yCSd9UQxAwJPES4CXGHyQo3DHzivqbwXpGoaH4dt7XVptOuNT3yy3E+k2Js7eR3kZyyxF3Kk7ssSxy2498V4/8AsZn+y/h74p8LtxJ4Z8W6vpZX0X7QZlP0KzAj6175XnZnWlPEVIWVuZtaa6tta79TpwkEqUZdbL8kv0Cvh7/gpz4keLRvA2gI/wAk9xc30i+6KiIf/Ij19w1+eX/BTcufHXgsHPl/2dNj6+aM/wBK+LzuTjgKlutvzR+yeF1CNfizCc/2ed/NQlb8dT4ur2n9jvwUnjr9obwnaTxiS0s521GYEZGIVLrn2LhB+NeLV9c/8E1LBJ/jNr90wy1vocgX2LTw8/kP1r87y2mquMpQe11+Gp/ZHGuMngOHMdXpu0lTkl5OXu3+VzqP26vhv8RPiV8X7STw94U1jVNH0/TIrdLi0gZo3kLO7kEf7yj/AIDXxz4o8O674K1mbSNdsrrStThCmS0ugUkQMAy5HbIIP41+4dfjh+054k/4Sz4/+O9QDb0/tSW2RvVYcQr+kYr6DPsFCh/tCk3Kb26H5B4T8TYnNUsmlQhGlh6fxK/M3dWvd21u2dd+xBo8mv8A7SfhbeWeKzFxeOCScbIX2n/vorX6w1+cP/BNLQftnxX8Sasy5Wx0jyQcdGllTH6RtX6PV7fD8OXBcz6t/wCX6H5f4wYlVuJfYx2p04x++8v/AG5BXE/GL4dWnxP8C3ujXUl5HgrcxGwEJmMiZIVRMDGd3K/MMfNnIxkdtRX1MJypyU47o/DpRU4uL2Z8xaH8N20L4XfEjVNb0vxVaan/AMI9c2aXPimbTC7W4tWUpGLBtmwBEyJOcgEdzX46ea3qfzr90f2rfES+Fv2b/iNfswUnRbi1Q/7cy+Sn47pBXxR/w781T/nxH5Gv0nh7NKWGp1a2JdudpL/t1a/mj5bMsHOrKEKWvKvzf/APpZRqvw7/AGjfif4e0Z0trvx54fXX9AeXHlLqVvEYJk54JP7mQ54xXoXwV07xPazXt1qy6vaaXcQqYrHxBqAu7xZlmlBkJGRGrxeSSgOA2QAMZOV+1L4O1W+8L6P468MW5uPF/gW8/tmyhT711AF23VrxziSLPA5JVRWP4cTRLvXLH4ueFf7X8W3fiy13WFjaxqEVSiArPO3ESRkMNpIwcgK7KK/OsfF1I0sYtbe7LyaVk/nG3q79j7/KqkXSxGXSsnL3otq7fXlvdKKvd8z+Fdrs+g6+F/8Agp1oDNaeA9bVfkR7qzkb3YRug/8AHXr7V0DWU1my3GS2e8gIhvI7SbzY4Z9qsyB8DONw5wPoOleJftzeBW8bfs9a1LDH5l1oskeqxgDnahKyflG7n8K8LNKft8FUjHtf7tf0PrOBcb/ZPE+DrVdFz8r/AO304/d71z8oa+tP+CbGpLa/GzWbRiAbrQ5dvuVmhOPyz+VfJhr2P9kLxingj9obwdeTSeXbXN0dPlJOBidTGufYMyn8K/M8uqKli6U33X46H9v8Z4OWP4dx2Hhq3Tk16xXMl87H62a7q0Wg6JqGp3BxBZ28lxIfRUUsf0FfhzqV9Lqmo3N5O26e4laaRvVmJJ/U1+vX7WHiT/hFf2dvHV5u2PLp7WSnvmdhDx/38r8fB1r6TiWpepTpdk39/wDwx+MeCGC5cHjca18UoxX/AG6m3/6Uj9Bv+CY/h/yPCXjbWyv/AB9XsFmrY/55Rs5/9HCvtevm/wD4J/aF/ZH7OWnXO3a2p391dk+uH8ofpFX0hX1OVU/Z4KlHyv8Afr+p+C8e4v67xPjqt9puP/gCUf0CuH+KPjy28IWFvZzWWrXU2q77aBtJVRKH25IR3KqHCCRwM5PlnGTgHtycCvJbnVj4/wBUlstbtbGz0+xiD614X8T2CSoI1LEXUE/3HXjr8y/LzsYGu6tJ25Y7v+v6/I+Vy+lCVT2tZXhDV6/dtrv6K9k5K6PKPiP4hs/i5p3wp+H2leIb3xTbeJ9fXUr+41G3WCddNsSJ5Y5UWNMEuIlBKjOe/Wvq/wApfQV82fss+GrPxj4t8T/Fm208afoV4G0TwpbFSuzTY5WeW4weczzln55wo7Yr6Wr1cTF0YU8LLeC97/E9X92i+R5U5069eriKSajJvlva/L0vZJeeitqJ1r5Y1jT4/wBmPxpqWl6g1xb/AAT8bXLH7TbTPD/wjmoyn51LoQY7eY8hgQEY44Byfqis3xH4c0zxdoV9o2s2UOpaXexNBcWtwu5JEPUEf5xUYetGneFRXhLRr9V5rp92zZnOMrqdN2lHVM5rwNoGuaHf3Ee7R9P8JRIbfTNF023JaGNT8kpmyAS4LFk24Hy4YncW2v7U0fxe+uaEHW+S3X7JfxhSYwZEOYi3TdtIJXqAy56ivnk3niv9keGXS9SfU/FHwbZSlnrdsv2jU/DCngJMuCZrdOqvglAMEEYB6DTLfXbhvDdp8MdaEngO9iiY67Zm2ulkZmle8nuJHzIZmxGEKjG9239MDmxWHlhIxlBc9N7Nflbo+6e3TTU9vBzhmdSbq1FTqpJ66LTd3Sbk+1ruTbbd1Z/CPjn9kf4k+HvGOs6bpnhDV9W022upI7W+t7Yuk8W47HBHquM++ax7b9mr4uWdxFPB4D8QRTROHR1tGBVgcgj8a/UTwt8dvDHie11+88+TTdM0dofN1G/Ait5Y5c+VIjk/dbgjODhlPRhXf2t5BfQRT280c8MqLJHJEwZXQjIYEdQR0NfGLh/CVHzQqP5WP3qfi9n+CgqGKwcLpJNtS1dk9dbXaabXmfLH7Ulr45+Kn7MPhqz07wrqkviLU7i1fVNNSAiS32I5k3L6eYq49QQa+If+GXPiz/0IGuf+Apr9hbi7gtQhmmSISOI03sF3MeijPUn0rF8XePND8CwW8utXjW32gsIY4oJJ5JNq7m2pGrMcLknA4AzXdjcoo4ufta1RqyS6Hy/DHiLmPD+GeX5dhISUpykl7zevRWetkkvRHOfs9+ErjwL8E/BmiXlu1re22mxG4gcYaOVhvdSPUMxBr0JmCgkkADnmuR1T4r+GtI1Pw7Y3F8wk1/Z9glWFzDJvH7vL42jd0AJycivH9evL342DxFoeuLL4E13w5L9qt9RWZVhNoXKyxu5Yh0IjyzYAGY2xkc+sqkaMI0qXvNaJei/yPz14TEZliamNxn7uM25Sk1tzSabS3aUtHa9vz634h+L4vHWv6n8NLRr/AEbV2jjnhvLi3Js73ad7QOUO9Y2ClSw2kgNgnGG828VPqPxg1OH4HeGNVvbjQdNC/wDCb+IjcGZreAncNLinwC8jfcLH5lRfmyxYU6Txtrvx11N9A+FFwfsUUX9na38W7q0jSR4g2WgsSqqJZMk/OoCKeRyQa9++GPwx8P8Awi8IWnhzw5afZrGDLvJId01xKfvyyv1d2PJJ+gwAAPco0f7Pbr1/4r+Ffyro5ea6L5vpfwcXjI4ulHB4ZWpLWT/mlazadk7O3Xbpvpv6PpFnoGk2emadbR2dhZwpb29vCu1Io1AVVUdgAAKuUUVwttu7OZK2iCiiikMa6LIhVgGVhggjIIrwnxD+y8NA1y68SfCXxHP8NdcuH825sLeIT6PfN6zWhwqk9N8e0jJOCa94oroo4iph23Te+63T9U9H8zKdOFT4l/n958uT+MPF/ga3Nl8RvgvLeWIv4tSm1v4cAXltc3ERUpLLa/LMMFEJ3bvuj0qj4c+NvwXuvi/qfjFviTb6Tqd3bmA6br1tPYS2zeXHHsLSlF8seXu2bfvOx3dMfWNfOn7YX/Iqx/7hrso0sHjasYVKXK77xdlf0af4NI1+v47BU5unWbTTTTV9Ha6v52XnoZfgnxZ4P0HwVbabe/G3wjqM9vrttqa3T+IonP2eNoy8RZpOS2x+w+98xY7naT46fHD4HeM9N0uzv/iXoTzWF8LuNbGIat5v7t42jMUYcMGWQ8EEZA4Nfmrqf/IeH+9/Wvvr9h/oP9w/yr3Mbw5g8BhueTlJW2ul+NmctHiXH4nFqtFqM027pdXo9PToa2neNJfFmk+HNO+H3we8R+M20OD7PY+IPGoGl2IXKMJD5mGmAaNGCiMbSi7cYGOtg/Zp8QfFHUU1X40+KU8QxAqy+E9ARrPSE2klRKc+bc4JJG8gDJ4wa+hx0FLXzscTGhphaah57y+97fJI6KrrYp3xVRz30e2ru9PN6+pU0vSrLQ9Ot7DTrSCwsbdBHDbW0YjjjUdFVRwAPQVbooribbd2VtogooopAf/Z"/>
                    <br/><br/>
                       
                         <b style='padding-left : 12px'>
                        <xsl:for-each select="n1:Invoice">
							<xsl:for-each select="cbc:Note">
								<xsl:if test="substring(.,1,13) = 'GONDERIMTURU:'"> 
									
										<b>
										   <xsl:value-of select="substring(.,14,40)"/>
										</b>
									
								</xsl:if>
							</xsl:for-each>
							</xsl:for-each>
                        </b>
                        <br/>  <br/>                   
                  <!--Orta İmza. Eskisi : iVBORw0KGgoAAAANSUhEUgAAAJoAAAB5CAMAAAD28cgJAAAACXBIWXMAABcSAAAXEgFnn9JSAAAABGdBTUEAALGOfPtRkwAAACBjSFJNAAB6JQAAgIMAAPn/AACA6QAAdTAAAOpgAAA6mAAAF2+SX8VGAAADAFBMVEUTExMUFBQVFRUWFhYXFxcYGBgZGRkZGRkaGhobGxscHBwdHR0eHh4fHx8gICAhISEiIiIjIyMkJCQlJSUmJiYmJiYnJycoKCgpKSkqKiorKyssLCwtLS0uLi4vLy8wMDAxMTEyMjIyMjIzMzM0NDQ1NTU2NjY3Nzc4ODg5OTk6Ojo7Ozs8PDw9PT0+Pj4+Pj4/Pz9AQEBBQUFCQkJDQ0NERERFRUVGRkZHR0dISEhJSUlKSkpLS0tLS0tMTExNTU1OTk5PT09QUFBRUVFSUlJTU1NUVFRVVVVWVlZXV1dXV1dYWFhZWVlaWlpbW1tcXFxdXV1eXl5fX19gYGBhYWFiYmJjY2NkZGRkZGRlZWVmZmZnZ2doaGhpaWlqampra2tsbGxtbW1ubm5vb29wcHBwcHBxcXFycnJzc3N0dHR1dXV2dnZ3d3d4eHh5eXl6enp7e3t8fHx9fX19fX1+fn5/f3+AgICBgYGCgoKDg4OEhISFhYWGhoaHh4eIiIiJiYmJiYmKioqLi4uMjIyNjY2Ojo6Pj4+QkJCRkZGSkpKTk5OUlJSVlZWVlZWWlpaXl5eYmJiZmZmampqbm5ucnJydnZ2enp6fn5+goKChoaGioqKioqKjo6OkpKSlpaWmpqanp6eoqKipqamqqqqrq6usrKytra2urq6urq6vr6+wsLCxsbGysrKzs7O0tLS1tbW2tra3t7e4uLi5ubm6urq7u7u7u7u8vLy9vb2+vr6/v7/AwMDBwcHCwsLDw8PExMTFxcXGxsbHx8fHx8fIyMjJycnKysrLy8vMzMzNzc3Ozs7Pz8/Q0NDR0dHS0tLT09PU1NTU1NTV1dXW1tbX19fY2NjZ2dna2trb29vc3Nzd3d3e3t7f39/g4ODg4ODh4eHi4uLj4+Pk5OTl5eXm5ubn5+fo6Ojp6enq6urr6+vs7Ozs7Ozt7e3u7u7v7+/w8PDx8fHy8vLz8/P09PT19fX29vb39/f4+Pj5+fn5+fn6+vr7+/v8/Pz9/f3+/v7///9QcBYHAAAKYklEQVR42sybf1BU1xXHP4hOgBpZY7dRlzQVbEGDo4VUh6opjgaaSqXjj+pY45gx1Rp/dtoZrdbO2NbGaWsyopZaU0erdWzI+GNkrBh/Ra01okYFRikRfxSUiAELGJBfp3+893bf231vF3ff4t5/uO++cy/fvfec7z33nPuihEgtPWwdreknxyMT2oP1zw4bZ99wUfYt6OOXS2mPtg9aT9tGuj4ETkZHoq7dGQLzvxOJZlC1GL67xlabskvXoqBna1QkkscE4J69yGyaNekBjnqbKTcICy2vOHSTfTG6lrafAZ/Zvh3Ik5XmfJN+I6H/IbG7PCG01hxIyJwVD5f0g8AmecrQboPjtohIMcOuu1vXQqI8ZWguaFSrP2a1e42fXC26VJ6EPC5Uk99brY/hU3czjDWRftTU0m1m0EDsVt10D1cre2CVr/AOgPRuWtBtxp+B02MDZuNCSSG83x3QOuGgoeNQ5W8qVPtKl8EHIp0hKWGXde1DyDI0KJx7oZS4gb7SR2AyRMV0i67BIuPzdBERJzgPmUrPEREZS1P4F7SAhYbnI2xS2IQbJtJtjK0REZlASdgX9It9zDA0XIz/GnxUDSSaiN8gsx+A0B72BV3mLZiccVfScMHbZuJrqRARkRFcDfuCwnivHStfZjBtuflPa89Vm4dS1w0Wmml4OkRWy5709UdJNz311aiVeGLDvaC34Jb+eQ/sjIkXwXXMTHxfH5dSWUnYKfcP0Gpc3ySokUILtl9Nslr7IOyU+x/opXssgxsMfp5P+J6p+GeadOfEsFPucPrrSOt3gENEVrLOXHyotrHnh3+jitKfIc79HlgIFHltXe7yP028NPwb1Vi9XAyQpmhchxXVZKjsF/5ZG6yrT2yBLReBm2QH6l0e/iNyInRo9ePgmgdw2ZzUuvn0PhVUFn0wtSXxj1UA7HdlBehW2x0OOKz3aes/2+qsCmNERORU97iSB3xaajKspeM0Ogx/OGYPp7ybbjLLQrgNotRe8eGHNv0aBV5NRfS2ipyCA4CjTAw/NFKyN35uaGgpsZRthy+r1bTuiK8d3njF8Hz/4ymWlgVfUWZvUG63BLGGGx//fXGmn1l7UbGC+YOfRlTyKoOsXnVCfyXiMPKpBEx38C0/C+oCGnd6ucbdBa2agX4WdADwjxOJUU8Fmp+u7crR/gIZ3Row1UqVn2jfNWXUWOq7GFAMNb52pNYY1AhEWV80Rzu6Mu7CqLwQF3Tf+330j/+1VjWVkG4xoSvjbvyTFYF1kdd+u6vcmGCojLYmBuXM8lbanwOPe2Xhv1R+DlLXLgNrjE0jks5Zit8FkXYuBx5Y8RnOh3AOTQbuGZsSU8ssxe/BYynhceCBFX1t8H1x/3qXoD3oD8zw/kmj71unPaCyfNiIwCPPASf6c6RWckjrArQHkOwVKxWRTiW0Z15aYO8ANVYUQE8OVkGe9y9zAHF1AaG15cJ5X42sZ7n4m7WlXVDi+snESRGOa17tCwA4GwhaLixrTPeNkhWzw7rTQ9BOBwE0uKx5PNu9wp8J8GodbPAP7SZwRPL4tc+bvZz1my4i9nQgZJWQK3+HTkPrFnCItMJqf9BqnbCyTkaYrc3bxqCWT4bBR4FMs0oiSawwNB4Gl4hUwC5/0KaCs1WacUekdBr4c3/hxnFAwElTUiHR7DcYRhIDCkWkgphqa2i/AmaLSBx9TLRpmjHeZignfJm83VeqmNgi2W8U7HB3LPRDuUmQ3ilSmcoSk9D/7VHGfi1rszK0hNRogEF6g03zJR+pzWS7yGhmexlGU8DdYCVwWUSmmXK1VBhDQBuzPRN1FkDLEomIyFJguvcox5xUisTG7NO1PYKEQBtVQSJsU0J3/NOULOMwmJp7N368ClhiiJeXKf66trsyBRGRafQUqWCefnYnMaczALSpwG2VPFaYTuoZnY6kAOnV+dAgUg+4zp+CHzW8OyFnbquIXE2iXuBNEZFMJXtVJyLR5IhMMiRj0r001ARaEbBFRKRxPmlVptBOeka5CCy9LSUOakU2A5ukAt6aDJArIhuIE4EcEakF1p+B3SIC60TiDdrinT/1gXYDWHxfyydaGMlR95s64JyINL7BHRFgnUiNx+XKlzQWiaSTotDFX8YDSSKt0CBiGL8AL0Ly+ud1a4ATSn03lpRfqA1anMIrV0VEOlZwS8pY9LHKAbDl6kqgCU6KzAWpzoXNS5R4jdSAPt2rJAm/7ddfywbmeljAiroOaK8mg3JzoXkBd+T04haNTYkXaQO2wjUlxbUWpjSoQSapApFmkgw+wW/8QKtGZ1tlxO+3gqZRZR3a7lyVTo2e6OPOuomkWWQJzARGQ2zRMQc75DqI3CPFM2S+j8XpoDUmQrrbobiezHsSCFomYz2a0uiJV8LyNm36UOhRjRy1ixSyU4HWQZzHk3DRYg1tsiHvuhMfWd8FxaFlepZDm55PlNsyiSq0HBXZgEoRaaFYykkSgxnk+2qPu2EmMM4s9q9LmWlaKAeVHWUXVbpkgiHNUCQiImtUaK9Cyb0qzQALRGrLlP9Rq3V5gVwLaMeBoa0GN5YzRsnqLM+WfgQqRK4lZ+h+SYxHdJJm5aWpbmgHTCY/g6UeIzhnfnp/53WgQJ8hK4ZvGE+FA7f+zS0QDXVwuHyxO0ZpSHs8p1WSX4CRgAMaTQ6aP6TwjhqyhlSzc+hVJ7DX6Li85te5Pw35Ijot3q5uuu77T0c0Lci+q2hSnuntAVyaqvU3OYcWAHy/2df1yNlteSftchyvyyds1i/hQ50B91F1Tdp/qa3XarNxfgEb1KWdYwJtN7Cj1vfIBDjPWEArc5DZkJdQ7G4w0nNxsrfDMpzZFucDZbZiefcJTu9VW1euKrZ4dyeD5NKEjZ6GV8jWL9Scv3r3aDph5Rdm14s8wlEWwnUnfWmYRcwyvQ9emFYdXJzuDPBYzrMglPia4T5RX9q36XPeWW8MDC7yONoF2yjlZduiknkYiCyE0gicnoOJXxjk9fShQBZ2lN73p54aaxpjCzLM/BxKAN6G4tziCRfaAS0RNW1hQ0nZr2wnNkGLB56xK5OdOwx677czb9DLtiz7PehYbBs0B3TahazlAVCVZBe0503VI7hyAZw9qUy5ZA+0WN31ilBLLQx5Ccp/ag+0b4Jtn0x9Dusul8KpvnbNmm0fGHTCV0kBHtoCrW+It3K8iovoUUpOMHRofWzEpXx89ZpdFhprIzTluPqMN5pgoSUDd22dtX3gsgVaAvChfRM3YtVL52FvkJcpfDfRT+1b0CtXAC9/MthZ6wd8ZNOXU+oFoNRLPWzJvQvgLBJ7Sr4LnxCWSNBfn0UBW+bZpWuNPb4UwsUdk3LHNjN41t57HnCWcJagFzTxJkS3hxNa0LM2EBvdInuhjcHHVYgUaKMASiIS2gvRQFlEQksYB7wXkdD6pQJXIhJar0RvrzRioJGtuvWRR7nKLlo5KBJnDSfQFJELymRCvrYfLmhvYl8gy2Zd6+gJ950ROWvRyYq+RSA0ClzJ4eS1ULzcr/+gH5Gpa3DpxX6RCi285f8DANe9D2eqLCTmAAAAAElFTkSuQmCC-->
                     <img align="left" width="90"  src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCACaAL0DASIAAhEBAxEB/8QAHQABAAICAwEBAAAAAAAAAAAAAAcIBQYCBAkDAf/EAEEQAAEDBAECAwUGAgcHBQAAAAECAwQABQYHEQgSEyExFCJBUXEJFSMyYYEWUhcYJEJDYrElY2SCkZKhdIOio7T/xAAUAQEAAAAAAAAAAAAAAAAAAAAA/8QAFBEBAAAAAAAAAAAAAAAAAAAAAP/aAAwDAQACEQMRAD8A9U6UpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUqDOrS/ZCMTxTVuKXqXZ7ltLK4WJruUNzw5MKAtt2ROdaX6oc9mjPISoeYU4kjjjkBms36sOnHXV7cxjK9wY8ze2VFDtrivmbNQr17VR44W4FcfAp5rW09dHTaojuyTKm2z5B13Ar+hv91KhAD6mpS1zqrXOpMfj4vrbDLVj1tjoCA3CjJQpz5qcX+ZxZPmVrJUT5kk1tdBClp61OlC8S/YGd/YbDkhQQWLncE29xKj8CmT2EH9CKluy5BYckhi4Y7e4F0iq44fhSUPtn/mQSK43nHMeyOOYmQ2K3XRhQ4LU2Kh5BHy4WCKiG+dFXTLdpirta9XQcUupJKbliMh6wykq9ee+Ctrnz8+FAjn4UE4Uqvh0j1F69Hjae6lpt9itfkseyLei7NLHwSJ8fwZaPl3LLx+JBPrwHVLketlph9T+n7tgkcKCDlNodN7xxXnwFOSGkB6ID/wAQyhI8+V8eZDZt7blyPDLjYdXansMTINmZp4xtEOYtSYNuiNcePcpyke8mM13JHan3nFqShPmSRq7fSjl+SNpuW0uqvcF0vTg7nv4cvYxy3NK+TMaGkEJHPA8Vxw8Acknmvh0tybbtfP8AanUszMj3KNfr2cRxeYy4l1r7itX4fcyseXY9MVLd5BIUPDPPkALH0FdH9IdS+uECbpjqYnZQ0wkkWDZ0Jq4syDz6C4xUNSmeByAVB31HIPFd/Cuq+3tZVD1h1AYVO1Nm89fhW9m6SEP2e9L54H3fckANPKPKfwl+G6CsDsJqfK13PdfYRtLFZ2E7Dxe35BY7gjskQpzIcbV8lD4oWk+aVpIUk8EEEc0GxUqqL6dydGH9qjuX3aujmCS8y4TKyXDmP5kKPvXGC358pP4zSOOCpLZ5sjhGc4hsnFbdm+B5FCvliuzIfhzobne26nngj5pUCClSSApKgQQCCKDO0pSgUpSgUpSgUpSgUpSgVAfU7zBzvp+v7iSWImzWojp+CTKtVwZQT/7ikJ+qhU+VA3W5Cko6eLzmlvaU5O1/cLXm0dKRyT91zWZTgHyJZadHP60E80rrwJsW5QY9xgupdjS2kPsrT6LQoBSSPqCK7FApSlAr4TVsNQ33JaEqYQ0pTqVDkFAB5BHxHFfeuhfmnHrHcWWgStyI8lIT68lBA4oKP9L+j9g4z054LvPp6yFVsy+/Wz79v2KXGStVhyUvuLdKFIJPsUjsUENvtcAdqQtKk+ltNMbesG6sKbyyyw5dtlR5L1tvFnnJ7JlouLJ7X4j6fgtCvj6KSUqHkoVpnRG82/0i6iW0UlIxG3IPb80tAH/yDWMwVhFh619o2expCLdfsKxzIbq2j8ibp7TOihzj0C1x47PJ9SG08+lBYGlKUCqr7E1Jm3TZlVz370xWN242W4PGbnetY3kzdU8fiXG1o9GZ6UjlTaQEvhIHHeB32opQapq7Z+E7kwS07I15emrpYr0x40d9HkpJB4W24n1Q4hQKVJPmFAitrqpeawv6nW9Y+17Cn2bUW2bs1b82gJ92PYb+8QiNeED8rbT6uGpB90dxQslR7Ui2lApSlApSlApSlApSlArGZNj9tyzG7rit5YD1vvMJ+3y21DkLZebKFpI/VKiKydKCD+jHILldunrHcdyB9Tt9wVcrCruVklXtVrfXEKlc8nlaGm3PPz4cFThVfNZAa66sto65X+Hb9gW+DsO0o8gkyUJTb7kkfM9zMJw/H8Y/tYOgUr89K+MybDt0ZydcJbMaOynuceecCEIHzKj5AfWg+9KxGW5TZ8KxO85rfpIYtVht0i6THuRwiOy2pxav2Skmqjat69M9zZ+9YO/o1+97ISIM+0WDG5SnYjFvmxUSGTc57qQ1DW13eG73e8VDhDaqDYdA7Zw7QHTlk1tzqephjWOa3/EY8NlAclS1Ce47AiRmk+bjrkeRGDaB5kEE8AEiRumrAMwtMLJNubVhJh57s2c1dLnAS4HBZ4TTfhwbYFDyV4DXJWR5F114jyNVB6BZ0bc/V5vfKtzWO1v51id8S9bI8GS85arfISXIMuRFYc8vFKYkVPtCk+IUn+7yRXpPQKUpQKUpQaZuXWdo3JqnK9W31DZiZNapFvK1p7vBcWg+E8B/M24EOJ+SkA1pHRfsa7bX6W9dZrf1uuXZ60fd9wdeV3OPS4bq4jzij/MtyOtR/VRqVcqyW0YZjF4zDIJIjWuxQJFynPH0bjstqccV+yUk/tUHfZ92G7490ea2ZvjPhS7lClXwp+TdwmvzW/8A65CDQSbtzc2CaTx5m/5tNlFc+QmFa7Zb4q5dwuktX5Y8WM2Ct1w/IDgDzUQOTUaRss6y9lp9rxfXeF6os7vJaey+S7eLutHwUYUNbbDKj69qpCyPIEck8Yvct9suo+qLEd2bUa8PBHsWfxaFfXm+6JjN3dlhxTshX+AiU14bXjnhKSwEqIDlWRhTYdyiM3C3S2ZUWQgOMvsOBbbiCOQpKh5EEfEUEFHSHUpdOHL/ANZt7jK495qwYXaIbPPzHtCJDn/VZodAb6j8KtvWvnyFjzHteN2CQn9x7Gn/AFqfKUEBrwTrRsKi5ZOoTX2TIB91jIcEdjKI+SnYcxI5PzDY+lcFbO6usQ88z6a7BlkVH55WDZegvcfMRbi1HJ+gdJ+Hn61P9KCBYXWrpSJLatWzv4l1ZcniEJj51ZH7U0VfJMxQVEX9UvEHzI5HNTdaLzaL/b2btYbrDuUGQnuZkxH0vNOD5pWkkEfQ19Lhb4F1hu2+6Qo8yK+nsdYkNpcbcT8lJUCCPrUH3no01MzcXsi1LJvupL+6vxVTsInG3sOr/wB/BIVDfHzC2Sf1HAICeKVXdWV9WOl+TnGKW/dGLs/mu+KsJtuQstj1U7bXF+BJP/p3EKPwaqTNVbw1humBIma+ylmdIgL8K4215tca4W534tyYroS6yrnke8kA8eRNBHXU+Bg+Yaj300fDaxTKm8fvTnoPui8hMNZUf5USjBc/TsP0PczrqPcwLqv19oC72+E3Z8/sM+VEuCu/x03NhfKGeeewNqbQ4PMclakAEeh3ffmARtqaUzfXslwNfftjlxWXioDwXy2Sy7yfQocCFA/5a859x7uwbqOuWi73iNxnZJs+RgE5TcLFWEz7jZMnbVbpkRa09yUIAfiyQ53qTw0HCrgGgmHqf2fm+x+mDq0tUi6eEjBcpjWi0Lio8FxmIz92vuJK08FRKlPEk+fCuPQAVvXVLv7WDvRpdrNkeWx3Mn2Brh2RarPFSqVOlOP27xEu+C0FLS0CQpTqgEJAJKhxVSrWnqA23oPqzvd8v9m1/Eh3u7XXK8URATNur05mCyVxy+tXYzEJYHatCFLUUr4XwOas9026w1hYPs9zluJ4fbYN5yjWUlV5uiWvElzHRAcQsOPL5WUBST2t89iB5JSAOKCG8gkdT20vs74uTZFmtrw/DYmEW+3RoFr/ALddclBDUXxZ0lxITHac57iy0kuHkhTnmRVlOkrWuB6z3LvrHdaYtCsOPWe4Y5Yo8eKjhK1sWlD7i1K81LWVTD3KUSonzJqJrRKJ+yCtdxVytNvxONLXwOfcj3BK1D/o2amTpvvzEbWe591F0eBfs5yq9NPd/KTFgH2FpQPy7LeFfUmgq30IyEWncGNbQYWBG2fl2xLBIcBPC3eYc+Nz8D7sKVwf14H5q9O68t+k2MbD0RaK2MkEKx3dEWU6vnj+zzZjtre5Py7ZfP1SK9SKBSlKBSlVz3X1XLtGVL0X072BrYm3ZKSlUBlZNsx5B8varrIT7rSEevhA+Io9qfdK0Ehgure8zt0ZjjfRRhct1L2XhF6z6bGWQq1Ysw6kuIKh+V2U4Eso9fIq7h2rBq0MCBCtUGNbLbFaixIbKGI7DSQlDTaAEpQkDyAAAAHyFRJ04dPiNKWm8X3Kckdy3Y2aSU3HL8nkICVz5ITwhppP+HGZSShpscBI54A57RMdB1rhb4F2gv2y6wY82HKbUy/HkNJcadQocKSpKgQpJHkQRxUEO9G+GY1KduOi8/zbUbzzinlw8YuQXaVuH1JtstD0VPn5/hoR58/M82ApQQL/AAV1oY8oJsu9tc5UwkjyyLCX4j5T+rkKYlBUfmGgP8tfi7110QHFBevdJXdoH3VR8oukRah+qVwlhP8A3Gp7pQQGNl9ZENYTN6WcOno595dv2UEn9kvQE8/uoUXvTqMhOdtz6KsqcQP8S25hY5CT9AuQ2r/4ip8pQQEep7PIZAu/Rzupr4ExGLPLA/7J/J/YVyd6u7TC4+9+n7e8Dn1JwCXJCfqY3ij/AM1PdKCA/wCuzpmOObvZNm2rj19s1xfU8fXtiGot23tzoW2nLjZlc9pTsEzS2p/2bmES1XGyXWHx6IW49HQHmfmy8Ftkc+78audSg8uNY7L6YL9sJ/XnV5v5G1HHCuZZMomZjI/hiayFKIZk21K0MQpCRx7rqVtr4BSvngVk9a7K0fhcbW8DFdgYO2xr3f8Ae7fFbhXWKO6xXRE5DMlASr3mAJ0dBcHuJDah3e4QPQnYOrNf7SxS64VnWLW+62m8xlxZTTrKe4pV/eSsDuQsHhSVAgpUAQQRXm6jRn8JYD1R6oy+22bKMy18/Zs3sE6Rbo6pVwtMVll1onhHmXGreWnwB7y1ucg94JDtZbrLBM3wLq52vjWzxYMst+SZUlqXa7i0tu+2b7njOuW+QySUPsrKnAhYHc24SpCuQQf3p+6m71rz7P696/znUmaqbx6xZLYImTW2Ii42xclKpPYzJLCi7D7VOIb73UBshPd3gEVqOIWnpx2D069XOf2jVeGTJEHLLnGw+W3Zo3jRGprbUa3JiKSnlsF7hTaUcDuWSByTW6Zj0P4vDz/INCWbp6DxzS82K7WfNI8JaYdksqWmE3llUhJCUOJXFcDbXqozkkccEgMjqHfvT3cvsw5epMh3PidsyZGBXuAu1zLi01MTJ4kllCWVELcUT4falIJVyAOSa3u35XiOO/ZbWrD8Iyi0zr1kWDQbAzFjzEKe+9LyUMOIUlJK0qD8xwq5HI4UT6VBcDo+6eMn0fr6xowRZz/K9p3LDWZybxO5j2+JfJrktaWPG8EBFviLRz2c8qSee491SfbPsaNRo2lOut4yiQ9gHiyZMGzxvGbuILqUhEd2Up1SFMskKUhSW0uK7gFqVx5h2rZjkHEPszM8g2xkmPh+U3u4Q0p8lAWzJ1vN/wD5h+1ehKFpcQlxCuUqAII+IryNndGGubP0bbQ2TaM82NbrhjN3yW0xrRDvwTbpXst1eixmX45bPidwS2FAEFRJ9Oat9D6LcowK1O3GF1071tsOGwX5Dl3yGPNjx0ITypR8drtQgAEn0AA5oLbVHO5uoPUmgrOzddm5dHt701Xh262MpVIuNydJCUtxorYLjpKilPIHaCodxSPOqpYXZOsraF8R/Qd1ZZQ5r8JWlzM8qw+0lqb6gG2R/AS9LRz5+OtTLR8iguip46dOkjF9JlzMMyvP9Iu0Z7ry7lnd3jLM99Clq8NtoOuvezIS0UtlLagFBPn8AA0d6L1YdWH4cs3Xp81bJ/M0lSDmd4YPwURyi1pIPmPeeSUkHuSqp50/pDV2hsUTh2q8SiWSAVeLIWjlciY98XZDyuVvLP8AMongeQ4AAG9UoFKUoFKUoFKUoFKUoFKUoFKUoFUE3fJm67+0nxvYRKFWC8YrZMZyKO413NvwrpOnQ0qV8OES24HcD8F+flV+6oh17wX059kM+MQl5GlbxeIyiPyy7PerdPYX/wAp7j+5oJPtX2f2osWz203rBZ8/HMKhqt8244VEAXCu1wgSH34Ul95wqePhuSVqLfJSsoa54CO02grqWqe3dbXDujI/DmR25CPP4LSFD/Wu3QV21Z0pzcC3pe9oXjNUXWwMTrzccRsaYpR90ybw8h+4urcKj3qK0FDfAHa2tY+PlYmlKCiKsYzG+Z7kvSE3h95atdz22vYN0vDkNxNu/hhT8e69iHyOxbrs/mMG0kkBKyRwmrK9TeFX/OtX/d9hsCci+7rxbLxNx1T6Wk32HFkoeeg9yiEcrSg9qVkIWpKUrISpRqWaUGm602rgW0LU7Iwu6JL1uUmPcLVIaMafanu0HwJUVYDjCwOPdUkcjgjkEE7lWj53pnANhXCNf7xa3oWQwUeHCv8AaZLkC6Rk9wV2JkslKy2SkEtKKm1ccKSoeVYNmHvvX4SiPcrfs+ztADtm+Far6hAB8w42kQ5Sye0AFEUepKzQSpStUw3ZGPZrIlWqM3Otl8tyEOT7LdI5jToqVEhKyg8hbZKVAOtqW0opUErPB42ugUpSgUpSgUpSgUpSgUpSgUpSgVSX7QtabXeoVzcADVx1Fsy1KUTx75gw3Wx+/hqP7VdqqYfaTa6zfYcDUtnwe1zJbt4yp7FbiuOwpwR4FziOMPuuFI9xCUJKio8AFI5+RC2OAsORsFxyM6T3tWmGhXPzDKQaz1cG222W0tNICUISEpSPQAegrnQKUpQKUpQKUrD5Ldp9ujMRbNHbfulxe9mhod58JCu0qU65x59iEpUogEFRASCCoUGq5mxHuu0cDj2ptJvFnfl3Kc8jkKYtS4rzKm1kD8rslUbtbUQFGOpY5LB4kKsNjWMRMcYfWH3ZlwnuB+4T3+PGlu8AdyuPJKQAEpQOEpSAAKzNApSlApSlApSlApSlApSlApSlApSlApSlApSlApSlArG+zeNkYlrTx7HCLbR+fjOAr5+ngN8fU1kq4hCQ4XAn3lAJJ+YHPH+poOVKUoFKUoFKUoFKUoFKUoFKUoFKUoFKUoFKUoFKUoFKUoFKUoFKUoFKUoP/2Q==" alt="UO_imza_16x13mm.jpg"/>
                 </td>
                <td width="40%">
                  <br/>
                  <table align="center" border="0" width="100%">
                    <tbody>
                      <tr>
                        <td>
                          <img width="120" height="1" src="data:image/jpg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/4QBYRXhpZgAATU0AKgAAAAgABVEAAAQAAAABAAAAAFEBAAMAAAABAAEAAFECAAEAAAAGAAAASlEDAAEAAAABAAAAAFEEAAEAAAABAAAAAAAAAAAAAAAAAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAABAAEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9/KKKKAP/2Q==" align="middle"/>
                        </td>
                        <td>
                          <img width="20" height="1" src="data:image/jpg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/4QBYRXhpZgAATU0AKgAAAAgABVEAAAQAAAABAAAAAFEBAAMAAAABAAEAAFECAAEAAAAGAAAASlEDAAEAAAABAAAAAFEEAAEAAAABAAAAAAAAAAAAAAAAAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAABAAEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9/KKKKAP/2Q==" align="middle"/>
                        </td>
                        <td>
                          <img width="180" height="1" src="data:image/jpg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/4QBYRXhpZgAATU0AKgAAAAgABVEAAAQAAAABAAAAAFEBAAMAAAABAAEAAFECAAEAAAAGAAAASlEDAAEAAAABAAAAAFEEAAEAAAABAAAAAAAAAAAAAAAAAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAABAAEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9/KKKKAP/2Q==" align="middle"/>
                        </td>
                      </tr>
					  <tr align="left">
                        <td align="left" colspan="3" width="100%">
                          <b>
                            <xsl:text>İLETİŞİM HİZMET FATURASI </xsl:text>
								<xsl:if test="//n1:Invoice/cbc:Note/. ='MusteriTip-T'">
									 <xsl:text>KURUMSAL</xsl:text>            
							</xsl:if>  
							<xsl:if test="//n1:Invoice/cbc:Note/. ='MusteriTip-G'">
								<xsl:text>BİREYSEL</xsl:text>            
							</xsl:if>  								
                          </b>
                          <br/>
                        </td>
                      </tr>
                      
                      <tr align="left">
                        <td align="left" width="150">
                          <h1>
                            <xsl:text>HİZMET NO</xsl:text>

                          </h1>
                        </td>
                        <td>
                          <h1>
                            <xsl:text>:</xsl:text>
                          </h1>
                        </td>
                        <td>
                          <h1>
                            <xsl:for-each select="//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyIdentification">
								<xsl:if test="cbc:ID/@schemeID = 'HIZMETNO'">
									<xsl:value-of select="."/>                                
								</xsl:if>                              
                            </xsl:for-each>
                          </h1>
                        </td>
                      </tr>
					  <xsl:for-each select="n1:Invoice">
                        <xsl:for-each select="cbc:Note">
                          <xsl:if test="substring(.,1,7) = 'HESAPNO'">
                            <tr>
                              <td >
                                <xsl:text>Hesap No</xsl:text>
                              </td>
                              <td>:</td>
                              <td>
                                <xsl:value-of select="substring(.,8,13)"/>
                              </td>
                            </tr>
                          </xsl:if>
                        </xsl:for-each>
					   </xsl:for-each>
                      
                      <tr align="left">
                        <td align="left" width="150">
                          <xsl:text>Fatura Dönemi</xsl:text>
                        </td>
                        <td>
                          <xsl:text>:</xsl:text>
                        </td>
                        <td>
                          <xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cac:InvoicePeriod">
                              <xsl:for-each select="cbc:StartDate">
                                <xsl:value-of select="substring(.,9,2)"/>/<xsl:value-of select="substring(.,6,2)"/>/<xsl:value-of select="substring(.,1,4)"/>
                              </xsl:for-each>
                              <xsl:text> - </xsl:text>
                              <xsl:for-each select="cbc:EndDate">
                                <xsl:value-of select="substring(.,9,2)"/>/<xsl:value-of select="substring(.,6,2)"/>/<xsl:value-of select="substring(.,1,4)"/>
                              </xsl:for-each>
                            </xsl:for-each>
                          </xsl:for-each>
                        </td>
                      </tr>
                      <tr align="left">
                        <td align="left" width="150">
                          <xsl:text>Vergi Dairesi/No/TCKN</xsl:text>
                        </td>
                        <td>
                          <xsl:text>:</xsl:text>
                        </td>
                        <td>
                          <xsl:for-each select="//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyTaxScheme/cac:TaxScheme">
                            <xsl:value-of select="cbc:Name"/>
                          </xsl:for-each>
                          <xsl:text> / </xsl:text>
                          <xsl:for-each select="//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyIdentification">
                            <xsl:if test="cbc:ID/@schemeID = 'VKN'">
                              <xsl:value-of select="cbc:ID"/>
                            </xsl:if>
                          </xsl:for-each>
						  <xsl:text> / </xsl:text>
                          <xsl:for-each select="//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyIdentification">
                            <xsl:if test="cbc:ID/@schemeID = 'TCKN'">
                              <xsl:value-of select="cbc:ID"/>
                            </xsl:if>
                          </xsl:for-each>
						  </td>
                      </tr>
                      <xsl:for-each select="n1:Invoice">
                        <xsl:for-each select="cbc:Note">
                          <xsl:if test="substring(.,1,14) = 'BirSonrakiFKT-'">
                            <tr>
                              <td >
                                <xsl:text>Bir Sonraki Fatura Kesim Tarihi</xsl:text>
                              </td>
                              <td>:</td>
                              <td>
                                <xsl:value-of select="substring(.,15,11)"/>
                              </td>
                            </tr>
                          </xsl:if>
                        </xsl:for-each>
                        <xsl:for-each select="cac:PaymentTerms">
                          <xsl:for-each select="cbc:Note">
                            <xsl:if test="substring(.,1,28) = 'Bir Sonraki Son Ödeme Tarihi'">
                              <tr>
                                <td >
                                  <xsl:value-of select="substring(.,1,28)"/>
                                </td>
                                <td>:</td>
                                <td>
                                  <xsl:value-of select="substring(.,32,10)"/>
                                </td>
                              </tr>
                            </xsl:if>
                          </xsl:for-each>
                        </xsl:for-each>

                      </xsl:for-each>
                    
                      <tr>
                        <td height="13" ></td>
                      </tr>
                      <tr>
                        <td align="left" colspan="3">
                          <table border="1" height="13" id="despatchTable" width="300">
                            <tbody>
                              <tr>
                                <td style="width:155px; " align="left">
                                  <span style="font-weight:bold; ">
                                    <xsl:text>Özelleştirme No:</xsl:text>
                                  </span>
                                </td>
                                <td align="left">
                                  <xsl:for-each select="n1:Invoice">
                                    <xsl:for-each select="cbc:CustomizationID">
                                      <xsl:apply-templates/>
                                    </xsl:for-each>
                                  </xsl:for-each>
                                </td>
                              </tr>
                              <tr style="height:13px; ">
                                <td align="left">
                                  <span style="font-weight:bold; ">
                                    <xsl:text>Senaryo:</xsl:text>
                                  </span>
                                </td>
                                <td align="left">
                                  <xsl:for-each select="n1:Invoice">
                                    <xsl:for-each select="cbc:ProfileID">
                                      <xsl:apply-templates/>
                                    </xsl:for-each>
                                  </xsl:for-each>
                                </td>
                              </tr>
                              <tr style="height:13px; ">
                                <td align="left">
                                  <span style="font-weight:bold; ">
                                    <xsl:text>Fatura Tipi:</xsl:text>
                                  </span>
                                </td>
                                <td align="left">
                                  <xsl:for-each select="n1:Invoice">
                                    <xsl:for-each select="cbc:InvoiceTypeCode">
                                      <xsl:apply-templates/>
                                    </xsl:for-each>
                                  </xsl:for-each>
                                </td>
                              </tr>
                              <tr style="height:13px; ">
                                <td align="left">
                                  <span style="font-weight:bold; ">
                                    <xsl:text>Fatura ID:</xsl:text>
                                  </span>
                                </td>
                                <td align="left">
                                  <xsl:for-each select="n1:Invoice">
                                    <xsl:for-each select="cbc:ID">
                                      <xsl:apply-templates/>
                                    </xsl:for-each>
                                  </xsl:for-each>
                                </td>
                              </tr>
                              <tr style="height:13px; ">
                                <td align="left">
                                  <span style="font-weight:bold; ">
                                    <xsl:text>Fatura Tarihi:</xsl:text>
                                  </span>
                                </td>
                                <td align="left">
                                  <xsl:for-each select="n1:Invoice">
                                    <xsl:for-each select="cbc:IssueDate">
                                      <xsl:value-of select="substring(.,9,2)"/>-<xsl:value-of select="substring(.,6,2)"/>-<xsl:value-of select="substring(.,1,4)"/> 23:59
                                    </xsl:for-each>
                                  </xsl:for-each>
                                </td>
                              </tr>
                              <xsl:for-each select="n1:Invoice/cac:DespatchDocumentReference">
                                <tr style="height:13px; ">
                                  <td align="left">
                                    <span style="font-weight:bold; ">
                                      <xsl:text>İrsaliye No:</xsl:text>
                                    </span>
                                    <span>
                                      <xsl:text>&#160;</xsl:text>
                                    </span>
                                  </td>
                                  <td align="left">
                                    <xsl:value-of select="cbc:ID"/>
                                  </td>
                                </tr>
                                <tr style="height:13px; ">
                                  <td align="left">
                                    <span style="font-weight:bold; ">
                                      <xsl:text>İrsaliye Tarihi:</xsl:text>
                                    </span>
                                  </td>
                                  <td align="left">
                                    <xsl:for-each select="cbc:IssueDate">
                                      <xsl:value-of select="substring(.,9,2)"/>-<xsl:value-of select="substring(.,6,2)"/>-<xsl:value-of select="substring(.,1,4)"/>
                                    </xsl:for-each>
                                  </td>
                                </tr>
                              </xsl:for-each>
                              <xsl:if test="//n1:Invoice/cac:OrderReference">
                                <tr style="height:13px">
                                  <td align="left">
                                    <span style="font-weight:bold; ">
                                      <xsl:text>Sipariş No:</xsl:text>
                                    </span>
                                  </td>
                                  <td align="left">
                                    <xsl:for-each select="n1:Invoice/cac:OrderReference">
                                      <xsl:for-each select="cbc:ID">
                                        <xsl:apply-templates/>
                                      </xsl:for-each>
                                    </xsl:for-each>
                                  </td>
                                </tr>
                              </xsl:if>
                              <xsl:if test="//n1:Invoice/cac:OrderReference/cbc:IssueDate">
                                <tr style="height:13px">
                                  <td align="left">
                                    <span style="font-weight:bold; ">
                                      <xsl:text>Sipariş Tarihi:</xsl:text>
                                    </span>
                                  </td>
                                  <td align="left">
                                    <xsl:for-each select="n1:Invoice/cac:OrderReference">
                                      <xsl:for-each select="cbc:IssueDate">
                                        <xsl:value-of select="substring(.,9,2)"/>-<xsl:value-of select="substring(.,6,2)"/>-<xsl:value-of select="substring(.,1,4)"/>
                                      </xsl:for-each>
                                    </xsl:for-each>
                                  </td>
                                </tr>
                              </xsl:if>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td height="15">
                  <br/>
                </td>
              </tr>
              <tr>
                <td colspan="3">
                  <table id="bantTable">
                    <tr style="height:20px;">
						<td width="440"  align="center" valign="top"  style="height: 19px; border-top: solid black; border-bottom:  solid black;">
                        <h1>
                          <span style="font-weight:bold;  color: #0000cc;">
                            <xsl:text>SON ÖDEME TARİHİ : </xsl:text>
                            <xsl:for-each select="n1:Invoice">
                              <xsl:for-each select="cac:PaymentMeans">
                                <xsl:for-each select="cbc:PaymentDueDate">
                                  <xsl:value-of select="substring(.,9,2)"/>/<xsl:value-of select="substring(.,6,2)"/>/<xsl:value-of select="substring(.,1,4)"/>
                                </xsl:for-each>
                              </xsl:for-each>
                            </xsl:for-each>
                          </span>
                        </h1>
                      </td>
                      <td align="left" width="70px">

                      </td>
                      <td width="440" align="center" valign="top" style="height: 19px; border-top: solid black; border-bottom:  solid black;">
                        <h1>
                          <span style="font-weight:bold;  color: #0000cc;">
                            <xsl:text>ÖDENECEK TUTAR : </xsl:text>
                            <xsl:for-each select="n1:Invoice">
                              <xsl:for-each select="cac:LegalMonetaryTotal">
                                <xsl:for-each select="cbc:PayableAmount">
                                  <xsl:value-of select="format-number(., '###.##0,00', 'european')"/>
                                  <xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:PayableAmount/@currencyID">
                                    <xsl:text> </xsl:text>
                                    <xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:PayableAmount/@currencyID = 'TRY'">
                                      <xsl:text>TL</xsl:text>
                                    </xsl:if>
                                    <xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:PayableAmount/@currencyID != 'TRY'">
                                      <xsl:value-of select="//n1:Invoice/cac:LegalMonetaryTotal/cbc:PayableAmount/@currencyID"/>
                                    </xsl:if>
                                  </xsl:if>
                                </xsl:for-each>
                              </xsl:for-each>
                            </xsl:for-each>
                          </span>
                        </h1>
                      </td>

                    </tr>
                  </table>
                  <br/>
                </td>
              </tr>
              <tr>
                <td colspan="3">
                  <table border="0" id="AltKalemler" width="950">
                    <tr>
					
                      <td colspan="2" valign="top">
                        <table border="0" id="AltKalemler" width="440" class="text">
                          <tr>
                            <td colspan="2">

                            </td>
                          </tr>
                          <xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cbc:Note">
                              <xsl:if test="substring(.,1,10) = 'TARİFENİZ:'">
                                <tr>
                                  <td colspan="2" bgcolor="#BBBBBB">
                                    <b>
                                      <xsl:value-of select="."/>
                                    </b>
                                  </td>
                                </tr>
                              </xsl:if>
                            </xsl:for-each>
                          </xsl:for-each>
                          <tr>
                            <td height="15">

                            </td>
                            <td></td>
                          </tr>
                          <xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cac:InvoiceLine">
                              <xsl:if test="substring(cac:Item/cbc:Name,1,4) != 'UMTH'">
                                <xsl:if test="substring(cac:Item/cbc:Name,1,6) != 'Taksit'">
                                  <tr>
                                    <td>
                                      <xsl:for-each select="cac:Item">
                                        <xsl:value-of select="cbc:Name"/>
                                      </xsl:for-each>
                                    </td>
                                    <td align="right">
                                      <xsl:for-each select="cac:Price">
                                         <xsl:value-of select="format-number(cbc:PriceAmount, '###.##0,00', 'european')"/>
                                   </xsl:for-each>
                                    </td>
                                  </tr>
                                </xsl:if>
                              </xsl:if>
                            </xsl:for-each>
                          </xsl:for-each>
                         
                          <tr>
                            <td height="15">

                            </td>
                            <td></td>
                          </tr>
                          <tr>
                            <td  style="height: 19px; border-top: solid black; border-bottom:  solid black;">
                              <b>
                                <xsl:text>ÜCRETLER TOPLAMI </xsl:text>
                              </b>
                            </td>
                            <td align="right" style="height: 19px; border-top: solid black; border-bottom:  solid black;">
                              <b>
                                <xsl:for-each select="n1:Invoice">
                                  <xsl:for-each select="cac:LegalMonetaryTotal">
                                    <xsl:for-each select="cbc:TaxExclusiveAmount">
                                      <xsl:value-of select="format-number(., '###.##0,00', 'european')"/>
                                      <xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:TaxInclusiveAmount/@currencyID">
                                        <xsl:text> </xsl:text>
                                        <xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:TaxInclusiveAmount/@currencyID != 'TRY'">
                                          <xsl:value-of select="//n1:Invoice/cac:LegalMonetaryTotal/cbc:TaxInclusiveAmount/@currencyID"/>
                                        </xsl:if>
                                      </xsl:if>
                                    </xsl:for-each>
                                  </xsl:for-each>
                                </xsl:for-each>
                              </b>
                            </td>
                          </tr>
						 
							<xsl:if test="//n1:Invoice/cbc:Note/. ='MusteriTip-T'">

                            <xsl:for-each select="/n1:Invoice/cac:TaxTotal">
                              <xsl:for-each select="cac:TaxSubtotal/cac:TaxCategory/cac:TaxScheme">
                                <xsl:if test="../../cbc:TaxAmount != '0.00' ">
                                  <tr>
                                    <td>
                                      <xsl:if test="cbc:TaxTypeCode='0015' ">
                                        <xsl:text>KDV</xsl:text>
                                         <xsl:text> %</xsl:text>
                                        <xsl:value-of select="../../cbc:Percent"/>
										<xsl:text>  (Matrah </xsl:text>
                                        <xsl:value-of select="format-number(../../cbc:TaxableAmount, '###.##0,00', 'european')"/>
										 <xsl:text> TL )</xsl:text>
                                        </xsl:if>
                                      <xsl:if test="cbc:TaxTypeCode='4080' ">
                                        <xsl:text>ÖİV</xsl:text>
                                        <xsl:text> %</xsl:text>
                                        <xsl:value-of select="../../cbc:Percent"/>
										 <xsl:text>  (Matrah </xsl:text>
                                        <xsl:value-of select="format-number(../../cbc:TaxableAmount, '###.##0,00', 'european')"/>
										 <xsl:text> TL )</xsl:text>
                                      </xsl:if>
                                      <xsl:if test="cbc:TaxTypeCode='1047' ">
                                        <xsl:text>Damga Vergisi</xsl:text>
                                      </xsl:if>

                                    </td>
                                     <td align="right">
                                      <xsl:value-of select="format-number(../../cbc:TaxAmount, '###.##0,00', 'european')"/>
                                      <xsl:if test="../../cbc:TaxAmount/@currencyID">
                                        <xsl:text> </xsl:text>
                                        <xsl:if test="../../cbc:TaxAmount/@currencyID != 'TRY'">
                                          <xsl:value-of select="../../cbc:TaxAmount/@currencyID"/>
                                        </xsl:if>
                                      </xsl:if>
                                    </td>
                                  </tr>
                                </xsl:if>
                              </xsl:for-each>
                            </xsl:for-each>
							
                          
							</xsl:if>
						<tr>
                            <td height="15">

                            </td>
                            <td></td>
                          </tr>
						<xsl:if test="//n1:Invoice/cbc:Note/. ='MusteriTip-T'">
								  <tr>
									

									
									<td style="height: 19px; border-top: solid black; border-bottom:  solid black;">
									
									 <xsl:text>DEVLETE İLETİLECEK VERGİLER TOPLAMI</xsl:text>            
								 
								  	
									</td>
									<td align="right" style="height: 19px; border-top: solid black; border-bottom:  solid black;">
							     <xsl:for-each select="n1:Invoice/cac:TaxTotal">
                                  <xsl:value-of select="format-number(cbc:TaxAmount, '###.##0,00', 'european')"/>
                                  <xsl:text> </xsl:text>
                                  </xsl:for-each>
                             
                            </td>
							<tr>
                             <td height="15">

                            </td>
                            <td></td>
							
                          </tr>

								  </tr>							</xsl:if>
						
                          
                          <tr>
                            <td bgcolor="#BBBBBB">
                              <b>
                                <xsl:text>FATURA TUTARI (TL) </xsl:text>
                              </b>
                            </td>
                            <td  bgcolor="#BBBBBB" align="right">
                              <b>
                                <xsl:for-each select="n1:Invoice">
                                  <xsl:for-each select="cac:LegalMonetaryTotal">
                                    <xsl:for-each select="cbc:TaxInclusiveAmount">
                                      <xsl:value-of select="format-number(., '###.##0,00', 'european')"/>
                                      <xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:TaxInclusiveAmount/@currencyID">
                                        <xsl:text> </xsl:text>
                                        <xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:TaxInclusiveAmount/@currencyID != 'TRY'">
                                          <xsl:value-of select="//n1:Invoice/cac:LegalMonetaryTotal/cbc:TaxInclusiveAmount/@currencyID"/>
                                        </xsl:if>
                                      </xsl:if>
                                    </xsl:for-each>
                                  </xsl:for-each>
                                </xsl:for-each>
                              </b>
                            </td>
                          </tr>

                          <xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cbc:Note">
                              <xsl:if test="substring(.,1,6)  = 'Taksit'">
                                <xsl:if test="substring(.,1,14)  = 'Taksit-Baslik-'">
                                  <tr>
                                    <td>
                                      <b>
                                        <xsl:value-of select="substring(.,15,72)"/>
                                      </b>
                                    </td>
                                    <td align="right">

                                    </td>
                                  </tr>
                                </xsl:if>
                                <xsl:if test="substring(.,1,14)  != 'Taksit-Baslik-'">
                                  <tr>
                                    <td>
                                      <xsl:value-of select="substring(.,8,72)"/>
                                    </td>
                                    <td align="right">
                                      <xsl:value-of select="substring(.,81,20)"/>
                                    </td>
                                  </tr>
                                </xsl:if>

                              </xsl:if>
                            </xsl:for-each>
                          </xsl:for-each>

                         
                           	<xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cbc:Note">
                              <xsl:if test="substring(.,1,11)  = 'Doğal Afet-'">
                               <tr>
                            <td height="5">

                            </td>
                            <td></td>
                          </tr>
                                 <tr>
                                    <td style="height: 19px; border-top: solid black; ">
                                      <xsl:value-of select="substring(.,1,72)"/>
                                    </td>
                                    <td align="right" style="height: 19px; border-top: solid black;">
                                      <xsl:value-of select="substring(.,73,20)"/>
                                    </td>
                                  </tr>
                               
                              </xsl:if>
                            </xsl:for-each>
                          </xsl:for-each>
						  
                         <xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cbc:Note">
                              <xsl:if test="substring(.,1,6)  = 'Mahsup'">
                                <xsl:if test="substring(.,1,14)  = 'Mahsup-Baslik-'">
                                  <tr>
                                    <td >
                                      <b>
                                        <xsl:value-of select="substring(.,15,72)"/>
                                      </b>
                                    </td>
                                    <td align="right">

                                    </td>
                                  </tr>
                                </xsl:if>
                                <xsl:if test="substring(.,1,14)  != 'Mahsup-Baslik-'">
                                  <tr>
                                    <td style="height: 19px;  border-bottom:  solid black;">
                                      <b><xsl:value-of select="substring(.,8,72)"/></b>
                                    </td>
                                    <td align="right" style="height: 19px;  border-bottom:  solid black;">
                                      <b><xsl:value-of select="substring(.,81,20)"/></b>
                                    </td>
                                  </tr>
                                </xsl:if>

                              </xsl:if>
                            </xsl:for-each>
                          </xsl:for-each>
						  
                          <xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cbc:Note">
                              <xsl:if test="substring(.,1,7)  = 'Önceki-'">
                                <tr>
                                  <td>
                                    <b>
                                      <xsl:value-of select="substring(.,8,72)"/>
                                    </b>
                                  </td>
                                  <td align="right">
                                    <xsl:value-of select="concat(substring(.,88,1), ',',substring(.,90,2))"/>
                                  </td>
                                </tr>
                              </xsl:if>
                            </xsl:for-each>
                          </xsl:for-each>
                          <xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cbc:Note">
                              <xsl:if test="substring(.,1,8)  = 'Gelecek-'">
                                <tr>
                                  <td>
                                    <b>
                                      <xsl:value-of select="substring(.,9,71)"/>
                                    </b>
                                  </td>
                                  <td align="right">
                                   <xsl:value-of select="concat(substring(.,89,1), ',',substring(.,91,2))"/>
                                    </td>
                                </tr>
                              </xsl:if>
                            </xsl:for-each>
                          </xsl:for-each>


                          <tr>
                            <td  bgcolor="#BBBBBB">
                              <b>
                                <xsl:text>ÖDENECEK TUTAR (TL) </xsl:text>
                              </b>
                            </td>
                            <td  bgcolor="#BBBBBB" align="right">
                              <b>
                                <xsl:for-each select="n1:Invoice">
                                  <xsl:for-each select="cac:LegalMonetaryTotal">
                                    <xsl:for-each select="cbc:PayableAmount">
                                      <xsl:value-of select="format-number(., '###.##0,00', 'european')"/>
                                      <xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:PayableAmount/@currencyID">
                                        <xsl:text> </xsl:text>
										<xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:PayableAmount/@currencyID != 'TRY'">
                                          <xsl:value-of select="//n1:Invoice/cac:LegalMonetaryTotal/cbc:PayableAmount/@currencyID"/>
                                        </xsl:if>
                                      </xsl:if>
                                    </xsl:for-each>
                                  </xsl:for-each>
                                </xsl:for-each>
                              </b>
                            </td>
                          </tr>
                         <xsl:if test="//n1:Invoice/cbc:Note/. ='MusteriTip-T'">
                            <xsl:for-each select="n1:Invoice/cbc:Note">
                              <xsl:if test="substring(.,1,4) = 'YAZI'">
                                <tr>
                                  <td colspan="2">
                                    <b>
                                      <xsl:value-of select="substring(.,6,100)"/>
                                    </b>
                                  </td>
                                </tr>
                              </xsl:if>
                            </xsl:for-each>
							</xsl:if>
						   <xsl:if test="//n1:Invoice/cbc:Note/. ='MusteriTip-G'">
							<xsl:for-each select="n1:Invoice/cbc:Note">
                              <xsl:if test="substring(.,1,6) = 'Vergi-'">
							  
                                <tr>
								
                                  <td colspan="2">
                                    <b>
									<br>
                                      <xsl:value-of select="substring(.,7,200)"/></br>
                                    </b>
                                  </td>
                                </tr>
                              </xsl:if>
                            </xsl:for-each>
							 </xsl:if>
                          <tr>
                            <td colspan="2" height="15">

                            </td>
                          </tr>
                          
					<tr>
					
					   <td valign="top">
                        <table border="0" id="AltKalemler" >
                          <tr>
                            <td>

                            </td>
                          </tr>
                          <tr>
                            <td height="10"></td>
                          </tr>
                          <xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cbc:Note">
                              <xsl:choose>
                                <xsl:when test="substring(.,1,10) = 'UMTH-GENEL'">
                                  <tr>
                                    <td bgcolor="#BBBBBB">
                                      <b>
                                        <xsl:value-of select="substring(.,6,74)"/>
                                      </b>

                                    </td>
                                    <td align="right" bgcolor="#BBBBBB">
                                      <b>
                                        <xsl:value-of select="substring(.,81,20)"/>
                                      </b>
                                    </td>
                                  </tr>
                                </xsl:when>
                                <xsl:when test="substring(.,1,11) = 'UMTH-TOPLAM'">
                                  <tr>
                                    <td bgcolor="#BBBBBB">
                                      <b>
                                        <xsl:value-of select="substring(.,6,74)"/>
                                      </b>

                                    </td>
                                    <td bgcolor="#BBBBBB" align="right">
                                      <b>
                                        <xsl:value-of select="substring(.,81,20)"/>
                                      </b>
                                    </td>
                                  </tr>
                                </xsl:when>
                                <xsl:when test="substring(.,1,12) = 'UMTH-BASLIK-'">
                                  <tr>
                                    <td>
                                      <b>
                                        <xsl:value-of select="substring(.,13,68)"/>
                                      </b>

                                    </td>
                                    <td align="right">
                                      <b>
                                        <xsl:value-of select="substring(.,81,20)"/>
                                      </b>
                                    </td>
                                  </tr>
                                </xsl:when>
                                <xsl:when test="substring(.,1,12) = 'UMTH-DEVLETE'">
                                  <tr>
                                    <td style="height: 19px; border-top: solid black; border-bottom:  solid black;">
                                      <b>
                                        <xsl:value-of select="substring(.,6,74)"/>
                                      </b>

                                    </td>
                                    <td align="right" style="height: 19px; border-top: solid black; border-bottom:  solid black;">
                                      <b>
                                        <xsl:value-of select="substring(.,81,20)"/>
                                      </b>
                                    </td>
                                  </tr>
                                </xsl:when>
                                <xsl:when test="substring(.,1,5) = 'UMTH-'">
                                  <tr>
                                    <td>
                                      <xsl:value-of select="substring(.,6,74)"/>
                                      <xsl:if test=". = 'UMTH-'">
                                        <br/>
                                        <br/>
                                      </xsl:if>
                                    </td>
                                    <td align="right">
                                      <xsl:value-of select="substring(.,81,20)"/>
                                    </td>
                                  </tr>
                                </xsl:when>
                                <xsl:otherwise>

                                </xsl:otherwise>
                              </xsl:choose>


                            </xsl:for-each>
                          </xsl:for-each>
                         
                        </table>
                      </td>
					  </tr>
                  
				  </table>
                </td>
              </tr>
              </table>
			  </td>
			  </tr>
			  
			 <tr>
                <td height="15"></td>
              </tr>
              
              <xsl:variable name="isSet">
                    
                                   <xsl:for-each select="n1:Invoice">
                      <xsl:for-each select="cbc:Note">
                        <xsl:if test="substring(.,1,8) = 'Tdetay3-'">
						 <xsl:value-of select="substring(.,1,8)"/>
                        </xsl:if> 
                      </xsl:for-each>
                    </xsl:for-each>
                    </xsl:variable>
                    
                    
                    
                <xsl:variable name="isSet3">
                    
                                   <xsl:for-each select="n1:Invoice">
                      <xsl:for-each select="cbc:Note">
                        <xsl:if test="substring(.,1,11) = 'Tdetay3HDT1'">
						 <xsl:value-of select="substring(.,1,11)"/>
                        </xsl:if> 
                      </xsl:for-each>
                    </xsl:for-each>
                    </xsl:variable>



              <xsl:variable name="isSet4">

                <xsl:for-each select="n1:Invoice">
                  <xsl:for-each select="cbc:Note">
                    <xsl:if test="substring(.,1,11) = 'Tdetay3HDT3'">
                      <xsl:value-of select="substring(.,1,11)"/>
                    </xsl:if>
                  </xsl:for-each>
                </xsl:for-each>
              </xsl:variable>
 
              
               <tr>
               
                <td colspan="6">
                
                 <table width="950">
                   
                    <xsl:if test="$isSet3 != ''">
                    <tr>
                      <td  align="left" colspan="4">
                         Hattınız 
                        
                        <xsl:for-each select="n1:Invoice">
                         <xsl:for-each select="cbc:Note">
                        <xsl:if test="substring(.,1,12) = 'Tdetay3HDT1-'">
						 <xsl:value-of select="substring(.,13,20)"/>
                        </xsl:if> 
                      </xsl:for-each>
                      </xsl:for-each>
                      
                      ile 
                     <xsl:for-each select="n1:Invoice">
                         <xsl:for-each select="cbc:Note">
                        <xsl:if test="substring(.,1,12) = 'Tdetay3HDT2-'">
						 <xsl:value-of select="substring(.,13,20)"/>
                        </xsl:if> 
                      </xsl:for-each>
                      </xsl:for-each>
                       tarihleri arasında hat dondurma statüsüne alınmıştır. Hattınız <xsl:for-each select="n1:Invoice">
                         <xsl:for-each select="cbc:Note">
                        <xsl:if test="substring(.,1,12) = 'Tdetay3HDT2-'">
						 <xsl:value-of select="substring(.,13,20)"/>
                        </xsl:if> 
                      </xsl:for-each>
                      </xsl:for-each> 
                      tarihinde yeniden aktif hale geldiğinde telefon hizmetiniz faturalandırılmaya başlanacaktır.
                      </td>
                    </tr>
                   </xsl:if>

                   <xsl:if test="$isSet4 != ''">
                     <tr>
                       <td  align="left" colspan="4">
                         Değerli müşterimiz,  

                         <xsl:for-each select="n1:Invoice">
                           <xsl:for-each select="cbc:Note">
                             <xsl:if test="substring(.,1,12) = 'Tdetay3HDT3-'">
                               <xsl:value-of select="substring(.,13,20)"/>
                             </xsl:if>
                           </xsl:for-each>
                         </xsl:for-each>

                         tarihinde hat dondurma süreniz sona ermiş olup hattınız belirtilen tarihte tekrar görüşmeye açılmıştır.
                         
                       </td>
                     </tr>
                   </xsl:if>
                    
                  </table>
                
                
                  
                </td>
                
                
              </tr>
              
              
               <td colspan="6">
                
                 <table width="950">
                   
                    <xsl:if test="$isSet3 != ''">
                    <tr>
                      <td  align="left" colspan="4"><xsl:for-each select="n1:Invoice">
                        <xsl:for-each select="cbc:Note">
                        <xsl:if test="substring(.,1,12) = 'Tdetay3HDTA-'">
						 <xsl:value-of select="substring(.,13,200)"/>
                        </xsl:if>
                        </xsl:for-each>
                      </xsl:for-each></td>
                    </tr>
                   </xsl:if>
                    
                  </table>
                       </td>     
                            
                            <tr>
                <td height="15"></td>
              </tr>
			
			<tr align="left">
             <td align="left" width="100">
			 
			 <table width="475"  id="notesTable">
			  <xsl:for-each select="n1:Invoice">
                        <xsl:for-each select="cbc:Note">
                          <xsl:if test="substring(.,1,8) = 'ToplamFT'">
                            <tr>
                              <td >
							  <span style="font-weight:bold; ">
                                <xsl:text>Vergi ve Matrah Açıklamaları</xsl:text>
								</span>
                              </td>
							  <td></td>
                              <td>
                                
                              </td>
                            </tr>
                          </xsl:if>
				</xsl:for-each>
				</xsl:for-each>
			   <xsl:for-each select="n1:Invoice">
                        <xsl:for-each select="cbc:Note">
                          <xsl:if test="substring(.,1,8) = 'ToplamFT'">
                            <tr>
                              <td >
                                <xsl:text>Toplam Fatura Tutarı</xsl:text>
                              </td>
                              <td>:</td>
                              <td>
                                <xsl:value-of select="substring(.,9,19)"/>
                              </td>
                            </tr>
                          </xsl:if>
				</xsl:for-each>
				</xsl:for-each>
				<xsl:for-each select="n1:Invoice">
                        <xsl:for-each select="cbc:Note">
                          <xsl:if test="substring(.,1,15) = 'Vergilendirilen'">
                            <tr>
                              <td >
                                <xsl:text>Vergilendirilen Tutar</xsl:text>
                              </td>
                              <td>:</td>
                              <td>
                                <xsl:value-of select="substring(.,16,26)"/>
                              </td>
                            </tr>
                          </xsl:if>
				</xsl:for-each>
				</xsl:for-each>
				<xsl:for-each select="n1:Invoice">
                        <xsl:for-each select="cbc:Note">
                          <xsl:if test="substring(.,1,5) = 'KDV18'">
                            <tr>
                              <td >
                                <xsl:text>KDV %18 Tutar ve Matrahı</xsl:text>
                              </td>
                              <td>:</td>
                              <td>
                                <xsl:value-of select="substring(.,6,16)"/>
                              </td>
                            </tr>
                          </xsl:if>
				</xsl:for-each>
				</xsl:for-each>
				<xsl:for-each select="n1:Invoice">
                        <xsl:for-each select="cbc:Note">
                          <xsl:if test="substring(.,1,5) = 'KDV08'">
                            <tr>
                              <td >
                                <xsl:text>KDV %08 Tutar ve Matrahı</xsl:text>
                              </td>
                              <td>:</td>
                              <td>
                                <xsl:value-of select="substring(.,6,16)"/>
                              </td>
                            </tr>
                          </xsl:if>
				</xsl:for-each>
				</xsl:for-each>
				<xsl:for-each select="n1:Invoice">
                        <xsl:for-each select="cbc:Note">
                          <xsl:if test="substring(.,1,4) = 'OIV5'">
                            <tr>
                              <td >
                                <xsl:text>ÖİV %5 Tutar ve Matrahı</xsl:text>
                              </td>
                              <td>:</td>
                              <td>
                                <xsl:value-of select="substring(.,5,15)"/>
                              </td>
                            </tr>
                          </xsl:if>
				</xsl:for-each>
				</xsl:for-each>
				<xsl:for-each select="n1:Invoice">
                        <xsl:for-each select="cbc:Note">
                          <xsl:if test="substring(.,1,5) = 'OIV15'">
                            <tr>
                              <td >
                                <xsl:text>ÖİV %15 Tutar ve Matrahı</xsl:text>
                              </td>
                              <td>:</td>
                              <td>
                                <xsl:value-of select="substring(.,6,16)"/>
                              </td>
                            </tr>
                          </xsl:if>
				</xsl:for-each>
				</xsl:for-each>
				<xsl:for-each select="n1:Invoice">
                        <xsl:for-each select="cbc:Note">
                          <xsl:if test="substring(.,1,5) = 'OIV75'">
                            <tr>
                              <td >
                                <xsl:text>ÖİV %7,5 Tutar ve Matrahı</xsl:text>
                              </td>
                              <td>:</td>
                              <td>
                                <xsl:value-of select="substring(.,6,16)"/>
                              </td>
                            </tr>
                          </xsl:if>
				</xsl:for-each>
				</xsl:for-each>
				<xsl:for-each select="n1:Invoice">
                        <xsl:for-each select="cbc:Note">
                          <xsl:if test="substring(.,1,5) = 'OIV10'">
                            <tr>
                              <td >
                                <xsl:text>ÖİV %10 Tutar ve Matrahı</xsl:text>
                              </td>
                              <td>:</td>
                              <td>
                                <xsl:value-of select="substring(.,6,16)"/>
                              </td>
                            </tr>
                          </xsl:if>
				</xsl:for-each>
				</xsl:for-each>
				</table>
				
            
			</td>
            </tr>
			
		<tr>
                <td height="15"></td>
              </tr>
			
             <xsl:variable name="isSet6">
                    
                    <xsl:for-each select="n1:Invoice">
                      <xsl:for-each select="cbc:Note">
                        <xsl:if test="substring(.,1,8) = 'TdetayT-'">
						 <xsl:value-of select="substring(.,1,8)"/>
                        </xsl:if> 
                      </xsl:for-each>
                    </xsl:for-each>
                    </xsl:variable>
                   
              <tr>
                <td colspan="6">
                  <table width="475"  id="notesTable">
                   
                        <xsl:if test="$isSet6 != '' ">
                          <tr>
                      <td  align="center" bgcolor="#BBBBBB" colspan="8">
                        <b>Toplu SMS Toplam Atım Adetleri</b>
                      </td>
                    </tr>
                    </xsl:if>
                    <xsl:for-each select="n1:Invoice">
                      <xsl:for-each select="cbc:Note">
                        <xsl:if test="substring(.,1,8) = 'TdetayT-'">
                          <tr>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,9,60)"/>
                              </b>
                            </td>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,69,60)"/>
                              </b>
                            </td>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,129,60)"/>
                              </b>
                            </td>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,189,60)"/>
                              </b>
                            </td>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,249,60)"/>
                              </b>
                            </td>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,309,60)"/>
                              </b>
                            </td>
                            
                          </tr>

                        </xsl:if>
                      </xsl:for-each>
                    </xsl:for-each>

                  </table>
                </td>
            </tr>
			
			<xsl:variable name="isSet7">
                    
                                   <xsl:for-each select="n1:Invoice">
                      <xsl:for-each select="cbc:Note">
                        <xsl:if test="substring(.,1,8) = 'TdetayH-'">
						 <xsl:value-of select="substring(.,1,8)"/>
                        </xsl:if> 
                      </xsl:for-each>
                    </xsl:for-each>
            </xsl:variable>
                   
              <tr>
                <td colspan="7">
                  <table width="475"  id="notesTable">
                   
                        <xsl:if test="$isSet7 != '' ">
                          <tr>
                      <td  align="center" bgcolor="#BBBBBB" colspan="8">
                        <b>Hızlı SMS Toplam Atım Adetleri</b>
                      </td>
                    </tr>
                    </xsl:if>
                    <xsl:for-each select="n1:Invoice">
                      <xsl:for-each select="cbc:Note">
                        <xsl:if test="substring(.,1,8) = 'TdetayH-'">
                          <tr>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,9,60)"/>
                              </b>
                            </td>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,69,60)"/>
                              </b>
                            </td>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,129,60)"/>
                              </b>
                            </td>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,189,60)"/>
                              </b>
                            </td>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,249,60)"/>
                              </b>
                            </td>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,309,60)"/>
                              </b>
                            </td>
                            
                          </tr>

                        </xsl:if>
                      </xsl:for-each>
                    </xsl:for-each>

                  </table>
                </td>
            </tr>
			
		
              <tr>
                <td height="15"></td>
              </tr>
			 
			 <tr>
                <td colspan="8"> 
			  <table width="950"  id="notesTable">
                   
                    <xsl:if test="$isSet != ''">
                    
					<tr>
                      <td  align="center" bgcolor="#BBBBBB" colspan="8">
                        <b>TAAHHÜT BİLGİLERİ</b></td>
                    </tr>
                     </xsl:if>
                    

                    <xsl:for-each select="n1:Invoice">
                      <xsl:for-each select="cbc:Note">
					  <xsl:if test="substring(.,1,8) = 'TdetayK-'">
                          <tr>
                            <td align="left"><b><xsl:value-of select="substring(.,9,60)"/></b></td>
                            <td align="center"><b><xsl:value-of select="substring(.,69,60)"/></b></td>
							<td align="center"><b><xsl:value-of select="substring(.,129,60)"/></b></td>
							<td align="center"><b><xsl:value-of select="substring(.,189,60)"/></b></td>
						
                          </tr>
                        </xsl:if>
                        <xsl:if test="substring(.,1,8) = 'Tdetay3-'">
                          <tr>
                            <td align="left"><xsl:value-of select="substring(.,9,60)"/></td>
                            <td align="center"><xsl:value-of select="substring(.,69,60)"/></td>
							<td align="center"><xsl:value-of select="substring(.,129,60)"/></td>
							<td align="center"><xsl:value-of select="substring(.,189,60)"/></td>
							
                          </tr>
                        </xsl:if>
                      </xsl:for-each>
                    </xsl:for-each>
                  </table>
				</td></tr>  
				  
			  <tr>
                <td colspan="6">
                  <table width="950"  id="notesTable">
                  <xsl:for-each select="n1:Invoice">
                    <xsl:for-each select="cbc:Note">
                    <xsl:if test="substring(.,1,8) = 'TdetaB4-'">
                    <tr>
                      <td  align="center" bgcolor="#BBBBBB" colspan="6">
                        <b>KAMPANYA BİLGİLERİ</b>
                      </td>
                    </tr>
                    </xsl:if>
                    </xsl:for-each>
                    </xsl:for-each>
                    <xsl:for-each select="n1:Invoice">
                      <xsl:for-each select="cbc:Note">
                        <xsl:if test="substring(.,1,8) = 'TdetaB4-'">
                          <tr>
                            <td align="left">
                              <b>
                                <xsl:value-of select="substring(.,9,60)"/>
                              </b>
                            </td>
							<td align="left">
                              <b>
                                <xsl:value-of select="substring(.,69,60)"/>
                              </b>
                            </td>
							<td align="center">
                              <b>
                                <xsl:value-of select="substring(.,129,60)"/>
                              </b>
                            </td>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,189,60)"/>
                              </b>
                            </td>
							<td align="center">
                              <b>
                                <xsl:value-of select="substring(.,249,60)"/>
                              </b>
                            </td>
							
                          </tr>
                        </xsl:if>
						<xsl:if test="substring(.,1,8) = 'Tdetay4-'">
                          <tr>
                            <td align="left">
                                <xsl:value-of select="substring(.,9,60)"/>
                             </td>
							<td align="left">
                                <xsl:value-of select="substring(.,69,60)"/>
                            </td>
							<td align="center">
                                 <xsl:value-of select="substring(.,129,60)"/>
                            </td>
                            <td align="center">
                                 <xsl:value-of select="substring(.,189,60)"/>
                            </td>
							<td align="center">
                                 <xsl:value-of select="substring(.,249,60)"/>
                            </td>
							
                          </tr>
                        </xsl:if>
                      </xsl:for-each>
                    </xsl:for-each>
                  </table>
                </td>
              </tr>
			  
			 <tr>
                <td height="15"></td>
              </tr>
              <tr>
                <td colspan="6"><br/>
                  <table width="950"  id="notesTable">
                  <xsl:for-each select="n1:Invoice">
                      <xsl:for-each select="cbc:Note">
                    <xsl:if test="substring(.,1,8) = 'TdetayC-'">
                    <tr>
                      <td  align="center" bgcolor="#BBBBBB" colspan="6">
                        <b>CİHAZ BİLGİLERİ</b>
                      </td>
                    </tr>
                    </xsl:if>
                    </xsl:for-each>
                    </xsl:for-each>
                    <xsl:for-each select="n1:Invoice">
                      <xsl:for-each select="cbc:Note">
                        <xsl:if test="substring(.,1,8) = 'TdetayC-'">
                          <tr>
                            <td align="left">
                              <b>
                                <xsl:value-of select="substring(.,9,60)"/>
                              </b>
                            </td>
							<td align="right">
                              <b>
                                <xsl:value-of select="substring(.,69,60)"/>
                              </b>
                            </td>
							<td align="right">
                              <b>
                                <xsl:value-of select="substring(.,129,60)"/>
                              </b>
                            </td>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,189,60)"/>
                              </b>
                            </td>
							<td align="right">
                              <b>
                                <xsl:value-of select="substring(.,249,60)"/>
                              </b>
                            </td>
							
                          </tr>
                        </xsl:if>
						<xsl:if test="substring(.,1,8) = 'Tdetay5-'">
                          <tr>
                            <td align="left">
                                <xsl:value-of select="substring(.,9,60)"/>
                            </td>
							<td align="right">
                                <xsl:value-of select="substring(.,69,60)"/>
                            </td>
							<td align="right">
                                 <xsl:value-of select="substring(.,129,60)"/>
                            </td>
                            <td align="center">
                                <xsl:value-of select="substring(.,189,60)"/>
                            </td>
							<td align="right">
                                <xsl:value-of select="substring(.,249,60)"/>
                            </td>
							
                          </tr>
                        </xsl:if>
                      </xsl:for-each>
                    </xsl:for-each>
                  </table>
                </td>
              </tr>
	<tr>
                <td height="15"></td>
              </tr>
	<!--Tarife Bilgileri Tablosu--> 
			<xsl:variable name="isSet5">
                    
                                   <xsl:for-each select="n1:Invoice">
                      <xsl:for-each select="cbc:Note">
                        <xsl:if test="substring(.,1,8) = 'TdetayN-'">
						 <xsl:value-of select="substring(.,1,8)"/>
                        </xsl:if> 
                      </xsl:for-each>
                    </xsl:for-each>
                    </xsl:variable>
             <tr>
                <td colspan="6">
                  <table width="950"  id="notesTable">
                   
                        <xsl:if test="$isSet5 != '' ">
                          <tr>
                      <td  align="center" bgcolor="#BBBBBB" colspan="8">
                        <b>TARİFE BİLGİLERİ</b>
                      </td>
                    </tr>
                    </xsl:if>
                    <xsl:for-each select="n1:Invoice">
                      <xsl:for-each select="cbc:Note">
					  <xsl:if test="substring(.,1,8) = 'TdetaTN-'">
                          <tr>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,9,60)"/>
                              </b>
                            </td>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,69,60)"/>
                              </b>
                            </td>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,129,60)"/>
                              </b>
                            </td>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,189,60)"/>
                              </b>
                            </td>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,249,60)"/>
                              </b>
                            </td>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,309,60)"/>
                              </b>
                            </td>
                            <td align="center"><b><xsl:value-of select="substring(.,369,60)"/></b></td>
                            
                          </tr>

                        </xsl:if>
                        <xsl:if test="substring(.,1,8) = 'TdetayN-'">
                          <tr>
                            <td align="center">
                                <xsl:value-of select="substring(.,9,60)"/>
							</td>
                            <td align="center">
                                <xsl:value-of select="substring(.,69,60)"/>
                            </td>
                            <td align="center">
                               <xsl:value-of select="substring(.,129,60)"/>
                            </td>
                            <td align="center">
                                 <xsl:value-of select="substring(.,189,60)"/>
                            </td>
                            <td align="center">
                                <xsl:value-of select="substring(.,249,60)"/>
                            </td>
                            <td align="center">
                                  <xsl:value-of select="substring(.,309,60)"/>
                            </td>
                            <td align="center">
								<xsl:value-of select="substring(.,369,60)"/>
							</td>
                           </tr>
						</xsl:if>
                      </xsl:for-each>
                    </xsl:for-each>

                  </table>
                </td>
            </tr>
              <tr>
              
             
              <xsl:variable name="isSet2">
                 <xsl:for-each select="n1:Invoice">
                      <xsl:for-each select="cbc:Note">
                        <xsl:if test="substring(.,1,7) = 'Tdetay-'">
						 <xsl:value-of select="substring(.,1,7)"/>
                        </xsl:if> 
                      </xsl:for-each>
					  </xsl:for-each>
              </xsl:variable>
               
             <tr>
                <td colspan="6">
                  <table width="950"  id="notesTable">
                   
                    <xsl:if test="$isSet2 != '' ">
                    <tr>
                      <td  align="center" bgcolor="#BBBBBB" colspan="8">
                        <b>TARİFE BİLGİLERİ</b>
                      </td>
                    </tr>
                    </xsl:if>
					
                    <xsl:for-each select="n1:Invoice">
                      <xsl:for-each select="cbc:Note">
					  <xsl:if test="substring(.,1,7) = 'TdetaT-'"> <!--Tarife bilgileri alt başlık-->
                          <tr>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,8,60)"/>
                              </b>
                            </td>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,68,60)"/>
                              </b>
                            </td>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,128,60)"/>
                              </b>
                            </td>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,188,60)"/>
                              </b>
                            </td>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,248,60)"/>
                              </b>
                            </td>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,308,60)"/>
                              </b>
                            </td>
                            <td align="center"><b><xsl:value-of select="substring(.,368,60)"/></b></td>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,428,60)"/>
                              </b>
                            </td>
                          </tr>
					</xsl:if>
                        <xsl:if test="substring(.,1,7) = 'Tdetay-'">
                          <tr>
                            <td align="center">
                                <xsl:value-of select="substring(.,8,60)"/>
                            </td>
                            <td align="center">
                                <xsl:value-of select="substring(.,68,60)"/>
                              </td>
                            <td align="center">
                                 <xsl:value-of select="substring(.,128,60)"/>
                             </td>
                            <td align="center">
                                  <xsl:value-of select="substring(.,188,60)"/>
                              </td>
                            <td align="center">
                                <xsl:value-of select="substring(.,248,60)"/>
                               </td>
                            <td align="center">
                                 <xsl:value-of select="substring(.,308,60)"/>
                               </td>
                            <td align="center"><xsl:value-of select="substring(.,368,60)"/></td>
                            <td align="center">
                                 <xsl:value-of select="substring(.,428,60)"/>
                               </td>
                          </tr>

                        </xsl:if>
						
                      </xsl:for-each>
                    </xsl:for-each>

                  </table>
                </td>
            </tr>			  
			  <tr>
                <td height="15"></td>
              </tr>
			  
			  <tr>
                            <td colspan="6">
						<table width="950" border="0" id="notesTable" >
							<tr>
								<td  align="center" bgcolor="#BBBBBB" colspan="4">
								<b>FATURA BİLGİLERİ (TL)</b>
								</td>
							</tr>
							<tr>
							<td valign="top" width="232">
								<!--AYLIK UCRETLER-->
								  <table width="232" valign="top" border="0" >
									
									<tr>
										<td  align="left" >
										  <b>
											AYLIK ÜCRETLER
										  </b>
										</td>
										<xsl:for-each select="n1:Invoice">
											<xsl:for-each select="cbc:Note">
											  <xsl:if test="substring(.,1,8) = 'Detay1A-'">
												
												<td align="right">
													  
													<b>
													<xsl:value-of select="substring(.,9,20)"/>
													</b>
													  
												</td>
												
											  </xsl:if>
											</xsl:for-each>
										</xsl:for-each>
										
									</tr>
                          <xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cbc:Note">
                              <xsl:if test="substring(.,1,7) = 'Detay1-'">
                                <tr>
                                  <td>

                                    <xsl:value-of select="substring(.,8,72)"/>

                                  </td>
                                  <TD align="right">
                                    <xsl:value-of select="substring(.,81,20)"/>
                                  </TD>
                                </tr>
                              </xsl:if>
                            </xsl:for-each>
                          </xsl:for-each>
						  </table>
								</td>
						  
                          
                          
                          <td valign="top" width="232">
								<!--KULLANIM ÜCRETLERİ-->
								  <table width="232">
									
									<tr>
										<td  align="left" >
										  <b>
											KULLANIM ÜCRETLERİ
										  </b>
										</td>
										<xsl:for-each select="n1:Invoice">
											<xsl:for-each select="cbc:Note">
											  <xsl:if test="substring(.,1,8) = 'Detay2A-'">
												
												<td align="right">
													  
													<b>
													<xsl:value-of select="substring(.,9,20)"/>
													</b>
													  
												</td>
												
											  </xsl:if>
											</xsl:for-each>
										</xsl:for-each>
										
									</tr>
                          <xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cbc:Note">
                              <xsl:if test="substring(.,1,7) = 'Detay2-'">
                                <tr>
                                  <td >

                                    <xsl:value-of select="substring(.,8,72)"/>

                                  </td>
                                  <TD align="right">
                                    <xsl:value-of select="substring(.,81,20)"/>
                                  </TD>
                                </tr>
                              </xsl:if>
                            </xsl:for-each>
                          </xsl:for-each>
						  </table>
						  </td>
						  <!--DİĞER ÜCRETLER-->
						  <td valign="top" width="232">
						  <table width="232">
									
									<tr>
										<td  align="left" >
										  <b>
											DİĞER ÜCRETLER
										  </b>
										</td>
										<xsl:for-each select="n1:Invoice">
											<xsl:for-each select="cbc:Note">
											  <xsl:if test="substring(.,1,8) = 'Detay3A-'">
												
												<td align="right">
													  
													<b>
													<xsl:value-of select="substring(.,9,20)"/>
													</b>
													  
												</td>
												
											  </xsl:if>
											</xsl:for-each>
										</xsl:for-each>
										
									</tr>
                          <xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cbc:Note">
                              <xsl:if test="substring(.,1,7) = 'Detay3-'">
                                <tr>
                                  <td >

                                    <xsl:value-of select="substring(.,8,72)"/>

                                  </td>
                                  <TD align="right">
                                    <xsl:value-of select="substring(.,81,20)"/>
                                  </TD>
                                </tr>
                              </xsl:if>
                            </xsl:for-each>
                          </xsl:for-each>
						 </table> </td>
                          <!--İNDİRİMLER-->
						  <td valign="top" width="232">
						  <table width="232">
									
									<tr>
										<td  align="left" >
										  <b>
											İNDİRİMLER(-)
										  </b>
										</td>
										<xsl:for-each select="n1:Invoice">
											<xsl:for-each select="cbc:Note">
											  <xsl:if test="substring(.,1,8) = 'Detay4A-'">
												
												<td align="right">
													  
													<b>
													<xsl:value-of select="substring(.,9,20)"/>
													</b>
													  
												</td>
												
											  </xsl:if>
											</xsl:for-each>
										</xsl:for-each>
										
									</tr>
                          <xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cbc:Note">
                              <xsl:if test="substring(.,1,7) = 'Detay4-'">
                                <tr>
                                  <td>

                                    <xsl:value-of select="substring(.,8,72)"/>

                                  </td>
                                  <TD align="right">
                                    <xsl:value-of select="substring(.,81,20)"/>
                                  </TD>
                                </tr>
                              </xsl:if>
                            </xsl:for-each>
                          </xsl:for-each>
						   </table> </td>
						  </tr>
                        </table>
                      </td>
                      <td></td>
                   
                    </tr>
			  
			  <tr>
                <td colspan="6"><br/>
                  <table width="950"  id="notesTable">
                  <xsl:for-each select="n1:Invoice">
                      <xsl:for-each select="cbc:Note">
                        <xsl:if test="substring(.,1,13) = 'TdetayA-Tarih'">
                    <tr>
                      <td  align="center" bgcolor="#BBBBBB" colspan="14">
                        <b>GÖRÜŞME AYRINTISI</b>
                      </td>
                    </tr>
                    </xsl:if>
                    </xsl:for-each>
                    </xsl:for-each>
                    <xsl:for-each select="n1:Invoice">
                      <xsl:for-each select="cbc:Note">
                        <xsl:if test="substring(.,1,8) = 'TdetayA-'">
                          <tr>
                            <td align="left" style="font-size: 10px;">
                              <b>
                                <xsl:value-of select="substring(.,9,10)"/>
                              </b>
                            </td>
							<td align="left" style="font-size: 10px;">
                              <b>
                                <xsl:value-of select="substring(.,19,10)"/>
                              </b>
                            </td>
							<td align="center" style="font-size: 10px;">
                              <b>
                                <xsl:value-of select="substring(.,29,25)"/>
                              </b>
                            </td>
                            <td align="center" style="font-size: 10px;">
                              <b>
                                <xsl:value-of select="substring(.,54,30)"/>
                              </b>
                            </td>
							<td align="center" style="font-size: 10px;">
                              <b>
                                <xsl:value-of select="substring(.,84,30)"/>
                              </b>
                            </td>
							<td align="left" style="font-size: 10px;">
                              <b>
                                <xsl:value-of select="substring(.,114,30)"/>
                              </b>
                            </td>
							<td align="left" style="font-size: 10px;">
                              <b>
                                <xsl:value-of select="substring(.,144,30)"/>
                              </b>
                            </td>
							<td align="left" style="font-size: 10px;">
                              <b>
                                <xsl:value-of select="substring(.,174,10)"/>
                              </b>
                            </td>
							<td align="left" style="font-size: 10px;">
                              <b>
                                <xsl:value-of select="substring(.,184,10)"/>
                              </b>
                            </td>
							<td align="center" style="font-size: 10px;">
                              <b>
                                <xsl:value-of select="substring(.,194,25)"/>
                              </b>
                            </td>
                            <td align="center" style="font-size: 10px;">
                              <b>
                                <xsl:value-of select="substring(.,219,30)"/>
                              </b>
                            </td>
							<td align="center" style="font-size: 10px;">
                              <b>
                                <xsl:value-of select="substring(.,249,30)"/>
                              </b>
                            </td>
							<td align="left" style="font-size: 10px;">
                              <b>
                                <xsl:value-of select="substring(.,279,30)"/>
                              </b>
                            </td>
							<td align="left" style="font-size: 10px;">
                              <b>
                                <xsl:value-of select="substring(.,309,30)"/>
                              </b>
                            </td>
                          </tr>
                        </xsl:if>
                      </xsl:for-each>
                    </xsl:for-each>
                  </table>
                </td>
              </tr>
             
                <td colspan="3">
                  <table id="notesTable" width="950" align="left" height="100">
                    <tbody>
                      <tr align="left">
					  <td>&#160;&#160;&#160;&#160;</td>
					  
                        <td id="notesTableTd">
 <xsl:if test="//n1:Invoice/cac:PaymentMeans/cbc:InstructionNote">
                            
                            <xsl:value-of select="//n1:Invoice/cac:PaymentMeans/cbc:InstructionNote"/>
                            <br/>
                          </xsl:if>
                          <xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cbc:Note">
                              <xsl:if test="substring(.,1,4) = 'Cdr-'">


                                <xsl:value-of select="substring(.,5,5000)"/>
                                <br/>


                              </xsl:if>
                            </xsl:for-each>
                          </xsl:for-each>
                         <!-- <br/>
                          <xsl:if test="//n1:Invoice/cac:PaymentMeans/cac:PayeeFinancialAccount/cbc:PaymentNote">
                            <b>	Hesap Açıklaması: </b>
                            <xsl:value-of select="//n1:Invoice/cac:PaymentMeans/cac:PayeeFinancialAccount/cbc:PaymentNote"/>
                            <br/>
                          </xsl:if> -->
                          
                        </td>
						<td>&#160;</td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td height="15"></td>
              </tr>
              <tr>
                <td colspan="3">
				<xsl:if test="//n1:Invoice/cbc:Note/. ='MusteriTip-T'">

                  <img width="950" align="middle" alt="Turk Telekom" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAACbAAAACRCAIAAAHgb6uJAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAENlJREFUeNrs2uENgjAQBlBhB11Gt9GxHMdpnEISk+bSllrUH0DeizEFe2AM3xWjw/F8PQBrNfoIQEQBEQURBUQUEFFY4vm4T4+4WR2nmdnORkl18k8RrR6u8Y5hH06XW7rC03huZjYhlpTl7aONHztHilwax/2d4aw2obL9ZOeVdlayhE4pql7q5UtzC9jX5WNn54ib7ybxPnQ8QRpXT1l2kVibtZY0wfXBCuO66K540Qr8n++iMZNZkOIeyyB7Smb1uVyQyrvcnvJGUgZ/AISl62e2GGYvVe9AO8tFFDbGjy4gooCIgogCIgqIKGzFSwB27iAJYRCGAqjD/Q/tztEQAthN0fd2HVtk85s6NfHSBVRRQERBRAERBUQURBR+Rz0JJRz2bZjFJJRpw2b7eqPwP/kcNYulH4WW0T41Wx3erdjWqGn18Tkkpd9KaF0dtcCGE9J7DNyqihZDUgrvA0l2L2/Fomm+wxiU9H4QZqak01XC+a+VR98LR5TQaXlbWfnSb9GVRVemLcFZJTRNXf8sWQxGSZ8xp4nwB0DYrqX1lJO0ko0umdZSEYVb89IFRBQQURBRQEQBEYVTPAVg7w5yEAaBKICy8P5ndmFsCMyMiG3j4r24Q6Dt5jtpZLx0AQC/cwFAiAKAEAUAIQoACFEAOMvDIwDgd9mRXdtDrTyA+obdL6xEF0/YrI/MX1zzH07zdKIowEaCHinw1dAcnMfnrxJ0M0RXOkCE2RlOLII2ayAxr5NdUj93vWVF2BAnGwWQoO3dfyTsg/LVUFaP1YXW0c1k3iXcohi6PERf11r3XAkbQvUT6xPyswfRukO7+y2GrjDFD5mP99WS3jP9dM1mALKyZ7E0+jg016D19LDjX/idlWtetPlOdOh1VnejOKK+j8+hcVu4+BBjwworYRZOyRq1ZdPn+wUgK5nCemN9aHv3Yakbdm96LQFwblV61rvJsJrau4Dr3owKUQDY5H+iACBEAUCIAoAQBQAhCgBMngKwc4epDYNgGIDZ2BW2y2y32Y614+w0O8UKARGN34yNrYXn+VFKjMaEkrefKfXrXABQiQKAEAUAIQoAQhQAEKIAIEQBQIgCgBAFACEKAAhRABCiALCaF5cAgOv9/nxfXt8+vs5qSq25eM9zj64SBeBRE/SOE1ulEr1MsXN+8XlOvcQATM2wo01HQ+4GYXxyJfrvyZ/ecdnPAYCkjDNsrOmsnJ5aBD8Pzzh/rd8UZ9Xq0jr53e2tjq2ZFH3rLkX34k0weZkKcPRO29k05hKHcaYMNE2sRA99L9hdj03fAuqdt/1bg2xv6h2CHN0dKr9wxeCqUoBFytDtXn2o4rpZGToYotuB4+uSWvMzLDq2Rqj79mRba9h0sYqYzHeLp1RP2zNagOF6Y6wUCXrd8Z78suAlbq3Hii6ANZOyWJ7Nb9djTUU0pkdpQWWZjxNMrGfOcyvRgbJ1Nxe3kq5V/OVN18RnayXZqizAo1i2iBqvRINnk/XJ14GfbyxWVtPyd70979jz8LJYGU7de0Yo9lEKA8TZlt82x5o6b8hBtTrj6IGn1/dPnwMArnfuXy70/13RjKN38o9FADxe1bsIlSgADFKJAoAQBQAhCgBCFACEKAAgRAFAiAKAEAUAIQoAQhQAEKIAIEQBYDV/ArB3rznRwmAYQNHMFvw2o7vRZbkcV+MqvkkmmRAuLy8dWhk4J/5QoPQCMT7Tgl5ADwAAQFM+zQUAAEAQBQAAQBAFAAAAQRQAAABBFAAAAARRAAAABFEAAAAEUQAAABBEAQAAEEQBAACg3MUQAAAAO/H7833//t/HV6UixaX21vfn7b4gCgAASKGF7Zyz9rSnSqGCKAAAsK8IeoAU2jiB1y4liE4P4rbDV3DaW5GdTOgDAIAUWimFDmwSAU6YQrvGLyu69rzG5we7qvHkv0GMNgAAJ0mhm3f/PCm0azYj2u/2YMpxPJ0YbInnHvt7gxq7qfn0+EpM5qu4GXF1kx3MCNoZdKp2AzLX17wxAACZdLSYzSYPqFSqarItaNLjI5YvdYQgOpc5y84TR9DFGieD7vXrwWDZzSzrnds4rjGTxuObOL7t6jUgc31FUAAAFtNRpezXIHStfcTvtHOhTYNoyzs4eXDBItJBkUGQi++8qte7xmu7AACgzV+wz/WS2MwKwcVazvxfao4TRO/TcZlLXnwZMsffFqBagwoAAGWhdG7X4iLV8a7MItVkqXwkyWfRR5pUacRappjLH95nZc8oLobAYMSvu+4PK+YfE+3Hy+D424/98/e3rH0qNTmM40rnmrRtAzL3tygOAABMenl7/zQKAADATux81W7mXSqbTPl0h161e3GjAwAArApviy/yJGZGFAAAoDCRCp9lzIgCAACsI3k+6NUQAAAAIIgCAAAgiAIAAIAgCgAAgCAKAAAAgigAAACCKAAAAIIoAAAACKIAAAAIogAAACCIAgAA8Bz+C8DeHd7GCYNhACYVK6TLJNukY3WcTtMpWumkEwLis40xtnke5Ud0wWB8nMR7n03e3j++jAIAAADVqIgCAAAgiAIAACCIAgAAgCAKAACAIAoAAACCKAAAAIIoAAAAgigAAAAIogAAAAiiAAAAIIgCAAAgiAIAAIAgCgAAgCAKAACAIAoAAACCKAAAAIIoAAAACKIAAAAIogAAACCIAgAAIIgCAAAgiAIAAEBJsyEAAABa8PfP7+fvPz9/NdiqtdPv99wFUQAAYITsenar+LYZ6e5Il3pkai4AANBWpMwr09Vsdarhy6GCKAAAcK/sOsak3N4JogAAQH9JrP6k3Arnfp8QPvd+sRYfvtTdPt/Lm3x1AQAALSSxpasm5RaPAPcJ4SqiAABAK5RDewnhB831h7jaOatVXvIRMtoAANwhiSmHHqEiCgAANEE5tJcQftxcf4iXizC3ZbTwK+EFnLt7nr5Z9plawYtf1xs+XLgnMQOS18lTOxDz/p60phcAgH7tJrGkB+psb/5jbjjzWp2aZiO7lDdix8e51yBa4QuD+GtitWVMQNo2SXqrIo+4jXPxB00anDM6cNUnFgCAMXRXDl3tM/VW+c7l0GmYqbnxZcPllo+fpJ1v64S7Gy93u9s8cFmstok5aEb/a3YAAACGT2L/Tyf7jO42J3mqVhHdnfNZ6u3Z3XORI+4efbnnlyk00HxbYNw2TL0iA9uf2oGXo21GLgAAd0hikbMIb14OnUZ6WFHSmIZnpeZdIi8nXl8yAmqYAAA0SBIbL4Qnme95BWe0kugAAKCRJBafJ8s+kuflJM0poih6vEsDhPB5mMv3yKN0Ru0JAAB0cScf/lP8fMYpuGwtr1VkxiteITtyIqmTJev/h4sRgmhg0WPBLwMynhwtAAMAAGx1v0b0Eb3Oe1ZyXrRLenpQhfG5sAMAAAAr9Sqiy7rl85Xtiwctq4JJR4z/vy/hJt8V5ctWSldBd/ekVt07owO7jyku+7BiAABAEG1X/LTs1AnceRO+n5F4t3nxkLY9SuUOAABA0u1r4EZ6d7OMJtmtWjv3kU7/4e3948vHAAAAaERGQKr2QN3d2X8Fqz41nwx8bRCdXegAAADZibFUCr2VH4YAAACgCClUEAUAAJBCW2SNKAAAQJprF1gOwBpRAACANMLnQabmAgAAIIgCAAAgiAIAAIAgCgAAgCAKAAAAgigAAACCKAAAAIIoAAAACKIAAAAIogAAACCIAgAAIIgCAACAIAoAAIAgCgAAgCAKAAAAgigAAACCKAAAAAiiAAAACKIAAAAgiAIAACCIAgAAIIgCAACAIAoAAIAgCgAAADH+CdDeHd3GjUNRAB0H04LdjN2NXVbKSTWpIgYCGIlnxKEkiqKuzsF+bHY1fiKfpABzTerp+fXdLAAAAAAAAACR/GYuAAAAAAAAEEsgCgAAAAAAAMQSiAIAAAAAAACxBKIAAAAAAABALIEoAAAAAAAAEEsgCgAAAAAAAMQSiAIAAAAAAACxBKIAAAAAAABALIEoAAAAAAAAEEsgCgAAAAAAAMQSiAIAAAAAAACxBKIAAAAAAABALIEoAAAAAAAAEEsgCgAAAAAAAMQSiAIAAAAAAACxBKIAAAAAAABALIEoAAAAAAAAEEsgCgAAAAAAAMQSiAIAAAAAAACxBKIAAAAAAABALIEoAAAAAAAAEEsgCgAAAAAA8J/fv35+/pNUqHMtrdf6oTw9v767uAEAAAAAAP6aio5e3j4OWqhzrYdF59r0JLX+DK6eawAAAAAAAJdigNc2PepWqHMtrdf6MdkyFwAAAAAAQCSm9VofSyAKAAAAAACcnUis55RqfXbrB2TL3P1vrcGvvA5ne6wJAQAAAAAgjEisp6FSAK0/CStEAQAAAACA8xKJab3Wx7ue4SJObfAZxoi/hFzbAAAAAMB2RGJar/VncD3bRWyM4NoGAAAAALhMfxvZLabq8Ja6TWstMMhpaP3Z2DIXAAAAAAA4HZHYLtOr9Rdp6B6iVohONfvbf/+394X/tf6Yxdt+3g7k6+MLxlj/9Fl/V2xUojAhrarfTmDNuvKN1p4vHsihZ6DV/Wu7XQAAAADgocr0aH3I1K1Q51oNZ77zF7lav1fr93W90PF2WvDZJlfh3fO5Tbw+/7i43HYlmmS9/x5WcwLlH1hTrr5WkysnZgb2uu8AAAAAgPOwQHDkdmx6wlp/WlGB6NSqtV1avqBozWkvGGPhyNsVdcsy0Y1KVI5xbmJacwKt1gqvfGY1nKjxZ6DJ/ev5DgAAAACUicSO0qDjdkTrB+QdoptYmSl+frzVlTorZC3fPJ1LVE5IZXi2coy7/B3cNjYefwZ2ue8AAAAAgFMRiR2rWQ2/ytb6k7Nl7rjP34PWWl/CxqeeVgAAAAAAzYnEelq25ufuAevHovUIRAd9KLteTQgAAAAAAK0UgrcFq3QKW/d1K9S51kZqpm5lRqD1Y7a+M4HoEFq9y3PWk6XDw2v8CQEAAAAAAEbw8vZhC0k24h2iQ9zh3/7lrya3fYc3RzYvMWtCUl+NWT+QM7wcFAAAAAAAYLFTrBCtX1x4e2TneOnbrz+sOfPCkZd76zi/6rbajLtJiQUTUtPEHRebTk3C7W++FK698vmPPAMLrgGLgwEAAAAAgDWenl/fIwdWDjIr93R++NmpH/IwryofXPnT6sd4abfisDC07UrUnENl9crJXNzlWZt6Lx7IQWeg1bX09cEzbGsOAAAAAOxo6rvK5t9GdivUuVaTE9vl3LQ+TOwK0anI5HJvZV75sP57kN4ui7x7OdaPsXI4K6/47UrUTEi5+iA3c80G6A+ncWos489A/f7vs65tAAAAAABGM3fzv6HSUPLErhAFAAAAAABYwzLB5lXm2v0FcFqf4YfHGQAAAAAAAK0cPQ0lz9UUAAAAAAAAMA5RKG0JRAEAAAAAAGjm5e1jwSJRISjb8Q5RAAAAAAAAIJZ3iAIAAAAAAACxBKIAAAAAAABALIEoAAAAAAAAEEsgCgAAAAAAAMQSiAIAAAAAAACxBKIAAAAAAABALIEoAAAAAAAAEEsgCgAAAAAAAMQSiAIAAAAAAACxBKIAAAAAAABALIEoAAAAAAAAEEsgCgAAAAAAAMQSiAIAAAAAAACxBKIAAAAAAABALIEoAAAAAAAAEEsgCgAAAAAAAMQSiAIAAAAAAACxBKIAAAAAAABALIEoAAAAAAAAEEsgCgAAAAAAAMQSiAIAAAAAAACxBKIAAAAAAABALIEoAAAAAAAAEEsgCgAAAAAAAMT6Aw8A10tmRMLDAAAAAElFTkSuQmCC" />
                </xsl:if>
				<xsl:if test="//n1:Invoice/cbc:Note/. ='MusteriTip-G'">
				<img width="950" align="middle" alt="Turk Telekom" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAACbAAAACRCAIAAACXaJsfAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAEsVJREFUeNrs3V9sVNedB3CCPRn8Z7DBgLGBxIgSoKm1zaJslXS38LCqKoFaHvKQ7FZK8kL6QFZqojzQh126UsNDFUVqeGh5CUirTaTNQ5RdtKuqUshDU221bNolLUsISwjYxgQDZoyxO8bdm8yuub4ej2fMmPGc+Xxkoflz55x7z++OH/j6nHNfz7sfLwEAAAAAAAAI0VJDAAAAAAAAAIRKIAoAAAAAAAAESyAKAAAAAAAABEsgCgAAAAAAAARLIAoAAAAAAAAESyAKAAAAAAAABEsgCgAAAAAAAARLIAoAAAAAAAAESyAKAAAAAAAABEsgCgAAAAAAAARLIAoAAAAAAAAESyAKAAAAAAAABEsgCgAAAAAAAARLIAoAAAAAAAAESyAKAAAAAAAABEsgCgAAAAAAAARLIAoAAAAAAAAESyAKAAAAAAAABEsgCgAAAAAAAARLIAoAAAAAAAAESyAKAAAAAAAABEsgCgAAAAAAAARLIAoAAAAAAAAESyAKAAAAAAAABEsgCgAAAAAAAARLIAoAAAAAAAAESyAKAAAAAAAABEsgCgAAAAAAAARLIAoAAAAAAAAESyAKAAAAAAAABEsgCgAAAAAAAARLIAoAAAAAAAAESyAKAAAAAAAABEsgCgAAAAAAAARLIAoAAAAAAAAESyAKAAAAAAAABEsgCgAAAAAAAARLIAoAAAAAAAAESyAKAAAAAAAABEsgCgAAAAAAAARLIAoAAAAAAAAESyAKAAAAAAAABEsgCgAAAAAAAARLIAoAAAAAAAAESyAKAAAAAAAABEsgCgAAAAAAAARLIAoAAAAAAAAESyAKAAAAAAAABEsgCgAAAAAAAARLIAoAAAAAAAAESyAKAAAAAAAABEsgCgAAAAAAAASrof2ZvzEKAAAAAABA3drT1vSvjz3YnPvjXyxfFv00TP7xfO62jtRdR8G4r+fdj40CAAAAAABQt375Zw90N6cSL2Zzk298cq1/LHd06KaO5mdPW9O21nT0YO/mjiKHbTx+Vt1DqvsiZMlcAAAAAACgfu1f1z4zQ4pkUkv3bu4YnpjUUel2tKSj3qOf//r6xnM7N736SHfUY/E0VN0DqPvi1+g3HQAAAAAAUJ+6Ghue6lkx27snrtx6e/iWjua0oyX92le7M6ml6l5Xda8hZogCAAAAAAB16pnOTJEY7x8uXNNRKXpb76+hNFTd65BAFAAAAAAAqEe96VSR1VwrOKkuvI4SsjW1HKu61yFL5i5Sn6+y/dDqLW3p/NNXTl0+NJit27OtrdEAAAAAAKAmPLe+vci7FZxUF15HCbW1P6W61yEzRBep3tb7p/I/Z1tbowEAAAAAwOLXm07t2rB8tncrO3svsI5mOjs2oe51WPcaIhAFAAAAAADqzgs9K4u8W8FJdeF1NNOV25PqXod1ryECUQAAAAAAoL7saWva2dU627sVnFQXXkcFDUzczuYm1b3e6l5D6nQP0ac7WrqXpZY3Ln1y44ro6bd/9enJ8ZxrBDc2AAAAAFAPvrthRZF3KzipLryOZvPR8Pj2VU0zXz98Zij696HWdJHcTt1rt+61ok4D0Ufbm4usp+wawY0NAAAAAIRqT1tTwegur7Kz9wLrqIh/Hhg+cW00etA/ljs6dDPx7r7OTNUDUXWvZ42GAAAAAAAAqB+mCS6EmSGoutdD3WuFPUQBAAAAAIB6YZqguqt7HRKIAgAAAAAA9cI0QXVX9zpUX0vm7mlrevWR7pmvv/PYA/Gnxy7c2Hf2s6mn+zozL25bM/X0+x/0FwzV/+2R9Vva0vnHJ67ceuLD/vi7O1rSRx5dP/X0lVOXDw1mowf717U/1bMik1papOXZRG2+9tXu6LP9o7l/6bsRvXKw7/r8rjHR7OPtTVNnlXf4zNCpkfGK/DXBArVfcDQKHhmNeWL35qheJ66NHhnMDkzcLtL+bBXcsaZ1qvTRwP7TpRvv3RyPd7d9RfPUn2nkT2+2cytdbzq1e1VLvOupE+gby71//Vb8HAIYgfnd2AvxvQMAAAAAapdpguqu7vXJHqJzyzQ2lHJYIpdK6G29P/lKOvXjL3fGP3V2bKL0s4o+/vLDnflEp7s5tXdzx5vn7jbt72pseHnTqoLbGkftR/9+Z2DkB2evFMnMqtV+iaORyLanRL81op/oU0Vy4pkVfLqj5cWta+KxbmTXhuXRTz54K9hd/vSe6lnx/G/6i2SWxR3atDrqpeBb+dezpy4XbDyYESj1xqj09w4AAAAAqGmmCaq7utcngWh1ZBobfvonXd3Nqfl9vKux4cdf7ox//NiFG/vPX72bU4rafL23q3isu7Or9fXm1Lc+uLio2i9xNIqEiFOiA9Y2pRKzewva3d1W5Fr2fmnV9vbmgtHv/90AqaUvP9z59V9/Oo+RfOsr3UX+7iPv5MgfZr4YzAhU63sHAAAAANQ00wTVXd3rlj1EC+gbyy10F3s3d8xMZU6Ol9rva1unTXGLbu7ZZvWVfo2JNvtHc4fPDOV/4odFx+xf1z6PS1649ksZjYMPrpwzC8yLfokc2rR6zsOKJ7uZ1NIiWWBedAPs68yUO4yvb+mcMw3N5iZnzrwMZgTu5st7l987AAAAAKCmmSao7upet8wQXSz6R0tNZQ5tWh2PxE4Pj5cyn6+4fZ2ZeJuJPVCPDGbf+tN1U0nSUz0rim82eS/bL2U09rQ1Pblx2m+HYxdu/Oj81XwXvenUCz0r4+ndrg3Lf3FlZM4/oIj6eu/ySH4vzJmNTHU01VR0Gi9tWR0P5La2LlvyxZaWJYpamDNl/GJ4R0MdgSp+7wAAAACAmlbipLodLenH2wsf9v71W6XsAhZeR+qu7gGor0A0ugnePn52yYy1Q7/9q0/v/Syx4wMjRy5eL/ee27+uPX7m/aO5Z08O3P017u5uiz/94ZlpMywHJm4fPjt0oHdt/mkmtfSbbcuODt0s/bQXqP05RyMv8bcSb567Fl9QNxqWZ08Pvv7Fgr1TL35n7fLicWDBRn7Zlo6nfd//oD/eSPT42u8Gjzy6fuqVtU3lrd06848+otP4Sd/wnOFxACNQqS/v/L53AAAAAECtK3FSXW/r/Xs3dxQ8Jnvqcin/tRheR+qu7gGwZG51HD4z9OzpwXJvuB0t6fjtm81Nfu+3A2XN1Jyt2fjyp8cHRmYmTD8fHos/3dqarnr7JY5GdFjibyV+0jc8s7UjF6/Hn+7sau1qbCjSe8GlWT8anlbQs2MTiQMSFe9qLuMvEnrTqcSFHDh5af//z/IsPlBhjEC1vncAAAAAQK2b+f+rcccu3KjUnovhdaTu6h4GgWh1ZMtPMbsaG15+uPNOC7nJ53/TX5GJrYlJ05cKBV2J1G3tslR12y99NBK9Hx8YKZggvndz/PT0MO9rLfeXO5I3JyYX7p7Zvaol/jQ62xIn6QYzAlX53gEAAAAAAXhufXuRd382fbqIjtRdR+ERiNaMlzetii9G+rcfXqrURLd109PHGyVkWplUQ3XbL300Er1/NDLroA1M305yWzmzYO+B5Y3Tvq0fXB2d3/jX7ggAAAAAAMxDbzoV34cr4diFG5XaUy+8jtRd3YPRaAhqws7VmcQk6G2t6UpNfE7s47h3c8ds60pPKWul04q3X9ZoJHovMkew4NTVxWNzZln8aV/JZxvMCAAAAAAAzMNfrc0UebeCk+rC60jd1T0YZojWhplLQu/d3NGbThmNckcjO/v81MTU1XXLwhxeIwAAAAAA1I+uxoZd69tme7eCk+rC60jd1T0kAtEa9kLPymp1PTA6sdjaL3E0hhf3Jpf3gBEAAAAAAOrHM52ZTGrWKKSCk+rC60jd1T0kAtFacuLKrWzuTpq1s6t1T1tTVc4km7td9fbnNxrrl826GG9ik86+QNePNQIAAAAAQP3Yve4e7bkYXkfqru4hsYdozTg9PP7Eh/3717XHN+B8acvqt3/96V22nEgfXzl1+dBgtoJnvhDtlz4aid4zjQ2ztbl2Wal7bVZF4kJKX882mBEAAAAAACjLjpZ0d/Os/5VawUl14XWk7uoeGIFobegfzb30+8HowZHB7O51y6fu++jBvs7MXeaLl6ZPBCwSmC2S9ssajY9GxncuaZ16mpgEGdc1/bfJyZE/LKp7IDGMj6xsXnL+aikfDGYEAAAAAADKsqKx2DKZ7zz2QFmtvbhtTfSTf3z4zNDBvusBd6Tu6h4YS+aWra2xCoP2xvlr+fnOAxO3o8fxt/Z+aVXX3UWM/z0yHn+6fUVzZU++4u2XNRqnpvf+jc7Wgm32plNb2tJTT7O5yfduji+qGy8xjNHZPt3RUsoHgxkBAAAAAACAeajTQDSxFujKohln4uDukpcqXSCHBrOnh+8kVZnU0mc6M3dzjUeHbsY349y+qql40rajJV3WCS9o+3OOxtvD07Ya7W5OFez9ufXt8afHLg4vtpv258Nj8QuJHOhde/DBlb3pOW7IYEZgHl9eAAAAAICFcM92HAuvI3VX96qo0yzhxsS0YOn5jR3xaYX717XHn14cm4gfvHdzRyJP2tPWdGjT6nt5/j/9n6HEKc1Mxcq6xjc+mTbPMp+0JS4z+kj0c27npqipck94QdufczQSvb+4dU1Usvgr0cns2nBnO+JsbvIfL2WrdXNGJxMNQvTz1le64xfy+XTY6RcSeXLjinceeyB/fPwnESqHMQLzuLEBAAAAAADqdA/RI4PZvZvvpG7bVzW9/+c98QP6x3JHh27mH789fOvvc5OZ1J3w+EDv2gNVPf/olL575VZ02lOvPLe+fd/Zz+Z9jYnNOJd8kbR9fqWFeo+a6mpsGCjnrwYWtP05R+Ng3/Uda1qn1oONSvnqI91RTd/45NryxqX5M4k7/PGV/JK8997THS1T5xNd0d8tWf3Eh/1FhnE2j7c3xRe8DWYEyr2xAQAAAAAWQmImlY7UXUeLXJ3OEB2YuP3muWtFDkisiztzZl7V/fDMtPhz14blibl0ZV1jdPD3fjvQP1pqBvbNtmXlDviCtj/naLz0+8FE75nU0r2bOwpkgWeGDg1WbXJk4sbram5MDOMPfjeYWDi3oIdak8sOhzEC8/jyAgAAAABU3Nl7FSOF15G6q3tV1O/2e/vPXz124cZs766bnqkc7LteJIPpH80dOHnpHp//yfFc4vwTe0CWe41Rg0/8Z1+R4+My5W/cuKDtzzka+d6PD4wUaSSbm4zqGNW6irdl/1gucUqJA967Of7X/3HxxJVbxdvpmjGLNJgRKPfGBgAAAACorGxu8t6ssRdeR+qu7tXSWM/37r6zn/3iysjX2pvik+SyX6wj+v71ZOC0//zVf79+6y9Xtca3Wnzz3LW+sVx+Ot2Be37+Pzp/9RtrW+Nr+c5cabasa4w+Gx3/s4vXd69qeag1vbOrNXHA4TOf79Z5ZDA7MK9ddhe0/TlHI3r87OnBHRevP97elFh49tiFG1EdqxsE5h0durn13LV8sU4Pjx/86LOZx3webX7Yv6MlHV3I9hXN8bWCp67lSKEpnsGMQLk3NgAAAABABZW+GqKO1F1Hi8R9Pe9+7CYGAAAAAAA4t3PTbG+9cupyBbcbC6+j4vZ1Zl7ctma2dzceP6vuQdZ98ajfJXMBAAAAAACA4DUaAgAAAAAAAObt0KbV8Q0HyzXbdMaqzxwlGGaIAgAAAAAAAMESiAIAAAAAAADBEogCAAAAAACwuGRzkwaBShGIAgAAAAAAsLhkc7cNApUiEAUAAAAAAGBxMUOUChKIAgAAAAAAsLgMjOYMApUiEAUAAAAAAGBxuTQmEKViBKIAAAAAAAAsLjcmLJlLxdzX8+7HRgEAAAAAAAAIkhmiAAAAAAAAQLAEogAAAAAAAECwBKIAAAAAAABAsASiAAAAAAAAQLAEogAAAAAAAECwBKIAAAAAAABAsASiAAAAAAAAQLAEogAAAAAAAECwBKIAAAAAAABAsASiAAAAAAAAQLAEogAAAAAAAECwBKIAAAAAAABAsASiAAAAAAAAQLAEogAAAAAAAECwBKIAAAAAAABAsASiAAAAAAAAQLAEogAAAAAAAECwBKIAAAAAAABAsASiAAAAAAAAQLAEogAAAAAAAECwBKIAAAAAAABAsASiAAAAAAAAQLAEogAAAAAAAECwBKIAAAAAAABAsASiAAAAAAAAQLAEogAAAAAAAECwBKIAAAAAAABAsASiAAAAAAAAQLAEogAAAAAAAECwBKIAAAAAAABAsASiAAAAAAAAQLAEogAAAAAAAECwBKIAAAAAAABAsASiAAAAAAAAQLAEogAAAAAAAECwBKIAAAAAAABAsASiAAAAAAAAQLAEogAAAAAAAECwBKIAAAAAAABAsASiAAAAAAAAQLAEogAAAAAAAECwBKIAAAAAAABAsASiAAAAAAAAQLD+V4ABAAL1VYSv5hh6AAAAAElFTkSuQmCC" />
				</xsl:if>
				
				</td>
              </tr>
            </tbody>
          </table>

        </xsl:for-each>

      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>


