﻿<?xml version="1.0" encoding="UTF-8"?><!-- DWXMLSource="1.xml" -->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" exclude-result-prefixes="cac cbc ccts clm54217 clm5639 clm66411 clmIANAMIMEMediaType fn link n1 qdt udt xbrldi xbrli xdt xlink xs xsd xsi" xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:ccts="urn:un:unece:uncefact:documentation:2" xmlns:clm54217="urn:un:unece:uncefact:codelist:specification:54217:2001" xmlns:clm5639="urn:un:unece:uncefact:codelist:specification:5639:1988" xmlns:clm66411="urn:un:unece:uncefact:codelist:specification:66411:2001" xmlns:clmIANAMIMEMediaType="urn:un:unece:uncefact:codelist:specification:IANAMIMEMediaType:2003" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:link="http://www.xbrl.org/2003/linkbase" xmlns:n1="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2" xmlns:qdt="urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2" xmlns:udt="urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2" xmlns:xbrldi="http://xbrl.org/2006/xbrldi" xmlns:xbrli="http://www.xbrl.org/2003/instance" xmlns:xdt="http://www.w3.org/2005/xpath-datatypes" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2" xmlns:ext="urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <xsl:decimal-format name="european" decimal-separator="," grouping-separator="." NaN=""/>
  <xsl:decimal-format name="european" decimal-separator="," grouping-separator="." NaN=""/>
	
  <xsl:output version="4.0" method="html" indent="no" encoding="UTF-8" doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN" doctype-system="http://www.w3.org/TR/html4/loose.dtd"/>
  <xsl:param name="SV_OutputFormat" select="'HTML'"/>
  <xsl:variable name="XML" select="/"/>
  <xsl:template match="/">
    <html>
      <head>
        <title></title>
        <style type="text/css">

          body {
          background-color: #FFFFFF;
          font-family: 'Tahoma', "Times New Roman", Times, serif;
          font-size: 11px;
          color: #666666;
          }

          h1, h2 {
          padding-bottom: 3px;
          padding-top: 3px;
          margin-bottom: 5px;
          text-transform: uppercase;
          font-family: Arial, Helvetica, sans-serif;
          }

          h1 {
          font-size: 1.4em;
          text-transform:none;
          }

          h2 {
          font-size: 1em;
          color: brown;
          }

          h3 {
          font-size: 1em;
          color: #333333;
          text-align: justify;
          margin: 0;
          padding: 0;
          }

          h4 {
          font-size: 1.1em;
          font-style: bold;
          font-family: Arial, Helvetica, sans-serif;
          color: #000000;
          margin: 0;
          padding: 0;
          }

          hr{
          height:2px;
          color: #000000;
          background-color: #000000;
          border-bottom: 1px solid #000000;
          }

          p, ul, ol {
          margin-top: 1.5em;
          }

          ul, ol {
          margin-left: 3em;
          }

          blockquote {
          margin-left: 3em;
          margin-right: 3em;
          font-style: italic;
          }

          a {
          text-decoration: none;
          color: #70A300;
          }

          a:hover {
          border: none;
          color: #70A300;
          }

          #despatchTable{
          border-collapse:collapse;
          font-size:11px;
          float:left;
          border-color:gray;

          }

          #ettnTable{
          border-collapse:collapse;
          font-size:11px;
          border-color:gray;
          }

          #customerPartyTable{
          border-width: 0px;
          border-spacing: ;
          border-style: inset;
          border-color: gray;
          border-collapse: collapse;
          background-color:
          }

          #customerIDTable{
          border-width: 2px;
          border-spacing: ;
          border-style: inset;
          border-color: gray;
          border-collapse: collapse;
          background-color:
          }

          #customerIDTableTd{
          border-width: 2px;
          border-spacing: ;
          border-style: inset;
          border-color: gray;
          border-collapse: collapse;
          background-color:
          }

          #lineTable{
          border-width:2px;
          border-spacing: ;
          border-style: inset;
          border-color: black;
          border-collapse: collapse;
          background-color: ;
          }

          #lineTableTd{
          border-width: 1px;
          padding: 1px;
          border-style: inset;
          border-color: black;
          background-color: white;
          }

          #lineTableTr{
          border-width: 1px;
          padding: 0px;
          border-style: inset;
          border-color: black;
          background-color: white;
          -moz-border-radius: ;
          }

          #lineTableDummyTd {
          border-width: 1px;
          border-color:white;
          padding: 1px;
          border-style: inset;
          border-color: black;
          background-color: white;
          }

          #lineTableBudgetTd{
          border-width: 2px;
          border-spacing:0px;
          padding: 1px;
          border-style: inset;
          border-color: black;
          background-color: white;
          -moz-border-radius: ;
          }

          #notesTable{

          border-width: 2px;
          border-spacing: ;
          border-style: inset;
          border-color: black;
          border-collapse: collapse;
          background-color:
          }

          #notesTableTd{


          border-width: 0px;
          border-spacing: ;
          border-style: inset;
          border-color: black;
          border-collapse: collapse;
          background-color:
          }

          table{

          border-spacing:0px;

          }

          #budgetContainerTable{

          border-width: 0px;
          border-spacing: 0px;
          border-style: inset;
          border-color: black;
          border-collapse: collapse;
          background-color: ;

          }

          td{
          border-color:gray;
          }


          .header {font-family:Tahoma, sans-serif; font-size: 12px; COLOR:#200000; padding-left:10; padding-right:5; font-weight:900 }
          .mheader {font-family:Tahoma, sans-serif; font-size: 10px; COLOR:#200000; padding-left:10; padding-right:5; font-weight:900 }
          .text {font-family:Tahoma,sans-serif; font-size: 11px; color:#000000; padding-left:20; padding-right:10 }
          .text2 {font-family:Verdana,sans-serif; font-size: 10px; color:#000000; padding-left:20; padding-right:10 }
          .news {font-family:Arial, sans-serif; font-size: 9px; color:#00000; padding-left:10; padding-right:5; font-weight:900; }
          a:link{text-decoration: none; color:#004FDF}
          a:visited{text-decoration: none; color: #004FDF}
          a:hover{text-decoration: underline; color: #004FDF}
          a:active{text-decoration: none; color: #004FDF}
          li {
          list-style : url(images/pic.jpg);
          }

        </style>
        
      </head>
      <body style="margin-left=0.6in; margin-right=0.6in; margin-top=0.79in; margin-bottom=0.79in">
        <xsl:for-each select="$XML">
          <table style="border-color:blue; " border="0" cellspacing="0px" width="950" cellpadding="0px">
            <tbody>
              <tr>
                <td colspan="3">
                   <!--üst banner-->
                  <img width="330px" align="right" alt="TurkTelekom" src="data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAAwAAAAFXCAYAAAAVlSnoAAAACXBIWXMAAC4jAAAuIwF4pT92AAAA
GXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAALzhJREFUeNrs3V+QXFl9H/AjdgFJ
y3q0MItgIaiXxQH/2WjWgA1xUuq1H1KOQ2kIrlScF7VSScWpPGi2yn5wUolaD3FebGv0kFQcgzWq
VMp/KtSOjLGTh1g9savAxrCjwjaYgNXCsIvYYXcGdiXtHzE5p/sMDFqNNH27+/bt7s+n6tassWa6
+/Tt27/vvb9z7p7Nzc0AAABMhz0CAAAACAAAAIAAAAAACAAAAIAAAAAACAAAAIAAAAAACAAAAIAA
AAAACAAAAIAAAAAACAAAACAAAAAAAgAAACAAAAAAAgAAACAAAAAAAgAAACAAAAAAAgAAACAAAAAA
AgAAACAAAACAAAAAAAgAAACAAAAAAAgAAACAAAAAAAgAAACAAAAAAAgAAACAAAAAAAgAAACAAAAA
AAIAAAAgAAAAAAIAAAAgAAAAAAIAAAAgAAAAAAIAAAAgAAAAAAIAAAAgAAAAAAIAAABUw7Pven8j
/bzv859YKvNxX2XoAQBgJGpxOxuDQCtudQEAAACmw5G4XYghYCluNQEAAACmw7G4rcYQ0IzbAQEA
AAAm30zcTuYgMC8AAADAdDgUt8fz/IA5AQAAAKZDmh/wRJ4fMJC2IAEAAACqL80PaKf5AQIAAABM
h878gBgC2v0sGyoAAADAeEnzAy7k+QE1AQAAAKZDmh9wKYaAxV7mBwgAAAAw3k6E7vyAhgAAAADT
Ic0POBtDwOqd5gcIAAAAMDkOh+78gOWd5gcIAAAAMHmOhu78gObN8wMEAAAAmFwn47a6fX7A3cYE
AAAmWlo29GwOAU1XAAAACniw9aVm3BbjdmBSX+NnvvgDC81Pnft8fI1Lk/w6p8hc3GoCAABAcZ3l
F2NxvDBhhX89bu34n6fvCjf2xp/H8utsesvH1plU/N/3+U8sCQAAAP1Jyy+ejsVxKpDrY1741+K2
HP/zQui2jdz8Ok9OwuucMitxezAW/gtxW0//gzkAAACDkQrmC7E4TgVX41L9ofYYFf6pvSddxTg5
ya9zylxO72ks+pdv/n+4AgAAMFhH4nYpzxGofN98LP4b8Ud7l8X/rV7novkBlbIRt1Ox8K/dqvgX
AAAAhicV1KldplHRwj/1+a/G/zwbuu09RZ2o8uucMudCt8+/ebt/JAAAAAxPKqzPxuJ4tSp987nP
fyl0+/wPT+rrnDKpHevRWPg3tvr8b8ccAACA4UuFduqbPx9/Loyib35bn/9C6O+Mf6Vf55RJff7N
tLJPL7/kCgAAQHmOxm217PkBuc8/tfucHGLxf/PrHJt5EGOo0+cft7lei38BAACgfDO5EF8ddt98
LPzn4tYK3T7/QyN4raW8zilzPhf+zd20+wgAAADVkQry1DffitvcgAv/A7nP/4nQXa2nKq+z7m0v
7GLo9vnPx63dzx8SAAAARisV6E/E4nhpEO0ysfBvhu6ynscq+Dov5NdZ87bvWmr3OR6L/nTWvzWI
PygAAABUQyrY03KazYKFf1rWMxX+ZfX59/M6V4u+zimT+vxrRfr8BQAAgPHQmR8Qi+MUBOZ3WfjX
cp9/Wtbz0KS+zimTlvV8sJ8+/9uxDCgAQPWkQv7xWBynQrBxq+U087KezdC9EdckvM60bOjqlL/v
aVnPxqBafXbiCgAAQHWlvvm0nObi9vkBsfhPa/m3x7z4v/l1DmwexBhKff6PxcK/NuziXwAAABgP
qdBv//jvr/yXWPyns+SnQ7X7/IvamgexMEXv7ZnQ7fNfLOsB92xubvpIAQD0KE9iPVnW47W/9dJa
+NzabPj28xsfft/jM4+8daWUx/34s79w+TeePzKKuQWddphL9Yda9rbBcgUAAKDCnn95c6N98ekQ
Ll6ZDS/eCOHlvTP/4o9/NnzwfzfXvvbNd0zyS0+h40K+f0DNniAAAABMtJc3N6+1v7hx/elPfnUm
fOuFV/z/v/zsm2Z/+vd/PvznP/m5jedeuH+Sh2JrHkRzSucHCAAAAJOu/fS1K1/55JP7wte+tfdO
//Y3Ls3NHFk+ef1jf/FPrk34sKR2qzQ/oGEPEQAAACbCl5976Ur7M1dC+KtvHAw3epinuXn33uZn
f2Lfux//lbUnvnJkkocoTXw+G0NAupFY3R4jAAAAjKVOn/9frF399uqVg+HqS8X/0Av3zKb5Af/q
//zbKxM+P+Bw6M4PWDY/QAAAABgbL29uXm1f/tZGp8//2ev7B/V3/+zptx386T947OoUzA84GrdV
8wMEAACAyuv0+X/qa/vD32wMZz3/zbv2d+YHfOzfbUz4/IA0fidzEGjYswQAAIBK6fT5p2U9U59/
WtZz2F7eO5PmB7z3Y//pyv/7+rsneWjTsqFn87KhdXuaAAAAMFIvb25eb3/+mWudPv9bLOs5bN9+
/r6D//QP/2VnfsAULBua5gcsaQsSAAAARiL1+X/lk0/uDWtX9436uaT5AUeWm9fS/IAJH/Zjobts
aNMeKAAAAJRT+D/zwlr7T58KnT7/Xpb1HLbNu/al+QHv/p+LG1OwbOjJGAJSEJi3RwoAAABDsXb9
xlqnz/8vn54tpc+/qJf3zqRlQ//u7/3S2oQvG5rmBzye5wfMCQAAAAymnt7cvNb+4sb15/7sqdlR
9PkX9cJzr5/96d//+dD8o8fWpmB+wBMxBCxO8/wAAQAAYADaTz6/9pVPPrkvfO1be8f1NXzsq++c
TfMDfnP12PUJf7tOhO78gAUBAACA3gr/b73U7fP/62dnK9XnX9TmXft++fPv3/vux39lbQrmB5zO
8wPq07TP7tnc3PTJBQAq4cDDH0qF2Or6Zz+6Xvki6nc/txSefO5YWLs60e/J+x9+8RtPzbz9DWU9
3vMvb4ann70eaveXvmDS+bgtXKo/1J70z5krAABAlaQA0I5BoFn1J1r7vte03/rO+66GvzWzEe7a
M3nvxJ4b1/75g6sbP/XA3zxX1kO2v37t6tOfenIjtNcvj+AVH43bpbRs6KTPDxAAAICq6SzdGENA
CgKVXrrx7j179tcO3Ttz/3sf2Aiz+69Nyhvwnvu/fGVlvrnv3/zYf50ppfD/1kuh/ekrV8IXvrE/
3NicGfHLPxm68wMaAgAAQLk6SzfGENCKW6WXbrzn7j0ztXe9ft+r5g5eCfe+dnwLw3uevfLhv/eb
4dd+8pcOvu61Tw/98VK7T/vP166Ei1dCuPbSwYqF0LMxBKxO4vwAAQAAqLrO0o0xBCzGrdKtGW97
3asP1g7fH8I733BlrNqC7r6+0Xz4D6996gO/ePCRt66U8pDtL61vPP0nX70W1q8frPDIHI7bhRgC
luJWEwAAAMrVWboxhoDKL91Yu3/fwbe+74FrnfkBVbbnxtXU57/ygf8484Ef+p1SZt22n3khtD/x
1Y3w1HMzYTPsG5N971jcVidlfoAAAACMk87SjXl+QL3KT/TuPXv2deYHvO8tG+G+vZVbKuht931t
7eM/dXp/6vMvo93nyvUbof2ZK2vhL+Njjb7Pv+i+dzIHgcY4f4judhwBAMZQmh9wIYaA1K/SWP/s
R9tVfaJpfsA9PzQbvvzcS1e+/YVnDoarL432Cb32+bUPv/d3Zx9568psGQ/XWdbzi8+uhbWr6fFm
J2TfO5tDQFo2dFUAAAAoT5ofcCkGgVPx52KV7x+Q5geEHznYuWNwuLxe/k3D9rx8vfnD/3fzAz/0
O6UV4e0vf/N6+JtvbobNiSj8b7XvPRGDwLkcBNbH5YlrAQIAJkFn6cYYBBpVf6K1B+6Z7cwPeNO9
18t6zE6f//ypvaX2+f/pU2vhy9/cO0Z9/kWl+QFp2dCmAAAAUK7O0o0xBKyOxfyAd8zs7cwPGOKy
oa993TNrH/+HvxxK7fO/+HS3z//FG7NTtu+djCEgBYH5qj9ZLUAAwKTpLN0YQ8D5+HOh8vMDDt+f
boS1Fj63NhuL5gFVeNc3Pvy+x2dK7fO/vNFd2SeE2Sne9zr3roghoDM35VL9oUrue64AAACT6mjc
0tWAZtXvH1C799WztR99c+gsG9rP/QP23Lj28+/6xPVP/8zCTGnr+T/5/LWnP/XkVvFPV2duSgwC
i1VcNlQAAAAm2XeWbhyL+QGH7p156/seuB5m91/r9Xffc/+Xr6zMN/f97Ny5vaUU/qnP/9NXroS/
fnbfmC7rWYbOvStiCKjUvSsEAABgGnSWbowhoBW3uSo/0bv37Nlbe9fr973uPW9e2838gFfd8+yV
3/qJXw+/9pO/dLC0Pv8/X7vS6fO/9tJBu9auQujpGALS/QPqAgAAQLk6SzfGELBU9bag2b13zdYO
3x/CO99wJbzmrlskhesbv/qej1391Ad+8eD3v/HTpTyn9pfWN659+qmrYf26wr93nbkpMQQsx60m
AAAAlKuzdGOaH1D1J1q7f9/Bt773TVe/Mz9gz42raVnP1Od/5B0f319K4f/1a1fbn/hqt89/M+y3
+/QlzU1J8wOao5ofIAAAANOqMz8ghoCFqj/Ru/fs2Z/mB9z3/Xf98cf+wZkbaVnPMh734motfOT0
514dvvCN/fr8B65z74p8R2EBAACgBGmZnEfWP/vRxTF5ro8+8zM//vcfOPCFt8X/PjfMB9tYnwm/
8IuPXHns1P7w1UtPv2RXGWoIPRtDQKvM+QHuAwAATJvLoXt/gOUxea7NS/WHlrb+hx95x+fW44/G
Z774A+l/a4buvIaB+chH5q7/9h+8tPnyjRf0+ZcnvYdpfsC5/H63h/lgrgAAANNiI26n4jY3JsV/
57luL/63i0GgFbd6/M/jOSj05Y//6G+Hf/TPfnjjf/zei3tfvrG5z+4yEmluyuqw5wcIAADANDiX
C/9m3NYr/lzTHYwfjIV/OhN8x+caQ0AKCHM5MGz0+mCXL705HP/Xf2ftP/zq3eHqtW/r8x+979y7
IoaA+WE8gBYgAGCSpd75VPS3xuC5XozbQiz6e36uuS2omduC0pyGo3f6ndTnf3bp7Ru/u/JCLDhf
nrWrVE66d8XjMQSs5P1iVQAAALhNfRu6ff5L4/Jcd2r16TEItOOP+RgE6jkIHL7Vv/ut33r42kc+
+u09N15+wRn/6uvcuyLPD1jYzVWhO9ECBABMmtQKUxuT4v9Meq6DKP5vCgJpfkBqCzoetrUFpT7/
Dx17eO2//faNfTde3txrVxkrnXtXxCDQ97K1rgAAAJMi9c6ns/7tMXiuqa2jMezVXtL8gM988QeW
P/PpB3/11//7zE/91eUX3xTCDe0+4ytdsTmdQ0CjSLuYAAAATIKLufBvjcFzvdxP4darAw9/6EAI
P5iKxX8cwovafSZHmh9wIc8P6DlICgAAwLhKrS3NMbmRV3qui2lln7IeMBb/jdC9T8Ahu8rESvMD
LsUgkFrJmrudH2AOAAAwjjq982NS/KfJm7Wyiv9Y+Nfj1or/eVbxPzVOhO78gMZu/rErAADAOOm0
PIxRn/9Al2+8Q+GfbhyVAtExu8lUSi1eZ/P8gNsuJysAAADj4HIu/Ftj8lxTAVba3YZj8d9Mj5mL
QKZbWvo1zQ84n/fDV4RlLUCMVD5bAeCYwE5S7/ypWPjXxqD47zzXuM2VVfzHz8x83FKBd1Lxz03S
zeDS/IBm3A4IAFTli74Rf7SNBOO6/+YvXQZU+MdtKf7nstFgm07vfCz8m2PyXFPh3xzEjZp28ZmZ
y33+jwd9/txeCoer2+cHaAFiFF/09XCbuxOC/Xe6Cv/QbVvYal1YMSrk/SAt67k6Js+1We6ynp2V
fU7YTehBColncwho3p3T45FxPUDEg0Pdezo2X/S1+GNpjPe3QY9HM6fysRQ/e3vsv/Q5po1giUJe
aXFMzvgnS2Uu65mlY5Hah6LS3aFrrgBQxpe8sxWM+/5rVY3Bjmk9HxOEKV4hFv/r4/Jch30X3x3G
J10VmcsBOh2b9P2zW9+5V4AAwLC/6BfyF70DFOO4/6Z916oagxvPWj4eCFPQfxBYip+p5XyMOmlE
uI1X3C1YAGBYX/TzoXtmwqV9xnH/bQStKYMczwOKFBhKCEhXS5p5An36zj1qVNhmx+VoBQAG/UU/
lw9CLu0zjvtvPWhNGUaY0qYAww0C7fhjPh/DUhhw8mK6peVoF283P0UAYNCeMASMsQuGYKDFf/ry
cdYfygsCrfijpv12qqXlaBfutBTtxAeA+CHYLPBrj97uZiPD+JsAAAMKAou5LSiFAAtwTIeelqN1
IzAAgMkLAetxS1cCHgzurzHJUp//8Vj413u5F4UAAAAwuUGgne+Z9MFcLDIZUp//qdC9+/RSr79s
DgAAwOQHgbQSzLLljSfC+dDt828X/QOuAAAATE8QSAGgFrqTRRkvF+P2aCz85/u9CV26ArAUt1af
T6gRel9y6nJ+7H607QuMsdYA/kY9FFuy8pThB5jaEJBWiGkcePhDaYleS3dXX2r3WSjS6rNjAEh3
kuv3j+R1Z3sNAO2cQmFaD8CtfkNAvpR7pMBj++wB+B5ajT/qbn5YaemE3eKdlvXsOQAYVwAoXyy6
aqHbirHdaj47C2UGgaW4P6Y5AgvB/ICqSCs3Nfpt9REAAGB0xX49dFv26rnoP3Sbf5t+pDbZ1bwt
5zO1MMwQkIJnM98/ILUFHTUqI3E5F/6tYT6IAAAAwyv6G3GbD72fUT2Ut1SEnYx/K/UApzO0i8IA
Qw4C7bTP5v03BYHDRqUU6TOebuS1WMaDTUMAeLTA76yO4G9W8Yvre56/y9IMYL+qhe7Zz639q36r
75/8eVnP+13LyN12TA/EH3N5O7Dt581a2376PA/3PUlFfzMMtp86BYhjaYt//2IOAktjegyo33Qc
2L6PpuKzlYvQYTzmrT4fW8cbn43vDQJpPObS/rz5mtf8eyMyVGlFpoVB9/lPdQAYRvEw6oIkfhh7
ffzVfDfA2x0Y6/nAePg2oaeShVi+XFkr+nbGreGAP9T3J539nM/72G4LoqPbfj/9SL2QnTWsB1kY
jPGYps9qI4/pbs/ObU0WP5n/xsX8mV6apDPKeWz6OYOW9rHFPvb1xTD8iZTpPT8bH6/Trz2s76S8
QszcLv7pYl5j/nYhNY3Nwh321yPbfqevkJO/2xby4x7q8bNxPu8HS4HO/IB7/9dfveu5rz73c+Gp
58wNGKyVXPiXfgzWAjSe+l6uK5/hXwhj3uOXV8E51sefeFTxP5T35UDevxoDKoaO5O10/nJenLYr
A9uKqOaAxvRw3k7Ev72Sg8DSBIzRUijesnAxFFieetvjln08Ta/zQv5MDONExtwuv2/Wc0Df6Rhd
ZFLpVshp5tfW2uV7UcufkX6+F9L7eHTrud8u3EyL2b13X5996MDMlbfcG6795dpauPrSrG+6vlzO
hf/I9i03ApvO4iyd1bkwAcV/CjEn+/gTx7WXDL4Ay1+a7fzeDONM6NFc9CznL/tpGNeFPKZnhzSm
R3Kx1cpn0MdVP/3Kqf92vtciOh+H2iM+nqbHXh3he3c0h6Dt45JaR1bzcaCfs8aH8ue9ucvPyaU+
i/+bH/vxfKw54AgfwsG9d4XajxycDT94fwh37dkwIoWOM6di4V8bZfEvAExncZYK3hOT8FrCDmec
dumcy7sDf0/S2elBfOH3WvQ0JnhMt4qo0yWNaQoCT+ym2KrgWDX6KPzSl3K91/ayXHBeCNVYMvFQ
fu9G9XmYv+lY0AqDnTx6Mrd77vTdtpQ/J8M61rSEgO+qvf61ofb+t8yEt33f9bAnXDMiu6s74jYX
C/9KHF8FgOkySXf7W+7jS3clftE37A4DLb7SvvV4KP8mMmkfOLtTYTDmY5qKyyfCaFbgODlOZz0H
0Pe/0Os8iCEXnP04O6IQMJ/HpZ6PBcMIRcd2+Ky3wuDO+u/ksBBwiyDwtu/be/+PvWVfmN2/ZjR2
rjni9mgs/Ie2pr8AwJ2KiWMT8lqafQSZi2HbmSr6fi+qclXp2CSFgIoUl2Nx1nPb1cCiBeepXq8G
5venysfTUYSAem7JWy7hs9646b0oKyQfLuH1jZ177t4Tau96/Ww4fDCEfa++YkS+I/X5H49Ff33Y
a/oXMVGTgPNZoF6/rNqDWFXkFstm3lGJ/edpTJoT8h6n4r1o33+hHl9uW3i1QnXWiE6FQRj3qzsV
Ky4P5/e4yvMC0ngVvfKUWgGbPb4/43Iy5Wz+PJQVjGfyvlJGO9RivmttYwTvxZG0DxRdKWqS1e59
dQjvPniw/fVrV8OXnnkp3Nic5hWDTqX9tMxlPac6AIRiLS6nBlQcXyjwO3tK/BKfhIKzFgqs0LFN
3RKSA9Wq4L6VQkBrXOd3VPTM8uHU4rXTUsIjHq9+VjJLVwMXeny8dALi9BjtUikEtEs82VRWC+BM
/i6oj2hcO3fLdTJphyDwxn37wxvfEtpfWt8IX3vuNWEz7Juil59W5FqoUqvPTrQAMU76ucx/3N0z
B16oVjVYLo7j6kAVP7N8Ihe/VRqvuT6K8cv5hMB6D4/X7wmIkR03J7Rv/WgY3eTrmV7D41QGgYcO
zHTmBxzYOw1tQemEQurznx+H4l8AYJyKo36W9ztlxZ+BvheNUO0WiJlxK9T6LGbLDFYHKjJe/awC
VrQVcGmABWcKIGlFkHQF+tGbtvS/nc/Pcyo/D2NiwYTgO+vMD/jh2YOdZUMnc35A+pymPv+5Kvb5
344bgTEOxVE681h0kmnPPb7csfAaRO/r1p19V29uT8iPUQ/dydpFg0bq062P0X0eBlGgXcxj2srj
un7TmM7lMW0ULGRTe0c669msyHgVbTdpFFjxJ43ZIFZQS0X/0h32y9ZNx7702P3eYyCt0z8/whta
Xc7Hjdb2sd92J/pmGE770J0et5/3dSZ/noSrXUjLhobXHzzYfvL5a+Hy+osTMj/gTNp3q9znLwAw
zgVnrY8DbM89vtzRYujvLGgqgJq3m4uRC9dUqCznFZ+WCn5JN8PoeoR72cfT8+ynnWolj2nrDmOa
/v+t/HjNgqF6Ic8HWB/hePXT93+81yJ4QKF3JQePdi+/lJ/r8rZlTo/0+dkdRQA4tdNJmDwe6fO9
lPfLkwN83DM7zVu56XHTvyl69U0A6DUIPHDPvuffuH/f05c3NsJTz41rCOh8nsel1WcnWoDYqXBe
ydupvI1qRy/a999zjy+7CmP93Gjp0bRCTy9FUPq3cavn4NCrI1W/q20uLvsJqY+l8enlSkf6TOTC
6HiBx9s66zmq8eqnVarozf8W+gy9W+9R4WNoOnudPwen+ngeh0awNOjx3V6Bzf/u1AAfd2GXj7tY
8LMQxuEEQxV12oIeOjCz7z1vDuHe147T/QNSXfFoXtazPe7vgysAbBVn6YtxuUotE330/Vvuczia
fexf9X4mYafgkIvlXs/8piKgUeExLVpcDmJMl3KoO1ngOS+N4HiwtexsEf3c/G9hVPv9rYrktKpP
/M+zfXyGy3rvep57lV/ffOjvitj5Ao+7VLDNayaFUgtMFHNw713xnb5/tv3MCyF88Zm18OKN2QrX
SItVuYPvoLgCoPBPZ6cOpLMlFSv+08G4aN9/wwF54O9HKhSLnv0fVBHUCL1PjKz6Td8KF6WDGNN8
1vVij792eESrLBW9Glj45n/5ODSSgHa7YjUUP2N9qKTVnLZ674vot92qaGArWtzVAn1J8wNqP/rm
2fD2+66FPeF6xZ5euvpcm7TiXwCYbulLca6KNzPZ1u9axGMjnOg2yYoWDacGVQTlKzq97hczRW7S
V9J+nsa0yMTH8wNe1apZ4v5QdKzScyzS/76RTwgUvRo40oB2hxDwWMmvqReF18jvc98+V7TVKp8A
u1zgVyvdZjhWQeCBe/bd/2Nv2Rtm91ehLSi1QD8SC//GuE7yFQDYqfiv5E2x8mX+9AVQ5KzbOXdn
HJoiRcPlIazAVKQ4qFd0TIsW0QOd2J4D80ZVxzQHuKKTQwufhc9XOYqEjvNlLDucj3UrBX71aAnL
V/b7+lcK/l6/J39cOR6xzvyAd71+tjM/4A373zSCp5BC4Adzn/9E7w8CwPSp+uTYon3//fT4cudQ
VuQ9GXgRlENrry0rVT1DV6SIPjek4L5cxTHtc73/fm/+V+T92QjlzjlplPjaegn+/e6j7YLHh1EE
gKqeYBhraX5A7e0zry3xIdNnN01CT+v5T0UXgQAwfRpVLf77uMFU4R5fhvoFtzSk59Ma9wCQzy4X
af8Z1hdTr4XPoZKGqmjf/5kBnIUvst+XukRqLrTPlfiZHlYRPYgAcDFAMedy4d+c1HYfAYCVqt4Y
qY++/357fBlOAX1xiC1mvb7Xhyo4poUKsCHOb1kt8JmtD/mY0AzFWnDO7XYJyCG8R0sj2JcWS/pM
lxkAyjguQGo1ezT3+ben7cVbBnS6VLI/vs++/7oVfypZrB7IBVwlnk86416xOS+1Ar9zeYhjWqvY
MSG9x0X6/gd5879eg+PFUexj6fgXx+tyj8/3SJg8U1fAUVg6cbgQi/6laR4EAWCKdvgKr45TtO//
uOK/nHqsYPF0skKvoVaxAmFuAsY0vYbWkN6rIseqraU3+z4TXPDqxiiPr+mxT/T4Gg9M2JVTAYDd
SH3+i9PU6iMAUOVCuUjf/5kyVtqg47AhqESompbXUKRla2DFfx9aI3zsIsf3uRE/ZyjT+dA96y8o
CgBTZ5IO9CsD6vEFJsOg190vEm5GWVgoauDWLubCX9i9iUnA02OSWmXmRnQnUhjYPmwIBmp+1O9P
Fe+rAlMsXRV8LBb+c4p/AWDaTVK/W5os7G6/jPs+zOCkm1u5KggkZ+JWi4W/G4MKAEygw0NcEQWG
7bIhGLhmXk54ENq9/kIJd9cFbi8t6/lgLPwXTPIVAJhsJ4e9FjkMSXsCXkOrYs8nXVVZGuH7M8q2
LuGDaZZOqHwwFv11k3wFAKbHkjNv3ElVb4DHwKUrg6O67D/KAFDksZ0hZdylPv9TsehP7T7agntk
FSDGXVoycCkMfhIg35Uuq/Z646BB3pBpErVDsZsxPVqh11DVhQVOxBCw3GfoK/La6mF0N1vs+fjn
HiqMuXOhu7qPICsAMMXSBMCG+wIMTZED7GFn3e8YAAr9ntVmdmU53/25UHGQfi/+fjq72Mtk7foo
bq6VV0Tr9V4dF+0ijKmVXPgLsH3SAjS4AzCjteh9GJrVgp8LS13urGg4qhu6XRnEfIDVAo85iiuR
jbI+0zBCqc//eO7zt/8KAJWh8KzGF74ewGoVqw1DN/ACTKvb7vW7NGiR40mzzBeY5z8tlPiZhrJ1
+vzjltbzXzIcAsAgD2r1ATyuM52D9VgotkyipUGHILfybBQJACZo7zimqU2kSBvG0Sm80rWRjwlF
9LM0aJHvk0MlH4PSY82U9NqgbOdy4d/U6y8AVFXdEAzuAx+LozSRruiZTkuDDkeRs6GpMDEReGdL
Jf/euBb/9XxMOFNwHyw0XnmSbJETEQtltL/l49yJAr960TwSKi6dHHk0Fv0Ny3oKAMN0pJ9fzmc4
jxrGgRX/jW1fvqeKFkjOPFciAAhkwxnTI1N019v5bavVNEPxK4NFV+cpEh5mhn0MyleBiu4/7o5K
lQN/6vNPZ/1bhkMA6EWhHSatINPHYzrDORgrW8X/lvh/N0OxNolDYbrOkg5dfC+WQ/G71y6bEHzL
MW3HH+cL/vrpPo9b4+D49pWkcttU0dd8omAQLXocSavytIYRAvLfXA7FWn82grlSVFM64VfT5y8A
FNUu+HuFvlRyUXPSbtS3VOTPD/K9CXlpUEM7UM2Cv5cKlSeGedY6vdejmv/RZ09+P2djzw7zplfx
b8+P8KZaj91qWd8cCIpeGVzutSDPIe1cnyFgboDvyVz+njtcdH8re5lSuIN0EuRBff4CQF/ywbrI
WcqeL6nnLxJJdTAWdvpS6rMVyNKgg/18LYXiVwGSdNa6NaiWoPR3UoEat7TvnA3ddqNRvN+NPsY0
FbQrfTx2OrPdHlTYTQVmHtN0LH08FD9z3tcJgdzzv9OYNUOxK4NF5wP0Eyy3QsDCAN6b9DyeCMXO
/CcbQfsP1ZG+S1Kf/7w+fwFgUIpe3tz1JfV8FmY1FD8LQ29FUrNgkWRp0CGEtT5/P825uRA/Q6up
KNptwZ4Cdy740+8s56L/QuhOgpwZRDHez5j0WST3+5xTy9vZHAQWeznjvG1Ml3LR/0Qe00MDKoAL
feSHOGY9Lw2aTyyd6eP1zOTvl0JBLV/dSs+h36vNzv5TBZ1VvWLRX9PnP1qTeCfgpVBsZYSQv0Tn
84GytUPhn748jtl1StfIoavXs1+dpUFziKD/MJaK73TJtt+J7yk8n86F0Ub47rr4W5+7Wvju/TXm
enjfUzFbdqEzk0PNxW2Bc+v5L93pDtWpwIy/e2oABd6hfOxLZ+3DttC8NaYHwneXLO5lTNMV0rlt
k3GrsB+u9jFmaWnQVo+vp5mPQTN9vj9bbVvLeVu9eUWe/D2TthQq5/t8zC2XHQOpgBSktfoIAEP9
YrgYip+dT4XN0ZuKkl6/MBn8+9rOl8BPF/j1k/kL39mGwYWxVhjcFbD0udpajevIAP7WQih+1rqf
Y8fhW/xua5f7dzNfRTgywPdpUGMa8pg2KnZMaOYTNocL7CNLoYf7t6RAmR/rwoD292N5CzmsDZsb
yDFK6WTEgjv4VsukLgPaHMDf2CpKtjbF/+i/8BdD8X5pS4MO7n3YWo1lo6JPcaGP97o9wuc9H4r1
tpfhWEXn08wX3A97Xho0n0A4M4Yf2ceqdPWGqZL6/D8YC/+64l8AKKtAWQ79TayjuooWnpYGHexn
LB3M6xUNATOh+BnP1oiDVb3CIaBZwf2w3cfz6nmCc3y8hTH7bjl3uwnVMCTpe+FU7vM3D08AKF3R
M0P9JF3K+cJvFPx1S4NOTwgoWhQuj3hMqxwCKnkVoM8rg8sFrhZV+UrNdhdvvrcKlBE6Q3c9/6ah
EABG/UVaVnGSDrTn7VKlvLfLfYy1pUEHHwJqoXpnRQ8VCXs5YK6MeEy3jl3nKviWV7WgbBQ81ve8
NOgYXKkJ+bnVHaEoUTpupmU9Gyb5CgBVKU7KCAFbd6x0qWs8vvC9TwMuWOOWPmenKvbUmiX/3qDH
NO3jHwzVusKyUNF9sN3HcyuyNOhWCKhiO1AKjnVLflKS1P1wPPf5twyHAFDFEDCsszXHty3zp7As
sUAKxc9GHh7VXWMn/D1JY/pgGP2VsI2tIqjg60hfYmcqMqbpmFIL1bgakJ7DXIX3v6U+9r1mr3fs
3RZ8qzQx+FQKjop/SjrOppM+c7HwXzIcAkBlQ0Dc5vLOOqizaSlQPLJ9je980DX5uNziqOiX78kR
3OF0Gt6TdtxSj/QjIyha02fyeCqYcxHU7uN1LFSk6N5+NeDB/JzKntv0WHrsfse0JI1QUivQTfvK
qK/UdO6qaq1/SnI+F/7W9BcAxqY4SQfHWg4CRSftdi53pUCxw9JqrgKUq9nHe2lp0OGG7lSM3ZcL
yGEE4438RXQ8F6jpM7k0qLOf+fk/FirSgpPDVSMfw46H4V1pWcmvO53gSGFqcQwK/++EpdDflcHF
go87yis16STInPucUIJ0kiX1+c/HrW04xtugbgSWzoD0WkiNJDXmL4hUNG5d8q3nLR28D+9Q8Kcd
PR1cl3exnvJS+N4biA3DowV+p6w1eEt9bvkGPVvv3zRaCiNcunKXn7dUVC3msJXeq63P3YGw+5s4
XczHjFb+PK6WsbZ5Kn7j817KRWW6snGnm2ptHS9W88/lIY3pUt5C3v+3xrVWYExXt7YhjGmR/XO9
z/FJK/s8OqJ9vZFDRPqOOTrkh0xhoznkcFbku709ov2mPY776zh911jZZ7Ls2dzcNArASOUwfnOh
sVrFXuYcZOZuKv5aFXyetVsE4/a4nM2fgH26Fr57B+VB3Ujycg6Vi95HQAAAgOqGgXT1qJ63wz3+
emrJSldmltzRFxAAAGA8A0E9/2d9h3/Situ6gh8QAAAAAAEAAAAQAAAAAAEAAAAEAKMAAAACAAAA
IAAAAAACAAAAIAAAAAACAAAAIAAAAAACAAAAIAAAAAACAADT7MDDH2rGH8vrn/3oqtEAeKVXGQIA
Jkw9bk/EILAUtwOGA0AAAGA6HItbO18RACDTAgTARIkFfyv+OHLT/3w5bo31z360ZYSAaecKAADT
4FDcLqRwELea4QAEAACYDunKwKUYAhbNDwAEAACYHidCd35Aw1AAAgAATIeZuJ2NIWA1bnXDAQgA
ADAdDofu/IBl8wMAAQAApsfR0J0f0DQ/ABAAAGB6nIzbqvkBgAAAANMjLRt6Ni8bWjccgAAAANMh
LRua5gcsmR8ACAAAMD2OhW5bUNNQAAIAAEyHtGzoybRs6DS82AdbX2rEzWRoEAAAYKqdiVt9Sl5r
I27tGAIWvO0wWe42BABwRyupIF7/7EfbU/a601WP0zkENC7VH2rZFUAAAIBJdjluC7HwX57ycUir
Il2IQWAlB4G2XQMEAACYJBtxW4yFf9NQfI+0KtKlGARSK1QzBoF1QwLjxxwAAPhe5+JWU/zf1onQ
nR/QMBQgAADAuErtLY/Ewj/1+juzfWdpfsDZGAJW41Y3HDA+tAABMO1Sn38zFv1LhqKQw6E7P+B8
/LlgfgBUnysAAEyr1Od/Km5ziv+BOBq3dDWg6f4BIAAAQNWcy4V/U7vPQHVulpaDQMNwQDVpAQJg
mlwM3WU9W4ZiqNKyoWdzCGi6fwAIAABQto1c+C8ZilKlZUPT/IB0xWXBsqFQDVqAAJh0qc+/pvgf
qWOhu2xo01DA6LkCAMCk6qxKEwv/tqGohM78gNwWlK4GLBsSEAAAYBBSwd/U519ZaX7A4zEIrOQg
sGpIoFx7Njc3jQIA8D1igZ4C1JESHupM6E4UNj8ASmIOAAAwSidCd37AgqGAcrgCAAC8QolXALZL
d2VuWDYUhssVAACgKtL8gLRsaCtuNcMBAgAAMB3SlYdLadnQuB0wHCAAAADT4WTozg9oGAoQAACA
6ZDuH3A2hoDVuNUNBwgAAMB0OBy68wOWzA+A/lgFCAB4hRGtArRbG3FbTJv7B0DvXAEAAMZNagtK
8wNWzQ8AAQAAmB5p2dCzednQOcMBAgAAMB1Sq9ITeX6AZUNBAAAApsSx0F02tGkoYGcmAQMAr1Dx
ScC7cTluC5fqDy17N+F7uQIAAEyiND/g8Tw/oGY4QAAAAKZDuopxKYaARfMDQAAAAKbHidCdH7Bg
KBAAAACmQ7p/wOkYAtL9A+qGAwEAAGA6HI7bhRgCls0PQAAAAJgeR0N3fkDT/AAEAACA6XEybk3D
wLS42xAAAFPsYujeL6BlKBAAAAAm10Yu/JcMBdNGCxAAMG3OxK2m+GdauQIAAEyLlbg1YuHfNhQI
AAAAk+tyLvxbhgIEAABgcqU+/2Ys/BcNBQgAAMBkOxe6k3zXDQUIAADA5FrJhf+qoQABAACYXJdz
4b9sKEAAAAAmV+rzTz3+i9p9QAAAACZb6vNvWtYTBAAAYLKt5MK/ZShAAAAAJldq91lwB1/oz6sM
AQAwBk7Frab4h/65AgAAVNn50D3r3zYUIAAAAJPrYi78W4YCBAAAYHKlPv80wXfRUMBwmAMAAFTF
mdDt81f8wxC5AgAAjFpa1rOhzx8EAABgsl3OhX/LUIAAAABMrtTnvxgL/6ahAAEAAJhs50J3dZ91
QwECAAAwuVZy4b9qKEAAAAAmV+rzb7qDLwgAAMBk6/T5h26vv3YfEAAAgAmW+vyblvUEAQAAmGwX
Q7fPv2UoQAAAACbXRi78lwwFVN+rDAEA0IdTcasp/mF8uAIAABRxPnTP+rcNBQgAAMDkSst6NvT5
gwAAAEy21OefVvZZNBQw3swBAADu5Ezo9vkr/mECuAIAAOxkJXTbfdqGAibHns3NTaMAAHyPB1tf
qin8QQAAAAAEAAAAQAAAAAAEAAAAQAAAAAAEAAAAQAAAAAAEAAAAQAAAAAAEAAAAEAAAAAABAAAA
EAAAAAABAAAAEAAAAAABAAAAEAAAAAABAAAAEAAAAAABAAAAEAAAAEAAAAAABAAAAEAAAAAABAAA
AEAAAAAABAAAAEAAAAAABAAAAEAAAAAABAAAAEAAAAAAAQAAABAAAAAAAQAAABAAAAAAAQAAABAA
AAAAAQAAABAAAAAAAQAAABAAAAAAAQAAAAQAAQAAAAQAAABgAv1/AQYAb5w/x0u2FCYAAAAASUVO
RK5CYII=" />

                </td>

              </tr>
              <tr>
                <td>
                  <img width="380" height="1" src="data:image/jpg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/4QBYRXhpZgAATU0AKgAAAAgABVEAAAQAAAABAAAAAFEBAAMAAAABAAEAAFECAAEAAAAGAAAASlEDAAEAAAABAAAAAFEEAAEAAAABAAAAAAAAAAAAAAAAAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAABAAEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9/KKKKAP/2Q==" align="middle"/>
                </td>
                <td>
                  <img width="190" height="1" src="data:image/jpg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/4QBYRXhpZgAATU0AKgAAAAgABVEAAAQAAAABAAAAAFEBAAMAAAABAAEAAFECAAEAAAAGAAAASlEDAAEAAAABAAAAAFEEAAEAAAABAAAAAAAAAAAAAAAAAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAABAAEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9/KKKKAP/2Q==" align="middle"/>
                </td>
                <td>
                  <img width="380" height="1" src="data:image/jpg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/4QBYRXhpZgAATU0AKgAAAAgABVEAAAQAAAABAAAAAFEBAAMAAAABAAEAAFECAAEAAAAGAAAASlEDAAEAAAABAAAAAFEEAAEAAAABAAAAAAAAAAAAAAAAAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAABAAEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9/KKKKAP/2Q==" align="middle"/>
                </td>
              </tr>
              <tr valign="top">
                <td width="320px">
                  <br/>
                  <table align="center" border="0" width="420px">
                    <tbody>
                      <tr align="left">
                        <xsl:for-each select="n1:Invoice">
                          <xsl:for-each select="cac:AccountingSupplierParty">
                            <xsl:for-each select="cac:Party">
                              <td align="left">
                                <xsl:if test="cac:PartyName">
                                  <xsl:value-of select="cac:PartyName/cbc:Name"/>
                                  <br/>
                                </xsl:if>
                                <!-- <xsl:for-each select="cac:Person">
                                  <xsl:for-each select="cbc:Title">
                                    <xsl:apply-templates/>
                                    <span>
                                      <xsl:text>&#160;</xsl:text>
                                    </span>
                                  </xsl:for-each>
                                  <xsl:for-each select="cbc:FirstName">
                                    <xsl:apply-templates/>
                                    <span>
                                      <xsl:text>&#160;</xsl:text>
                                    </span>
                                  </xsl:for-each>
                                  <xsl:for-each select="cbc:MiddleName">
                                    <xsl:apply-templates/>
                                    <span>
                                      <xsl:text>&#160;</xsl:text>
                                    </span>
                                  </xsl:for-each>
                                  <xsl:for-each select="cbc:FamilyName">
                                    <xsl:apply-templates/>
                                    <span>
                                      <xsl:text>&#160;</xsl:text>
                                    </span>
                                  </xsl:for-each>
                                  <xsl:for-each select="cbc:NameSuffix">
                                    <xsl:apply-templates/>
                                  </xsl:for-each>
                                </xsl:for-each> -->
                              </td>

                            </xsl:for-each>
                          </xsl:for-each>
                        </xsl:for-each>
                      
                      </tr>
                      <tr align="left">
                        <xsl:for-each select="n1:Invoice">
                          <xsl:for-each select="cac:AccountingSupplierParty">
                            <xsl:for-each select="cac:Party">
                              <td align="left">
                                <xsl:for-each select="cac:PostalAddress">
                                  <xsl:for-each select="cbc:StreetName">
                                    <xsl:apply-templates/>
                                    <span>
                                      <xsl:text>&#160;</xsl:text>
                                    </span>
                                  </xsl:for-each>
                                  <xsl:for-each select="cbc:BuildingName">
                     *               <xsl:apply-templates/>
                                  </xsl:for-each>
                                  <xsl:if test="cbc:BuildingNumber">
                                    <xsl:for-each select="cbc:BuildingNumber">
                                      <xsl:apply-templates/>
                                    </xsl:for-each>
                                    <span>
                                      <xsl:text>&#160;</xsl:text>
                                    </span>
									<span>
										<br/>
								   </span>
                                  </xsl:if>
								  
                                  <xsl:for-each select="cbc:PostalZone">
                                    <xsl:apply-templates/>
                                    <span>
                                      <xsl:text>&#160;</xsl:text>
                                    </span>
                                  </xsl:for-each>
                                  <xsl:for-each select="cbc:CitySubdivisionName">
                                    <xsl:apply-templates/>
                                  </xsl:for-each>
                                  <span>
                                    <xsl:text> </xsl:text>
                                  </span>
                                  <xsl:for-each select="cbc:CityName">
                                    <xsl:apply-templates/>
                                    <span>
                                      <xsl:text>&#160;</xsl:text>
                                    </span>
                                  </xsl:for-each>
                                </xsl:for-each>
                              </td>
                            </xsl:for-each>
                          </xsl:for-each>
                        </xsl:for-each>
                      </tr>
					   <tr align="left">
                        <td align="left">
                          <xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cac:AccountingSupplierParty">
                              <xsl:for-each select="cac:Party">
                                <xsl:for-each select="cac:PartyTaxScheme">
                                  <xsl:for-each select="cac:TaxScheme">
                                    <xsl:for-each select="cbc:Name">
                                    <xsl:apply-templates/>
                                    </xsl:for-each>
                                  </xsl:for-each>
								</xsl:for-each>
                              </xsl:for-each>
                            </xsl:for-each>
                          </xsl:for-each>
                          <xsl:for-each select="//n1:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PartyIdentification">
						  <xsl:if test="cbc:ID/@schemeID = 'VKN'">
                              <xsl:text> Vergi No: </xsl:text>
                            <xsl:value-of select="cbc:ID"/>
						</xsl:if>
                          </xsl:for-each>
                          
                        </td>
                      </tr>
                      
					  <tr>
                        <td> <xsl:for-each select="//n1:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PartyIdentification">
						<xsl:if test="cbc:ID/@schemeID = 'MERSISNO'">
						<xsl:text>Mersis No: </xsl:text>
                             <xsl:value-of select="cbc:ID"/>
						</xsl:if>
                        </xsl:for-each>
                      </td>
                      </tr>
					  
                      <xsl:if test="//n1:Invoice/cac:AccountingSupplierParty/cac:Party/cac:Contact/cbc:Telephone or //n1:Invoice/cac:AccountingSupplierParty/cac:Party/cac:Contact/cbc:Telefax">
                        <tr align="left">
                          <xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cac:AccountingSupplierParty">
                              <xsl:for-each select="cac:Party">
                                <td align="left">
                                  <xsl:for-each select="cac:Contact">
                                    <xsl:if test="cbc:Telefax">
                                      <span>
                                          <xsl:text> Fax: </xsl:text>
                                      </span>
                                      <xsl:for-each select="cbc:Telefax">
                                        <xsl:apply-templates/>
                                      </xsl:for-each>
                                    </xsl:if>
                                    <span>
                                      <xsl:text>&#160;</xsl:text>
                                    </span>
									</xsl:for-each>
                                 <xsl:for-each select="//n1:Invoice/cac:AccountingSupplierParty/cac:Party/cbc:WebsiteURI">
                            <xsl:value-of select="."/>
                          </xsl:for-each>
								</td>
                              </xsl:for-each>
                            </xsl:for-each>
                          </xsl:for-each>
                        </tr>
                      </xsl:if>
                      
                     
                       <tr>
                        <td height="30"></td>
                      </tr>
                      <!-- <tr class="header">
                        <td>
                          <span style="font-weight:bold; ">
                            <xsl:text>SAYIN </xsl:text>
                          </span>
                        </td>
                      </tr> -->
                      <tr class="header">
                        <xsl:for-each select="n1:Invoice">
                          <xsl:for-each select="cac:AccountingCustomerParty">
                            <xsl:for-each select="cac:Party">
                              <td style="width:469px; " align="left">

                                <xsl:if test="cac:PartyName">
                                  <xsl:value-of select="cac:PartyName/cbc:Name"/>
                                  <br/>
                                </xsl:if>
                                <!-- <xsl:for-each select="cac:Person">
                                  <xsl:for-each select="cbc:Title">
                                    <xsl:apply-templates/>
                                    <span>
                                      <xsl:text>&#160;</xsl:text>
                                    </span>
                                  </xsl:for-each>
                                  <xsl:for-each select="cbc:FirstName">
                                    <xsl:apply-templates/>
                                    <span>
                                      <xsl:text>&#160;</xsl:text>
                                    </span>
                                  </xsl:for-each>
                                  <xsl:for-each select="cbc:MiddleName">
                                    <xsl:apply-templates/>
                                    <span>
                                      <xsl:text>&#160; </xsl:text>
                                    </span>
                                  </xsl:for-each>
                                  <xsl:for-each select="cbc:FamilyName">
                                    <xsl:apply-templates/>
                                    <span>
                                      <xsl:text>&#160;</xsl:text>
                                    </span>
                                  </xsl:for-each>
                                  <xsl:for-each select="cbc:NameSuffix">
                                    <xsl:apply-templates/>
                                  </xsl:for-each>
                                </xsl:for-each> -->
                              </td>
                            </xsl:for-each>
                          </xsl:for-each>
                        </xsl:for-each>
                      </tr>
                      <tr>
                        <td height="10"></td>
                      </tr>
                      <tr class="header">
                        <xsl:for-each select="n1:Invoice">
                          <xsl:for-each select="cac:AccountingCustomerParty">
                            <xsl:for-each select="cac:Party">
                              <td style="width:469px; " align="left">
                                <xsl:for-each select="cac:PostalAddress">
                                  <xsl:for-each select="cbc:StreetName">
                                    <xsl:value-of select="."/>
                                    <br/>
                                  </xsl:for-each>
                                  <!-- <xsl:for-each select="cbc:CityName">
                                    <xsl:value-of select="."/>
                                  </xsl:for-each>
                                  <xsl:text> </xsl:text>
                                  <xsl:for-each select="cbc:PostalZone">
                                    <xsl:value-of select="."/>
                                  </xsl:for-each>-->
                                </xsl:for-each> 
                              </td>
                            </xsl:for-each>
                          </xsl:for-each>
                        </xsl:for-each>
                      </tr>
					   <tr>							
											   <xsl:for-each select="n1:Invoice">
													<xsl:for-each select="cbc:Note">
													  <xsl:if test="substring(.,1,14) = 'BolgeBayiSube-'">
														  <td >
															<b>Bölge/Bayi/Şube Kodu : </b>
															<xsl:value-of select="substring(.,15,string-length(.)-1)"/>
														  </td>
													  </xsl:if>
													</xsl:for-each>
												</xsl:for-each>
											</tr>
                      <tr style="height:13px;">
                        <td align="left" valign="top">
                          <span style="font-weight:bold; ">
                            <xsl:text>ETTN: </xsl:text>
                            <xsl:for-each select="n1:Invoice">
                              <xsl:for-each select="cbc:UUID">
                                <xsl:apply-templates/>
                              </xsl:for-each>
                            </xsl:for-each>
                          </span>
                        </td>
                      </tr>
                      <tr>
                        <td height="15">
                          <br/>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
				
                <td align="left" valign="top">
                  <br/>
                        <img width="91px" align="middle" alt="E-Fatura Logo" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/4QBoRXhpZgAASUkqAAgAAAADABIBAwABAAAAAQAAADEBAgAQAAAAMgAAAGmHBAABAAAAQgAAAAAAAABTaG90d2VsbCAwLjIyLjAAAgACoAkAAQAAAKYBAAADoAkAAQAAAKYBAAAAAAAA/+EJ9Gh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8APD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iWE1QIENvcmUgNC40LjAtRXhpdjIiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczpleGlmPSJodHRwOi8vbnMuYWRvYmUuY29tL2V4aWYvMS4wLyIgeG1sbnM6dGlmZj0iaHR0cDovL25zLmFkb2JlLmNvbS90aWZmLzEuMC8iIGV4aWY6UGl4ZWxYRGltZW5zaW9uPSI0MjIiIGV4aWY6UGl4ZWxZRGltZW5zaW9uPSI0MjIiIHRpZmY6SW1hZ2VXaWR0aD0iNDIyIiB0aWZmOkltYWdlSGVpZ2h0PSI0MjIiIHRpZmY6T3JpZW50YXRpb249IjEiLz4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8P3hwYWNrZXQgZW5kPSJ3Ij8+/9sAQwADAgIDAgIDAwMDBAMDBAUIBQUEBAUKBwcGCAwKDAwLCgsLDQ4SEA0OEQ4LCxAWEBETFBUVFQwPFxgWFBgSFBUU/9sAQwEDBAQFBAUJBQUJFA0LDRQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQU/8AAEQgAaQBpAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A/VOiioL6+ttMsp7y8njtbSBGlmnmcIkaKMszMeAABkk0bgT1458QP2nfDvhbxDJ4W8N2F/8AEHxsvB0Hw6gla3PTNzMf3cC567jkelcJqHjHxT+1FJeL4Z1a48B/Bq03i88Vg+Tfa0qZ8wWpb/UwDBzMeTjj+IVTl+JHhz4QeArPT/gf4dtJ7SG/FtqEj6dcuVLQmSGaX7ssiT4wtyPMU/wiQkLXuUcCoO1Vc0/5dkv8T6P+6tel09DzqmIurwdl36v0X6/mdDdaJ8c/HdpJfeJ/GWh/B7QgNz2OhwpfXqIf4ZbubEaN/tRrisTSv2evhJ4v8XXnhrxD4w8W/EHxDaq7Twa9r94UOzZ5gTyzHG2wyR7lTOzeoYDIr1P4l/CeL41aDod415eeGNUjETuypuZ7dmjkmtJoyQGB2Lz1VlBHcHW0D4L+GfDPxC1Xxlp0E9vq2pl3uFWUiFncIHfb3J8tepIB3FQCzZFjeSD5ZcktdIpKz0teW7W/VsHQ5parmXdu/wCGy+4+KPi34e+Cvwt8W+NPDSfBfSr+60p7VNLaTUrkG/zBHcXhY7iV8qKRW4znPOK9b1f4H/Anwn4p1LQNHvPFPgTXtOsZdSdtB1bULYeVFGskjRu7NExVWUkD1I6g4+gfEHwW8EeK9VudS1bw5aX1/cGQy3Eu7e3mQJA/IPG6KKNDjsorD1/9m7wVr2peItQa3vbO/wBes7yyvZ7a8flLpY1nZEYsiMwhQZC9j611vNIzjCLqTTS195u706N7aN7dTH6m4tvli9dNLaa+W/8AkeYeFtE+Lek28M/gP4lP4th+wWuonw98RNM/exxTqWRDf24GZcKQV+bbwTwwJ6rw/wDtT2mka3beHfin4cvfhdr87eXBNqMizaVeN6Q3q/Jnvh9pGQOTVHx/8NvF1l4ss4fBPnpqOq+IV1m8164RFstPtY7B7RINgk3SMn7t1j27WYnJA3Yk8G+L734o+MvEnw08V+FYtY8L6bFNaTXWq+XLPN5TJHHLcIMAGf8AeSJhFwqBlLZ+XOfsq8eecVJWu2rRkvu0evdXfdFR56cuWLad+uqf6r5Ox7+jrKiujBkYZDA5BFOr5QdtX/Za8SX9p4K1R/Hfw/05EuNX8Dtci41bw9A+SJ7XJ3vDgE+U3IAyDySPpTwX400X4h+GLDxD4e1CHVNHvoxLBcwHIYdwR1BByCpwQQQRkV5NfCuilUi+aD2f6NdH+fRtHbSrKb5XpJdP8u/9XNuiiiuI6Ar5m8X3M37U/wARNR8IW9y9t8I/CtwE8R3sTlBrV6mG+wq4/wCWMfBlIPJwPQ13X7TfxD1Twd4FtdE8MMP+E18W3iaFovPMUsv37g+ixR7nz0BC5615L9v8P+GPDKfBnw7pZ8XeE7SyxfX3htxeX9ldQXCec9/aEDzElmOSiszOvmDYV5HuYGhKEPbr4nt5Jby9VtHzvbVI87EVE37N7Lfz7L/Py9To/EfirUNS+KZ8F6PpNv4T1rS7SCTw3GYhPb6rp5a4juIrpIgwhtD9nQKRypeFiMkR17N8P/hZoXw6tIYtMt2MsMBtIZ5yHlitfNeSO2V8AmKMyFUByQuBmsr4HfCWP4R+CLPSJboajfRhla4HmbIkLErDCJHdkiXsm4jJYjGcV6LXHia6b9lRfur8fPv52u92b0aTXvz3/IK+Zf2vv2s4/gnYL4e8NyQ3PjS6QPl1DpYRHo7joXP8Kn6njAPo/wC0d8cLH4D/AA5utcmEc+qz5t9Ns2P+unI4JHXav3mPoMdSK/IfxL4k1Hxdr1/rOr3cl9qV9M09xcSHJdiefoPQdAOBXw2dZo8JH2FF++/wX+Z/QfhlwLHiCs80zGN8NTdkn9uS6f4V17vTue6f8N7/ABl/6GC0/wDBbB/8RR/w3t8Zf+hgtP8AwWwf/EV88gV9ifsa/sejx6bXxx41tSPDiMH0/TZRj7eQf9Y4/wCeQPQfxf7v3vksJWzHGVVSpVZX9Xp5s/oTiDLeDuGsDLH47A0lFaJKEbyfSKVt3+C1eh6x+zL4o+P/AMaGg13X/EMWheDshll/suAT3w9IgU4X/bIx6A84+vJ45HtpEjlMUrIVWXaDtOODjoa8y8Y/tIfDH4XeILfwzrXiW00zUFCJ9kiid1twQNocopWMYxwxGBg9K9NtrmK8t4p4JEmhlUOkkbBldSMggjqCK/RsHGNKLpqpzyW93d39Oh/GHEdevjq8ca8EsNRn/DUYcsXHunZc77v7rI+PtE8Az/AL4gQeJ/HGpy3K27XN3ay2d0ss+vag8TrPcSeZGv2aPyNm6NphCrxxnICiti51K1+AOqad8WPBwkl+DfjDybrX9KjjIXS5JwPL1KGP+FTuUSoB3BweNv0Z478B6L8RNBfS9c0201S3DrNFHexeZGsqnKkgEEjPBGRuUsp4JFeA/DbT00Dxj4p0/wCKfivStd1TXZW0aHR5rZlmisnfy4FMccrxW9vMVbYpRSTJEGkZ2Ar7WniliIudTV2tKP8AMvJdGt79H5Oy/O5UXSkox23T7Pz/ACt1Ppu2uYb22iuLeVJoJUEkcsbBldSMggjqCO9S18//ALM+o3nw/wBd8T/BbWbmS5n8LFLvQbmc5e60aUnyee5hYGInpwor6Arw8RR9hUcL3W6fdPVP7j0aVT2kFLZ9fXqfPujIPib+2HrmoS/vdL+HOjxadaKeVGoXo8yaRT6rCqIfTdXp9z4K8I6t8RYtZ/s5I/F2mQpI1/brJBI8UgkRUkdcLMvyP8jFgCAcDg185fCLwrrvjv4f6x400S2g1W5vviPqHiGXSrq9e0j1G3haS3hhMqq2PLZI5FDAqWiAPByPoL4O2fiCHSdcvfEMipPqOrz3dvpyagb4adGQim387AziRJX2jhPM2DhRXp42PsnaM7ciUbX+/wA9Xd7W13vocdB8+8d3e/5fojvqQkAEk4Apa8a/a6+I7/DL4DeI7+3l8rUL2MabaMDgiSX5SR7qm9h/u187WqxoU5VZbJXPosuwNXM8ZRwVH4qklFerdvwPz3/a++Nknxm+Ld9Lazl/D+kFrHTUB+VlU/PKPd2Gc/3Qo7V4dSk5NPghe5mjiiRpJXYKiKMliTgAe9fjVetPE1ZVZ7tn+leV5bh8mwNLA4ZWhTikvlu35t6vzPe/2Pf2eG+OPj/7RqcLf8Ino5Wa/PIFwx+5AD/tYy2Oig9CRX6ZfEfxEnw3+F/iLWrSCONdG0ue4t4FXCAxxkogA6DIAxXP/s6/Ci2+C3wm0Tw8FRdQ8sXOoSDGZLlwC/PcDhB7IK7Lxn4bs/G3hHWvD95JttdUs5bOVlIyqyIVJHuM5r9Oy7A/UsLyx+OS19ei+R/DHGfFS4mz5VarbwtKXLFd4p+9L1la/pZdD8SNV1S71vU7rUL+eS6vbqVp555TlpHY5ZifUkmv1v8A2P7m9u/2bfAz6gzNOLNkUuefKWV1i/DYFr4y8N/8E8fH9547GnaxPYWXhuKb95q8NwrmaIH/AJZx/eDEdmAA9T3/AEg8PaDZeFtC0/R9NhFvp9hbpbW8Q/gjRQqj8hXkZDgsRQq1KtZNaW1667n6L4s8T5RmmBwuX5ZUjUafPeO0VytJeTd9ultbaGhXgP7Q/g/RdD1jSvHz6fpE+p200apJrt9cR2cdwvMMwtoI3a5nGAqjggKMHgY9+rmPiWryeCNVSK6ns7howIZLW+SylaTcNqJM4IQscLnH8XHNffYWo6VVNddHrbRn8v1oKcGjwb4n63eaTqXwP+M11YTaPe/aYdD1+2miaFktL9Qp8xW+ZVjnCMFbkbuea+ntwr4+8T6HonjP9lH4ry2N5p93qklnLcmWx8XzeIpGazUXCb5pMbJAwJ2IMAFTnnjiP+Hg8v8Aeh/Svell9bG00qEbuDcflo136trfZHnRxMKEm5v4rP57P8kdx+y7oHj/AFX4LfCu78Ha5ZaJYQ2niBdSfU7R7yCSd9UQxAwJPES4CXGHyQo3DHzivqbwXpGoaH4dt7XVptOuNT3yy3E+k2Js7eR3kZyyxF3Kk7ssSxy2498V4/8AsZn+y/h74p8LtxJ4Z8W6vpZX0X7QZlP0KzAj6175XnZnWlPEVIWVuZtaa6tta79TpwkEqUZdbL8kv0Cvh7/gpz4keLRvA2gI/wAk9xc30i+6KiIf/Ij19w1+eX/BTcufHXgsHPl/2dNj6+aM/wBK+LzuTjgKlutvzR+yeF1CNfizCc/2ed/NQlb8dT4ur2n9jvwUnjr9obwnaTxiS0s521GYEZGIVLrn2LhB+NeLV9c/8E1LBJ/jNr90wy1vocgX2LTw8/kP1r87y2mquMpQe11+Gp/ZHGuMngOHMdXpu0lTkl5OXu3+VzqP26vhv8RPiV8X7STw94U1jVNH0/TIrdLi0gZo3kLO7kEf7yj/AIDXxz4o8O674K1mbSNdsrrStThCmS0ugUkQMAy5HbIIP41+4dfjh+054k/4Sz4/+O9QDb0/tSW2RvVYcQr+kYr6DPsFCh/tCk3Kb26H5B4T8TYnNUsmlQhGlh6fxK/M3dWvd21u2dd+xBo8mv8A7SfhbeWeKzFxeOCScbIX2n/vorX6w1+cP/BNLQftnxX8Sasy5Wx0jyQcdGllTH6RtX6PV7fD8OXBcz6t/wCX6H5f4wYlVuJfYx2p04x++8v/AG5BXE/GL4dWnxP8C3ujXUl5HgrcxGwEJmMiZIVRMDGd3K/MMfNnIxkdtRX1MJypyU47o/DpRU4uL2Z8xaH8N20L4XfEjVNb0vxVaan/AMI9c2aXPimbTC7W4tWUpGLBtmwBEyJOcgEdzX46ea3qfzr90f2rfES+Fv2b/iNfswUnRbi1Q/7cy+Sn47pBXxR/w781T/nxH5Gv0nh7NKWGp1a2JdudpL/t1a/mj5bMsHOrKEKWvKvzf/APpZRqvw7/AGjfif4e0Z0trvx54fXX9AeXHlLqVvEYJk54JP7mQ54xXoXwV07xPazXt1qy6vaaXcQqYrHxBqAu7xZlmlBkJGRGrxeSSgOA2QAMZOV+1L4O1W+8L6P468MW5uPF/gW8/tmyhT711AF23VrxziSLPA5JVRWP4cTRLvXLH4ueFf7X8W3fiy13WFjaxqEVSiArPO3ESRkMNpIwcgK7KK/OsfF1I0sYtbe7LyaVk/nG3q79j7/KqkXSxGXSsnL3otq7fXlvdKKvd8z+Fdrs+g6+F/8Agp1oDNaeA9bVfkR7qzkb3YRug/8AHXr7V0DWU1my3GS2e8gIhvI7SbzY4Z9qsyB8DONw5wPoOleJftzeBW8bfs9a1LDH5l1oskeqxgDnahKyflG7n8K8LNKft8FUjHtf7tf0PrOBcb/ZPE+DrVdFz8r/AO304/d71z8oa+tP+CbGpLa/GzWbRiAbrQ5dvuVmhOPyz+VfJhr2P9kLxingj9obwdeTSeXbXN0dPlJOBidTGufYMyn8K/M8uqKli6U33X46H9v8Z4OWP4dx2Hhq3Tk16xXMl87H62a7q0Wg6JqGp3BxBZ28lxIfRUUsf0FfhzqV9Lqmo3N5O26e4laaRvVmJJ/U1+vX7WHiT/hFf2dvHV5u2PLp7WSnvmdhDx/38r8fB1r6TiWpepTpdk39/wDwx+MeCGC5cHjca18UoxX/AG6m3/6Uj9Bv+CY/h/yPCXjbWyv/AB9XsFmrY/55Rs5/9HCvtevm/wD4J/aF/ZH7OWnXO3a2p391dk+uH8ofpFX0hX1OVU/Z4KlHyv8Afr+p+C8e4v67xPjqt9puP/gCUf0CuH+KPjy28IWFvZzWWrXU2q77aBtJVRKH25IR3KqHCCRwM5PlnGTgHtycCvJbnVj4/wBUlstbtbGz0+xiD614X8T2CSoI1LEXUE/3HXjr8y/LzsYGu6tJ25Y7v+v6/I+Vy+lCVT2tZXhDV6/dtrv6K9k5K6PKPiP4hs/i5p3wp+H2leIb3xTbeJ9fXUr+41G3WCddNsSJ5Y5UWNMEuIlBKjOe/Wvq/wApfQV82fss+GrPxj4t8T/Fm208afoV4G0TwpbFSuzTY5WeW4weczzln55wo7Yr6Wr1cTF0YU8LLeC97/E9X92i+R5U5069eriKSajJvlva/L0vZJeeitqJ1r5Y1jT4/wBmPxpqWl6g1xb/AAT8bXLH7TbTPD/wjmoyn51LoQY7eY8hgQEY44Byfqis3xH4c0zxdoV9o2s2UOpaXexNBcWtwu5JEPUEf5xUYetGneFRXhLRr9V5rp92zZnOMrqdN2lHVM5rwNoGuaHf3Ee7R9P8JRIbfTNF023JaGNT8kpmyAS4LFk24Hy4YncW2v7U0fxe+uaEHW+S3X7JfxhSYwZEOYi3TdtIJXqAy56ivnk3niv9keGXS9SfU/FHwbZSlnrdsv2jU/DCngJMuCZrdOqvglAMEEYB6DTLfXbhvDdp8MdaEngO9iiY67Zm2ulkZmle8nuJHzIZmxGEKjG9239MDmxWHlhIxlBc9N7Nflbo+6e3TTU9vBzhmdSbq1FTqpJ66LTd3Sbk+1ruTbbd1Z/CPjn9kf4k+HvGOs6bpnhDV9W022upI7W+t7Yuk8W47HBHquM++ax7b9mr4uWdxFPB4D8QRTROHR1tGBVgcgj8a/UTwt8dvDHie11+88+TTdM0dofN1G/Ait5Y5c+VIjk/dbgjODhlPRhXf2t5BfQRT280c8MqLJHJEwZXQjIYEdQR0NfGLh/CVHzQqP5WP3qfi9n+CgqGKwcLpJNtS1dk9dbXaabXmfLH7Ulr45+Kn7MPhqz07wrqkviLU7i1fVNNSAiS32I5k3L6eYq49QQa+If+GXPiz/0IGuf+Apr9hbi7gtQhmmSISOI03sF3MeijPUn0rF8XePND8CwW8utXjW32gsIY4oJJ5JNq7m2pGrMcLknA4AzXdjcoo4ufta1RqyS6Hy/DHiLmPD+GeX5dhISUpykl7zevRWetkkvRHOfs9+ErjwL8E/BmiXlu1re22mxG4gcYaOVhvdSPUMxBr0JmCgkkADnmuR1T4r+GtI1Pw7Y3F8wk1/Z9glWFzDJvH7vL42jd0AJycivH9evL342DxFoeuLL4E13w5L9qt9RWZVhNoXKyxu5Yh0IjyzYAGY2xkc+sqkaMI0qXvNaJei/yPz14TEZliamNxn7uM25Sk1tzSabS3aUtHa9vz634h+L4vHWv6n8NLRr/AEbV2jjnhvLi3Js73ad7QOUO9Y2ClSw2kgNgnGG828VPqPxg1OH4HeGNVvbjQdNC/wDCb+IjcGZreAncNLinwC8jfcLH5lRfmyxYU6Txtrvx11N9A+FFwfsUUX9na38W7q0jSR4g2WgsSqqJZMk/OoCKeRyQa9++GPwx8P8Awi8IWnhzw5afZrGDLvJId01xKfvyyv1d2PJJ+gwAAPco0f7Pbr1/4r+Ffyro5ea6L5vpfwcXjI4ulHB4ZWpLWT/mlazadk7O3Xbpvpv6PpFnoGk2emadbR2dhZwpb29vCu1Io1AVVUdgAAKuUUVwttu7OZK2iCiiikMa6LIhVgGVhggjIIrwnxD+y8NA1y68SfCXxHP8NdcuH825sLeIT6PfN6zWhwqk9N8e0jJOCa94oroo4iph23Te+63T9U9H8zKdOFT4l/n958uT+MPF/ga3Nl8RvgvLeWIv4tSm1v4cAXltc3ERUpLLa/LMMFEJ3bvuj0qj4c+NvwXuvi/qfjFviTb6Tqd3bmA6br1tPYS2zeXHHsLSlF8seXu2bfvOx3dMfWNfOn7YX/Iqx/7hrso0sHjasYVKXK77xdlf0af4NI1+v47BU5unWbTTTTV9Ha6v52XnoZfgnxZ4P0HwVbabe/G3wjqM9vrttqa3T+IonP2eNoy8RZpOS2x+w+98xY7naT46fHD4HeM9N0uzv/iXoTzWF8LuNbGIat5v7t42jMUYcMGWQ8EEZA4Nfmrqf/IeH+9/Wvvr9h/oP9w/yr3Mbw5g8BhueTlJW2ul+NmctHiXH4nFqtFqM027pdXo9PToa2neNJfFmk+HNO+H3we8R+M20OD7PY+IPGoGl2IXKMJD5mGmAaNGCiMbSi7cYGOtg/Zp8QfFHUU1X40+KU8QxAqy+E9ARrPSE2klRKc+bc4JJG8gDJ4wa+hx0FLXzscTGhphaah57y+97fJI6KrrYp3xVRz30e2ru9PN6+pU0vSrLQ9Ot7DTrSCwsbdBHDbW0YjjjUdFVRwAPQVbooribbd2VtogooopAf/Z" />
                        <br/><br/>
                        <b style='padding-left : 12px'>
                       <xsl:for-each select="n1:Invoice">
							<xsl:for-each select="cbc:Note">
								<xsl:if test="substring(.,1,13) = 'GONDERIMTURU:'"> 
									
										<b>
										   <xsl:value-of select="substring(.,14,40)"/>
										</b>
									
								</xsl:if>
							</xsl:for-each>
							</xsl:for-each>
                        </b>
                        
                        <br/>                  
                  <!--Orta İmza-->
				  <img align="left" width="90"  src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCACaAL0DASIAAhEBAxEB/8QAHQABAAICAwEBAAAAAAAAAAAAAAcIBQYCBAkDAf/EAEEQAAEDBAECAwUGAgcHBQAAAAECAwQABQYHEQgSEyExFCJBUXEJFSMyYYEWUhcYJEJDYrElY2SCkZKhdIOio7T/xAAUAQEAAAAAAAAAAAAAAAAAAAAA/8QAFBEBAAAAAAAAAAAAAAAAAAAAAP/aAAwDAQACEQMRAD8A9U6UpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUqDOrS/ZCMTxTVuKXqXZ7ltLK4WJruUNzw5MKAtt2ROdaX6oc9mjPISoeYU4kjjjkBms36sOnHXV7cxjK9wY8ze2VFDtrivmbNQr17VR44W4FcfAp5rW09dHTaojuyTKm2z5B13Ar+hv91KhAD6mpS1zqrXOpMfj4vrbDLVj1tjoCA3CjJQpz5qcX+ZxZPmVrJUT5kk1tdBClp61OlC8S/YGd/YbDkhQQWLncE29xKj8CmT2EH9CKluy5BYckhi4Y7e4F0iq44fhSUPtn/mQSK43nHMeyOOYmQ2K3XRhQ4LU2Kh5BHy4WCKiG+dFXTLdpirta9XQcUupJKbliMh6wykq9ee+Ctrnz8+FAjn4UE4Uqvh0j1F69Hjae6lpt9itfkseyLei7NLHwSJ8fwZaPl3LLx+JBPrwHVLketlph9T+n7tgkcKCDlNodN7xxXnwFOSGkB6ID/wAQyhI8+V8eZDZt7blyPDLjYdXansMTINmZp4xtEOYtSYNuiNcePcpyke8mM13JHan3nFqShPmSRq7fSjl+SNpuW0uqvcF0vTg7nv4cvYxy3NK+TMaGkEJHPA8Vxw8Acknmvh0tybbtfP8AanUszMj3KNfr2cRxeYy4l1r7itX4fcyseXY9MVLd5BIUPDPPkALH0FdH9IdS+uECbpjqYnZQ0wkkWDZ0Jq4syDz6C4xUNSmeByAVB31HIPFd/Cuq+3tZVD1h1AYVO1Nm89fhW9m6SEP2e9L54H3fckANPKPKfwl+G6CsDsJqfK13PdfYRtLFZ2E7Dxe35BY7gjskQpzIcbV8lD4oWk+aVpIUk8EEEc0GxUqqL6dydGH9qjuX3aujmCS8y4TKyXDmP5kKPvXGC358pP4zSOOCpLZ5sjhGc4hsnFbdm+B5FCvliuzIfhzobne26nngj5pUCClSSApKgQQCCKDO0pSgUpSgUpSgUpSgUpSgVAfU7zBzvp+v7iSWImzWojp+CTKtVwZQT/7ikJ+qhU+VA3W5Cko6eLzmlvaU5O1/cLXm0dKRyT91zWZTgHyJZadHP60E80rrwJsW5QY9xgupdjS2kPsrT6LQoBSSPqCK7FApSlAr4TVsNQ33JaEqYQ0pTqVDkFAB5BHxHFfeuhfmnHrHcWWgStyI8lIT68lBA4oKP9L+j9g4z054LvPp6yFVsy+/Wz79v2KXGStVhyUvuLdKFIJPsUjsUENvtcAdqQtKk+ltNMbesG6sKbyyyw5dtlR5L1tvFnnJ7JlouLJ7X4j6fgtCvj6KSUqHkoVpnRG82/0i6iW0UlIxG3IPb80tAH/yDWMwVhFh619o2expCLdfsKxzIbq2j8ibp7TOihzj0C1x47PJ9SG08+lBYGlKUCqr7E1Jm3TZlVz370xWN242W4PGbnetY3kzdU8fiXG1o9GZ6UjlTaQEvhIHHeB32opQapq7Z+E7kwS07I15emrpYr0x40d9HkpJB4W24n1Q4hQKVJPmFAitrqpeawv6nW9Y+17Cn2bUW2bs1b82gJ92PYb+8QiNeED8rbT6uGpB90dxQslR7Ui2lApSlApSlApSlApSlArGZNj9tyzG7rit5YD1vvMJ+3y21DkLZebKFpI/VKiKydKCD+jHILldunrHcdyB9Tt9wVcrCruVklXtVrfXEKlc8nlaGm3PPz4cFThVfNZAa66sto65X+Hb9gW+DsO0o8gkyUJTb7kkfM9zMJw/H8Y/tYOgUr89K+MybDt0ZydcJbMaOynuceecCEIHzKj5AfWg+9KxGW5TZ8KxO85rfpIYtVht0i6THuRwiOy2pxav2Skmqjat69M9zZ+9YO/o1+97ISIM+0WDG5SnYjFvmxUSGTc57qQ1DW13eG73e8VDhDaqDYdA7Zw7QHTlk1tzqephjWOa3/EY8NlAclS1Ce47AiRmk+bjrkeRGDaB5kEE8AEiRumrAMwtMLJNubVhJh57s2c1dLnAS4HBZ4TTfhwbYFDyV4DXJWR5F114jyNVB6BZ0bc/V5vfKtzWO1v51id8S9bI8GS85arfISXIMuRFYc8vFKYkVPtCk+IUn+7yRXpPQKUpQKUpQaZuXWdo3JqnK9W31DZiZNapFvK1p7vBcWg+E8B/M24EOJ+SkA1pHRfsa7bX6W9dZrf1uuXZ60fd9wdeV3OPS4bq4jzij/MtyOtR/VRqVcqyW0YZjF4zDIJIjWuxQJFynPH0bjstqccV+yUk/tUHfZ92G7490ea2ZvjPhS7lClXwp+TdwmvzW/8A65CDQSbtzc2CaTx5m/5tNlFc+QmFa7Zb4q5dwuktX5Y8WM2Ct1w/IDgDzUQOTUaRss6y9lp9rxfXeF6os7vJaey+S7eLutHwUYUNbbDKj69qpCyPIEck8Yvct9suo+qLEd2bUa8PBHsWfxaFfXm+6JjN3dlhxTshX+AiU14bXjnhKSwEqIDlWRhTYdyiM3C3S2ZUWQgOMvsOBbbiCOQpKh5EEfEUEFHSHUpdOHL/ANZt7jK495qwYXaIbPPzHtCJDn/VZodAb6j8KtvWvnyFjzHteN2CQn9x7Gn/AFqfKUEBrwTrRsKi5ZOoTX2TIB91jIcEdjKI+SnYcxI5PzDY+lcFbO6usQ88z6a7BlkVH55WDZegvcfMRbi1HJ+gdJ+Hn61P9KCBYXWrpSJLatWzv4l1ZcniEJj51ZH7U0VfJMxQVEX9UvEHzI5HNTdaLzaL/b2btYbrDuUGQnuZkxH0vNOD5pWkkEfQ19Lhb4F1hu2+6Qo8yK+nsdYkNpcbcT8lJUCCPrUH3no01MzcXsi1LJvupL+6vxVTsInG3sOr/wB/BIVDfHzC2Sf1HAICeKVXdWV9WOl+TnGKW/dGLs/mu+KsJtuQstj1U7bXF+BJP/p3EKPwaqTNVbw1humBIma+ylmdIgL8K4215tca4W534tyYroS6yrnke8kA8eRNBHXU+Bg+Yaj300fDaxTKm8fvTnoPui8hMNZUf5USjBc/TsP0PczrqPcwLqv19oC72+E3Z8/sM+VEuCu/x03NhfKGeeewNqbQ4PMclakAEeh3ffmARtqaUzfXslwNfftjlxWXioDwXy2Sy7yfQocCFA/5a859x7uwbqOuWi73iNxnZJs+RgE5TcLFWEz7jZMnbVbpkRa09yUIAfiyQ53qTw0HCrgGgmHqf2fm+x+mDq0tUi6eEjBcpjWi0Lio8FxmIz92vuJK08FRKlPEk+fCuPQAVvXVLv7WDvRpdrNkeWx3Mn2Brh2RarPFSqVOlOP27xEu+C0FLS0CQpTqgEJAJKhxVSrWnqA23oPqzvd8v9m1/Eh3u7XXK8URATNur05mCyVxy+tXYzEJYHatCFLUUr4XwOas9026w1hYPs9zluJ4fbYN5yjWUlV5uiWvElzHRAcQsOPL5WUBST2t89iB5JSAOKCG8gkdT20vs74uTZFmtrw/DYmEW+3RoFr/ALddclBDUXxZ0lxITHac57iy0kuHkhTnmRVlOkrWuB6z3LvrHdaYtCsOPWe4Y5Yo8eKjhK1sWlD7i1K81LWVTD3KUSonzJqJrRKJ+yCtdxVytNvxONLXwOfcj3BK1D/o2amTpvvzEbWe591F0eBfs5yq9NPd/KTFgH2FpQPy7LeFfUmgq30IyEWncGNbQYWBG2fl2xLBIcBPC3eYc+Nz8D7sKVwf14H5q9O68t+k2MbD0RaK2MkEKx3dEWU6vnj+zzZjtre5Py7ZfP1SK9SKBSlKBSlVz3X1XLtGVL0X072BrYm3ZKSlUBlZNsx5B8varrIT7rSEevhA+Io9qfdK0Ehgure8zt0ZjjfRRhct1L2XhF6z6bGWQq1Ysw6kuIKh+V2U4Eso9fIq7h2rBq0MCBCtUGNbLbFaixIbKGI7DSQlDTaAEpQkDyAAAAHyFRJ04dPiNKWm8X3Kckdy3Y2aSU3HL8nkICVz5ITwhppP+HGZSShpscBI54A57RMdB1rhb4F2gv2y6wY82HKbUy/HkNJcadQocKSpKgQpJHkQRxUEO9G+GY1KduOi8/zbUbzzinlw8YuQXaVuH1JtstD0VPn5/hoR58/M82ApQQL/AAV1oY8oJsu9tc5UwkjyyLCX4j5T+rkKYlBUfmGgP8tfi7110QHFBevdJXdoH3VR8oukRah+qVwlhP8A3Gp7pQQGNl9ZENYTN6WcOno595dv2UEn9kvQE8/uoUXvTqMhOdtz6KsqcQP8S25hY5CT9AuQ2r/4ip8pQQEep7PIZAu/Rzupr4ExGLPLA/7J/J/YVyd6u7TC4+9+n7e8Dn1JwCXJCfqY3ij/AM1PdKCA/wCuzpmOObvZNm2rj19s1xfU8fXtiGot23tzoW2nLjZlc9pTsEzS2p/2bmES1XGyXWHx6IW49HQHmfmy8Ftkc+78audSg8uNY7L6YL9sJ/XnV5v5G1HHCuZZMomZjI/hiayFKIZk21K0MQpCRx7rqVtr4BSvngVk9a7K0fhcbW8DFdgYO2xr3f8Ae7fFbhXWKO6xXRE5DMlASr3mAJ0dBcHuJDah3e4QPQnYOrNf7SxS64VnWLW+62m8xlxZTTrKe4pV/eSsDuQsHhSVAgpUAQQRXm6jRn8JYD1R6oy+22bKMy18/Zs3sE6Rbo6pVwtMVll1onhHmXGreWnwB7y1ucg94JDtZbrLBM3wLq52vjWzxYMst+SZUlqXa7i0tu+2b7njOuW+QySUPsrKnAhYHc24SpCuQQf3p+6m71rz7P696/znUmaqbx6xZLYImTW2Ii42xclKpPYzJLCi7D7VOIb73UBshPd3gEVqOIWnpx2D069XOf2jVeGTJEHLLnGw+W3Zo3jRGprbUa3JiKSnlsF7hTaUcDuWSByTW6Zj0P4vDz/INCWbp6DxzS82K7WfNI8JaYdksqWmE3llUhJCUOJXFcDbXqozkkccEgMjqHfvT3cvsw5epMh3PidsyZGBXuAu1zLi01MTJ4kllCWVELcUT4falIJVyAOSa3u35XiOO/ZbWrD8Iyi0zr1kWDQbAzFjzEKe+9LyUMOIUlJK0qD8xwq5HI4UT6VBcDo+6eMn0fr6xowRZz/K9p3LDWZybxO5j2+JfJrktaWPG8EBFviLRz2c8qSee491SfbPsaNRo2lOut4yiQ9gHiyZMGzxvGbuILqUhEd2Up1SFMskKUhSW0uK7gFqVx5h2rZjkHEPszM8g2xkmPh+U3u4Q0p8lAWzJ1vN/wD5h+1ehKFpcQlxCuUqAII+IryNndGGubP0bbQ2TaM82NbrhjN3yW0xrRDvwTbpXst1eixmX45bPidwS2FAEFRJ9Oat9D6LcowK1O3GF1071tsOGwX5Dl3yGPNjx0ITypR8drtQgAEn0AA5oLbVHO5uoPUmgrOzddm5dHt701Xh262MpVIuNydJCUtxorYLjpKilPIHaCodxSPOqpYXZOsraF8R/Qd1ZZQ5r8JWlzM8qw+0lqb6gG2R/AS9LRz5+OtTLR8iguip46dOkjF9JlzMMyvP9Iu0Z7ry7lnd3jLM99Clq8NtoOuvezIS0UtlLagFBPn8AA0d6L1YdWH4cs3Xp81bJ/M0lSDmd4YPwURyi1pIPmPeeSUkHuSqp50/pDV2hsUTh2q8SiWSAVeLIWjlciY98XZDyuVvLP8AMongeQ4AAG9UoFKUoFKUoFKUoFKUoFKUoFKUoFUE3fJm67+0nxvYRKFWC8YrZMZyKO413NvwrpOnQ0qV8OES24HcD8F+flV+6oh17wX059kM+MQl5GlbxeIyiPyy7PerdPYX/wAp7j+5oJPtX2f2osWz203rBZ8/HMKhqt8244VEAXCu1wgSH34Ul95wqePhuSVqLfJSsoa54CO02grqWqe3dbXDujI/DmR25CPP4LSFD/Wu3QV21Z0pzcC3pe9oXjNUXWwMTrzccRsaYpR90ybw8h+4urcKj3qK0FDfAHa2tY+PlYmlKCiKsYzG+Z7kvSE3h95atdz22vYN0vDkNxNu/hhT8e69iHyOxbrs/mMG0kkBKyRwmrK9TeFX/OtX/d9hsCci+7rxbLxNx1T6Wk32HFkoeeg9yiEcrSg9qVkIWpKUrISpRqWaUGm602rgW0LU7Iwu6JL1uUmPcLVIaMafanu0HwJUVYDjCwOPdUkcjgjkEE7lWj53pnANhXCNf7xa3oWQwUeHCv8AaZLkC6Rk9wV2JkslKy2SkEtKKm1ccKSoeVYNmHvvX4SiPcrfs+ztADtm+Far6hAB8w42kQ5Sye0AFEUepKzQSpStUw3ZGPZrIlWqM3Otl8tyEOT7LdI5jToqVEhKyg8hbZKVAOtqW0opUErPB42ugUpSgUpSgUpSgUpSgUpSgUpSgVSX7QtabXeoVzcADVx1Fsy1KUTx75gw3Wx+/hqP7VdqqYfaTa6zfYcDUtnwe1zJbt4yp7FbiuOwpwR4FziOMPuuFI9xCUJKio8AFI5+RC2OAsORsFxyM6T3tWmGhXPzDKQaz1cG222W0tNICUISEpSPQAegrnQKUpQKUpQKUrD5Ldp9ujMRbNHbfulxe9mhod58JCu0qU65x59iEpUogEFRASCCoUGq5mxHuu0cDj2ptJvFnfl3Kc8jkKYtS4rzKm1kD8rslUbtbUQFGOpY5LB4kKsNjWMRMcYfWH3ZlwnuB+4T3+PGlu8AdyuPJKQAEpQOEpSAAKzNApSlApSlApSlApSlApSlApSlApSlApSlApSlApSlArG+zeNkYlrTx7HCLbR+fjOAr5+ngN8fU1kq4hCQ4XAn3lAJJ+YHPH+poOVKUoFKUoFKUoFKUoFKUoFKUoFKUoFKUoFKUoFKUoFKUoFKUoFKUoFKUoP/2Q==" alt="UO_imza_16x13mm.jpg"/>
                </td>
				
                <td width="40%">
                  <br/>
                  <table align="center" border="0" width="100%">
                    <tbody>
                      <tr>
                        <td>
                          <img width="120" height="1" src="data:image/jpg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/4QBYRXhpZgAATU0AKgAAAAgABVEAAAQAAAABAAAAAFEBAAMAAAABAAEAAFECAAEAAAAGAAAASlEDAAEAAAABAAAAAFEEAAEAAAABAAAAAAAAAAAAAAAAAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAABAAEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9/KKKKAP/2Q==" align="middle"/>
                        </td>
                        <td>
                          <img width="20" height="1" src="data:image/jpg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/4QBYRXhpZgAATU0AKgAAAAgABVEAAAQAAAABAAAAAFEBAAMAAAABAAEAAFECAAEAAAAGAAAASlEDAAEAAAABAAAAAFEEAAEAAAABAAAAAAAAAAAAAAAAAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAABAAEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9/KKKKAP/2Q==" align="middle"/>
                        </td>
                        <td>
                          <img width="180" height="1" src="data:image/jpg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/4QBYRXhpZgAATU0AKgAAAAgABVEAAAQAAAABAAAAAFEBAAMAAAABAAEAAFECAAEAAAAGAAAASlEDAAEAAAABAAAAAFEEAAEAAAABAAAAAAAAAAAAAAAAAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAABAAEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9/KKKKAP/2Q==" align="middle"/>
                        </td>
                      </tr>
                      <tr align="left">
                        <td align="left" colspan="3" width="100%">
                          <b>
                            <xsl:text>İLETİŞİM HİZMET FATURASI KURUMSAL TEK FATURA</xsl:text>
                          </b>
                          <br/>
                        </td>
                      </tr>
                      <tr align="left">
                        <td align="left" width="150">
                          <h1>
                            <xsl:text>HESAP NO</xsl:text>

                          </h1>
                        </td>
                        <td>
                          <h1>
                            <xsl:text>:</xsl:text>
                          </h1>
                        </td>
                        <td>
                          <h1>
                            <xsl:for-each select="//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyIdentification">
								<xsl:if test="cbc:ID/@schemeID = 'HIZMETNO'">
									<xsl:value-of select="."/>                                
								</xsl:if>                              
                            </xsl:for-each>
                          </h1>
                        </td>
                      </tr>
                      
                      <tr align="left">
                        <td align="left" width="150">
                          <xsl:text>Fatura Dönemi</xsl:text>
                        </td>
                        <td>
                          <xsl:text>:</xsl:text>
                        </td>
                        <td>
                          <xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cac:InvoicePeriod">
                              <xsl:for-each select="cbc:StartDate">
                                <xsl:value-of select="substring(.,9,2)"/>/<xsl:value-of select="substring(.,6,2)"/>/<xsl:value-of select="substring(.,1,4)"/>
                              </xsl:for-each>
                              <xsl:text> - </xsl:text>
                              <xsl:for-each select="cbc:EndDate">
                                <xsl:value-of select="substring(.,9,2)"/>/<xsl:value-of select="substring(.,6,2)"/>/<xsl:value-of select="substring(.,1,4)"/>
                              </xsl:for-each>
                            </xsl:for-each>
                          </xsl:for-each>
                        </td>
                      </tr>
                      <tr align="left">
                        <td align="left" width="150">
                          <xsl:text>Vergi Dairesi/No/TCKN</xsl:text>
                        </td>
                        <td>
                          <xsl:text>:</xsl:text>
                        </td>
                        <td>
                          <xsl:for-each select="//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyTaxScheme/cac:TaxScheme">
                            <xsl:value-of select="cbc:Name"/>
                          </xsl:for-each>
                          <xsl:text> / </xsl:text>
                          <xsl:for-each select="//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyIdentification">
                            <xsl:if test="cbc:ID/@schemeID = 'VKN'">
                              <xsl:value-of select="cbc:ID"/>
                            </xsl:if>
                          </xsl:for-each>
						  <xsl:text> / </xsl:text>
                          <xsl:for-each select="//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyIdentification">
                            <xsl:if test="cbc:ID/@schemeID = 'TCKN'">
                              <xsl:value-of select="cbc:ID"/>
                            </xsl:if>
                          </xsl:for-each>
                        </td>
                      </tr>
                      <xsl:for-each select="n1:Invoice">
                        <xsl:for-each select="cbc:Note">
                          <xsl:if test="substring(.,1,14) = 'BirSonrakiFKT-'">
                            <tr>
                              <td >
                                <xsl:text>Bir Sonraki Fatura Kesim Tarihi</xsl:text>
                              </td>
                              <td>:</td>
                              <td>
                                <xsl:value-of select="substring(.,15,11)"/>
                              </td>
                            </tr>
                          </xsl:if>
                        </xsl:for-each>
                        <xsl:for-each select="cac:PaymentTerms">
                          <xsl:for-each select="cbc:Note">
                            <xsl:if test="substring(.,1,28) = 'Bir Sonraki Son Ödeme Tarihi'">
                              <tr>
                                <td >
                                  <xsl:value-of select="substring(.,1,28)"/>
                                </td>
                                <td>:</td>
                                <td>
                                  <xsl:value-of select="substring(.,32,10)"/>
                                </td>
                              </tr>
                            </xsl:if>
                          </xsl:for-each>
                        </xsl:for-each>

                      </xsl:for-each>
                     
                      <tr>
                        <td height="13" ></td>
                      </tr>
                      <tr>
                        <td align="left" colspan="3">
                          <table border="1" height="13" id="despatchTable" width="300">
                            <tbody>
                              <tr>
                                <td style="width:155px; " align="left">
                                  <span style="font-weight:bold; ">
                                    <xsl:text>Özelleştirme No:</xsl:text>
                                  </span>
                                </td>
                                <td align="left">
                                  <xsl:for-each select="n1:Invoice">
                                    <xsl:for-each select="cbc:CustomizationID">
                                      <xsl:apply-templates/>
                                    </xsl:for-each>
                                  </xsl:for-each>
                                </td>
                              </tr>
                              <tr style="height:13px; ">
                                <td align="left">
                                  <span style="font-weight:bold; ">
                                    <xsl:text>Senaryo:</xsl:text>
                                  </span>
                                </td>
                                <td align="left">
                                  <xsl:for-each select="n1:Invoice">
                                    <xsl:for-each select="cbc:ProfileID">
                                      <xsl:apply-templates/>
                                    </xsl:for-each>
                                  </xsl:for-each>
                                </td>
                              </tr>
                              <tr style="height:13px; ">
                                <td align="left">
                                  <span style="font-weight:bold; ">
                                    <xsl:text>Fatura Tipi:</xsl:text>
                                  </span>
                                </td>
                                <td align="left">
                                  <xsl:for-each select="n1:Invoice">
                                    <xsl:for-each select="cbc:InvoiceTypeCode">
                                      <xsl:apply-templates/>
                                    </xsl:for-each>
                                  </xsl:for-each>
                                </td>
                              </tr>
                              <tr style="height:13px; ">
                                <td align="left">
                                  <span style="font-weight:bold; ">
                                    <xsl:text>Fatura ID:</xsl:text>
                                  </span>
                                </td>
                                <td align="left">
                                  <xsl:for-each select="n1:Invoice">
                                    <xsl:for-each select="cbc:ID">
                                      <xsl:apply-templates/>
                                    </xsl:for-each>
                                  </xsl:for-each>
                                </td>
                              </tr>
                              <tr style="height:13px; ">
                                <td align="left">
                                  <span style="font-weight:bold; ">
                                    <xsl:text>Fatura Tarihi:</xsl:text>
                                  </span>
                                </td>
                                <td align="left">
                                  <xsl:for-each select="n1:Invoice">
                                    <xsl:for-each select="cbc:IssueDate">
                                      <xsl:value-of select="substring(.,9,2)"/>-<xsl:value-of select="substring(.,6,2)"/>-<xsl:value-of select="substring(.,1,4)"/> 23:59
                                    </xsl:for-each>
                                  </xsl:for-each>
                                </td>
                              </tr>
                              <xsl:for-each select="n1:Invoice/cac:DespatchDocumentReference">
                                <tr style="height:13px; ">
                                  <td align="left">
                                    <span style="font-weight:bold; ">
                                      <xsl:text>İrsaliye No:</xsl:text>
                                    </span>
                                    <span>
                                      <xsl:text>&#160;</xsl:text>
                                    </span>
                                  </td>
                                  <td align="left">
                                    <xsl:value-of select="cbc:ID"/>
                                  </td>
                                </tr>
                                <tr style="height:13px; ">
                                  <td align="left">
                                    <span style="font-weight:bold; ">
                                      <xsl:text>İrsaliye Tarihi:</xsl:text>
                                    </span>
                                  </td>
                                  <td align="left">
                                    <xsl:for-each select="cbc:IssueDate">
                                      <xsl:value-of select="substring(.,9,2)"/>-<xsl:value-of select="substring(.,6,2)"/>-<xsl:value-of select="substring(.,1,4)"/>
                                    </xsl:for-each>
                                  </td>
                                </tr>
                              </xsl:for-each>
                              <xsl:if test="//n1:Invoice/cac:OrderReference">
                                <tr style="height:13px">
                                  <td align="left">
                                    <span style="font-weight:bold; ">
                                      <xsl:text>Sipariş No:</xsl:text>
                                    </span>
                                  </td>
                                  <td align="left">
                                    <xsl:for-each select="n1:Invoice/cac:OrderReference">
                                      <xsl:for-each select="cbc:ID">
                                        <xsl:apply-templates/>
                                      </xsl:for-each>
                                    </xsl:for-each>
                                  </td>
                                </tr>
                              </xsl:if>
                              <xsl:if test="//n1:Invoice/cac:OrderReference/cbc:IssueDate">
                                <tr style="height:13px">
                                  <td align="left">
                                    <span style="font-weight:bold; ">
                                      <xsl:text>Sipariş Tarihi:</xsl:text>
                                    </span>
                                  </td>
                                  <td align="left">
                                    <xsl:for-each select="n1:Invoice/cac:OrderReference">
                                      <xsl:for-each select="cbc:IssueDate">
                                        <xsl:value-of select="substring(.,9,2)"/>-<xsl:value-of select="substring(.,6,2)"/>-<xsl:value-of select="substring(.,1,4)"/>
                                      </xsl:for-each>
                                    </xsl:for-each>
                                  </td>
                                </tr>
                              </xsl:if>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td height="15">
                  <br/>
                </td>
              </tr>
              <tr>
                <td colspan="3">
                  <table id="bantTable">
                    <tr style="height:20px;">
                      <td width="440"  align="center" valign="top"  style="height: 19px; border-top: solid black; border-bottom:  solid black;">
                        <h1>
                          <span style="font-weight:bold;  color: #0000cc;">
                            <xsl:text>SON ÖDEME TARİHİ : </xsl:text>
                            <xsl:for-each select="n1:Invoice">
                              <xsl:for-each select="cac:PaymentMeans">
                                <xsl:for-each select="cbc:PaymentDueDate">
                                  <xsl:value-of select="substring(.,9,2)"/>/<xsl:value-of select="substring(.,6,2)"/>/<xsl:value-of select="substring(.,1,4)"/>
                                </xsl:for-each>
                              </xsl:for-each>
                            </xsl:for-each>
                          </span>
                        </h1>
                      </td>
                      <td align="left" width="70px">

                      </td>
                      <td width="440" align="center" valign="top" style="height: 19px; border-top: solid black; border-bottom:  solid black;">
                        <h1>
                          <span style="font-weight:bold;  color: #0000cc;">
                            <xsl:text>ÖDENECEK TUTAR : </xsl:text>
                            <xsl:for-each select="n1:Invoice">
                              <xsl:for-each select="cac:LegalMonetaryTotal">
                                <xsl:for-each select="cbc:PayableAmount">
                                  <xsl:value-of select="format-number(., '###.##0,00', 'european')"/>
                                  <xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:PayableAmount/@currencyID">
                                    <xsl:text> </xsl:text>
                                    <xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:PayableAmount/@currencyID = 'TRY'">
                                      <xsl:text>TL</xsl:text>
									  </xsl:if>
                                    <xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:PayableAmount/@currencyID != 'TRY'">
                                      <xsl:value-of select="//n1:Invoice/cac:LegalMonetaryTotal/cbc:PayableAmount/@currencyID"/>
                                    </xsl:if>
                                  </xsl:if>
                                </xsl:for-each>
                              </xsl:for-each>
                            </xsl:for-each>
                          </span>
                        </h1>
                      </td>

                    </tr>
                  </table>
                  <br/>
                </td>
              </tr>
              <tr>
                <td colspan="3">
                  <table border="0" id="AltKalemler" width="950">
                    <tr>
                      <td>
                        <img width="300" height="1" src="data:image/jpg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/4QBYRXhpZgAATU0AKgAAAAgABVEAAAQAAAABAAAAAFEBAAMAAAABAAEAAFECAAEAAAAGAAAASlEDAAEAAAABAAAAAFEEAAEAAAABAAAAAAAAAAAAAAAAAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAABAAEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9/KKKKAP/2Q==" align="middle"/>
                      </td>
                      <td>
                        <img width="100" height="1" src="data:image/jpg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/4QBYRXhpZgAATU0AKgAAAAgABVEAAAQAAAABAAAAAFEBAAMAAAABAAEAAFECAAEAAAAGAAAASlEDAAEAAAABAAAAAFEEAAEAAAABAAAAAAAAAAAAAAAAAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAABAAEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9/KKKKAP/2Q==" align="middle"/>
                      </td>
                      <td>
                        <img width="120" height="1" src="data:image/jpg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/4QBYRXhpZgAATU0AKgAAAAgABVEAAAQAAAABAAAAAFEBAAMAAAABAAEAAFECAAEAAAAGAAAASlEDAAEAAAABAAAAAFEEAAEAAAABAAAAAAAAAAAAAAAAAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAABAAEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9/KKKKAP/2Q==" align="middle"/>
                      </td>
                      <td>
                        <img width="400" height="1" src="data:image/jpg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/4QBYRXhpZgAATU0AKgAAAAgABVEAAAQAAAABAAAAAFEBAAMAAAABAAEAAFECAAEAAAAGAAAASlEDAAEAAAABAAAAAFEEAAEAAAABAAAAAAAAAAAAAAAAAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAABAAEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9/KKKKAP/2Q==" align="middle"/>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2" valign="top">
                        <table border="0" id="AltKalemler" width="400" class="text">
                          <tr>
                            <td colspan="2">

                            </td>
                          </tr>
                          <xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cbc:Note">
                              <xsl:if test="substring(.,1,13) = 'HİZMET TÜRÜ :'">
                                <tr>
                                  <td colspan="2" bgcolor="#BBBBBB">
                                    <b>
                                      <xsl:value-of select="."/>
                                    </b>
                                  </td>
                                </tr>
                              </xsl:if>
                            </xsl:for-each>
                          </xsl:for-each>
                          <tr>
                            <td height="15">

                            </td>
                          </tr>
                          <xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cac:InvoiceLine">
                              <xsl:if test="substring(cac:Item/cbc:Name,1,4) != 'UMTH'">
                                <xsl:if test="//n1:Invoice/cac:InvoiceLine/cac:Item/cbc:Name != 'Taksit'">
                                  <tr>
                                    <td>
                                      <xsl:for-each select="cac:Item">
                                        <xsl:value-of select="cbc:Name"/>
                                      </xsl:for-each>
                                    </td>
                                    <td align="right">
                                      <xsl:for-each select="cac:Price">
                                       <xsl:value-of select="format-number(cbc:PriceAmount, '###.##0,00', 'european')"/>
                                       </xsl:for-each>
                                    </td>
                                  </tr>
                                </xsl:if>
                              </xsl:if>
                            </xsl:for-each>
                          </xsl:for-each>
                          <xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:AllowanceTotalAmount != '0.00'">
                            <tr>
                              <td>

                              <b> <xsl:text>İNDİRİMLER (-) </xsl:text> </b>

                              </td>
                              <td align="right">
							  <b>
                                <xsl:value-of select="format-number(//n1:Invoice/cac:LegalMonetaryTotal/cbc:AllowanceTotalAmount, '###.##0,00', 'european')"/>
                                <xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:AllowanceTotalAmount/@currencyID">
                                  <xsl:text> </xsl:text>
                                 
                                  <xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:AllowanceTotalAmount/@currencyID != 'TRY'">
                                    <xsl:value-of select="//n1:Invoice/cac:LegalMonetaryTotal/cbc:AllowanceTotalAmount/@currencyID"/>
                                  </xsl:if>
                                </xsl:if></b>
                              </td>
                            </tr>
                          </xsl:if>
                          <tr>
                            <td height="15">

                            </td>
                            <td></td>
                          </tr>
                          <tr>
                            <td  style="height: 19px; border-top: solid black; border-bottom:  solid black;">
                              <b>
                                <xsl:text>ÜCRETLER TOPLAMI </xsl:text>
                              </b>
                            </td>
                            <td align="right" style="height: 19px; border-top: solid black; border-bottom:  solid black;">
                              <b>
                                <xsl:for-each select="n1:Invoice">
                                  <xsl:for-each select="cac:LegalMonetaryTotal">
                                    <xsl:for-each select="cbc:TaxExclusiveAmount">
                                      <xsl:value-of select="format-number(., '###.##0,00', 'european')"/>
                                      <xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:TaxInclusiveAmount/@currencyID">
                                        <xsl:text> </xsl:text>
                                       
                                        <xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:TaxInclusiveAmount/@currencyID != 'TRY'">
                                          <xsl:value-of select="//n1:Invoice/cac:LegalMonetaryTotal/cbc:TaxInclusiveAmount/@currencyID"/>
                                        </xsl:if>
                                      </xsl:if>
                                    </xsl:for-each>
                                  </xsl:for-each>
                                </xsl:for-each>
                              </b>
                            </td>
                          </tr>
                          <xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cac:TaxTotal">
                              <xsl:for-each select="cac:TaxSubtotal/cac:TaxCategory/cac:TaxScheme">
                                <xsl:if test="../../cbc:TaxAmount != '0.00' ">
                                  <tr>
                                    <td>
                                      <xsl:if test="cbc:TaxTypeCode='0015' ">
                                         <xsl:text>KDV</xsl:text>
                                         <xsl:text> %</xsl:text>
                                        <xsl:value-of select="../../cbc:Percent"/>
										<xsl:text>  (Matrah </xsl:text>
                                        <xsl:value-of select="format-number(../../cbc:TaxableAmount, '###.##0,00', 'european')"/>
										 <xsl:text> TL )</xsl:text>
                                        </xsl:if>
                                      <xsl:if test="cbc:TaxTypeCode='4080' ">
                                        <xsl:text>ÖİV</xsl:text>
                                        <xsl:text> %</xsl:text>
                                        <xsl:value-of select="../../cbc:Percent"/>
										 <xsl:text>  (Matrah </xsl:text>
                                        <xsl:value-of select="format-number(../../cbc:TaxableAmount, '###.##0,00', 'european')"/>
										 <xsl:text> TL )</xsl:text>
                                      </xsl:if>
                                      <xsl:if test="cbc:TaxTypeCode='1047' ">
                                        <xsl:text>DAMGA VERGİSİ</xsl:text>
                                      </xsl:if>

                                    </td>
                                    <td align="right">
                                      <xsl:value-of select="format-number(../../cbc:TaxAmount, '###.##0,00', 'european')"/>
                                      <xsl:if test="../../cbc:TaxAmount/@currencyID">
                                        <xsl:text> </xsl:text>
                                       
                                        <xsl:if test="../../cbc:TaxAmount/@currencyID != 'TRY'">
                                          <xsl:value-of select="../../cbc:TaxAmount/@currencyID"/>
                                        </xsl:if>
                                      </xsl:if>
                                    </td>
                                  </tr>
                                </xsl:if>
                              </xsl:for-each>
                            </xsl:for-each>
                          </xsl:for-each>
                          <tr>
                            <td height="15">

                            </td>
                            <td></td>
                          </tr>
                          <tr>
                            <td style="height: 19px; border-top: solid black; border-bottom:  solid black;">
                              <b>
                                <xsl:text>DEVLETE ILETILECEK VERGILER TOPLAMI </xsl:text>
                              </b>
                            </td>
                            <td align="right" style="height: 19px; border-top: solid black; border-bottom:  solid black;">
                              <xsl:for-each select="n1:Invoice">
                                <xsl:for-each select="cac:TaxTotal">
                                  <xsl:value-of select="format-number(cbc:TaxAmount, '###.##0,00', 'european')"/>
                                  <xsl:text> </xsl:text>
                                  <xsl:if test="cbc:TaxAmount/@currencyID = 'TRY'">
                                      </xsl:if>
                                </xsl:for-each>
                              </xsl:for-each>
                            </td>
                          </tr>
                          <tr>
                            <td height="15">

                            </td>
                            <td></td>
                          </tr>
                          <tr>
                            <td bgcolor="#BBBBBB">
                              <b>
                                <xsl:text>FATURA TUTARI (TL) </xsl:text>
                              </b>
                            </td>
                            <td  bgcolor="#BBBBBB" align="right">
                              <b>
                                <xsl:for-each select="n1:Invoice">
                                  <xsl:for-each select="cac:LegalMonetaryTotal">
                                    <xsl:for-each select="cbc:TaxInclusiveAmount">
                                      <xsl:value-of select="format-number(., '###.##0,00', 'european')"/>
                                      <xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:TaxInclusiveAmount/@currencyID">
                                        <xsl:text> </xsl:text>
                                       
                                        <xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:TaxInclusiveAmount/@currencyID != 'TRY'">
                                          <xsl:value-of select="//n1:Invoice/cac:LegalMonetaryTotal/cbc:TaxInclusiveAmount/@currencyID"/>
                                        </xsl:if>
                                      </xsl:if>
                                    </xsl:for-each>
                                  </xsl:for-each>
                                </xsl:for-each>
                              </b>
                            </td>
                          </tr>

                         <xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cbc:Note">
                                      
                              <xsl:if test="substring(.,1,6)  = 'Taksit'">
                                <xsl:if test="substring(.,1,14)  = 'Taksit-Baslik-'">
                                  <tr>
                                    <td>
                                      <b>
                                        <xsl:value-of select="substring(.,15,72)"/>
                                      </b>
                                    </td>
                                    <td align="right">

                                    </td>
                                  </tr>
                                </xsl:if>
                                <xsl:if test="substring(.,1,14)  != 'Taksit-Baslik-'">
                                  <tr>
                                    <td>
                                      <xsl:value-of select="substring(.,8,72)"/>
                                    </td>
                                    <td align="right">
                                      <xsl:value-of select="substring(.,81,20)"/>
                                    </td>
                                  </tr>
                                </xsl:if>

                              </xsl:if>
                            </xsl:for-each>
                          </xsl:for-each>

                         	<xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cbc:Note">
                              <xsl:if test="substring(.,1,11)  = 'Doğal Afet-'">
                               <tr>
                            <td height="5">

                            </td>
                            <td></td>
                          </tr>
                                 <tr>
                                    <td style="height: 19px; border-top: solid black; ">
                                      <xsl:value-of select="substring(.,1,72)"/>
                                    </td>
                                    <td align="right" style="height: 19px; border-top: solid black;">
                                      <xsl:value-of select="substring(.,73,20)"/>
                                    </td>
                                  </tr>
                               
                              </xsl:if>
                            </xsl:for-each>
                          </xsl:for-each>
						  
                         <xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cbc:Note">
                              <xsl:if test="substring(.,1,6)  = 'Mahsup'">
                                <xsl:if test="substring(.,1,14)  = 'Mahsup-Baslik-'">
                                  <tr>
                                    <td>
                                      <b>
                                        <xsl:value-of select="substring(.,15,72)"/>
                                      </b>
                                    </td>
                                    <td align="right">

                                    </td>
                                  </tr>
                                </xsl:if>
                               <xsl:if test="substring(.,1,14)  != 'Mahsup-Baslik-'">
                                  <tr>
                                    <td style="height: 19px; border-bottom:  solid black;">
									<b>
                                      <xsl:value-of select="substring(.,8,72)"/>
                                    </b></td>
                                    <td align="right" style="height: 19px; border-bottom:  solid black;"><b>
                                      <xsl:value-of select="substring(.,81,20)"/></b>
                                    </td>
                                  </tr>
                                </xsl:if>
                              </xsl:if>
                            </xsl:for-each>
                          </xsl:for-each>
                         	
							
						  
                          <xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cbc:Note">
							           
                              <xsl:if test="substring(.,1,7)  = 'Önceki-'">
                                <tr>
                                  <td>
                                    <b>
                                      <xsl:value-of select="substring(.,8,72)"/>
                                    </b>
                                  </td>
                                  <td align="right">
                                    <xsl:value-of select="concat(substring(.,88,1), ',',substring(.,90,2))"/>
                                  </td>
                                </tr>
                              </xsl:if>
                            </xsl:for-each>
                          </xsl:for-each>
                          <xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cbc:Note">
                              <xsl:if test="substring(.,1,8)  = 'Gelecek-'">
                                <tr>
                                  <td>
                                    <b>
                                      <xsl:value-of select="substring(.,9,71)"/>
                                    </b>
                                  </td>
                                  <td align="right">
                                    <xsl:value-of select="concat(substring(.,89,1), ',',substring(.,91,2))"/>
                                  </td>
                                </tr>
                              </xsl:if>
                            </xsl:for-each>
                          </xsl:for-each>



                          <tr>
                            <td  bgcolor="#BBBBBB">
                              <b>
                                <xsl:text>ÖDENECEK TUTAR (TL) </xsl:text>
                              </b>
                            </td>
                            <td  bgcolor="#BBBBBB" align="right">
                              <b>
                                <xsl:for-each select="n1:Invoice">
                                  <xsl:for-each select="cac:LegalMonetaryTotal">
                                    <xsl:for-each select="cbc:PayableAmount">
                                      <xsl:value-of select="format-number(., '###.##0,00', 'european')"/>
                                      <xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:PayableAmount/@currencyID">
                                        <xsl:text> </xsl:text>
                                       
                                        <xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:PayableAmount/@currencyID != 'TRY'">
                                          <xsl:value-of select="//n1:Invoice/cac:LegalMonetaryTotal/cbc:PayableAmount/@currencyID"/>
                                        </xsl:if>
                                      </xsl:if>
                                    </xsl:for-each>
                                  </xsl:for-each>
                                </xsl:for-each>
                              </b>
                            </td>
                          </tr>
                          <xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cbc:Note">
                              <xsl:if test="substring(.,1,4) = 'YAZI'">
                                <tr>
                                  <td colspan="2">
                                    <b>
                                      <xsl:value-of select="substring(.,6,100)"/>
                                    </b>
                                  </td>
                                </tr>
                              </xsl:if>
                            </xsl:for-each>
                          </xsl:for-each>
                          <tr>
                            <td colspan="2" height="15">

                            </td>
                          </tr>
                         
						 </table>
                      </td>
                      <td></td>
                      <td valign="top">
                        <table border="0" id="AltKalemler" width="400">
                          <tr>
                            <td>

                            </td>
                          </tr>
                          <tr>
                            <td height="10"></td>
                          </tr>
                          <xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cbc:Note">
                              <xsl:choose>
                                <xsl:when test="substring(.,1,10) = 'UMTH-GENEL'">
                                  <tr>
                                    <td bgcolor="#BBBBBB">
                                      <b>
                                        <xsl:value-of select="substring(.,6,74)"/>
                                      </b>

                                    </td>
                                    <td align="right" bgcolor="#BBBBBB">
                                      <b>
                                        <xsl:value-of select="substring(.,81,20)"/>
                                      </b>
                                    </td>
                                  </tr>
                                </xsl:when>
                                <xsl:when test="substring(.,1,11) = 'UMTH-TOPLAM'">
                                  <tr>
                                    <td bgcolor="#BBBBBB">
                                      <b>
                                        <xsl:value-of select="substring(.,6,74)"/>
                                      </b>

                                    </td>
                                    <td bgcolor="#BBBBBB" align="right">
                                      <b>
                                        <xsl:value-of select="substring(.,81,20)"/>
                                      </b>
                                    </td>
                                  </tr>
                                </xsl:when>
                                <xsl:when test="substring(.,1,12) = 'UMTH-BASLIK-'">
                                  <tr>
                                    <td>
                                      <b>
                                        <xsl:value-of select="substring(.,13,68)"/>
                                      </b>

                                    </td>
                                    <td align="right">
                                      <b>
                                        <xsl:value-of select="substring(.,81,20)"/>
                                      </b>
                                    </td>
                                  </tr>
                                </xsl:when>
                                <xsl:when test="substring(.,1,12) = 'UMTH-DEVLETE'">
                                  <tr>
                                    <td style="height: 19px; border-top: solid black; border-bottom:  solid black;">
                                      <b>
                                        <xsl:value-of select="substring(.,6,74)"/>
                                      </b>

                                    </td>
                                    <td align="right" style="height: 19px; border-top: solid black; border-bottom:  solid black;">
                                      <b>
                                        <xsl:value-of select="substring(.,81,20)"/>
                                      </b>
                                    </td>
                                  </tr>
                                </xsl:when>
                                <xsl:when test="substring(.,1,5) = 'UMTH-'">
                                  <tr>
                                    <td>
                                      <xsl:value-of select="substring(.,6,74)"/>
                                      <xsl:if test=". = 'UMTH-'">
                                        <br/>
                                        <br/>
                                      </xsl:if>
                                    </td>
                                    <td align="right">
                                      <xsl:value-of select="substring(.,81,20)"/>
                                    </td>
                                  </tr>
                                </xsl:when>
                                <xsl:otherwise>

                                </xsl:otherwise>
                              </xsl:choose>


                            </xsl:for-each>
                          </xsl:for-each>
                          <tr>
                            <td height="10"></td>
                          </tr>
                          <tr>
                            <td>

                            </td>
                          </tr>
                          <tr>
                            <td height="10"></td>
                          </tr>
                          <tr>
                            <td>

                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            
			<tr>
                <td height="25"></td>
              </tr>
              
			  <tr> 
						 <td colspan="6">
						<table width="950" border="0" id="notesTable" >
							<tr>
								<td  align="center" bgcolor="#BBBBBB" colspan="4">
								<b>FATURA BİLGİLERİ (TL)</b>
								</td>
							</tr>
							<tr>
                          <td valign="top" width="232">
							<!--AYLIK UCRETLER-->
							<table width="232" valign="top" border="0" >
									
							<tr>
							<td  align="left" >
							  <b>
								TOPLAM AYLIK ÜCRETLER
							  </b>
							</td>
							<xsl:for-each select="n1:Invoice">
								<xsl:for-each select="cbc:Note">
								  <xsl:if test="substring(.,1,8) = 'Detay1A-'">
									
									<td align="right">
										  
										<b>
										<xsl:value-of select="substring(.,9,20)"/>
										</b>
										  
									</td>
									
								  </xsl:if>
								</xsl:for-each>
							</xsl:for-each>
							</tr>
                          <xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cbc:Note">
                              <xsl:if test="substring(.,1,7) = 'Detay1-'">
                                <tr>
                                  <td>

                                    <xsl:value-of select="substring(.,8,72)"/>

                                  </td>
                                  <TD align="right">
                                    <xsl:value-of select="substring(.,81,20)"/>
                                  </TD>
                                </tr>
                              </xsl:if>
                            </xsl:for-each>
                          </xsl:for-each>
						   </table>
								</td>
                          
                         <td valign="top" width="232">
						<!--KULLANIM ÜCRETLERİ-->
						  <table width="232">
							
							<tr>
								<td  align="left" >
								  <b>
									TOPLAM KULLANIM ÜCRETLERİ
								  </b>
								</td>
								<xsl:for-each select="n1:Invoice">
									<xsl:for-each select="cbc:Note">
									  <xsl:if test="substring(.,1,8) = 'Detay2A-'">
										
										<td align="right">
											  
											<b>
											<xsl:value-of select="substring(.,9,20)"/>
											</b>
											  
										</td>
										
									  </xsl:if>
									</xsl:for-each>
								</xsl:for-each>
								
							</tr>
                          <xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cbc:Note">
                              <xsl:if test="substring(.,1,7) = 'Detay2-'">
                                <tr>
                                  <td >

                                    <xsl:value-of select="substring(.,8,72)"/>

                                  </td>
                                  <TD align="right">
                                    <xsl:value-of select="substring(.,81,20)"/>
                                  </TD>
                                </tr>
                              </xsl:if>
                            </xsl:for-each>
                          </xsl:for-each>
                         
                          </table>
						</td>
						
						 <td valign="top" width="232">
						  <table width="232"> 
						
						<tr>
							<td  align="left" >
							  <b>
								TOPLAM DİĞER ÜCRETLER
							  </b>
							</td>
							<xsl:for-each select="n1:Invoice">
								<xsl:for-each select="cbc:Note">
								  <xsl:if test="substring(.,1,8) = 'Detay3A-'">
									
									<td align="right">
										  
										<b>
										<xsl:value-of select="substring(.,9,20)"/>
										</b>
										  
									</td>
									
								  </xsl:if>
								</xsl:for-each>
							</xsl:for-each>
							
						</tr>
                          <xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cbc:Note">
                              <xsl:if test="substring(.,1,7) = 'Detay3-'">
                                <tr>
                                  <td >

                                    <xsl:value-of select="substring(.,8,72)"/>

                                  </td>
                                  <TD align="right">
                                    <xsl:value-of select="substring(.,81,20)"/>
                                  </TD>
                                </tr>
                              </xsl:if>
                            </xsl:for-each>
                          </xsl:for-each>
                          </table> </td>
                          
						   <td valign="top" width="232">
						  <table width="232">
									
									<tr>
										<td  align="left" >
										  <b>
											TOPLAM İNDİRİMLER(-)
										  </b>
										</td>
										<xsl:for-each select="n1:Invoice">
											<xsl:for-each select="cbc:Note">
											  <xsl:if test="substring(.,1,8) = 'Detay4A-'">
												
												<td align="right">
													  
													<b>
													<xsl:value-of select="substring(.,9,20)"/>
													</b>
													  
												</td>
												
											  </xsl:if>
											</xsl:for-each>
										</xsl:for-each>
										
									</tr>
                          <xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cbc:Note">
                              <xsl:if test="substring(.,1,7) = 'Detay4-'">
                                <tr>
                                  <td>

                                    <xsl:value-of select="substring(.,8,72)"/>

                                  </td>
                                  <TD align="right">
                                    <xsl:value-of select="substring(.,81,20)"/>
                                  </TD>
                                </tr>
                              </xsl:if>
                            </xsl:for-each>
                          </xsl:for-each>
						  </table> </td>
                        </tr></table>
                      </td>
                      <td></td>
					  </tr>
			  
              <xsl:variable name="isSet">
                    
                                   <xsl:for-each select="n1:Invoice">
                      <xsl:for-each select="cbc:Note">
                        <xsl:if test="substring(.,1,8) = 'Tdetay2-'">
						 <xsl:value-of select="substring(.,1,8)"/>
                        </xsl:if> 
                      </xsl:for-each>
                    </xsl:for-each>
                    </xsl:variable>



              <xsl:variable name="isSet1">

                <xsl:for-each select="n1:Invoice">
                  <xsl:for-each select="cbc:Note">
                    <xsl:if test="substring(.,1,8) = 'Tdetay3-'">
                      <xsl:value-of select="substring(.,1,8)"/>
                    </xsl:if>
                  </xsl:for-each>
                </xsl:for-each>
              </xsl:variable>




              <xsl:variable name="isSet3">
                    
                                   <xsl:for-each select="n1:Invoice">
                      <xsl:for-each select="cbc:Note">
                        <xsl:if test="substring(.,1,11) = 'Tdetay3HDT1'">
						 <xsl:value-of select="substring(.,1,11)"/>
                        </xsl:if> 
                      </xsl:for-each>
                    </xsl:for-each>
                    </xsl:variable>



              <xsl:variable name="isSet4">

                <xsl:for-each select="n1:Invoice">
                  <xsl:for-each select="cbc:Note">
                    <xsl:if test="substring(.,1,11) = 'Tdetay3HDT3'">
                      <xsl:value-of select="substring(.,1,11)"/>
                    </xsl:if>
                  </xsl:for-each>
                </xsl:for-each>
              </xsl:variable>


              <xsl:variable name="isSet5">

                <xsl:for-each select="n1:Invoice">
                  <xsl:for-each select="cbc:Note">
                    <xsl:if test="substring(.,1,8) = 'Tdetay1-'">
                      <xsl:value-of select="substring(.,1,8)"/>
                    </xsl:if>
                  </xsl:for-each>
                </xsl:for-each>
              </xsl:variable>
			  
			  
			  <xsl:variable name="isSet19">

                <xsl:for-each select="n1:Invoice">
                  <xsl:for-each select="cbc:Note">
                    <xsl:if test="substring(.,1,9) = 'Tdetay19-'">
                      <xsl:value-of select="substring(.,1,9)"/>
                    </xsl:if>
                  </xsl:for-each>
                </xsl:for-each>
              </xsl:variable>
			   <tr>
                <td colspan="6">
                  <table width="950"  id="notesTable" >

                    <xsl:if test="$isSet19 != '' ">
                      <tr>
                        <td  align="center" bgcolor="#BBBBBB" colspan="13">
                          <b>Taahhütlü Toplu SMS Atım Adetleri</b>
                        </td>
                      </tr>
                    </xsl:if>
                    <xsl:for-each select="n1:Invoice">
                      <xsl:for-each select="cbc:Note">
                        <xsl:if test="substring(.,1,9) = 'Tdetay19-'">
                          <tr>
                            <td align="left" width="7%">
                               <xsl:value-of select="substring(.,10,30)"/>
                                </td>
                            <td align="center" width="7%">
                                <xsl:value-of select="substring(.,40,30)"/>
                               </td>
                            <td align="center" width="7%">
                               <xsl:value-of select="substring(.,70,30)"/>
                            </td>
                            <td align="center" width="7%">
                                <xsl:value-of select="substring(.,100,30)"/>
                            </td>
                            <td align="center" width="7%">
                                 <xsl:value-of select="substring(.,130,30)"/>
                               </td>
                            <td align="center" width="7%">
                                <xsl:value-of select="substring(.,160,30)"/>
                                </td>
                            <td align="center" width="7%">
                              
                                <xsl:value-of select="substring(.,190,30)"/>
								<td align="center" width="7%">
                              
                                <xsl:value-of select="substring(.,210,30)"/>
                              
                            </td>
                              
                            </td>
                          </tr>

                        </xsl:if>
                      </xsl:for-each>
                    </xsl:for-each>

                  </table>
                </td>
              </tr>
			
			 
			
			<tr>
                <td height="20"></td>
              </tr>
			  

             <tr>
                <td colspan="6">
                  <table width="950"  id="notesTable" >

                    <xsl:if test="$isSet5 != '' ">
                      <tr>
                        <td  align="center" bgcolor="#BBBBBB" colspan="13">
                          <b>FATURA ÜCRET DETAYLARI</b>
                        </td>
                      </tr>
                    </xsl:if>
                    <xsl:for-each select="n1:Invoice">
                      <xsl:for-each select="cbc:Note">
                        <xsl:if test="substring(.,1,8) = 'TdetayF-'">
                          <tr>
                            <td align="center" width="7%">
                              <b>
                                <xsl:value-of select="substring(.,9,30)"/>
                              </b>
                            </td>
                            <td align="center" width="20%">
                              <b>
                                <xsl:value-of select="substring(.,39,60)"/>
                              </b>
                            </td>
                            <td align="center" width="7%">
                              <b>
                                <xsl:value-of select="substring(.,99,30)"/>
                              </b>
                            </td>
                            <td align="center" width="7%">
                              <b>
                                <xsl:value-of select="substring(.,129,30)"/>
                              </b>
                            </td>
                            <td align="right" width="7%">
                              <b>
                                <xsl:value-of select="substring(.,159,30)"/>
                              </b>
                            </td>
                            <td align="right" width="7%">
                              <b>
                                <xsl:value-of select="substring(.,189,30)"/>
                              </b>
                            </td>
                            <td align="right" width="7%">
                              <b>
                                <xsl:value-of select="substring(.,219,30)"/>
                              </b>
                            </td>
                            <td align="right" width="7%">
                              <b>
                                <xsl:value-of select="substring(.,249,30)"/>
                              </b>
                            </td>
                            <td align="right" width="7%">
                              <b>
                                <xsl:value-of select="substring(.,279,30)"/>
                              </b>
                            </td>
                            <td align="right" width="5%">
                              <b>
                                <xsl:value-of select="substring(.,309,30)"/>
                              </b>
                            </td>
                            <td align="right" width="5%">
                              <b>
                                <xsl:value-of select="substring(.,339,30)"/>
                              </b>
                            </td>
							<td align="right" width="5%">
                              <b>
                                <xsl:value-of select="substring(.,369,30)"/>
                              </b>
                            </td>
							<td align="right" width="9%">
                              <b>
                                <xsl:value-of select="substring(.,399,30)"/>
                              </b>
                            </td>
                          </tr>

                        </xsl:if>
						<xsl:if test="substring(.,1,8) = 'Tdetay1-'">
                          <tr>
                            <td align="center" width="7%">
                              
                                <xsl:value-of select="substring(.,9,30)"/>
                              
                            </td>
                            <td align="center" width="20%">
                              
                                <xsl:value-of select="substring(.,39,60)"/>
                              
                            </td>
                            <td align="center" width="7%">
                              
                                <xsl:value-of select="substring(.,99,30)"/>
                              
                            </td>
                            <td align="center" width="7%">
                              
                                <xsl:value-of select="substring(.,129,30)"/>
                              
                            </td>
                            <td align="right" width="7%">
                              
                                <xsl:value-of select="substring(.,159,30)"/>
                              
                            </td>
                            <td align="right" width="7%">
                              
                                <xsl:value-of select="substring(.,189,30)"/>
                              
                            </td>
                            <td align="right" width="7%">
                              
                                <xsl:value-of select="substring(.,219,30)"/>
                              
                            </td>
                            <td align="right" width="7%">
                              
                                <xsl:value-of select="substring(.,249,30)"/>
                              
                            </td>
                            <td align="right" width="7%">
                              
                                <xsl:value-of select="substring(.,279,30)"/>
                              
                            </td>
                            <td align="right" width="5%">
                              
                                <xsl:value-of select="substring(.,309,30)"/>
                              
                            </td>
                            <td align="right" width="5%">
                              
                                <xsl:value-of select="substring(.,339,30)"/>
                              
                            </td>
							<td align="right" width="5%">
                             
                                <xsl:value-of select="substring(.,369,30)"/>
                              
                            </td>
							<td align="right" width="9%">
                           
                                <xsl:value-of select="substring(.,399,30)"/>
                              
                            </td>
                          </tr>

                        </xsl:if>
                                        
					</xsl:for-each>
                    </xsl:for-each>

                  </table>
                </td>
              </tr>
			  
			  <xsl:variable name="isSet6">

                <xsl:for-each select="n1:Invoice">
                  <xsl:for-each select="cbc:Note">
                    <xsl:if test="substring(.,1,8) = 'Tdetay4-'">
                      <xsl:value-of select="substring(.,1,8)"/>
                    </xsl:if>
                  </xsl:for-each>
                </xsl:for-each>
              </xsl:variable>

             <tr>
                <td colspan="6">
                  <table width="950"  id="notesTable" >

                    <xsl:if test="$isSet6 != '' ">
                      <tr>
                        <td  align="center" bgcolor="#BBBBBB" colspan="18">
                          <b>FATURA ÜCRET DETAYLARI</b>
                        </td>
                      </tr>
                    </xsl:if>
					 <xsl:for-each select="n1:Invoice">
                      <xsl:for-each select="cbc:Note">
                        <xsl:if test="substring(.,1,8) = 'TdetayY-'">
                          <tr>
                            <td align="left" width="10%">
                              <b>
                                <xsl:value-of select="substring(.,9,30)"/>
                              </b>
                            </td>
                            <td align="left" width="20%">
                              <b>
                                <xsl:value-of select="substring(.,39,60)"/>
                              </b>
                            </td>
                            <td align="center" width="5%">
                              <b>
                                <xsl:value-of select="substring(.,99,30)"/>
                              </b>
                            </td>
                            <td align="center" width="10%">
                              <b>
                                <xsl:value-of select="substring(.,129,30)"/>
                              </b>
                            </td>
                            <td align="center" width="5%">
                              <b>
                                <xsl:value-of select="substring(.,159,30)"/>
                              </b>
                            </td>
                            <td align="center" width="5%">
                              <b>
                                <xsl:value-of select="substring(.,189,30)"/>
                              </b>
                            </td>
                            <td align="center" width="5%">
                              <b>
                                <xsl:value-of select="substring(.,219,30)"/>
                              </b>
                            </td>
                            <td align="center" width="5%">
                              <b>
                                <xsl:value-of select="substring(.,249,30)"/>
                              </b>
                            </td>
                            <td align="center" width="5%">
                              <b>
                                <xsl:value-of select="substring(.,279,30)"/>
                              </b>
                            </td>
                            <td align="center" width="5%">
                              <b>
                                <xsl:value-of select="substring(.,309,30)"/>
                              </b>
                            </td>
                            <td align="center" width="5%">
                              <b>
                                <xsl:value-of select="substring(.,339,30)"/>
                              </b>
                            </td>
							<td align="center" width="5%">
                              <b>
                                <xsl:value-of select="substring(.,369,30)"/>
                              </b>
                            </td>
							<td align="center" width="5%">
                              <b>
                                <xsl:value-of select="substring(.,399,30)"/>
                              </b>
                            </td>
							<td align="center" width="18%">
                              <b>
                                <xsl:value-of select="substring(.,429,30)"/>
                              </b>
                            </td>
                          </tr>

                        </xsl:if>
                       <xsl:if test="substring(.,1,8) = 'Tdetay4-'">
                          <tr>
                            <td align="left" width="10%">
                              
                                <xsl:value-of select="substring(.,9,30)"/>
                              
                            </td>
                            <td align="left" width="20%">
                              
                                <xsl:value-of select="substring(.,39,60)"/>
                              
                            </td>
                            <td align="center" width="5%">
                              
                                <xsl:value-of select="substring(.,99,30)"/>
                              
                            </td>
                            <td align="center" width="10%">
                              
                                <xsl:value-of select="substring(.,129,30)"/>
                              
                            </td>
                            <td align="center" width="5%">
                              
                                <xsl:value-of select="substring(.,159,30)"/>
                              
                            </td>
                            <td align="center" width="5%">
                              
                                <xsl:value-of select="substring(.,189,30)"/>
                              
                            </td>
                            <td align="center" width="5%">
                              
                                <xsl:value-of select="substring(.,219,30)"/>
                              
                            </td>
                            <td align="center" width="5%">
                              
                                <xsl:value-of select="substring(.,249,30)"/>
                              
                            </td>
                            <td align="center" width="5%">
                              
                                <xsl:value-of select="substring(.,279,30)"/>
                              
                            </td>
                            <td align="center" width="5%">
                              
                                <xsl:value-of select="substring(.,309,30)"/>
                              
                            </td>
                            <td align="center" width="5%">
                              
                                <xsl:value-of select="substring(.,339,30)"/>
                              
                            </td>
							<td align="center" width="5%">
                              
                                <xsl:value-of select="substring(.,369,30)"/>
                              
                            </td>
							<td align="center" width="5%">
                              
                                <xsl:value-of select="substring(.,399,30)"/>
                              
                            </td>
							<td align="center" width="18%">
                              
                                <xsl:value-of select="substring(.,429,30)"/>
                              
                            </td>
                          </tr>

                        </xsl:if>
                    
					  </xsl:for-each>
                    </xsl:for-each>
 
                  </table>
                </td>
              </tr>


              <tr>
                <td height="20"></td>
              </tr>
              
              
 
              

                 <xsl:variable name="isSet2">
                    
                                   <xsl:for-each select="n1:Invoice">
                      <xsl:for-each select="cbc:Note">
                        <xsl:if test="substring(.,1,7) = 'Tdetay-'">
						 <xsl:value-of select="substring(.,1,7)"/>
                        </xsl:if> 
                      </xsl:for-each>
                    </xsl:for-each>
                    </xsl:variable>   
              
             <tr>
                <td colspan="6">
                  <table width="950"  id="notesTable">
                   
                        <xsl:if test="$isSet2 != '' ">
                          <tr>
                      <td  align="center" bgcolor="#BBBBBB" colspan="11">
                        <b>TARİFE BİLGİLERİ</b>
                      </td>
                    </tr>
                    </xsl:if>
                    <xsl:for-each select="n1:Invoice">
                      <xsl:for-each select="cbc:Note">
                        <xsl:if test="substring(.,1,7) = 'TdetaT-'">
                          <tr>
                            <td align="left">
                              <b>
                                <xsl:value-of select="substring(.,8,60)"/>
                              </b>
                            </td>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,68,30)"/>
                              </b>
                            </td>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,98,30)"/>
                              </b>
                            </td>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,128,30)"/>
                              </b>
                            </td>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,158,30)"/>
                              </b>
                            </td>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,188,30)"/>
                              </b>
                            </td>
                            <td align="center"><b><xsl:value-of select="substring(.,218,30)"/></b></td>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,248,30)"/>
                              </b>
                            </td>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,278,30)"/>
                              </b>
                            </td>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,308,30)"/>
                              </b>
                            </td>
                            <td align="center">
                              <b>
                                <xsl:value-of select="substring(.,338,30)"/>
                              </b>
                            </td>
                          </tr>

                        </xsl:if>
                      <xsl:if test="substring(.,1,7) = 'Tdetay-'">
                          <tr>
                            <td align="left">
                              
                                <xsl:value-of select="substring(.,8,60)"/>
                              
                            </td>
                            <td align="center">
                              
                                <xsl:value-of select="substring(.,68,30)"/>
                              
                            </td>
                            <td align="center">
                              
                                <xsl:value-of select="substring(.,98,30)"/>
                              
                            </td>
                            <td align="center">
                              
                                <xsl:value-of select="substring(.,128,30)"/>
                              
                            </td>
                            <td align="center">
                              
                                <xsl:value-of select="substring(.,158,30)"/>
                              
                            </td>
                            <td align="center">
                              
                                <xsl:value-of select="substring(.,188,30)"/>
                              
                            </td>
                            <td align="center"><xsl:value-of select="substring(.,218,30)"/></td>
                            <td align="center">
                              
                                <xsl:value-of select="substring(.,248,30)"/>
                              
                            </td>
                            <td align="center">
                              
                                <xsl:value-of select="substring(.,278,30)"/>
                              
                            </td>
                            <td align="center">
                              
                                <xsl:value-of select="substring(.,308,30)"/>
                              
                            </td>
                            <td align="center">
                              
                                <xsl:value-of select="substring(.,338,30)"/>
                              
                            </td>
                          </tr>

                        </xsl:if>
                    
					  </xsl:for-each>
                    </xsl:for-each>

                  </table>
                </td>
            </tr>


                <tr>
                  <td height="20"></td>
                </tr>
				
				<tr>
                <td colspan="6">
                  <table width="950"  id="notesTable">
                  <xsl:for-each select="n1:Invoice">
                      <xsl:for-each select="cbc:Note">
                        <xsl:if test="substring(.,1,8) = 'TdetayC-'">
							<tr>
							  <td  align="center" bgcolor="#BBBBBB" colspan="6">
								<b>CİHAZ BİLGİLERİ</b>
							  </td>
							</tr>
                    
							<tr>
                              <td align="left">&#160;
                                <b>
                                  <xsl:value-of select="substring(.,9,60)"/>
                                </b>
                              </td>
                              <td align="right">
                                <b>
                                  <xsl:value-of select="substring(.,69,60)"/>
                                </b>
                              </td>
                              <td align="right">
                                <b>
                                  <xsl:value-of select="substring(.,129,60)"/>
                                </b>
                              </td>
                              <td align="center">
                                <b>
                                  <xsl:value-of select="substring(.,189,60)"/>
                                </b>
                              </td>
                              <td align="center">
                                <b>
                                  <xsl:value-of select="substring(.,249,60)"/>
                                </b>
                              </td>
                           </tr>
					
					</xsl:if>
                    <xsl:if test="substring(.,1,8) = 'Tdetay3-'">
							<tr>
							  <td  align="center" bgcolor="#BBBBBB" colspan="6">
								CİHAZ BİLGİLERİ
							  </td>
							</tr>
                    
							<tr>
                              <td align="left">&#160;
                                
                                  <xsl:value-of select="substring(.,9,60)"/>
                                
                              </td>
                              <td align="right">
                                
                                  <xsl:value-of select="substring(.,69,60)"/>
                                
                              </td>
                              <td align="right">
                                
                                  <xsl:value-of select="substring(.,129,60)"/>
                                
                              </td>
                              <td align="center">
                                
                                  <xsl:value-of select="substring(.,189,60)"/>
                                
                              </td>
                              <td align="center">
                                
                                  <xsl:value-of select="substring(.,249,60)"/>
                                
                              </td>
                           </tr>
					
					</xsl:if>
					</xsl:for-each>
                    </xsl:for-each>
                    <xsl:for-each select="n1:Invoice">
                      <xsl:for-each select="cbc:Note">
                        <xsl:if test="substring(.,1,12) = 'Tdetay3HDT1-'">
                         <tr>
                              <td align="left">&#160;
                                <b>
                                  <xsl:value-of select="substring(.,13,60)"/>
                                </b>
                              </td>
                              <td align="right">
                                <b>
                                  <xsl:value-of select="substring(.,73,60)"/>
                                </b>
                              </td>
                              <td align="right">
                                <b>
                                  <xsl:value-of select="substring(.,133,60)"/>
                                </b>
                              </td>
                              <td align="center">
                                <b>
                                  <xsl:value-of select="substring(.,193,60)"/>
                                </b>
                              </td>
                              <td align="center">
                                <b>
                                  <xsl:value-of select="substring(.,253,60)"/>
                                </b>
                              </td>
                           </tr> 
                        </xsl:if>
                      </xsl:for-each>
                    </xsl:for-each>
                  </table>
                </td>
              </tr>
				
				
			  <tr>
                <td height="15"></td>
              </tr>
			  
			                 <tr>
               
                <td colspan="6">
                
                 
                
                
                  <table width="950"  id="notesTable">
                   
                    <xsl:if test="$isSet != ''">
                    <tr>
                      <td  align="center" bgcolor="#BBBBBB" colspan="10">
                        <b>TAAHHÜT BİLGİLERİ</b></td>
                    </tr>
                     </xsl:if>
                    

                    <xsl:for-each select="n1:Invoice">
                      <xsl:for-each select="cbc:Note">
                        <xsl:if test="substring(.,1,8) = 'TdetayT-'">
                          <tr>
                            <td align="left"><b><xsl:value-of select="substring(.,9,60)"/></b></td>
                            <td align="left"><b><xsl:value-of select="substring(.,69,75)"/></b></td>
							<td align="center"><b><xsl:value-of select="substring(.,144,60)"/></b></td>
							<td align="center"><b><xsl:value-of select="substring(.,204,60)"/></b></td>
							<td align="center"><b><xsl:value-of select="substring(.,264,60)"/></b></td>
                           
                          </tr>
                        </xsl:if>
						<xsl:if test="substring(.,1,8) = 'Tdetay2-'">
                          <tr>
                            <td align="left"><xsl:value-of select="substring(.,9,60)"/></td>
                            <td align="left"><xsl:value-of select="substring(.,69,75)"/></td>
							<td align="center"><xsl:value-of select="substring(.,144,60)"/></td>
							<td align="center"><xsl:value-of select="substring(.,204,60)"/></td>
							<td align="center"><xsl:value-of select="substring(.,264,60)"/></td>
                           
                          </tr>
                        </xsl:if>
                      </xsl:for-each>
                    </xsl:for-each>
                  </table>
                </td>
                
                
              </tr>
              
              
                
                            
                            <tr>
                <td height="15"></td>
              </tr>
             
              <tr>
              
              
             
             
                <td colspan="3">
                  <table id="notesTable" width="950" align="left" height="100">
                    <tbody>
                      <tr align="left">
                        <td id="notesTableTd">
					 <xsl:if test="//n1:Invoice/cac:PaymentMeans/cbc:InstructionNote">
						&#160;&#160;&#160;&#160;&#160;
						<xsl:value-of select="//n1:Invoice/cac:PaymentMeans/cbc:InstructionNote"/>
						<br/>
					  </xsl:if>
                          <xsl:for-each select="n1:Invoice">
                            <xsl:for-each select="cbc:Note">
                              <xsl:if test="substring(.,1,4) = 'Cdr-'">


                                &#160;&#160;&#160;&#160;&#160;&#160;<xsl:value-of select="substring(.,5,5000)"/>
                                <br/>


                              </xsl:if>
                            </xsl:for-each>
                          </xsl:for-each>
                         
                          <!-- <xsl:if test="//n1:Invoice/cac:PaymentMeans/cac:PayeeFinancialAccount/cbc:PaymentNote">
                            <b>&#160;&#160;&#160;&#160;&#160;	Hesap Açıklaması: </b>
                            <xsl:value-of select="//n1:Invoice/cac:PaymentMeans/cac:PayeeFinancialAccount/cbc:PaymentNote"/>
                            <br/>
                          </xsl:if> -->
                          
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td height="15"></td>
              </tr>
              <tr>
                <td colspan="3">

					<img width="950" align="middle" alt="Turk Telekom" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAACbAAAACRCAIAAAHgb6uJAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAENlJREFUeNrs2uENgjAQBlBhB11Gt9GxHMdpnEISk+bSllrUH0DeizEFe2AM3xWjw/F8PQBrNfoIQEQBEQURBUQUEFFY4vm4T4+4WR2nmdnORkl18k8RrR6u8Y5hH06XW7rC03huZjYhlpTl7aONHztHilwax/2d4aw2obL9ZOeVdlayhE4pql7q5UtzC9jX5WNn54ib7ybxPnQ8QRpXT1l2kVibtZY0wfXBCuO66K540Qr8n++iMZNZkOIeyyB7Smb1uVyQyrvcnvJGUgZ/AISl62e2GGYvVe9AO8tFFDbGjy4gooCIgogCIgqIKGzFSwB27iAJYRCGAqjD/Q/tztEQAthN0fd2HVtk85s6NfHSBVRRQERBRAERBUQURBR+Rz0JJRz2bZjFJJRpw2b7eqPwP/kcNYulH4WW0T41Wx3erdjWqGn18Tkkpd9KaF0dtcCGE9J7DNyqihZDUgrvA0l2L2/Fomm+wxiU9H4QZqak01XC+a+VR98LR5TQaXlbWfnSb9GVRVemLcFZJTRNXf8sWQxGSZ8xp4nwB0DYrqX1lJO0ko0umdZSEYVb89IFRBQQURBRQEQBEYVTPAVg7w5yEAaBKICy8P5ndmFsCMyMiG3j4r24Q6Dt5jtpZLx0AQC/cwFAiAKAEAUAIQoACFEAOMvDIwDgd9mRXdtDrTyA+obdL6xEF0/YrI/MX1zzH07zdKIowEaCHinw1dAcnMfnrxJ0M0RXOkCE2RlOLII2ayAxr5NdUj93vWVF2BAnGwWQoO3dfyTsg/LVUFaP1YXW0c1k3iXcohi6PERf11r3XAkbQvUT6xPyswfRukO7+y2GrjDFD5mP99WS3jP9dM1mALKyZ7E0+jg016D19LDjX/idlWtetPlOdOh1VnejOKK+j8+hcVu4+BBjwworYRZOyRq1ZdPn+wUgK5nCemN9aHv3Yakbdm96LQFwblV61rvJsJrau4Dr3owKUQDY5H+iACBEAUCIAoAQBQAhCgBMngKwc4epDYNgGIDZ2BW2y2y32Y614+w0O8UKARGN34yNrYXn+VFKjMaEkrefKfXrXABQiQKAEAUAIQoAQhQAEKIAIEQBQIgCgBAFACEKAAhRABCiALCaF5cAgOv9/nxfXt8+vs5qSq25eM9zj64SBeBRE/SOE1ulEr1MsXN+8XlOvcQATM2wo01HQ+4GYXxyJfrvyZ/ecdnPAYCkjDNsrOmsnJ5aBD8Pzzh/rd8UZ9Xq0jr53e2tjq2ZFH3rLkX34k0weZkKcPRO29k05hKHcaYMNE2sRA99L9hdj03fAuqdt/1bg2xv6h2CHN0dKr9wxeCqUoBFytDtXn2o4rpZGToYotuB4+uSWvMzLDq2Rqj79mRba9h0sYqYzHeLp1RP2zNagOF6Y6wUCXrd8Z78suAlbq3Hii6ANZOyWJ7Nb9djTUU0pkdpQWWZjxNMrGfOcyvRgbJ1Nxe3kq5V/OVN18RnayXZqizAo1i2iBqvRINnk/XJ14GfbyxWVtPyd70979jz8LJYGU7de0Yo9lEKA8TZlt82x5o6b8hBtTrj6IGn1/dPnwMArnfuXy70/13RjKN38o9FADxe1bsIlSgADFKJAoAQBQAhCgBCFACEKAAgRAFAiAKAEAUAIQoAQhQAEKIAIEQBYDV/ArB3rznRwmAYQNHMFvw2o7vRZbkcV+MqvkkmmRAuLy8dWhk4J/5QoPQCMT7Tgl5ADwAAQFM+zQUAAEAQBQAAQBAFAAAAQRQAAABBFAAAAARRAAAABFEAAAAEUQAAABBEAQAAEEQBAACg3MUQAAAAO/H7833//t/HV6UixaX21vfn7b4gCgAASKGF7Zyz9rSnSqGCKAAAsK8IeoAU2jiB1y4liE4P4rbDV3DaW5GdTOgDAIAUWimFDmwSAU6YQrvGLyu69rzG5we7qvHkv0GMNgAAJ0mhm3f/PCm0azYj2u/2YMpxPJ0YbInnHvt7gxq7qfn0+EpM5qu4GXF1kx3MCNoZdKp2AzLX17wxAACZdLSYzSYPqFSqarItaNLjI5YvdYQgOpc5y84TR9DFGieD7vXrwWDZzSzrnds4rjGTxuObOL7t6jUgc31FUAAAFtNRpezXIHStfcTvtHOhTYNoyzs4eXDBItJBkUGQi++8qte7xmu7AACgzV+wz/WS2MwKwcVazvxfao4TRO/TcZlLXnwZMsffFqBagwoAAGWhdG7X4iLV8a7MItVkqXwkyWfRR5pUacRappjLH95nZc8oLobAYMSvu+4PK+YfE+3Hy+D424/98/e3rH0qNTmM40rnmrRtAzL3tygOAABMenl7/zQKAADATux81W7mXSqbTPl0h161e3GjAwAArApviy/yJGZGFAAAoDCRCp9lzIgCAACsI3k+6NUQAAAAIIgCAAAgiAIAAIAgCgAAgCAKAAAAgigAAACCKAAAAIIoAAAACKIAAAAIogAAACCIAgAA8Bz+C8DeHd7GCYNhACYVK6TLJNukY3WcTtMpWumkEwLis40xtnke5Ud0wWB8nMR7n03e3j++jAIAAADVqIgCAAAgiAIAACCIAgAAgCAKAACAIAoAAACCKAAAAIIoAAAAgigAAAAIogAAAAiiAAAAIIgCAAAgiAIAAIAgCgAAgCAKAACAIAoAAACCKAAAAIIoAAAACKIAAAAIogAAACCIAgAAIIgCAAAgiAIAAEBJsyEAAABa8PfP7+fvPz9/NdiqtdPv99wFUQAAYITsenar+LYZ6e5Il3pkai4AANBWpMwr09Vsdarhy6GCKAAAcK/sOsak3N4JogAAQH9JrP6k3Arnfp8QPvd+sRYfvtTdPt/Lm3x1AQAALSSxpasm5RaPAPcJ4SqiAABAK5RDewnhB831h7jaOatVXvIRMtoAANwhiSmHHqEiCgAANEE5tJcQftxcf4iXizC3ZbTwK+EFnLt7nr5Z9plawYtf1xs+XLgnMQOS18lTOxDz/p60phcAgH7tJrGkB+psb/5jbjjzWp2aZiO7lDdix8e51yBa4QuD+GtitWVMQNo2SXqrIo+4jXPxB00anDM6cNUnFgCAMXRXDl3tM/VW+c7l0GmYqbnxZcPllo+fpJ1v64S7Gy93u9s8cFmstok5aEb/a3YAAACGT2L/Tyf7jO42J3mqVhHdnfNZ6u3Z3XORI+4efbnnlyk00HxbYNw2TL0iA9uf2oGXo21GLgAAd0hikbMIb14OnUZ6WFHSmIZnpeZdIi8nXl8yAmqYAAA0SBIbL4Qnme95BWe0kugAAKCRJBafJ8s+kuflJM0poih6vEsDhPB5mMv3yKN0Ru0JAAB0cScf/lP8fMYpuGwtr1VkxiteITtyIqmTJev/h4sRgmhg0WPBLwMynhwtAAMAAGx1v0b0Eb3Oe1ZyXrRLenpQhfG5sAMAAAAr9Sqiy7rl85Xtiwctq4JJR4z/vy/hJt8V5ctWSldBd/ekVt07owO7jyku+7BiAABAEG1X/LTs1AnceRO+n5F4t3nxkLY9SuUOAABA0u1r4EZ6d7OMJtmtWjv3kU7/4e3948vHAAAAaERGQKr2QN3d2X8Fqz41nwx8bRCdXegAAADZibFUCr2VH4YAAACgCClUEAUAAJBCW2SNKAAAQJprF1gOwBpRAACANMLnQabmAgAAIIgCAAAgiAIAAIAgCgAAgCAKAAAAgigAAACCKAAAAIIoAAAACKIAAAAIogAAACCIAgAAIIgCAACAIAoAAIAgCgAAgCAKAAAAgigAAACCKAAAAAiiAAAACKIAAAAgiAIAACCIAgAAIIgCAACAIAoAAIAgCgAAADH+CdDeHd3GjUNRAB0H04LdjN2NXVbKSTWpIgYCGIlnxKEkiqKuzsF+bHY1fiKfpABzTerp+fXdLAAAAAAAAACR/GYuAAAAAAAAEEsgCgAAAAAAAMQSiAIAAAAAAACxBKIAAAAAAABALIEoAAAAAAAAEEsgCgAAAAAAAMQSiAIAAAAAAACxBKIAAAAAAABALIEoAAAAAAAAEEsgCgAAAAAAAMQSiAIAAAAAAACxBKIAAAAAAABALIEoAAAAAAAAEEsgCgAAAAAAAMQSiAIAAAAAAACxBKIAAAAAAABALIEoAAAAAAAAEEsgCgAAAAAAAMQSiAIAAAAAAACxBKIAAAAAAABALIEoAAAAAAAAEEsgCgAAAAAA8J/fv35+/pNUqHMtrdf6oTw9v767uAEAAAAAAP6aio5e3j4OWqhzrYdF59r0JLX+DK6eawAAAAAAAJdigNc2PepWqHMtrdf6MdkyFwAAAAAAQCSm9VofSyAKAAAAAACcnUis55RqfXbrB2TL3P1vrcGvvA5ne6wJAQAAAAAgjEisp6FSAK0/CStEAQAAAACA8xKJab3Wx7ue4SJObfAZxoi/hFzbAAAAAMB2RGJar/VncD3bRWyM4NoGAAAAALhMfxvZLabq8Ja6TWstMMhpaP3Z2DIXAAAAAAA4HZHYLtOr9Rdp6B6iVohONfvbf/+394X/tf6Yxdt+3g7k6+MLxlj/9Fl/V2xUojAhrarfTmDNuvKN1p4vHsihZ6DV/Wu7XQAAAADgocr0aH3I1K1Q51oNZ77zF7lav1fr93W90PF2WvDZJlfh3fO5Tbw+/7i43HYlmmS9/x5WcwLlH1hTrr5WkysnZgb2uu8AAAAAgPOwQHDkdmx6wlp/WlGB6NSqtV1avqBozWkvGGPhyNsVdcsy0Y1KVI5xbmJacwKt1gqvfGY1nKjxZ6DJ/ev5DgAAAACUicSO0qDjdkTrB+QdoptYmSl+frzVlTorZC3fPJ1LVE5IZXi2coy7/B3cNjYefwZ2ue8AAAAAgFMRiR2rWQ2/ytb6k7Nl7rjP34PWWl/CxqeeVgAAAAAAzYnEelq25ufuAevHovUIRAd9KLteTQgAAAAAAK0UgrcFq3QKW/d1K9S51kZqpm5lRqD1Y7a+M4HoEFq9y3PWk6XDw2v8CQEAAAAAAEbw8vZhC0k24h2iQ9zh3/7lrya3fYc3RzYvMWtCUl+NWT+QM7wcFAAAAAAAYLFTrBCtX1x4e2TneOnbrz+sOfPCkZd76zi/6rbajLtJiQUTUtPEHRebTk3C7W++FK698vmPPAMLrgGLgwEAAAAAgDWenl/fIwdWDjIr93R++NmpH/IwryofXPnT6sd4abfisDC07UrUnENl9crJXNzlWZt6Lx7IQWeg1bX09cEzbGsOAAAAAOxo6rvK5t9GdivUuVaTE9vl3LQ+TOwK0anI5HJvZV75sP57kN4ui7x7OdaPsXI4K6/47UrUTEi5+iA3c80G6A+ncWos489A/f7vs65tAAAAAABGM3fzv6HSUPLErhAFAAAAAABYwzLB5lXm2v0FcFqf4YfHGQAAAAAAAK0cPQ0lz9UUAAAAAAAAMA5RKG0JRAEAAAAAAGjm5e1jwSJRISjb8Q5RAAAAAAAAIJZ3iAIAAAAAAACxBKIAAAAAAABALIEoAAAAAAAAEEsgCgAAAAAAAMQSiAIAAAAAAACxBKIAAAAAAABALIEoAAAAAAAAEEsgCgAAAAAAAMQSiAIAAAAAAACxBKIAAAAAAABALIEoAAAAAAAAEEsgCgAAAAAAAMQSiAIAAAAAAACxBKIAAAAAAABALIEoAAAAAAAAEEsgCgAAAAAAAMQSiAIAAAAAAACxBKIAAAAAAABALIEoAAAAAAAAEEsgCgAAAAAAAMQSiAIAAAAAAACxBKIAAAAAAABALIEoAAAAAAAAEEsgCgAAAAAAAMT6Aw8A10tmRMLDAAAAAElFTkSuQmCC" />

                </td>
              </tr>
            </tbody>
          </table>

        </xsl:for-each>

      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>

