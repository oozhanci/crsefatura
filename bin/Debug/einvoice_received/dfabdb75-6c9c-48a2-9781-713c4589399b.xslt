<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"
                xmlns:ccts="urn:un:unece:uncefact:documentation:2" xmlns:clm54217="urn:un:unece:uncefact:codelist:specification:54217:2001" xmlns:clm5639="urn:un:unece:uncefact:codelist:specification:5639:1988"
                xmlns:clm66411="urn:un:unece:uncefact:codelist:specification:66411:2001" xmlns:clmIANAMIMEMediaType="urn:un:unece:uncefact:codelist:specification:IANAMIMEMediaType:2003" xmlns:fn="http://www.w3.org/2005/xpath-functions"
                xmlns:link="http://www.xbrl.org/2003/linkbase" xmlns:n1="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2" xmlns:qdt="urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2"
                xmlns:udt="urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2" xmlns:xbrldi="http://xbrl.org/2006/xbrldi" xmlns:xbrli="http://www.xbrl.org/2003/instance" xmlns:xdt="http://www.w3.org/2005/xpath-datatypes"
                xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                exclude-result-prefixes="cac cbc ccts clm54217 clm5639 clm66411 clmIANAMIMEMediaType fn link n1 qdt udt xbrldi xbrli xdt xlink xs xsd xsi">
	<xsl:decimal-format name="european" decimal-separator="," grouping-separator="." NaN=""/>
	<xsl:output version="4.0" method="html" indent="no" encoding="UTF-8" doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN" doctype-system="http://www.w3.org/TR/html4/loose.dtd"/>
	<xsl:param name="SV_OutputFormat" select="'HTML'"/>
	<xsl:variable name="XML" select="/"/>
	<xsl:template match="/">
		<html>
			<head>
				<title/>
				<style type="text/css">body {
						width: 800px;
                    	background-color: #FFFFFF;
                      	font-family: 'Tahoma', "Times New Roman", Times, serif;
                      	font-size: 11px;
                      	color: #666666;
						margin:0;
                  	  }
					  #invoiceInfo{
					  	width:100%;
						color: white;
						margin: 10px 0px;
					<!-- background: linear-gradient(#4485C6, #1074BC);-->
						background-color:#1074BC;
						min-height: 107px;
					  }
						#invoiceInfo td{
							border: 2px solid white;
						}

						#despactTableRightCol{
							width:30%;
							border-right-width:0 !important;
						}
						#despatchInfo{
							border-left-width:0 !important;
						}

						#despatchTable td{
							border: 0;
							padding:5px 10px;
						}
						#despatchTable td div:first-child{
							font-size: 13px;
						}

						#despatchTable td div:last-child{
							font-weight:bold;
							min-height:15px;
							font-size: 14px;
						}

						#despactTableRightCol td{
							border: 0;
							padding: 3px 5px;
						}

                      h1, h2 {
                          padding-bottom: 3px;
                          padding-top: 3px;
                          margin-bottom: 5px;
                          text-transform: uppercase;
                          font-family: Arial, Helvetica, sans-serif;
                      }
                      h1 {
                          font-size: 1.4em;
                          text-transform:none;
                      }
                      h2 {
                          font-size: 1em;
                          color: brown;
                      }
                      h3 {
                          font-size: 1em;
                          color: #333333;
                          text-align: justify;
                          margin: 0;
                          padding: 0;
                      }
                      h4 {
                          font-size: 1.1em;
                          font-style: bold;
                          font-family: Arial, Helvetica, sans-serif;
                          color: #000000;
                          margin: 0;
                          padding: 0;
                      }
                      p, ul, ol {
                          margin-top: 1.5em;
                      }
                      ul, ol {
                          margin-left: 3em;
                      }
                      blockquote {
                          margin-left: 3em;
                          margin-right: 3em;
                          font-style: italic;
                       }
                      a {
                          text-decoration: none;
						  color:#666666;
                      }
                      a:hover {
                          border: none;
						  color:#666666;
                      }
						#ettnRow td{
							padding: 0; 
							padding-left: 10px; 
							padding-bottom: 20px; 
						}

						#ettnRow div{
							font-weight: normal !important;
							font-size:12px !important;
							font-style:normal !important;
						}

					  #subTotal{
						margin-bottom:75px;					  
					  }

                      #lineTable{
					    border:0;
					}
					
					.lineTableTd{
					    border-width:1px;
					    padding:3px;
					    border-style:solid;
					    border-color:white;
					}

					#lineTable thead th{
						color: #666666;
						font-weight:bold;
						text-align:center;
					}

					#lineTable tbody td{
						background-color: #E8E8E8;
						text-align:right;
						padding: 5px;
						height:40px;
					}

					#lineTable thead th:first-child, #lineTable tbody td:first-child, #subTotal tbody td:first-child{
						border-left-width: 0;
					}

					#lineTable thead th:last-child, #lineTable tbody td:last-child, #subTotal tbody td:last-child{
						border-right-width: 0;
					}

					.lineTableTr{
					    border-width:1px;
					    padding:0px;
					    border-style:solid;
					    border-color:white;
					    -moz-border-radius:;
					}
					
					#lineTableBudgetTd{
					    border-width:1px;
					    border-spacing:0px;
					    padding:3px;
					    border-style:solid;
					    border-color:white;
						text-align:right;
						background: #E8E8E8;

					}

					#lineTableBudgetTd:first-child{
						background-color:white;
					}

					#budgetContainerTable tr:first-child td{
						  background-color:#1074BC;
						  color: white;
						  font-weight:bold;
					} 

					#budgetContainerTable tr:first-child td:first-child{
						background: white;
						  color: #666666;
					}

					#notesTable{
						width:100%;
						font-size:10px;
					}
					
					#notesTableTd{
					    border-width:0px;
					    border-spacing:;
					    border-style:inset;
					    border-color:black;
					    border-collapse:collapse;
					      padding: 5px 54px;
					    }
					
					table{
					    border-spacing:0;
					}
					
					#budgetContainerTable{
					    border-width:0px;
					    border-spacing:0px;
					    margin-top:10px !important;
					}
					
					td{
					    border-color:gray;
					}
					
					#hesapBilgileri{
						font-size: 10px;
						margin-bottom: 10px;
            margin:0 auto;
						width: 77%;
					}

					#hesapBilgileri td, #hesapBilgileri th{
						padding:4px;
						text-align:center;
					}

					#hesapBilgileri td, #hesapBilgileri th{
						text-align:left;
					}

					#hesapBilgileri tr td:first-child, #hesapBilgileri tr th:first-child{
						text-align:left;
					}
					
					#notTablosu td, #notTablosu th{
						padding:4px;
					}
					.m-r-10{
						margin-right:10px;
					}
					#supplierPartyTable{
						text-align:right;
					}
					#supplierPartyTable td{
						vertical-align:top;
					}
					#payableAmountRow td{
						background-color: #1074BC !important;
						color: white;
							font-weight: bold;
					}

					#footer{
						  width: 50%;
						  text-align: center;
						  border-top: 1px dashed rgb(16, 116, 188);
						  margin-top: 2px;
						  padding-top: 10px;
						  font-size: 10px;
						  margin-left: auto;
						  margin-right: auto;
							
						}
						
						.icons{
							width:25px;
						}</style>
				<title>e-Fatura</title>
			</head>
			<body>
				<xsl:for-each select="$XML">
					<table border="0" cellspacing="0" cellpadding="0px" style="width:100%; padding: 50px 30px 0 30px;">
						<tbody>
							<tr>
								<td>
									<table>
										<tbody>
											<tr>
												<td width="40%" style="vertical-align:top; height: 95px;">
													<img alt="" style="margin-bottom:10px;margin-right: 18px; margin-left: -1px;width:180px"
													     src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAkAAAACqCAMAAAByBADJAAAAclBMVEVHcEwLWI8LWI8LWI8LWI8LWI8LWI8LWI8LWI8LWI8LWI8LWI8LWI8LWI8LWI8LWI8LWI8LWI8LWI8LWI8LWI8LWI8LWI8LWI8LWI8LWI8LWI8LWI8LWI8LWI8LWI8LWI8LWI8LWI8LWI8LWI8LWI8LWI8ghD9MAAAAJXRSTlMAD9ICLoC9zKBgkB4l+hZy8cnEBd8zCkhqtUGnrlA5V3mb6IiWCOrv8gAAGe1JREFUeNrtXed6o7wSNqYZ08GmG3C7/1s8STb7RRIqI4EIex7m565jC+ll+rw6HHbZZZdddtlll1+UYLBPTua6Tn2OG+M3VuDf7PvnCrL6NZbRfiL/FHpix8XkfitWRk9V4ytoB2M/l39Eri93Kk614gEGT8oKsnhXQ/+CRLTD+4LQbaUVGCNjBZmX7+ezdSkzlyn3VVRA77BX0Ab7CW1actvliXPVvwQv5a0gK/dD2rAULy5+3DRtNK/A5CP4Q4b9mLaLn7vo9FzdCBLix3W9/aC2Kmfx6bmpVisWA1bg3vaT2qZUkNNzHY2e9A20gtTaz2qL0rswuZu6VuBnMADVxX5aG3SAiNSve6p634ism+2s5caSLlgXN34UBeVIrmDcj2vzBuz5k3HJbzi2Mk1GrMRX8Prxtsymxf9vN2KbEwMzHzXuKhfxCgogx2Dq4AkfE08PvfYD25iYmAI6TepeJXp+mZayGOZBdz7531cM4XtGemOSo27GieKkNqnuVAxqpWqKlbRQBNn7kW1LGlGgbnqoftCwggBVcYHIR8r2QGxbgpbg6clm86TXglRiDYcucq+JbcsFQizYnfEZS68NQ/BZM7o2onS3YRsV1H70gDTNefEVFCkg0WTrtaK7LBEB1SbkQ4uvANFvKTPIuyI4352gLUkFsQ2RzuO7iW3o4WBmeyC/eR+aU+uu9R0fGuTF7I+9RK7+Lr8jZ4ALhDlBi9cSYhCEx72pY5sCgwYMZmpigyL0eO9M3Ka8ZAG0eFsZTLfEuwbavAlrQKkanSZsmKundlldbFCOEMk2+kuvwAMV+08ajeguyxwfO0eIZBvTxSf8GkiOEM02+vupbUgaSJnS05kHRnPhTGwg5dTU3E9tQxIBOlbNWmclCs0RxoBg8bQf2qakE5cyb3ona87inlm0klHtZ7YpGYVnY6AtZxq6oge0IZv6iRxtObvuZ7YpsUSzg+ZZriU5D5qhikc7jr3yCmmBNVKRhkNBXu8u0HZtGLUhEKNcEeRgjHJscYoE5+xZoiM/Cyao0ZbI3YJtTrCW9oxMshQYaVDNC+KN24vOr1GPFlgJuimpg/Cu/2znmtqa4EM1bowF81YHHU63bN50aTfw2kDwucInZveCE768/cC2nAr6Mjre35fc7AnShY6pgKyXcLLeY0MoSAlKO/8HloRF3PkSNyiTw29H7zZU5wlhGCsAYpLjuZyJQaaf9QVV+2MF3rPe+Tn+BYkcGLnCSP9zc4BRI7juiZVpLjrYF5z3w9qkwOg5TnQDFt1dsGSsZHcAwmC9G7CNygA4vY4e//SOKyNPhifUpwAbuHdDb1WMVnx6dPMzpK6ctHQc5mKStGxn5thqIO9BtMiTdvKxKy01TY/casBfvnYNtEm5Aj3YzDOF0RMoGpvgIAC6UWm8z4RtT/1IKJHWB+AnvcdDb/lRYJWUOJyCINODm8F6N2OFVXrxaNtx5ZXB70cVfiujPHC2b4/yAbv/VhLf2sr3KD+AcbgYL5kVpFvl+s19y7J8vQea92N3eWPycJ7Db1Z3ro6k/Yl/zFg5BQa1YEG5QAXhIQpqyRU8N2fGzN6uH98HmhzbsdQCo/zWJm+6pPZveYe3VNqDef7NBk2YVZ2BVekIJiHWf42N10x6BadtZYOi8TE50XZ5S/F8vHnC3vxfSP+kdXu/n1rG0b7+aICcNE0271wbUs9828KGgeA/K2Cox3ZDCDIrmlpwlobPPXyL5LL+jUY0du9uLIO/CzH66k454FdOCeBFd6EYhBL605ZBw09t335WcPVe2ZYRFDl0fbCs22yL4fMFoZUrhdPTc+KJLTVuUyf4aR4OFv7HtS9+VadlLWsCjmyctJ8V5YvnQ/0ufo5v/QDqL2+odGv601Y6cYHpOnDaqhEfzBaSXuYazH5axGW1fEzc8HQbVdXCfWsHkGm/JeSxHnWJUcMvBezJVGOJR2AdzKTgCOqKlkwTsr+GaCvbyM09rzcUQNXxR2y5Y3rLyWotv4RauXNtUF4RLV+10i0smBVLO5k7CcmekXQDwxn9GwwgVI3IDLb5x7esrKScCXtSiRrfOQkjiTt0OBVTW+TWBDjgNnDvSqYdQMHlLS+rkPnjSZwUQHgRdQvcA2fUM76EuBVv3KACSpYFUMDI/Vyc9nw+3zuXnll8rvDwZ+4oBv3wT4y8nsygFqN1LQVRthAtH79dFsM9oOTcfFryImiql7sMgCKa/nHtBnEVraoOf8MPws4xBfKlMJqGAnXkyrY655jj9stD8jn2+uOpKXMJABXpVMOdp7sd2RM9FWqPxVqJYUE0a0bzXSRtJy31DH5jCrWF67dgIlCoAKidwIeR7J8mGi+aM62N4qSVNT39VJatx57TKm9iGG5/FUAjemD58gCqSPxwkrUBqaw0a2fUm2llKijTBg5phy2Y1yqPYf9XCX9RBSEMAeQBZBFaJaz4xp1AkNaqBsanIOXCmBNHup+FXoWveG5lygfJ0CTF4gDKiSR3ItqlmDBiOrMctvqoMKk/FLgyhnmzXlGml2wGLIgPnR0WB9BIlCiEIadJmDyNWY4cOQKnmAE+tWSMMdOJ8rZR0Cikkr+yAPJxA5ZAUhZ40SzR93I1c8hSfPz4VbyQdk4Uh/Nd/aIb7SOHFS8OoA73f2BWvl4pnWjPugMVL4yrhIvxjDTSV3gCYeXULpaMDy0LoF4pMWhgicdEWyg/jzITy0EqXf50m5kN9DdBtqAVQHibGpgZt1zFC/JnxVAYZavaJdwYn5TKxRenGVmEfwJAuAJ6wL0ZLPd40UQGiCgAR+UnxrkFTcyLVnH1Bp33320BQLgzIxEp4L63pjRZPDOPMssH/5PimOkFB1u4vlAjgAIMP5nMqs56Z0O+5DXz4qRodhg9+wZdZwPXF2oEEIaCt5SbEaEqKNTzdtXzGiLMaKYHczhkcwH02gBrtD4A4WV+yfbqu/56RjovCMc0kFrjyeyusHED5TB9ABreMxwZS3tJ1UR8aKUvMOZyphazSVeHDcTx+gCEudBHaf2ApoJ0xGH57Jt35row/mwnqtzA/ZfaAGSEs5oLK2X/SR5Ail66M7MpsJ+dCGw2UA3TBiAPK2JIexnRW6rGMgtAd7VvOM00gsO8TOYBu7xnEQCZWwIQlgxUyHNlKjns/6xD6cW2XdGjK+Nmd8fHl4J8XFzHcRQBZM+sRT3n/X1Rjt+PESYXt4sb5bKzaXnn+nhJPr4reVycUyzDy6ILQCYWgylYaLQRJKG8/j9COCD50P7trqa4JlGcUnhlnioawJsZxyMmMJN9943KmU4hHM+NvA4xvC6hzTxwUhsVsvkn1NXNToR8vRcl8g/ofOCF/PQJTehiZQyVTM4VKYJQmHBYDnqE8sdMABScWPwOF1taCfSzegLNQL2UGp1ZFEuPs1xGsW+ZhBfHinVqHXi27wuFMfjjF4YCUZqx/1ZhSUc3RHQAmTG2swSAjBePHiRsJbO5aByfyqeSYtUoPh+5LCc1XJs2GZ+hYCx+D0D17IJ6/Q6dsWfpZCqA/JQ3Ht2I5mPDu5wbcZoTRmHVfBkLagnHxDvQm2BGYiDQmXZWAVAyOwxvSp7howGoT3jz9ZDnSKSCGbSjq5P1PlBej1TCxA8AkqVwBIyYDAnkTGk8emsACCuk6kgEUgBUhjyChifsGWq4EsK8GOlaQqvmQVWwxxAWV/MX9FCDXwHQoJMpjw6gJuQxfDyhD/GQUJcoz0Ir95Y0aqOlFfQxEr5NLRzwqSbNbwDoqZupZQKgIOFRxIA3/v0OByUbJhfJF6gHlIEtWCPBbsJzPI1U4ovC8hcAVCu2kikDqDjyOIauU+0UXo7f2UR1UocoVaGXmkAPHGP4CfUxLokkP07uvGWE5ChYA0CXyddoBtCLR1KVE+hKTsN31icq7VTu5WUmk90X3Ij1agMZxMmFnRf8+U2jHymJxTuoSvBnR2p76C0/uJbxfRKshkmwNoBy7Pdz/QAquSxn+CMkRHrDn/KCQHVQoEaugdN7gC38DT/Wc0Tk2C9AHTQ50a7Ejih4kirtiO2Yd0YEBaNzJuTrNW2Qf0ixxDkpNj0Iuxy0A6gg9+5yGr3h9q36CgwglBHQIiZ3DOrQnFXofQhWTbACwnTlZarXc4/cBmqbCWnPKfdOFWMCVGaaamGl7iCMABA+zXoZfXboQh9ijgitHgJbRHGanxQUyxMUdeBeIsyDPlI9ruIsJqcg7PmDjnqfcJPKdQHkwdC7FICwKY7LQPoiGVMZM/Nq0Fl5M5amqCMo8jKw731Cj51VuWswbXuJ+Gf4frusLzLPMJYLPQCytU8GYgBCPOhweomND0uKo0ygcK6FgqDL9ESetE+QdIKjf6zH3ONEhsipU9CBj0zxanhP0Ii5HgC9dEfxGICQPUkavjqsuUH5f9sKLy2YJF3mi19WLTNVak60v+Fo8iDNww8+rnDhqj/so2G0JoA6kPlcCkCvN3jLuG7u3/SaXGmdvK7Q4fxGRHJrZvAmkhiq1POWsxmYAgr5CRb8BoPzmgBydHc0owD6yaNRtwx1gQSNSYYrj59DPqEaOzGe2Kgml6pIVNBO4MxaXrM34yWTrQhCMdGOHgBhjn6gGUAiz/IBzyh8MhJLt/ZQSFtPtylSgzFTp2Ul3gNBZu2TGPdB3XYDdaRcU+a8GZ3pegCE5SOilQDECr0lMgqmlShMGQaUq7yy5w2Bc9FX7VxqYGRPH0JMP+j4kR12wfJrl98CUL4SgBjLN6RSUqUK3Bn3VWan51hVsX1mXGxwzhUBJE7NXgOhGgPl57Ck9XU9AGE5lcM6AGJNdxmqDA9wMRVuPJXFD7qniepCI9lh4SIRRfL6AZSsAyAm/RAKoMdBk1iONH5sSc2ManVV1rZKelj4LLJhegAUrg8gD7QYbSwocvfOq0wCHhegm6il87u9qK1CD4DeqwPIhb26ti4AHQIpHeTI5zac+XQTWDLbl1d81a8A6LIKgBrYzj/0cA2Z/UtO/ziVtLOOZnBCNU16VaC7OAvczP8TAPEiiiesTW8GfG6dvAudjpKTjN78DodYoc/4JnAh9QPosQaAeBFFqdavCoZPqQCfLwjZUlrIf8/uM+8UCkxoDEIze/8fUVgKtvwfW79sXurausqSVTIWFe/jaRUisaNKevfIR93/B4D4K78T/ZkLVnejpztLHIm1EJ2oD0923C4PVRyLll/N0AOgx7qJxAdfqVgT3gBvIS10c9y58gKrAoPsu73EclooUGLMiflnvkIpw9QOoLPES/QNuecCtKbRy11AMqgSMu3pMMVdhtDlpuRD3fiZ/P+LYqpobsinDU4d7ZnjRj1H/aTdefRuTX+9XptyqOwTT1U9gZ5QQWOHuDx7KIYqpVjC4kfUegCUrtrOcZTaOnQ/XqVyXsisWHion4M1NZFRU51Sxl90wC1iDKY+TgPIltlKXX5oGBauBaB6zYYySHr5xJy6dGIlRWQwzFfrccBQNDajbg9sLBuZj5HZYkV0F1XWxRGRsRKA2jVbWiGTr3nN41N4lZJOtRl1VN1TCZODeXNOZ5TGeJwaCa2NjZUGkshiXrjWRA+Azms21YMi0qLmU1CcpDBELXydgA5tFNPU0GjORtDnsPPAwRDaDCRhul2uNdEDoFHSwswCEKyqY55FvC5P8FtJa/95SVjCwqMA8AlDsGjSPGEv5Mh1ZpjicDP+KwwWnjYBIAglV3tVxU8rmRUwqlS1wawXMfW9nUYIIJnsbs11R/QACAsXso0AaDK/TBFIPDTFj3OTz3X556kOgn2LcRay3DlXgTMjA6CO24ikB0D+yuQKUGmExEqhsFFw2kL/VMt0lY5qk71VC98E2nyjTFs+A0DDSgAysXek2AyAPk9NtPVHvhLyHdVMMiAVAE7v9UKOHgpV30XtpT5xIaKJqf6oOxGkCqAPC/RKZtALFmT83s65cdsjPSF4r2rwfAh0afwPmzB8trnaFIA+QHBr+RhiL9h8zWyNF5RDUglnPC9ffAw9l3GifwVAtm5+lzkA+tr8My+YYe5ENbc1fmIRCY0mxbR4MPvnEY6gBaKwtcJ4nI3NVdzcG6csMBNAX48+Mpn+Q4bVJag40gXuCSTogty7bEDnV3UIexEytWnP7DcAhIVhoeIY08fKH/XYFJoA9BnY0y69+XJBqUsmpuDTRS4qJQjLVO5fNbyO/hg4O6aj1iJx5FaNdF339HjPHmP6e2NU6L6mqmgZAH1ufnwEc+S+NOBngqBUqbhbeFR16rDCKYkWiQcXd7oA1M0fhcCYOg1dAPrMDlESjLTBmRuuKhYrEhOkm52iX27RLiNCF/mUnGv+lpDbHKgLQPF79hFz53CXBNAHVlsAyXKULes//6hao55rxL5XOM1RZ4wzga/e50/Y6AIQNhKr1lPW8TowlwXQ5/WjpPsw8YLw/vlxyZgSz26n6i14wSRHjXzXoFThbvhbre3Ky8fcgjx2aWapG0CH3BZQRV/x3o1lh4MaPBKboc28hEkV3StVuD1+K/46l+6qlMMabhy3OIAOhxLfeuL5zFY9XQOx16rMd9MTvbBG5gyl1MqTrwi0AQi78EmlmvHiTvJqABBxYR0OerNc7ITpmgNLB3VzJlkCPD9dUOMp+P0TDp/AXxuAjHBeMjp/cGsLOgBEDEFjWi/vtDlA375qplYTo+kg1nWRjlwf8B9J+J3U2gCEN9aH0ip/4E8GaQEQ3jB6ZYbwtY7+Am+JUJ4SAg90awQM9Uw0mUJTW/oANICIzkEJ9OywEoCikJH+NDutBmzqZM1KMuUXOpFUqTCaWgk4QfQBCOc0SCRVUC+obeoBEOb6e6woSccNjJ9HgXWJzMKiTX11TdSLToA6rhYE0/oAhI9myPKR1NyUjDYAeQwlf18mTSOId1AEzRq9bhgbf5SetsJc2XJdAAV4bcBSVkA0D1wPgMyGvh2BMruzlB+Ntpc9Z30TA0BPAKst+41KinUBhLvRUr31ubCjURZAQSn96iLeJ3qpUxrpAtDBRn+G3g8Au9wnYtidXqDX+UE8FXI6AYSXMxhU+VQZhXRukgAqkwTUelpSFXburKGACBVEg8r1EoIyagFr2y+S53EVNlXoBBB+5wr4FsAP5IXC0Wg5AH0C0oXE3hU1U9Io3VA50wuiGO4hFN3QRFGkN5Zf+gC40WhQQb+pRh1AABtKqKALUPkbF3E/owyAihZs9E/UFC5qWl4a8YMHYuRumX88GMgt8yMrY2jJmQQrFGoMdQBBEglElwTsIrcie4u5GSQA5B/BgaBJvdknz3TngP6Tjt0vYvx1KQF1DoepNzKp1Er2FhJ6qAMIwjRLMjtBrpIkiDQYdwzCAYTcIjrKuEA/JgTthHZyrQDyWLrORG7kbIXXarJjF6xZXaQDKgCzsDqAUmlv+POPhKAvHGFnoBSAqlCirSSjBmGx3ioYFj6hE4vo23ZDX8VWgOI755GPcCN2FbuicwAEmgzJyVnii8CTDtw3qAQCBJBJMLTy23g8epSL1hh6vQDCMpY980V0eCG42fN6+TAVFPKSG9EFQm2uDiBYY39AjvCFI+8MSQaNYz4PQBN+zZQTzV8TqstooBbM1AygAfmxmLHxgvcwenDtBKbhQ7ZLF+HDBtflAQTKaZnDdPicueiAHFxnp6+hADLI+cGE+ZTWg75h/VL5Ych2+WjXI/LvGfg99I/8Q8Iiq3fI2g6iL40ZfMoBqHpLhmEHsiT2x7Gj0qFc2xA+Ywz2gawExn1CKD/k4aql+nRAgvTXZyZDrfD40hvsg7T7wQlzeKJZkjzGz+JSLAMgzIACR0PMmk5LiC3JtEZ3+ilOrhIMIPM2/d56uvJrzfTdUSIfXzuAnoyc5XU6zp8OEy3kn97CHAipzZIJX7l5O8ImdaUBhHO+J4R+ZOxuQSflCd3TJ5Vycxvic00lO+CFzBJ5II/KrowOvEYeaTtjuk5wtOMHc4Kw429C6nuIHL4xkAOqd7GT9HWQd/R1vo4T2oD4sBCAzIQ8h5v1+dN50MTthXWpgSGkdaIK9/5tmUw0nSU6PLbPOI7HczelWagRzW/o7wRiJaPxcyvp48vH7jzGcfxspxO2RwMSn/+3G2Mc23fnARzTVQLQgcprlPyFFdOTdZbGj1wtbAjlfhrb+GCJkT+4FGyXvU/kHuPBNLil1IZwE5eSABq4v8ROB52k8SPoO5YrpvYPmZ++oBtvNjqmmWHVDNIHDI4yj5FwclYyCOKnLSUBlF+UAEQkhAFyFiRcJNs5olpC/0TM6oK1AoBeHJeruEu8BtYyr5SA+lMSQHwVxP0hmbfnIQyXpTsSK6j+PxGqDy1kGCsAaER+b/ru36AHLxp99DOYGhNl+2QBdOgUAXTIR7ASasVJbvmW1ghkRqd5RqSXI10BP5jGo+wDgOT3K88oTJnnNuCLHGH3kzSADFcRQJ9JChCEMki9SR5A5sESUpuG9+mR/aSB0hWi+IN5EzWv+XfhNtagrrdAZNcvAERIA4gXUomX/BJzZMMcVbWm+oDL0BreA355s11DAzXi0Qyfz9DawVs/eRC6gG50lQfQwbRDVQB9cfzzfJ8z1E1VncooBgYb3Dur6B5Oy46KtGigHtK9lpcnxqtwlLtO3LfpvmlyKmF1YwUAffwqXZEA6c/9qntQM9Pwu/c+vuRHZKck8sau8QUk2Wtgfku7CO0KXK7A9kfzOhIp0NA9eQq1lqBq8e9JHLsB983lyEkUMi/yCf/RR21LHP8huNkn5/j4xGGYXNLuXPWqrX5qDRbGtfTi0R7j6tbzN926/ifBGgAyfn7vKoz6CuvvYwy9P6NZsrDKwYvtuPJufXRYSYz+VsW2HcdeaRmHXf5xMc19D3bZZZdddtlljvwPXckzi0azzF4AAAAASUVORK5CYII="/>
													<br/>
													<span style="color: rgb(114, 115, 117);float: left; ">
														<xsl:for-each select="//cac:AccountingSupplierParty/cac:Party/cac:PartyName">
															<xsl:value-of select="cbc:Name"/>
														</xsl:for-each>
													</span>
												</td>
												<td width="30%" align="center" valign="middle" rowspan="2">
													<img style="width:91px;" align="middle" alt="E-Fatura Logo"
													     src="data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABkAAD/4QMZaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjYtYzEzMiA3OS4xNTkyODQsIDIwMTYvMDQvMTktMTM6MTM6NDAgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjZDNDJBNEI2QjVCRDExRThCQjM0REIwQkZGMEQxODY0IiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjZDNDJBNEI1QjVCRDExRThCQjM0REIwQkZGMEQxODY0IiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzQgV2luZG93cyI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSIzREVENkU1N0FDREVDNEJBNzkxNUM2M0NCN0RENzM0NyIgc3RSZWY6ZG9jdW1lbnRJRD0iM0RFRDZFNTdBQ0RFQzRCQTc5MTVDNjNDQjdERDczNDciLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7/7gAOQWRvYmUAZMAAAAAB/9sAhAABAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAgICAgICAgICAgIDAwMDAwMDAwMDAQEBAQEBAQIBAQICAgECAgMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwP/wAARCABmAGkDAREAAhEBAxEB/8QAtwAAAgMAAQUBAAAAAAAAAAAACAkABwoGAQIEBQsDAQABBAIDAQAAAAAAAAAAAAAGAAQFBwgJAQIDChAAAAYBAwMCAwUHAwQDAAAAAQIDBAUGBwARCCESExQJMSIVQVEyIxbwYXGBoRcKkbHB0VIzJEI0JxEAAgECBAIHBAcGBAQHAAAAAQIDEQQAIRIFMQZBUWEiMhMHcYEUCJGhscFCIxXw0VJiMwlyU3Mk4YKiFtJDg9NUJRf/2gAMAwEAAhEDEQA/AN/GlhYmlhYmlhYprMnILDXH6BJYsvZDrlKZOD+CLZyb0hpywPTbAlGVqutgXnLFKLGMAEbsm66xhH8O3XUjtu1bhu0/kWMZdqVJqAB7SSAPpz6MNbi8t7ZdUzU9x+4HA4o8kORGWa9LT+DePCmO6yyKdRtkLl3KymIGT1gkQyzmaj8bRUJZMgpxaDb80DzKUEYwFEDFKUO/Up+k7VY0ivrlpL6tDDFHqoa+EyaqV7AD1YbC4u7gFoUCRUyYkfTpp9uB4XuHJG15XisNWf3DMc48v1hWZNoiAwjxNeSVZdyEtT5HIUbVm+XMqSF7obu5vaBEOJpCMI7byq0UT1hGfpzEOM2YNtG1ncYtnY28dSztcnXQOIy5irrCCRhHr0aNZ0atWWGzC4acQPcjWeHcyrStK8K0FaVrTOlMAhycznyH475S5B48ccs+TFun8Y4wr9uxikwDAMAXLN/ev8TtLLTE45fBsjFViNrTHM8PKKujLuVPpyL9UUCpMjrGK9i2PZd6sbO9+GjiinuCkpLFhDHSUrJ0FyxgkULQd7QNRLgCPupJ7aSSLWWZUquXiPdqOoU1Ka55VNKDBeROQOVddttBx7Ec9q/KZCvdLirdCVjkdxCVSo8uuvQ3eSJesQOZsXp4qrMpLRFSinr9fsO4XQZtV1ToGMgskmNGz2trWa9/SxJaxSFWZLo60GsRqzQhtSguVUErp1MBU1FX2q4jdUE5EjCoBTI5EkBiKEgAmnGg7McrxB7hXJFxUavdMr8RZ/ItFtGPqXlJnkPig7l7+6aUTISEm6p07N4juUPUrwU8ywiFXXpIVaedpIGIfxGTUTOdpuPKu3W7vbx3ax7kk0kRhZSQJI2AdPNqF7tfERQnu1qDjm33G7Kh5Iy0BUNqqB3TmDQD9uOGB4L5RYF5KRTqSw3kmvW5zFH8FirJF1Iu71F6XYqkdcKRLpMbVVn6Rx7RSetEDCPw3DqItuWz7ltThL2Mx1FQahgfeCRXs48MsxiTt723uFDRNU+/7wMX/qNw6xNLCxNLCxNLCxNLCx0H/YP266WFhdeU+WVtyLklbjjxHcVBe7lmVqneM73h0iOMcXT6UU9mpGp1eHFyye5nzJHV5gvIfpyMWIixaoHWkXTVMuxjOy5fgsbH9Y34P5QXUkKhiZFqFJaRCfKAZ08YFSQtQTiHnvJLiQW1kQGJzbLLp8LDPgeHtwAY3zGuPWrm54PreUs88nJeWjJcnKnLVLYZDtmUMVQl2PRM2XHjTBsnFkNCRuGbOo2aT9dY16OexUWqeRTiZYiBBWKfgbu90W27yQwbLGrBbcPp8iVo/MhjnkKJQTqC0beYUZqIXjOas0MUA1QBnuSRV6eJQaMyrU+AnvClQMwG6T6s+Icoc0OH9HcWyfmsF8h1KraWjO0hVLDXooXVgjLBjm1Gs+JJqZZTDjHeVqY6Uepwc2ZrMRZHjNydKPlmJU24rBuNhyzzHKIES72bzFqmtWailZF0TBSolicBfMjBR9LKC8TnVJNDLe2S6iY7mhzoQMwVNVr4WGek5ioOTDK4obhjhyFzND55YJ2COv7CEpsVLhBygQkBaHlCqTukViWnWrRuM24COrT0W304JEIhyVBso5auFmrdVOMk5k3KXbG2lyjWZZyuoamQSOHYKT3RVhXVp1irBWAZgXAsoVmFwKiSg4ZA0FBXp4dFacMshjzsm8LuN+Xrn/cK+Y+JMXAz6wSSk0E5YW6yj6zYlfYQklwboygM2/8A+dPzNkiIppJpOk0ngF9WkRcvFjzNvW3Wxs7SbTbFVXTpU5LMJwMxX+qAanMiq+EkYUtlbTOJJFq4JNaniVKH/pNPr44q+1+3dhGXkbnYqhK3jG9st1TyBWUp+vSzKRWr7zI2J4TDExaYn9RxsrIFsDClVxmRiY7oUGiyZjppgCyxVHtvzhukSRQ3KxT28UkbaWBAYRytMEbSVGkuzaqCpB45Cnk+3wsWZCyOwIqOiqhaitc6AUwM0zTc14bzfW8Z8dyPrFk3IGUrxkG6P31fyPCYMxlg+HwvH4OwUxus8oRlWrfCY1gGMa9SrUW99fPWtqsdII9I7t8zm4bnbNy2t73eSsdjBbpGgDRm4lnaYzzlFzZGkYuDK66Y4SAdZCI7VkmhnEVuCZXck1B0KoXSteg0FO6DVmqchUjjlKjcXc+8hT673FVywVl+jQknJYo5hY0lH9NzFLtavYGdTcy91QjaPCU+PaWyUclkWdTcy9yYLMCroyKce/arM0u94t1yrZosVwk9lMwE9vRdI1KW0JIWaRgo7rvoiGqhQSIwc+axx7g7dwpKtdD51yNKlaBRXoFWy46SCBdOL+Y17wjkNvx25oy1QmXQ2tpjuicrqAdmjji7297GsZmIoOZaswcP1MA5kkYWWZroMX6oRU0Dkh2C/cYEdRN9y/DuNou6bCrpqTW1uVbuKCU1JJIR5oZkbwVoarxAGO8F7LayC1vaNnQPUZmgNCqjKlRx48cNIAQMACA7gPUBDfYQ+z+ICH+ugsmhNeIxNY7tc4WJpYWOm/XbSp04WAa5EXG9ZVsT/jlh+0KUGOaRqcvyLzo2cN2quIaEugZ6EBVJB2PoEcm29i2UBFVYDJQ0cKj5UO4G5Dlmz29ttcUe9X6+ZMx/28IrWQhtLMStdOg5qGXvHgOnEVdNLdObWE6UHjbq6RkaVr2HLC+5q3wsvIYzwXxQM1uHGWbZ3TGuPYDjO8MllWlZ2gkcf3qIz7l/JttgUT4yewzhxKO0zOyvGc5FeoduTTakszikyqG1eKObc9+URb2nlyyNcgCKSA+ZEYIooyPOL0QMRR1koB5XlvKWpapWC1Oq3NVGjxBsm1Mx8NM6DgR/FqC4a/gjj/D4nhVX88SAsOSrHZH+RbrYYmIcxdYJlCz12JgshWjHFVk5KcLjdrf3UYeRlWkeumk9lH7x0oHe5UDQFu27ybhKFi1pZRoI41JBfylYtGsjgL5pjBCqzCqoqqMlGJa3t1hWrUMhOokcNRFCVBJ014kDiSTxJwRJjFIUxzCBSlARMY3QAAOoiI/YABqGHbhyATkMzjOh7nfuPPTOZfBeC7Q8g0IZx47zkKDknMa+I8bggv8ARq9KMFW7psdsoBiuVSH+ICQPt2qDnnnRrQ/p21OVkU95x0cMgGUgjtB/47QPlE+U223iKLnv1EtxLaTLWC2Y5FalTIzw3IYN/I6ZDt8OeWZ5f52TeLgnygzaBUznD8vKt2AoABhD8JZrp8Nvu1VLc47+h7t24PsX/wAONldp8sXo0YVL8u21SP8ANn6P/XxU07zr5IKPm0JWuQ2fpiYfrps2LZrlK9rLuXK5wTSSSbpzYnUOY5gAOn26UPNfNFy6xQ3T6iacE6faowx3b0G+Xzlyyk3HdtitUtolJbv3bUp/gkY/QDh9vtmYG5Vz9ormVORPIbkQ9FJRGUhsdI5Xui0IQiyCgpFtiDuVWI/MIKFHwB8hRD5u7VzcpbXvwVLzd7lmPEIQvt4o33Y1R/Mj6l+kUpn5c9M9igt4xVGnWW4JND/l3NuCOB4P78aMMg46ta2JcutuOi9HxDm/IUDJuYnIbinRrhBS9LNFEmNjtabJqkrNSZPIcib12m/FqqcFzt3ZCGbLXHt9/b/H2rbyJbna4nAaPWQfLBzVSfCOwFa8Ayk6hr4niYxyfC6UmYGhp09Z/Y+w8MJlwfx0xbS7dkeD5QMnWO8QytUk8ZSOLs0RFZtmcc0ucszUJb7Vk3OeTcZ3iwsbfjLGuYSS5anfJqtwbxm8lVzKS6SBUklrM3jeLu9tIW2fTcX+sSeZEWW3hEStGsUEMsSlJZIdBlhjlkUhFpHqBKwUFrGjsLmqw0pRs3YsQxZmVjVQ1dLFQcznTicfHa/5G4r5fh+EvIKxS91ptoZyL7h3n+xLCvKXOswjcF3mC8oy6opldZlo0ckZZm9EpAsEOQFdvVIOAMI7pa229WDcwbaAs8dPiYxWiFm0q4LEatfEhQafizqS9tXktJlsZjVGroPXQVIy6u0+zDPdtvhoNz92JboxOv36WOKHrxTGfssJYaxhPXBJr9Usaws65RK8Uf8A2LTkCyuk4am1tonsYyisrOu0SD2gJgT7jbDttqT2jb/1K9WBiBAAWcnhpXM9IOfDI9PVhtdz/Dwlx4zkPbUDtwoPM7fK+LpzFGI5CauOB5q62mwM8n8lsnMofIHC/PqeV6n3Wen5RpERLCZja7FlB9H1SutZd9TZFvClVWYTC+/0x3Ym1Dbr6K53MJFdhEVktoS0d7A0TZSQyFf6axB5XKJOoOlXiWgkSIlE0Hl24JiqSC7UMTBhwYA8SxCipQ8SGPhLJ+K/FtlghCzXSzOWM/mLJKqr25zLVpV146rMX1hn7qpjCiWKJx/QLNM4yrlxt8s5ijWBN7MESdgks5OmiiRMK37fW3Ux2sAKbbAKItXq5CqnnSK0kirK6Igfy9KVWoUEkmVtLQW+qRzWd+JyyFSdIIVSVBJpqqc+OC80O4eDCmvdM5knwFjlPGNJkSoZMyKycJeoRUTFeuVg3e3eyxiGIYSOHQgZFubpsfcwD8ugLnnmP9HsPhrc0vJRQdgyqc1I6eGMzvk79CT6o85DmDeIw3LG3SKWBP8AUlz0r3ZopAAQDqAZa5EHOmK3LuRVnay8OxcnOUVFDO1xP3KLrKj3HUOcdzHOcwiIiI9RHWM13cM0lK59P1dmPoE5U2CGyt0OmiKoCipNAAOnUa8OnAf2KZdeVGMjE13krILJtWrRukZdy5dLn8aaSSae51FFDjsAAHx14W1u08qQJ4mNB+1cSHNW/wBrsG3SXly2m3jQljQnICvQrHgOgHGlT2sPa+j6dHlzrnVi1/UyccacVCUKb0NJh0SerV3Kr+T9S8CfcooIfl/hAQ66yB5P5Sg2iAX17/XpU8e7w/hcg/RjRh8zXzIbx6ncxHk7lZv/AKsyGNRRD5rE0p+baxOmf89O3BjWz3ocH8ecpwNLh8RvZbF4yhoeVv6Ms3bvfGgqLUZiPizs1PUR5TmA4gKyZhT3EAEdgHtJ6l2druS2SRarQtQvqYdnh8sn68Se0fIJzXvnIEnNF5f+Tvwh1rbiGFw1aMAZhfKgyPHRl1VxozpVur96rEHbKu9RkIOwRbKWjHaBgOmuyfoEcNlSmD/uTUD/AF1a0MyTxLNHmjCo9hxrq3XbrvaNxm2y+Gm6gkKMKg0Ycc1JB9xI7cL/AOeXEaiZMjZbPB6vWrLZabXI1xc6lfLlM0rFt/q1FSt54dxlKXgKndbWWoY6hsg2V7IRcC3YubbHulYiRWcsDg0Mdcpcx3dhIu1CSSOCRzoeNFeWN5NFfKDPGmuQxRKryFhCwEsYWQasDe4WUcymegLACoJIVgK01UBNAGaoFNQOlqjLFNUnFeW+YnFS0Uzk1c/0pyztMLSOQGPm7C1UdwGBrxGM/LjiyY9pEBFRF4oMNVbxFLx8q1nDSjpy4I9aqSTryLopP76823YN/S42CPXsUUjw6yrgy0JqZJCWR20srKYwq6aflqDn4JDLe2ZS9IF0wDUqO77AKECoIzqa9JwbnCbkQ+5L8f6zebTFp1rKcBITuN82UvYCL0zL+PpVzWbvCrIB8yDVxIsfXsBH/wAsa8bqhuU4aFuY9qj2jdpLeA6rQ6WQ9YKg04k5EkZmpAB6cPNuuvirZWbKQZH3ZdQ40wWmoPD7CtOVOa6rHcs8R1m3hKvaRx4qsdnOwwtejFZ6ftWWsq3aOwFxxokLApmIaSsVjtlnkFI0m5Sg5bgc5kyl8pDnZdrnk2KURIPiL2oR2OlUjgDSTOx6ECK5b/DlU8Ia5nX45S3gh4gcSZAAoHaWIA49tMHKhlSMsGVk8RxrCMcykNTY293+LsTuTh7JXIiwrqpUCTgYVetPYG7R0jNwUm0kHDWWS+jPGSRTFVOsAEFzYvFYfqDlhG0hSMqAVYqPzAzagyEKyFQUOsMeFM5PzQ0vkilQtTXiK8KClDmDXPKmLm1HY9sehs9hjanXZqzTK5WsVAxjyVkHB9+1FoyQOuuce0pjfKmQR+A68ppY4I2nk8CAk+zEhtW23O77lBtdmNV1cSqijIVZjQZkgD2kgdZxgz5t8lZjM2UMgZUk3ah/rko6jaw3Oc4kYVdk4XRh2qJDAUUwFtsocAAA8ihh1ipzZvcu7bk92fATRR1AUHHSCfeK4+kb5cfSq09O+Q9v5atV0yrGHmNT3pGIdmI82QA50oracqgCtMKYnpYyaTp+5OInMBj9xuoiOw/fvoJHeI7cZVTOtlbgdAH2fThm/tEcNls65IVzZcoVSQgK9IGjaWzdpFO0eS4ABnUodM+4KFj0z7JdNgOO/wBmrk9O+XTLJ+pTr3QRpz+ng32jGpf55PXN7RP+wdnlo7qTOdPAVGkd+3NaiuaSZVzxqL9xtw548e31kN1XyKM3EqWErUo6bE2WTjZx+kzf7mT+YpVEFBKI/Zvqy+d7h9v5alkg40C17CR119mMHPk+2S05x+YDbLPcQHiBllUZjvJE7Ke6ycDQ5mlejGEzPmQFbi/iYyLFRycSJs2iCZDCos6dLJlApSiACJjG2ANYxRNJeXSBRVy33+7H0C75Jb8sbBM9wdKrCSeJ8K8ctfUeGPoVe2OWyxvFnFNctSi6ktB0mEZugXEfIRQjRIfCbcR6oFMBB+4Q1lxy+kkO2QxSZHR2fdj5l/WG8s9x5+3G+s/6UtwxB73RRfxAHOnUMMXEpTFEpgAxTAJTFMACBgENhAQHoICGpwZcMVbhLdLxZTeE3JpS3HpecpynSFrHHEbekIvEOMeN2K4zP1vpxWS7lL9Rt8t5xyROSqUBEy80VlKEVXjwcuytjoLuwsq43C45o2L4cS2iXKp5hQmeW5mNuj1AOkwwRIvmOiakIDFV1AquIVIksbrXpkMZOmvcVF1kcc9TMTQE0PCppmcXFj4n9gvc9y9j5EAZUPmniCJzzXGRNko9tmjDBozH+TisG5Pywd22lScLJOxApRM4ZKKmEx1h1FXbfqfJ1tIo/M293VyTxErilB2dwdJ4nLpUNbbdpEHgmAPvVa/v6sND0FYmsIDs2RsNPuTnNH+/0Ra31Gy1yOwpxnjrRTf1YSfxa54+cdJ/kTBX2GdUSOk7izmYHJ8K0PHuY8qa7CSfouxOVNBQDW3a2e6nYdvm2p41ubS2uJQr6NMqzTxQvERIQhVknbWrEhkDLTPAyskJupfiFYrKyCorVSqswbLMEFBQjgaHDHuIFbwvIK3LKeOuQGSeTllk2cFQ5fI2VJmIk5+v1yAVk7HCUWNZV+i46iIuPbObSu6WUNHHkXqqpBeOVzIpAkF8xT7ioisLuzgsYFLOI4lYKzNRWkJeSRiToAA1aFodCrU1mLNIe9LHI0rGgqxFQBUgZBR09VT0k0wb2hjD7CwPdvy6ti3iFa42Pc+nl8jv2FKZ9pxIqLR84TWlzJG3ASmJHInDcPh3aCufdyNhsEgXxy0X3VFeg9H24yz+TLklOcfWuxkuFraWKvMf8XluEGToeIJqK8OB6MLuXpoziSQjCHHxNkw7vtDuETb/AB/ntrFm6l1vkch99MfRhyxaCG2Eh8RA+wduB9Tr8nfLdWKDCpmWkrNNx0K2TTL3GFZ+5SQ7gAA3MCYKdw9PgGvXa7R729jt08TEftxHR24G/U7mmDljlm73iY0jt4S3AnOlAMkc5mg8JxsjwfyA4ve3DVKRh65Qlxf2SvUqvvpAavCNZBo2cSTBJc53Sp3iC4vnCvcqYBJ0KYvXWSS8wbJytGm13RYOi9Ac1qAa8GpXjxONEMnoX6s/MVc3HP8AsUcb2d1O1NclslCraSBWSFjpIpUxrXiK8cWFmn3XuB/JLE10xBeK5lBStW+GcxbwFq03RcNBUTN4HzU5pAQTdslRKomb7DF/lprf88cp7pYSWc7PokWnhk9vQB0jrwTcjfKB8x/pzzbZc17JBbJuVrLqU+fZN2EUeZ1zB4lTTjTCJuIeDOEWROYELSaJL5RyFZirS8pV21trMPH1uLbwzdd4s4kFmko5XcOG6JfyzeHtE4B0D46C+Utv5dk3cJZsZJeIykWgFTXM0OMmvmk5r9c7P03+L5pjWzsnYLKA1hMGJ0gKDGmsAE8QBWufDG3fDdFRo1WZx6JSlEECAPaGxR6BsABsGwB2/dq+7eNUjBHGn7v3Y03bnctdXTO3iqf24DFwa98R+FIc96rjNHM+Mckz87doG7xsMwr9RnsZ4MwRkq7VqdbTj2WauK1kHkHH2Sh4wt0uzfGK1OWPRk3SDYfTODqFSTLYHKV1ejbp7KJYntGYs6Sz3EUbrpA78dsySSopFT3iqk94UqcRG4RxGZJGLCQDIqqMwPYXBCk+yp6MftyugnGL8re1ZkBaw3C1S1W5Iu8OzNqvx4Y93moLPeLbPAPAs6tdiIKEJKL26LhjrJs2bZqUyHaRIpSlAPPZrmK62jfIgscYlSJlWPVoXy2diF1FmI4ULMT0kk48r1Cl5aMxZiC2ZpU5DjSgr7BhtG4fsA6A8TeoYVpwKga/I5k5/KzkUyf2mlc9L5Y6++et0l30Cjb8K4ziEn0YsYBO0UkoMXTYTF2MLdQ5N9jCAmfMDzx7Pt3lsRDLbsrAHxaXVsx0gEqR2jsxCbYEM82rNlK0y4VBH14aZoMxOY6Dv9n7f76WF0duM4Pv73QyCHHujlWMUqzy5WZZDu2KcGreJjUjmL/8thdG2+7rqm/Vm6IjtbToOth7tGXDtHTjaj/bQ2KK43XmHemH5kS2sYNTlr8+opqAzp/CfaMZHLm7M6n5FUR/Cqcob7CIAXcNugiHTb+Q6x/clmJHHG67bYxFZov7ccEZ7Y1D/uTzgoBXDUrplUzurQsVQonTIrHp9rUTBv07lDhtv032/dqwfTyz+I3lZWGahvrU9o4YwP8Ano5rfZPS+S0RqPdTxgmlaqk0bEU0N2Z1B9uHJ8q/bC5l5YzbdcnxeS6a3iLlLgvBQxU5UxouCIRNtGMVe5sZIDoNiABgKO2++jXfOQtx3XcHvjNpDUoNCmgAA/zB1dWMWPRz50+RPTfkiz5T/TNc0Wss3xFwKs8jOTp+BlAqWOQcgdFOGEWXz9ZUCRt9ZlZNhIuavKyEA4k2SQkQdOWK6rRdRDcpDdvlTMAbgA9NUxfQyWdy9oWqUNK0A6jwz+3G1/k7dbPm7l+y5iij8r4uLWF1M1MyBmQleFfCPZg0/Y3h39g5iWO2FKYxqzVlkU3AlEdlppwZmokU4dwAY7cTCP7g1ZvpZbh9wkmI8I/eOvtxrw/uJ8wNHylY7NXKWV6j/DoINdPSRw1DG+2DIYkWzA/4/CTu/jsAf021kMnhGNJLmrk9Zx7bXbHXCQ+cPGyNh85TOWn94lk6/myGvsfZohr7cWRuaY1JpN42wfiu2PyW/GLtZpj8V67iiKXizzMQ/dmcLSZUzuWe7RvaXKu9PPtS7ckS+faNGVY7nFY6iss8qApNnJRpnDeW6AAR1Cv3mgdwtwlwZix0SBqjyGlpVUU5r4clFNQPTxGQ57zKpMLRsccA6jXH8nJpSHuE8WbEwcTDd20knPrcjkuM4ANZNMsywatGHqBRavVFXTJomVsZQQSKAQW2yz3v6lcSBVK21DShGVQOGRJpmVyJq1M8d75Ar26irDUTnl1H9vow3/QVibwrnjssGOvcw564tcm9O1y1ROPXJaqIGH5XSKNef4ivKjYdxARZWCrs1HAbbgL5LfoIaNt6CS8n7VdA1kRrhX7PzO7/ANK9A9vbDWf5e5TxdiU+j/jhjtfu1RtjycYVizQc+7rEkaHsbaIlGkgvBy5Cd54uWSbKqHYP0yDuZFUCnKA9Q0DJIj10EGmCa626+so0lu4njjlBKE/iApWnsqPpGOUfw6fv/n+8NdtQpXDLGV7/ACDActsrcdHZu4Gq9NurUphEOwF0pWFUMX/t7jJqgP8AANUf6uEieyY/wy/bHjcF/bBKvtHNMP4/iLE/VdYy2WDcZOTEQ+Ky/wDpubbVGv3WoMbgrbK2WnVhm/sUxCUpy+uiypQE7KmNRSKbqOy8ukicd9h6CX4/Dpq3PSkK19IeJCj9vrONVf8AcXuJF5YsIwe6Z5K8P5ezG6e2Giaxjmcsr5NBNKv1eQkzLKFLsmVnHnWAw7lHt6k1fc7LDaPKclVSTjTdy/aTbrzDabfCNUs1wiKMh4iBStR9JI9ox84/PkyEg0np9UpE3dmn5SZXKUfwqSLpy9OXfoIgB1R1hzuMjXE5l6WJ+2vZj6nOSbJNs2G226LuxW8KIOnIKKcST0dJPtw3X/HdohnsvlS5rIAJX1kiItssJQMPhZMnSyxCiO+xfIcN9h1dnpXZlLOS4YcSPt9v3Y1E/wBw3mA3PNFptde7DHJX2kIf4R19ZxtJak8bZAgB0KmUP4dP+NXNjV0ak1x5OuOnCxXOSMt4zw/GRkzk+8VuhxEzMtq9GSlolGsQwdzTtBw5bRqbt4dJD1KzdmqcpRMG5UzD9mvGa4t7UB520gmnT92JfaNj3TfZng2qEzSohYgFRkOrURU9QFSegHAE8sHaOSea/ty4kjVUX7OBumVuTViKgcFE04fHOOHtVqL1QxREh2rmzZBIdI24h5m5dvs1YPL3l2/KG8XjLVpFgRDX+chuvodePu7A7cEeTdbaEZGNn1dmQ/dhne4/cP8AT/roFxNYVBz6UPx75A8Quc7dM6FUplwe8buQsikA+GMwpnh3GMYu3zB9wKlCUHJsbFvHZx6Jt1zqdRTApjrlZV3XaNw5ZC6ru4jVoc6UZCWbPIZ0XiQBn7DCbkTa3cF9/wCUrd/6qdZ6+AxxHjpjyJ4s8uJesW23YXoqOWnFzNixhEybgck5+hpmac3H6zdUAj2seErTJKQM0j3C7t66dA4dJoikkYiQ0zY267TuRs5GCliQgpm4FSSaVAp/MangMZVc7b3ceo3Iq7/YQNObUq104YKtuzlURVVtDSaxn+UjKgNWNSxw4z+nX/fcf36LBwxjfXrxnF/yIaS7cYuwRkpoj3I1i6TcHKLAUfymk/HMhbCYdhAAM8YgHUQ6jqn/AFctC+2295Sojdgf+bQB09nVjaJ/bI5jitOed55alOd3bRSKO2ETsTkp4A9LAZ8CcZDrITeQWULsX1BO8vUDfjDf/nWPRpWvHG8C1JMAXq/fhj3sd2tlVucbiEegHfcKi8YsxEwFAHEe5Rfh8TAAicpR+8eurT9LJ0h3RofxEfvxrM/uHbFNd8gW+5x+CCdieH4mjHSw7eAPDGx73C72FB4S5jmU1gRcvqf9CYnMYCiLqccNo9IC7iXc4g4HbYd/u1dfNd18Jy7PKDmUAHvI7DjVb8svL45i9b9j2+QVjFyzt7Eidv4l6R0GvVj59Gf34IsmbMDCAJoKKiUBEdvlECgI9R3ER+/WJ87apVIGYP7sfSvt6eTtrt06B9mNRv8Aj14/CI49sLAZM3ks1imJg5zAPVPv9MgIGH4lEiYgGsj/AE8t/K2SJz+KpJ9/t93140D/ADub8dz9XLuAHKAKn0qK/hH3+3GnoA2AAD7P3f8AGrIyrXGEWOgjsAjv9giH8g1xkw7Mc4RzyyyNyQtvKOpYMLjKnZawdY7lTl5Gt3DETy/4xkafLyo1yzHLldCGbRlQyLRCV5xJBHugXVEZY4CYWzYq2g7cbjc33dLSNQ9mzAU7tDUD8R7wIPUcZUchbFyBaenc2/XM7W3NixMwlAuC0dHIX8oMYZFdaCrLpFRUVrggOHnZnzljyp5eokIvQID6RxJwA8KIHau6xi96rI5jssSqQfTrsLJlFQjEFCbhtAATcDFOGrw5oij2bYdv5bHd3GJXe4GfFiGjB4qaKxHdY+EagDQDDuwd72+n3JzqSRu59Ybt6uI9mGj7D9/9NAWWJzLFbZixTTs54ryDh7IManL0rJVSnKbZGBw2MpGTrBZiss2U6Gbvmgqgs3WKJTorpkOQQMUBB3t17Pt15HewEiaNq5ZV6COB4gkHI5HhjwngS4iaKTwH9vtwjHGNcsVkRleNeZa3L5A5u8BY+NQxW3C2sqC45T4CRtEHK4jyF+rXxPGSOZOayzRsKaaoqpvmSyS3/wBzrIeoGwQXqw84bTDqtLipCaiCjLk9SzZ1cMa041AyKknnpPz3ebBLLyjfXYstsuP6kvlCbTRW0jQEZjqqEqGGnVqNQCMNJ4dZ+msy1mbibVYa9e7nSJaRh7vdsfQzuKxgnaTP1nTqiVV9KO1HtocUlg6bs3kkimVs4XIYRBFXuRKHbTePdQ6ZW1yrxalBx4AAUyGVRWvHtw/9QuWINhv1msoDbWEwGiMuXYBVUFmLMXGs1ajBaV00BBA4b7neBj8huGWYaUyag7n4+CNaqymAdx/rdbUJKNgT+Yo96hW5ybB1EB2+3bUZzjtn6tsM1sB3wNQ9oIPWOivTTFgfK56g/wD5v60bRv0jabRpjFJlXuyKy0/pyHMkCqrXPI4+eFJyiJSg2diZB6yUUauElQEpyKIHMRQhwNsJTkMUQEB+AhrEWRGjbQ/jGPp2s7+1ubeO7ib8mRQwyPAjtAP0gYsPidlxDCXLLCmTCPASjoy7xDaXMU+xRipJwRi8BQR32TKkt3D+4uiPlK//AE7fIJm/ip7a9HA9OMb/AJn+TYee/S7dNrhFZ/I1xmpyZSGJzeMHKuRNOzGxD3nMxQ7DhvjmJLIJla5ItVfeJOCKD41Y+JZFnCqfL+NNU/jH4h1H4Dq7/Uq+ROXUiU5yMOjoUqerqxqe+QLky6vvWue+ZavY2rAioGciSCvjHDT2/fjEpm+0x8xIrCxcEWRAhEExDuDcfw9Nw32MI/cG+2sdF/OnB/DUfdjePfk2G0NE+TCM1+j343e+zdj8KbxWxMzM29Ot+jop0uXsEgmVfInemOYPh3GIuXffqOssuUrX4baIUHAJ9uY6T0Y+aT5gt7be/Urc74NqRrgjhTwgD+FekHow5jfpv8P2/ftopNffiieGAb5n8i6tjKuR+Mo3NCeHcvZFcxsfRrWWlrZAjqq/cTMaziX94hkm67eMqNimHKEQd04MgXve/lnKcvcWG3XcIbdVtxL5V1J4G0lqUIrlSmYNM/aOGLT9NeT77eLmTf59s/UuXbIHzozOLepZW00bUHJQjXRQQaaSQGwDVwY3Pj/jpHj9iiBr1a5587Zd2pZ4Oh2202bHON2g+rj8k8jIyJmjphUq7GxCyj9RJEjb1Uyuk2KqooUhtHPp5y/bwLJzPu0YXbLahk7x7ztlHQK1RRmDZClaA5V0inqlzlLzDuceyWFwZ9uiqIWKBCoYL5gOpFdqU06nNSFrxNS3DBOGadx6w/j3CtCbGbVPHdaY16MFUpfVPlEAMvJzMicvRaVnpVdd67U+KrlwoceptR+7bjcbvuMu5XJ/NkIrw4ABVGQAyUAVoK0qcCFtbpbQLBH4FH2mp6+k4trUfj3xNL7MLAIc0+IcnnxtS8tYYtKOKOW+CnD2bwZlUUTqRyhnpCJz2NsiNGxfU2HGF4YlM1fs+4DIHOVwl85BIoUct78u2M9juKebsdzQTR1pWldLBlBcaSdVFIr9GI2/sWuNM8B03cZqp49XQTTo6cC/xCtWLuRGcHM5kAuQOPnMPj5CBXsg8TSWRGvUuqndyK0hZ8iUWAiW7ZrkSgZUfPkVTyx1XqRyJoFEqC+51WXMfJse03MW82rGbaJKmGXw1yGqqaiwpmKsADxFKgYMdu9Sd0uOXX5QcKok0iYEBmkKsGQltHdoQKBWGVAcq1LjHvLOvZdy1lSlRLCP/s/jtRtTHmV5CTj20DZMmvkWCr2hwqbx02dvn0Q1eHK68bdZEFg8flBQDJ6DbfcUu7p49I+FSlHr4iRUilARQ4L955BuOXNi2/cmmZ+YbkuzWwQVhVGoraw7KwZaNwFCSpqQcIh5Few1iLJF8suT8bZRuDeu3+Zf2hmwriNaka81CXeKu1koV4kgfzR/mUMKY95w2Hbfpqvb3022u5u3vA2UjV4Mew5+aPsGMyeVfnx9ROWdhteXLqEvJaxBNWq2WoGY7v6e1Mj/ABMes4HEv+PXCC5QU/ujk0gpHKYpixlf3KYpgEDFN6cAAxTBuGmy+mW3qKq2Y6aN/wC7iduP7gPOlzGY5baqkZ/mQZ/Rtww0HOvtcuOS2A8I43v2ZMmIDg2tKQEUsyZQR1bIYyLdu3lZwjhmoASLZk1KiUUhKUSfEBHroj3jk2HeLOC2uXyhBpkemnU69XWcUl6XfNPvHpTzNum/cv2lLjdTHrHmx5aNf+ZaTA18wnJUpTp6Fguv8eqAcSCapsnZKWSSdpq9ikbXwBUpFCnMU4+m3ADEDqO3TfUBF6ZbZCRKGrQ9T55/6uLlv/n8533G2eCa3prUiuu3yr7NuGNSXFXFwYixnA1dfuSQgIaNikllu1MRbRbFJmmor2lIQoiRABN9m+rSs4BawJEPCopX6us417cy7pJvW7y3x/qSuzU7WNepfsGKmyNz8xjEZmmuLNfdu4fPD1oszpg2qKUQq0vMStdZS1PcRjn1SATrGxvZAWzbsURKdVi77zpkRAx4yffrX4xtrhb/AH9KcDkSuoZ00nI9dMWBs/pBzC/K8HqFuUQ/7QJ1MweOpVZjC4KiUSr3wQSELdIHTgJ3Frs2Am1AyTysrEZm/wBwS1P7fX+MGHKUWOHJrmvWorJyepZHf06T/RkzU6jKJqPVZVwkEbENiidJUypTqiT8i8lXu+L+q7+/lWttVpJ6A+WCDp7iONZagGQOgGrdAw09U/Ubl7bp7nlT0srFyrdpEGj/ADGErIAxIa6jM0emSte8oemXd4nxxE4uWfF8jcc9Z+sLHIXK7M6LJTINoYpiNax9Wm+ziHwziwi6ZXTGg1dcxjnVU/8AZlHxjuVhAvgRRIuZeYItwEe2bYnk7FbEiNKljmalizAPRjmAxJFfcKT26yaCtzcHVdyZseHuoDT6Bg59CuJTE0sLE0sLE0qdOFgMOWPCTGXKVOuW1eTsGKs8Y3UO+xHyExw6+kZIoEgALCVqDsglbWWpPFFzethpEq7F0Uw/KRTZQpHsXMl3soe2AEu2Tf1YjQBxQimrSWXj+Hj01xHXu3RXbLL4Z08LZmnuqAffhQ/I1jlinV1jj33EMUT7mtwc5OTtb59cRsfI22nKy0/WXdMkLfyBwaELKuaVYzQDxMAkyNnzJu8SKZqsgKaYnc7nyTy9zlCrcsSiHdCtRasHYrQd6ksjqr1C6uJpXiOGDvkP1X5j9OtwM94nxO2yUEg1ImvTXRQrG7JpLdAowyYEcCLwRkbKpJJ3N8Tch4Qz9w9x/iOyR+N8d4stEFYLUs8qNMrTLHVVnIt6CVrr19krOq+PKHVdC3FumQiyCbpTvCvr3YOZuW79oLmMi0jFAn5fVQUcFq5940JHEVrliyH5m9L+deXUa+Uwc5zzFprpmuWy83UT5SqsNBF+WoABrTIDPBGOeYORsc27CmMMw8en43bJcVWHlinadKJoUauyFpsbKATg4qSs7eOJYp2tpvQdyzFFcrtBqQTN03QiUotTulxDLFBNDR3rUhuHTwpmejjxwxg9O9k3fab3e9m3Mm2tWUKrQHvlm0kljICi5agShJUioBqMdkR7hNSuCLUarj22RJmXI6k4Fn0bBGMJHyEt6ksRvYYxzB2AzFOMVSjAWK5Ms4MkkoQx2xu8ADm23uK5r5YJAdQewNWh4dnD68cbt6QblsZj+NnUrNbySIQozMWjUuUhNPzF7xAP8uPUcv8APHLDGOeMc0XA2HVb3S5itx9sm3zWl2ObCUcRl6gI6yUUtqYtlKxTpuaqD5yrGuZVZq1SWRMqqoKZOw/Xc7ndIbyNLGPWhFTmo6sswfu+nD3095b9Pt25Zu9w5rvvhdxjbShMcz0qG7wEbqp6BQhj0kUxWucWubnUtndnyzzzizCHEGx1WXhq4ynbnB1a0oSKUrX5+mT0dJVdCu2UyRTt3UfKMFZgx3gAUiaaqapgFxact8y8xXktgqs9jMUCKAmXAmrBlI7wz1sB0cMe0PPXppyPtWz7ry9aludbQ3HxE3mXFHDlkQmOZHhFInIHlKc6MxDChpjAFxyxeqpTqXwaxWa22auUlbHMr7iPISmzNNoqdKPYH0ulFYuhJlBW55caQblyQWCCPhhfI3KCqyfzdll2HJuycoxKea5vM3KJai1AYFwaFQZI3dVpXrzApqFSBUfO3qDufPG6XE21R/D7VOymlVcAhAHNWjjY6m1EigFTwPHDNeMXDSj8eH9iyJMWGw5k5DZAQbkyZnzISpHdwsZUDGOlCQDFIfpFEpTFQ+zeIi00UNilMuZdUPKLHf8Ame73tI7RFEW0QZQwih0AgA9/SrNUiverTgMCljt0NnWXxXDeJs8/dUge7BjaGgAMhiRxNLCxNLCxNLCxNLCxNLCx+K/g9Ot6rw+l8SnqPP2eDwdg+XzeT5PF49+7fpt8dLCwh7kzE+ytN5XcMJW0QtQ5GLLmB7OcLG2WneYWsl5zdhrShxXr9lWNMA4/8f1poo4327Om2rQ2B/UePbq7apfba8H8gdX8ZElPqwNXS7E0/fbTcV6pDnl1ZY5/ReMOfV49s/49+5pzTgYVQoGZQnKTjDKZAVQSETCgmc+VMeYkyJsUNwN6t6ocS7biHQdNJd42eI03XZ4JZOkx3YXPpyhFPrx2jtpSf9tckHLjH7KeL3YshvgX3MQRBuT3BsBnYFeCmMgThYw+qKPu05RcKNE8xkRLN7fMYu/d39Nea77yHkRsTaq//Mn92JE2nMHTd9//AEo8cUt3GXkYRms9zx7lnLWWiSFOZeJ4zcYTUJdYgFMKyZVKNR8v3jtMnuBfSukzgO3aO+2vVN52KVqbVs0EUnW92GFej+sKYYG2kUf7i5LL/p06v4Tir8JQns6QGVGUdZbgtd8/pPA+lz3OxpmZle3Mv8DDUG/KGtVWBJNd4CJ/ojQjrr83TbT/AHh/UiTbGN0nl7XpGSG3PdqKU0EyU4cOjjljpZrsauBG2qbrIkH25Yekz9J6Vt6H0/ofAl6P0nj9L6bxl8Hp/D+V4PHt29vy9u22qrOqve8WCMUplwx5OuMc4mlhYmlhYmlhY//Z"/>
													<h1 align="center" style="padding:0px">
														<span style="font-weight:bold; ">
															<xsl:text>e-Fatura</xsl:text>
														</span>
													</h1>
												</td>
												<td width="30%" rowspan="2" style="vertical-align:top;">
													<table id="supplierPartyTable" style="float:left; width:100%">
														<tbody>
															<tr>
																<td>
																	<img alt="homeIcon" class="icons"
																	     src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADoAAAA6CAYAAADhu0ooAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAa6cAAGunAcIJIQIAAAAZdEVYdFNvZnR3YXJlAEFkb2JlIEltYWdlUmVhZHlxyWU8AAAEvklEQVRoQ82bLW8VQRiF+xP6AxDVqEpkHbYG34Q/0FoMTbCEKhykDoGppIKkICABQRMgQUBSEmoQTQOiBrG8z717c3d3znzszOxtT3KSdjs7M2fej3l3drs2BZqm2TDuGA+MJ8ZLYwi0OTLuG7fabm4mbIKIQ9iZsRQszKFxu+3++mGT2TJikanAwu0a19shVwsbeGqBQ2Dl3Xb46WGDrRuJp1H4dXHVvP9x4TADp8Zp45gBjLHE0vy5+te8/Hje7L343Nx9/K65tfcqyNsPXjf3n39qnr392Xw9/9v2EsV+O626sI6JkyCOv/yeTViJGcM7j97MRLNgERA69WLXOiMDeoH1mJyadAmx9JPj7zHBuHK5WOvEKxI3u/f0g5xkTSIYbwmgTKzd7BWJa6lJTUnCImDdPLF2kzcmSTJqIqsgyS2QsE7b6afBbiC7OmA1U7Lo1MSVA2IPWhlhWEP2SbmF3ASRC0bExktHaySLget0Vx8R64lZDOWPV/ujdNlaiYcM3WWNLYl+PPC7sP3RqV1xDzVAKplIaGug/1JvYa/1YKOVtoRdlNZkoqrzFOIJqUBwiYWppQUOW3lL2EXHmlQ8qtMU5hTsJVmdPdaDpVX5ZX6tj9wVZoFygWVIMqrfGD2Luyz+7RdOBnogrlRnMQaSw2wixBMMLUZu8iPWBc5amTOhzvFH7lOIWlVcUsU6HuPbC3O9ybPdbEq3pbHqJEYmpxBKaLipSiQPj77J9jF6PGUXoZzW9ZCbhFRCwMKqbZfK5XJDx5OUTmR85u5raj/jmmrbJVYdImWBFFVfhkuEOttKbopXQlMXbYhcoVDFPUKdAl7dnELlgikZVGXqEqGqEkNoDyQGdXMK8YQhSGyxfVElkBSX91F5liO0ZCWhyqChxOLZ+4oeCVciVA0CiJvu3owQX9FQ4lVwJUJ9e+kY5O6hC65EKAyVdzGkxHSMKxNaYlUmqfocwyShQN08ljlWrWFNqMZGKOehPdQYLMeqNawJ8cohEOochuU+uQw5xqq1rAkVEMrr9B5qHYYxcQSkILe+HlIVLYZZUe+cFbHnqU5yqBLDECsYb34iaD849W7ug6+iqpa6KDmAG9LzID8/0LYfnJdJtdwXho5XiGN1Tw49bns5Ewnsl+35tSVqJgeonihqj+FJfv0jT7vgnBvVSveQUBgmploJCAa2s/73DnbByb61V5wadoEaFViXymMMJ628JeyifIuWe3bjIwJZwJrJznNOBPTXK/YH+fK3VgEBEVjTZQN7tWvNLqyBUxLSUcmD8JRU5V6LzVaShjWQL5vYn2rGaw0GSsy0b5BoOG/fx00SGxA5+jsG+Z0fYmsmkhwGRJJM3XeiIdgNZGEnXgExW7NsSyXeFIhJkPeNoN3oFQtqFhQxkvk92XWBnXbaebAOgmIp2GtuP0MSJp5ioIsykQtYR0GxgNituT+GjkM7ICbrf9JqnTovpIbAvZggVh6boRFHOHgetYZg4cclnjGwznnScUpFH5g0rocARRYlkmAU0r4OK4UNhCtHrTsB2PLCFc8UsEF5Wx78lrcSEDjt5+UpsEkgmGqqxr+CLEB4sIjXL1DBJrZp5CkIKyTHcgvuISQq/7/L2tp/krca5WB1etsAAAAASUVORK5CYII="/>
																</td>
															</tr>
															<tr>
																<td style="padding-bottom: 25px;">
																	<xsl:for-each select="n1:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PostalAddress">
																		<xsl:if test="cbc:Region !=''">
																			<xsl:value-of select="cbc:Region"/>
																			<span>
																				<xsl:text>&#xA0;</xsl:text>
																			</span>
																		</xsl:if>
																		<xsl:for-each select="cbc:StreetName">
																			<xsl:if test=". !=''">
																				<xsl:apply-templates/>
																			</xsl:if>
																		</xsl:for-each>
																		<br/>
																		<xsl:for-each select="cbc:BuildingName">
																			<xsl:if test=". != ''">
																				<xsl:apply-templates/>
																			</xsl:if>
																		</xsl:for-each>
																		<xsl:for-each select="cbc:BuildingNumber">
																			<xsl:if test=". !=''">
																				<span>
																					<xsl:text> No : </xsl:text>
																				</span>
																				<xsl:value-of select="."/>
																			</xsl:if>
																			<span>
																				<xsl:text> </xsl:text>
																			</span>
																		</xsl:for-each>
																		<xsl:for-each select="cbc:Room">
																			<xsl:if test=". !=''">
																				<span>
																					<xsl:text> / </xsl:text>
																				</span>
																				<xsl:value-of select="."/>
																			</xsl:if>
																		</xsl:for-each>
																		<br/>
																		<xsl:for-each select="cbc:PostalZone">
																			<xsl:if test=". != ''">
																				<xsl:apply-templates/>
																				<span>
																					<xsl:text> </xsl:text>
																				</span>
																			</xsl:if>
																		</xsl:for-each>
																		<xsl:for-each select="cbc:CitySubdivisionName">
																			<xsl:apply-templates/>
																		</xsl:for-each>
																		<span>
																			<xsl:text>&#xA0;/&#xA0;</xsl:text>
																		</span>
																		<xsl:for-each select="cbc:CityName">
																			<xsl:apply-templates/>
																		</xsl:for-each>
																	</xsl:for-each>
																</td>
															</tr>
															<xsl:for-each select="//cac:AccountingSupplierParty/cac:Party">
																<xsl:if test="cac:PartyIdentification/cbc:ID[@schemeID='VKN'] !=''">
																	<tr>
																		<td>VKN :&#xA0;<xsl:value-of select="cac:PartyIdentification/cbc:ID[@schemeID='VKN']"/></td>
																	</tr>
																</xsl:if>
																<xsl:if test="cac:PartyTaxScheme/cac:TaxScheme/cbc:Name !=''">
																	<tr>
																		<td>Vergi Dairesi :&#xA0;<xsl:value-of select="cac:PartyTaxScheme/cac:TaxScheme/cbc:Name"/></td>
																	</tr>
																</xsl:if>
															</xsl:for-each>
															<xsl:for-each select="//n1:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PartyIdentification">
																<xsl:if test="cbc:ID !='' and cbc:ID/@schemeID='MERSISNO'">
																	<tr>
																		<td>Mersis No :&#xA0;<xsl:value-of select="cbc:ID"/></td>
																	</tr>
																</xsl:if>
															</xsl:for-each>
															<xsl:for-each select="//n1:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PartyIdentification">
																<xsl:if test="cbc:ID !='' and cbc:ID/@schemeID='TICARETSICILNO'">
																	<tr>
																		<td>Ticaret Sic. No :&#xA0;<xsl:value-of select="cbc:ID"/></td>
																	</tr>
																</xsl:if>
															</xsl:for-each>
															<tr>
																<td style="padding-top: 25px; padding-left:0;">
																	<a href="https://twitter.com/CrsSoft" target="_blank">
																		<img alt="" class="m-r-10 icons"
																		     src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADoAAAA6CAYAAADhu0ooAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAa6cAAGunAcIJIQIAAAAZdEVYdFNvZnR3YXJlAEFkb2JlIEltYWdlUmVhZHlxyWU8AAAEuElEQVRoQ82br24VQRTG+wh9AEQ1qhJZh63BN+EFWouhCZZQhYPUITCVIEgKAhIQNAESBCQloQbRNCBqEMv53bvL3b1zzuyc3Zl790u+pN3uzsw3c/7N7HajBKqq2hLuCY+Ep8IrYQzccyI8FO7UzUwTMkDEIexcOBZMzLFwt25+/ZDB7AhZkVJg4vaFm3WXq4V0XFrgMljl/br78pDONoX4kws/L6+rd98vAw7AmbCsH9OBsC+wVL+v/1bPP1xUB88+Vbcfvq1uHLyI8ua9V9Xdpx+rJ29+VF8u/tSt9OKwHlZeSMP4SRQvP/+aDVgT4+GtB69nopmwHuA6+XxXGiMCmmD1GJw26DFkpR+9/NYnGFMeL1YaMUViZncev1cHmZMIxloiGCdWHjZFYlraoEoSt4is7jCx8pDpkwQZbSCrIMEtErDO6uGnQR4gugZgNldhqn3ElCNij2oZcciN5Ek1hUxBZMMesf2lo9ykFgPrNFeLRHrDZ1ko21/lj6rJriPwpBIrM2CbsPwxqF0xD62DKZFca2CrlraAXFRXc0p+aRF/pZZWcFzLW0AuBqtJxaM1PEWSYw0sVpVf5te6KFHWLZMgR9XDTqbZCGj3pdCIwoviX37hZKADOtca0zjEvEn8hrnNRGOOtEsgTN0oMEkKzmuZM6HB8YdnF0KI96wEInoK9f/wuo/R7rZqttysNaKxHd5T0xD3pYCVvX/y1ZXemBgF+wjltK4Dzyxigm0wuD7ftkxWA5OesnlvaASlU9U/vQFBMxcmyxLsAWautWGR+xVcITRIK54ZhJGEPYuEmF87YHnQ7ieVWvRFaFDAaw/3MVJgDwZuoPXVR22TjtAO8B/tYYusPsECc/f4XgqGCtUsLBA6pPHUVOEFA9b662MxoanpwgtPLm+zmFAiXW4f9eTyZRYTChFrJOtB8FZEbRYVCkkhBKUcPjtmQ5EkFGgPp5BJygFPyadRsyyEch7agbcaaUiqGbuapKih/TfUJhyhwWHY0GgHx/gqk+StyjRqQCiv0zsYazoQwZR+HnhrbI1MlIJZUR+cFZEqtEZSiUgtIFhgJYds3jUa/c5PBOWHoN71Rj1mEpPHbD1+ij/lPLIxytD5gbb8ELxMSjVfBjnEJ7GaHKbapmG2VzORQH7ZnV9bgFXxRD/uZeDsHKzintXDtHIEHI3GhHePPOVCcG7EoLQGp0gsy0D3ewe5EERf76quk0axclrLW0Auqm/RPMee66JxTgT0r1fkD+rL3zEFRGlicUaUD1ezDbkhKAlpqFQAGctIfb1dS9IhN6gvm0gHU/PXSFpL+waJG+f3dzElsRGR7u8Y1O/8EJuzkhnCiEiCafhONAZ5gCgc+CvAZ3PVph5iTRGfBMO+EZQHTbGAgmJVpkzkN6Jrg7162MMgDUTFUu6VTD+4Sc8qgnEiG0hDUbEgd5FOOkvYLOCT+T9plUaDF1LLwLwYIKvsNWvE4Q6JJ/5MvC/weCCNs9MJSkULrDRlJAI0MikJprmMtK/DxkI6wpR7V7cASHnxiqcEpFPelke/5c0EBJb9vDwFMggEU03l+FeQBrgHk7h+gRpkYNtCdkGsQrIv1+AZXCLz/7tsbPwDyPJrb1sc23sAAAAASUVORK5CYII="/>
																	</a>
																	<a href="https://plus.google.com/116049768507697252760/posts" target="_blank">
																		<img alt="" class="m-r-10 icons"
																		     src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADoAAAA6CAYAAADhu0ooAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAa6cAAGunAcIJIQIAAAAZdEVYdFNvZnR3YXJlAEFkb2JlIEltYWdlUmVhZHlxyWU8AAAE+klEQVRoQ92bvW4UMRSF8wh5AIrUVCkp09HS0EfiBUJLQyRaBBUdKB0FDSUUSIECJChAAiQKkIJEGooogoKGYrjfrs167Hs99s54d+FIR8rOemwf3x/f8U62WqDruh3hvvCu8Fh4LsyBNo+Fh8I9181mQiaIOISdCMeChTkSXnHdrx8ymT0hFmkFFu5AuO2GXC1k4NYCY2DlAzd8e8hg20LiqQrfzn51r76cJVwC74Rt45gBhEOJpfvx63f36M1pd/3h++7y7ZfdhetPsrx441l37cHb7v6Lr93H05+ul0EcumlNC+mYOMni6YfvswlrYmp46dbzmWgWbACEznSxK52RAU1gPSanTXoMsfSdp5+HBOPK48VKJ6ZI3OzqvdfqJKckgvGWDMaJlZtNkbiWNqmWJCwy1l1OrNxkxiRJRpvIKkhyyySsd276ZZAbyK4JWM1VuOoQceWM2LtORh7SkH1S3UI2QaTngNjh0lEaqcXAOt3VIpneiFkMZcerfKm67DoSTynxMgO2C8uXSe2Ke2gDbBLZaw3sOGkLyEXVmpsUlxaJV2ppBUdO3gJyMbEmFY/WcY4MevPxp6KCfcpFZI81sLAqH+bX+qgt60hYBfXprA2LofUxhkYWXhT/8oGTgR4oubTOLCLSw4vAunH84GJcj++fguEcApw4mTOhyfFHzVMI1YqHlrxisS2s6Wl41K7qtjTWOrFILHtYW1GYLIhfrc0UDOcS4AChnNb1UJuEwsSD9bQ28QS0Nrg0ITNm3zaS0rEan7VVUCjUslbovlhXaxNu/tr3JWSxFJwjNNlWSo4/QmKBEFq2DoVaHjOFUKhlX4QmBbx2c44IC6FZ1Q9O/Fvb1lRCtYd0hPZgudUQ49SOWCYOfXwiMuctUwmNszxIhFoxVkISgVGKqSIHjkd6qNmSmgv1xDLxYHyO2zFWKTZSqGeIkoron3HdmHH2Q+x/E6PEnVGG/YW1T69UKNBuzpHtIow3LEemZUAYW9baYqYSGldhAKGch/ZQ83SBK4YWtJIG18N2WtHAuCzY2KObcNE9EJochtU8uYTWGsqMLIrHsvt1CTUglJ/Teyhd0XDioMQTwoXRvh/LeE4Os6I+OStiMlonMeOgt0q7kF5o6Ri11BKRYH4iKH8k9W7JpMPkAYY8gT49mJDWZiyNymx+oC1/JD8mlbpvHPjcp7kwi+IngTVrEl4pDbc9n4kE8uHK/NoCZMiSydBGq1kRwyLEC9FKJNS2FUH/yFMuJOdGNe6FxXLFAgJrH+hrGIZFhP77DnIhyb6lVo3JoAj3bGXBkNreKTh28haQi+qvaFhJ63iTaJwTAf3tFflC/fG3poBYNfEWI1xSa4aQBklJSEdkNG2gddNwWbDrJOmQBuqPTS2z5bI0siwoeweJhvP2fWyS2IzI6vcY1Pf8EFtSNbVkRiTJNP1NNAe5gSycxCsgZtk2tEm0JN6UiUmw3DuCcqMpFlBQrMqVyfxWMeKw76a9HKSDrFjq15bbD2EyYEUwTqSHdJQVC6Yu8djOMrHoQUxO/0qrdJr8IBUD92KCWLnWrRFHOBiPWjFY+LrEUwPpnCedpFS0gKUpIxGgkUUpcM0YZW+HjYUMhCsPWrcB2PLyFU8LyKD8Wp59l3ciILDt6+UlkEkgmGpqin8F8SA8WMT1C9QgE9sV8hSEFYpj2YF7CImJ/99la+sP30OUoVe43KsAAAAASUVORK5CYII="/>
																	</a>
																	<a href="https://www.facebook.com/CRS-SOFT-155951617792634/timeline" target="_blank">
																		<img alt="" class="m-r-10 icons"
																		     src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADoAAAA6CAYAAADhu0ooAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAa6cAAGunAcIJIQIAAAAZdEVYdFNvZnR3YXJlAEFkb2JlIEltYWdlUmVhZHlxyWU8AAAEOklEQVRoQ+2bPW8TQRCG8xPyAyhSU7mkTEebhj4Sf8BpqSLRIlLRgdJR0FCSAsmhAAkKIgEdSEEiDUVkQZGG4pjH3pN9tzN7X3NrF3mlV7LPtx/v7czs3O56ZwwURbEnPBSeCGfCuTAF7nktPBbuh2q2E9JBxCHsUjgUPJhT4UGofvOQzuwLGZGxwIObCndDk3khDY8tsA5GeRqaHx/S2K4Qf+qEX9c3xYcf1xF74EI4rh/TgLApsBR/bv4Vrz5dFUcvvxT3n7wv7hy9SfLuo7fFwxefi+fvfhbfrv6GWhpxHLrlC6kYP0ni7OvvRYc1MV147/H5QjQPrAG4jp/vSmVEQBOMHp3TOj2EjPTTs+9NgjHl4WKlElMkZvbg2Ue1k55EMNaSwDCxUtgUiWlpnRqTuEVidPuJlUKmTxJktI7kIMEtEbAuQvfbQQoQXSPwNHOYahMx5YTYkyAjDbmReVKdQrZBZMkGsc2po9ykJgObNFeLRHrDZxko21/lR9VkNxF42hIrM2CbsPwY5a6Yh9bANpG51sBekLaCXFRH09svcQFyXM3k6LBWpon4K7m0gtMgbwW5GI0mGY9WcR/SmaYkvq9QyBxrYDWqfFleq8IzrcPPmzBEKDSi8Cr5ly+sDFRAyqVV1oeMZhsMFYpbKLgMMhdCo+UPj7eQkpZZ1d9RPaYwY7qZqGbLzVolfWlFxTESEOKKgilCWa2rwDMIwZxCDeuZqf7pnQXlFGrEgzlCo2mlzfJHF+YUCrXoi9AogdcKdyUPqwwyxmS+6JB3MILaSzpCK6BTWuGuZLS6Yuj0UlKzoEgoT1Yr3JV9hHqZ8q3QdWxSqFfKmVXoOrWGgdcI1nkrtA6t8BDmFqqlgQhlPbQCsgutgr7MLRSrrAOh0WKY55sLzC1UA0LZTq/AezEsp1AyMgWLpD5aK/JeEMsp1GhruSIoH6J813MZJadQI69eLmjLh2gzydN8cwk1zHa+EAnky8Hy2gqsMnhF31xCjdWF6pKnXIjWjeigVmFX5hCKqxmonneQC1H09RrVHEK1uVMwC/JWkIvqLprHsufYQhOL1/rpFflB3fwdmkCMKRSLM5Y449Fch9wQpYRU5L2O5EXDZMEkSNIhN6ibTSQR3jnwUBpRFrQ7g8SNy/ur2CaxCZGdzzGo5/wQ65k19WFCJME03hNNQQoQhSN/Bfis92TfhlhTwidBvzOCUtAUC4imuUyZyG9E1xKHodv9IBUkxZJEe7+/rhM3aRhFMExkCakoKRbgu557NkxnCV8sgU/6H2mVSqMNqTowLzrIKHc1a8ThDtYWRg08+G6Bpwukct50olTRAiNNGokAjTyUFqZZR7vTYUMhDWHKjaM7Apjy0hnPGJBG2S1PnuV1AgLHPV7eBtIJBJNNefwVpATuwUPcvEAN0rGJkLcgRqG1LwdQBpdw/r/Lzs5/kPeAHSatfb8AAAAASUVORK5CYII="/>
																	</a>
																	<a href="https://www.linkedin.com/company/crs-soft?trk=top_nav_home" target="_blank">
																		<img class="icons" alt=""
																		     src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADoAAAA6CAYAAADhu0ooAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAa6cAAGunAcIJIQIAAAAZdEVYdFNvZnR3YXJlAEFkb2JlIEltYWdlUmVhZHlxyWU8AAAEb0lEQVRoQ+2bvW4UMRSF9xHyABSpqVJSpqNNQx+JF0haGiLRIlLRgdJR0KQkRaRAARIURAIkCpCCRBqKKIIiDcVwv10P8+N7PfbOeHaKPdKRdmf9d2zf62uPd5YDRVFsCneFh8Iz4bUwBNIcCw+E266YaUIaiDiEXQj7go45Eu644lcPacy2kBHJBTpuT7jhqhwXUnFugW0wynuu+vyQyjaE2FMSfl7dFO++X3lcAufCvHZMBcIux1L8vvlbvPxwWey/+FTcffy2uLX/KsjbD06L+88/Fs/e/Ci+XP5xpXTiwDVrWEjB2EkQJ59/zRusiUnhnUev56LpsA5gOsPZrhSGBzTB6NE4rdF9yEg/OfnWJZip3F+sFGKKZJrde/pebeSQRDCzJYB+YiWzKZKppTUqJzGLwOguJ1YymTaJk9EaMgZxbgGHde6aHwfJgHf1QG/GeNHcZCoHxB46GWFIQtZJdQnpEkkDxuqIDrHdoaMkUoOBrun68PirS7lwUjRESzckqcOwWQbKtlf5UZ2yXY6HUWwDL6mlHZp4fQP2FJYfvdiV0dEqqJO1ToOWNget+gWbTloFeaiOZsw6qfXqWCNaklhawZGTV0EeeqNJxKMVqpHpXYJKx/bOrLEGqlHly+JZE6lhHenHiJQsGruhKviXL5wMNJAy9Rg9BLaZmqadVvstRFYGBRdO5lyod/yRsgux9pWxaVgmaKSWJnXDYCw3W+q0JbFWiMU+QhFoOJL/SInI6BgFewjltK6BFCcE+wiNBZ0RE4QYTulMtc/UoH0MoYDoq16mRjpDwTVCvWUldWkYS2hM8AK1GBihXgCvZQ6xr1A8fN3L1uPmNuplWtQ26QhtAFvQMofYR6i1jFnpY2abFhJ6QqlAyxxiH6HW0mHFrzHr6ySF1tPUuRa6FlpxLbQNLXOIUxOqhYEI5Ty0gdTznqkJ1epCqHcYlvr+ZGpCNSCU1+kNpJ7CT0koAYWCeVDvnRXFxpQlpyTUyLc4EZQPXrybstmdklAtoBcsDrTlg/cyKWX6TkWoMW2v5yKBfNlZPKvArj7W+05FqHG60DzylAfeuRGVaQW2yegjpM3UNHWWZ0htWrsXTM1A876DPPC8b8qorpraHlRw5uRVkIfqW7SUY89VMXB4rd9ekR/Ul7+pAcSYZMYZR5z+aNYhCbyQMOW4cWxitwa2nCQdkkB92cT6NDV7NbwsiLuDRMJF+iamJDYgMvkeg3rPD7EpUVMOBkTiTP13oiFIBrywZ68Am+0KxXKQ2RSwSbDcHUHJaIoFsQHFEMTzG961xK5r9nKQAoJiOQfOufxgJkYwUEc/kSWkoKBYgO2mvrMJkeUsYIslsMnhr7RKod4LqTaYXjSQUU710IjDHIytVht0fJrjSYEUzk7HCxUt0GimHgI00ikdDkZD3O2wvpCKmMqdo5sBLHnhiCcHpFLelgfv8g4EBOa9Xh4DaQSCiaaG+CtICcyDTly9QA3SsC0huyBGIdqWHciDSQz8f5fZ7B/8Tdptg4QU/gAAAABJRU5ErkJggg=="/>
																	</a>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr>
												<td style="vertical-align:top;">
													<table id="customerInfo" border="0">
														<tbody>
															<tr>
																<td style="padding-bottom:4px;">
																	<b>
																		<xsl:text>Sayın</xsl:text>
																	</b>
																</td>
															</tr>
															<tr>
																<xsl:for-each select="n1:Invoice">
																	<xsl:for-each select="cac:AccountingCustomerParty">
																		<xsl:for-each select="cac:Party">
																			<td>
																				<xsl:if test="cac:PartyName">
																					<xsl:value-of select="cac:PartyName/cbc:Name"/>
																				</xsl:if>
																				<xsl:for-each select="cac:Person">
																					<xsl:for-each select="cbc:Title">
																						<xsl:apply-templates/>
																						<span>
																							<xsl:text>&#xA0;</xsl:text>
																						</span>
																					</xsl:for-each>
																					<xsl:for-each select="cbc:FirstName">
																						<xsl:apply-templates/>
																						<span>
																							<xsl:text>&#xA0;</xsl:text>
																						</span>
																					</xsl:for-each>
																					<xsl:for-each select="cbc:MiddleName">
																						<xsl:apply-templates/>
																						<span>
																							<xsl:text>&#xA0; </xsl:text>
																						</span>
																					</xsl:for-each>
																					<xsl:for-each select="cbc:FamilyName">
																						<xsl:apply-templates/>
																						<span>
																							<xsl:text>&#xA0;</xsl:text>
																						</span>
																					</xsl:for-each>
																					<xsl:for-each select="cbc:NameSuffix">
																						<xsl:apply-templates/>
																					</xsl:for-each>
																				</xsl:for-each>
																			</td>
																		</xsl:for-each>
																	</xsl:for-each>
																</xsl:for-each>
															</tr>
															<tr>
																<xsl:for-each select="n1:Invoice">
																	<xsl:for-each select="cac:AccountingCustomerParty">
																		<xsl:for-each select="cac:Party">
																			<td>
																				<xsl:for-each select="cac:PostalAddress">
																					<xsl:if test="cbc:Region !=''">
																						<xsl:value-of select="cbc:Region"/>
																						<span>
																							<xsl:text>&#xA0;</xsl:text>
																						</span>
																					</xsl:if>
																					<xsl:for-each select="cbc:StreetName">
																						<xsl:apply-templates/>
																						<span>
																							<xsl:text>&#xA0;</xsl:text>
																						</span>
																					</xsl:for-each>
																					<xsl:for-each select="cbc:BuildingName">
																						<xsl:apply-templates/>
																					</xsl:for-each>
																					<xsl:for-each select="cbc:BuildingNumber">
																						<xsl:if test=". !=''">
																							<span>
																								<xsl:text> No : </xsl:text>
																							</span>
																							<xsl:value-of select="."/>
																						</xsl:if>
																					</xsl:for-each>
																					<xsl:for-each select="cbc:Room">
																						<xsl:if test=". !=''">
																							<span>
																								<xsl:text>/</xsl:text>
																							</span>
																							<xsl:value-of select="."/>
																							<span>
																								<xsl:text>&#xA0;</xsl:text>
																							</span>
																						</xsl:if>
																					</xsl:for-each>
																					<xsl:for-each select="cbc:PostalZone">
																						<xsl:apply-templates/>
																						<span>
																							<xsl:text>&#xA0;</xsl:text>
																						</span>
																					</xsl:for-each>
																					<xsl:for-each select="cbc:CitySubdivisionName">
																						<xsl:apply-templates/>
																					</xsl:for-each>
																					<span>
																						<xsl:text> / </xsl:text>
																					</span>
																					<xsl:for-each select="cbc:CityName">
																						<xsl:apply-templates/>
																						<span>
																							<xsl:text>&#xA0;</xsl:text>
																						</span>
																					</xsl:for-each>
																				</xsl:for-each>
																			</td>
																		</xsl:for-each>
																	</xsl:for-each>
																</xsl:for-each>
															</tr>
															<xsl:for-each select="n1:Invoice">
																<xsl:for-each select="cac:AccountingCustomerParty">
																	<xsl:for-each select="cac:Party">
                                    <xsl:if test="cac:PartyIdentification/cbc:ID[@schemeID = 'VKN'] !=''">
                                      <tr>
                                        <td>
                                          <xsl:for-each select="cac:PartyTaxScheme">
                                            <xsl:if test="cac:TaxScheme/cbc:Name !=''">
                                              <span>
                                                <xsl:text>V.D. : </xsl:text>
                                                <xsl:value-of select="cac:TaxScheme/cbc:Name"/>
                                              </span>
                                            </xsl:if>
                                          </xsl:for-each>
                                          <xsl:if test="cac:PartyTaxScheme/cac:TaxScheme/cbc:Name !='' and cac:PartyIdentification/cbc:ID[@schemeID = 'VKN'] !=''">
                                            <xsl:text> - </xsl:text>
                                          </xsl:if>
                                          <xsl:if test="//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyIdentification/cbc:ID !=''">
                                            <xsl:for-each select="//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyIdentification">
                                              <xsl:if test="cbc:ID/@schemeID = 'VKN'">
                                                <xsl:value-of select="cbc:ID/@schemeID"/>
                                                <xsl:text> : </xsl:text>
                                                <xsl:value-of select="cbc:ID"/>
                                              </xsl:if>
                                            </xsl:for-each>
                                          </xsl:if>
                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="cac:PartyIdentification/cbc:ID[@schemeID = 'TCKN'] !=''">
                                      <tr>
                                        <td>
                                          <xsl:for-each select="cac:PartyTaxScheme">
                                            <xsl:if test="cac:TaxScheme/cbc:Name !=''">
                                              <span>
                                                <xsl:text>V.D. : </xsl:text>
                                                <xsl:value-of select="cac:TaxScheme/cbc:Name"/>
                                              </span>
                                            </xsl:if>
                                          </xsl:for-each>
                                          <xsl:if test="cac:PartyTaxScheme/cac:TaxScheme/cbc:Name !='' and cac:PartyIdentification/cbc:ID[@schemeID = 'TCKN'] !=''">
                                            <xsl:text> - </xsl:text>
                                          </xsl:if>
                                          <xsl:if test="//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyIdentification/cbc:ID !=''">
                                            <xsl:for-each select="//n1:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyIdentification">
                                              <xsl:if test="cbc:ID/@schemeID = 'TCKN'">
                                                <xsl:value-of select="cbc:ID/@schemeID"/>
                                                <xsl:text> : </xsl:text>
                                                <xsl:value-of select="cbc:ID"/>
                                              </xsl:if>
                                            </xsl:for-each>
                                          </xsl:if>
                                        </td>
                                      </tr>
                                    </xsl:if>
																	</xsl:for-each>
																</xsl:for-each>
															</xsl:for-each>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
							<!--<tr align="left">
								<td>
									<table id="ettnTable">
										<tr>
											<td>
												<span style="font-weight:bold; ">
													<xsl:text>ETTN:</xsl:text>
												</span>
											</td>
											<td>
												<xsl:for-each select="n1:Invoice">
													<xsl:for-each select="cbc:UUID">
														<xsl:apply-templates/>
													</xsl:for-each>
												</xsl:for-each>
											</td>
										</tr>
									</table>
								</td>
							</tr>-->
						</tbody>
					</table>
					<table id="invoiceInfo">
						<tbody>
							<tr>
								<td id="despatchInfo">
									<table border="0" height="13" id="despatchTable" style="width:100%;">
										<tbody>
											<tr id="ettnRow">
												<td align="left" colspan="3">
													<div>
														<xsl:text>ETTN :&#xA0;</xsl:text>
														<xsl:for-each select="n1:Invoice">
															<xsl:for-each select="cbc:UUID">
																<xsl:apply-templates/>
															</xsl:for-each>
														</xsl:for-each>
													</div>
												</td>
											</tr>
											<tr>
												<td align="left" style="width: 50%;">
													<div>Ödenecek Tutar</div>
													<div>
														<xsl:for-each select="n1:Invoice/cac:LegalMonetaryTotal/cbc:PayableAmount">
															<xsl:call-template name="Curr_Type">
																<xsl:with-param name="valuePath" select="."/>
																<xsl:with-param name="format" select="'###.##0,00'"/>
															</xsl:call-template>
														</xsl:for-each>
														<xsl:choose>
															<xsl:when test="n1:Invoice/cbc:DocumentCurrencyCode = 'TRY' or n1:Invoice/cbc:DocumentCurrencyCode = 'TRL'"></xsl:when>
															<xsl:otherwise>
																<xsl:text>&#xA0;(</xsl:text>
																<xsl:value-of select="format-number(//n1:Invoice/cac:LegalMonetaryTotal/cbc:PayableAmount * //n1:Invoice/cac:PricingExchangeRate/cbc:CalculationRate, '###.##0,00', 'european')"/>
																<xsl:text> TL</xsl:text>
																<xsl:text>)</xsl:text>
															</xsl:otherwise>
														</xsl:choose>
													</div>
												</td>
												<xsl:if test="n1:Invoice/cbc:ID !=''">
													<td align="left" style="width: 30%;">
														<div>Fatura No</div>
														<div>
															<xsl:value-of select="n1:Invoice/cbc:ID"/>
														</div>
													</td>
												</xsl:if>
												<xsl:if test="n1:Invoice/cbc:IssueDate !=''">
													<td align="left">
														<div>Fatura Tarihi</div>
														<div>
															<xsl:call-template name="dateFormatter">
																<xsl:with-param name="date" select="n1:Invoice/cbc:IssueDate"/>
															</xsl:call-template>
														</div>
													</td>
												</xsl:if>
											</tr>
										</tbody>
									</table>
								</td>
								<td id="despactTableRightCol">
									<table border="0" height="13">
										<tbody>
											<tr style="height:13px; ">
												<td style="width: 50%" align="left">
													<xsl:text>Özelleştirme No</xsl:text>
												</td>
												<td style="width:1px;">
													<b>:</b>
												</td>
												<td>
													<xsl:value-of select="n1:Invoice/cbc:CustomizationID"/>
												</td>
											</tr>
											<xsl:if test="n1:Invoice/cbc:ProfileID !=''">
												<tr>
													<td>
														<xsl:text>Senaryo</xsl:text>
													</td>
													<td>
														<b>:</b>
													</td>
													<td>
														<xsl:value-of select="n1:Invoice/cbc:ProfileID"/>
													</td>
												</tr>
											</xsl:if>
											<xsl:if test="n1:Invoice/cbc:InvoiceTypeCode !=''">
												<tr>
													<td>
														<xsl:text>Fatura Tipi</xsl:text>
													</td>
													<td>
														<b>:</b>
													</td>
													<td>
														<xsl:value-of select="n1:Invoice/cbc:InvoiceTypeCode"/>
													</td>
												</tr>
											</xsl:if>
											<xsl:for-each select="n1:Invoice/cac:PricingExchangeRate">
												<xsl:if test="cbc:CalculationRate !=0 and cbc:SourceCurrencyCode != 'TRY'and cbc:TargetCurrencyCode = 'TRY'">
													<tr style="height:13px; ">
														<td>
															<xsl:text>Döviz Kuru</xsl:text>
														</td>
														<td>
															<b>:</b>
														</td>
														<td>
															<xsl:value-of select="cbc:CalculationRate"/>&#xA0;<xsl:value-of select="cbc:TargetCurrencyCode"/></td>
													</tr>
												</xsl:if>
											</xsl:for-each>
											<xsl:for-each select="n1:Invoice/cac:PaymentMeans">
												<xsl:if test="cbc:PaymentDueDate !=''">
													<tr>
														<td>
															<xsl:text>Vade Tarihi</xsl:text>
														</td>
														<td>
															<b>:</b>
														</td>
														<td>
															<xsl:call-template name="dateFormatter">
																<xsl:with-param name="date" select="cbc:PaymentDueDate"/>
															</xsl:call-template>
														</td>
													</tr>
												</xsl:if>
											</xsl:for-each>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
					<table style="width:100%;">
						<tbody>
							<tr>
								<td style="vertical-align:top; padding:0;">
									<table id="lineTable" width="100%">
										<thead>
											<tr class="lineTableTr">
												<th class="lineTableTd" style="width:3%;">
													<xsl:text>Sıra No</xsl:text>
												</th>
												<th class="lineTableTd" style="width:24%;">
													<xsl:text>Mal/Hizmet Adı</xsl:text>
												</th>
												<th class="lineTableTd" style="width:8%;">
													<xsl:text>Miktar</xsl:text>
												</th>
												<th class="lineTableTd" style="width:8%;">
													<xsl:text>Birim</xsl:text>
												</th>
												<th class="lineTableTd" style="width:12%;">
													<xsl:text>Birim Fiyat</xsl:text>
												</th>
												<th class="lineTableTd" style="width:7%;">
													<xsl:text>İskonto Oranı</xsl:text>
												</th>
												<th class="lineTableTd" style="width:9%;">
													<xsl:text>İskonto Tutarı</xsl:text>
												</th>
												<th class="lineTableTd" style="width:7%;">
													<xsl:text>KDV Oranı</xsl:text>
												</th>
												<th class="lineTableTd" style="width:10%;">
													<xsl:text>KDV Tutarı</xsl:text>
												</th>
												<th class="lineTableTd" style="width:12%;">
													<xsl:text>Mal Hizmet Tutarı</xsl:text>
												</th>
											</tr>
										</thead>
										<tbody>
											<xsl:for-each select="//n1:Invoice/cac:InvoiceLine">
												<xsl:choose>
													<xsl:when test=".">
														<xsl:apply-templates select="."/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:apply-templates select="//n1:Invoice"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:for-each>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</xsl:for-each>
				<table id="subTotal" style="width:100%">
					<tbody>
						<tr>
							<td style="width:52%;{$noteColWidth}"></td>
							<td style="vertical-align:top; width:48%;{$toplamTutarColWidth} padding: 0;">
								<table id="budgetContainerTable" width="100%" style="margin-top:0px">
									<tr style="display:none;{$tlRowStyle}">
										<td id="lineTableBudgetTd">
											<xsl:text>Fatura Döviz Cinsi</xsl:text>
										</td>
										<td id="lineTableBudgetTd" style="text-align:center; font-weight:bold;">TL Karşılığı</td>
										<td id="lineTableBudgetTd" style="text-align:center; font-weight:bold;">Fatura Dövizi</td>
									</tr>
									<tr>
										<td id="lineTableBudgetTd" style="width:50%;">
											<xsl:text>Mal Hizmet Toplam Tutarı</xsl:text>
										</td>
										<td id="lineTableBudgetTd" style="width:25%; display:none;{$tlColStyle}">
											<span>
												<xsl:text>&#xA0;</xsl:text>
												<xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:LineExtensionAmount/@currencyID != 'TRY'">
													<xsl:value-of select="format-number(//n1:Invoice/cac:LegalMonetaryTotal/cbc:LineExtensionAmount * //n1:Invoice/cac:PricingExchangeRate/cbc:CalculationRate, '###.##0,00', 'european')"/>
													<xsl:text> TL</xsl:text>
												</xsl:if>
											</span>
										</td>
										<td id="lineTableBudgetTd" style="width:25%;">
											<span>
												<xsl:for-each select="n1:Invoice/cac:LegalMonetaryTotal/cbc:LineExtensionAmount">
													<xsl:call-template name="Curr_Type">
														<xsl:with-param name="valuePath" select="."/>
														<xsl:with-param name="format" select="'###.##0,00'"/>
													</xsl:call-template>
												</xsl:for-each>
											</span>
										</td>
									</tr>
									<xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:AllowanceTotalAmount != 0">
										<tr>
											<td id="lineTableBudgetTd">
												<xsl:text>Toplam İskonto</xsl:text>
											</td>
											<td id="lineTableBudgetTd" style="display:none;{$tlColStyle}">
												<span>
													<xsl:text>&#xA0;</xsl:text>
													<xsl:if test="//n1:Invoice/cac:LegalMonetaryTotal/cbc:AllowanceTotalAmount/@currencyID != 'TRY'">
														<xsl:value-of select="format-number(//n1:Invoice/cac:LegalMonetaryTotal/cbc:AllowanceTotalAmount * //n1:Invoice/cac:PricingExchangeRate/cbc:CalculationRate, '###.##0,00', 'european')"/>
														<xsl:text> TL</xsl:text>
													</xsl:if>
												</span>
											</td>
											<td id="lineTableBudgetTd">
												<span>
													<xsl:for-each select="n1:Invoice/cac:LegalMonetaryTotal/cbc:AllowanceTotalAmount">
														<xsl:call-template name="Curr_Type">
															<xsl:with-param name="valuePath" select="."/>
															<xsl:with-param name="format" select="'###.##0,00'"/>
														</xsl:call-template>
													</xsl:for-each>
												</span>
											</td>
										</tr>
									</xsl:if>
									<xsl:for-each select="n1:Invoice/cac:TaxTotal/cac:TaxSubtotal">
										<tr>
											<td id="lineTableBudgetTd">
												<xsl:text>Hesaplanan </xsl:text>
												<xsl:value-of select="cac:TaxCategory/cac:TaxScheme/cbc:Name"/>
												<xsl:text>(%</xsl:text>
												<xsl:value-of select="cbc:Percent"/>
												<xsl:text>)</xsl:text>
											</td>
											<td id="lineTableBudgetTd" style="display:none;{$tlColStyle}">
												<span>
													<xsl:for-each select="cac:TaxCategory/cac:TaxScheme">
														<xsl:value-of select="format-number(../../cbc:TaxAmount * //n1:Invoice/cac:PricingExchangeRate/cbc:CalculationRate, '###.##0,00', 'european')"/>
														<xsl:text> TL</xsl:text>
													</xsl:for-each>
												</span>
											</td>
											<td id="lineTableBudgetTd">
												<xsl:for-each select="cac:TaxCategory/cac:TaxScheme">
													<xsl:text> </xsl:text>
													<xsl:call-template name="Curr_Type">
														<xsl:with-param name="valuePath" select="../../cbc:TaxAmount"/>
														<xsl:with-param name="format" select="'###.##0,00'"/>
													</xsl:call-template>
												</xsl:for-each>
											</td>
										</tr>
									</xsl:for-each>
									<xsl:for-each select="n1:Invoice/cac:WithholdingTaxTotal/cac:TaxSubtotal">
										<xsl:if test="cbc:TaxAmount != ''">
											<tr>
												<td id="lineTableBudgetTd">
													<xsl:text>KDV Tevkifat-[</xsl:text>
													<xsl:value-of select="cac:TaxCategory/cac:TaxScheme/cbc:TaxTypeCode"/>
													<xsl:text>]-</xsl:text>
													<xsl:text>(%</xsl:text>
													<xsl:value-of select="cbc:Percent"/>
													<xsl:text>)</xsl:text>
												</td>
												<td id="lineTableBudgetTd" style="display:none;{$tlColStyle}">
													<span>
														<xsl:text>&#xA0;</xsl:text>
														<xsl:value-of select="format-number(cbc:TaxAmount * //n1:Invoice/cac:PricingExchangeRate/cbc:CalculationRate, '###.##0,00', 'european')"/>
														<xsl:text> TL</xsl:text>
													</span>
												</td>
												<td id="lineTableBudgetTd">
													<xsl:for-each select="cac:TaxCategory/cac:TaxScheme">
														<xsl:text> </xsl:text>
														<xsl:call-template name="Curr_Type">
															<xsl:with-param name="valuePath" select="../../cbc:TaxAmount"/>
															<xsl:with-param name="format" select="'###.##0,00'"/>
														</xsl:call-template>
													</xsl:for-each>
												</td>
											</tr>
										</xsl:if>
									</xsl:for-each>
									<tr>
										<td id="lineTableBudgetTd">
											<xsl:text>Vergiler Dahil Toplam Tutar</xsl:text>
										</td>
										<td id="lineTableBudgetTd" style="display:none;{$tlColStyle}">
											<span>
												<xsl:text>&#xA0;</xsl:text>
												<xsl:value-of select="format-number(//n1:Invoice/cac:LegalMonetaryTotal/cbc:TaxInclusiveAmount * //n1:Invoice/cac:PricingExchangeRate/cbc:CalculationRate, '###.##0,00', 'european')"/>
												<xsl:text> TL</xsl:text>
											</span>
										</td>
										<td id="lineTableBudgetTd">
											<xsl:for-each select="n1:Invoice">
												<xsl:for-each select="cac:LegalMonetaryTotal">
													<xsl:for-each select="cbc:TaxInclusiveAmount">
														<xsl:call-template name="Curr_Type">
															<xsl:with-param name="valuePath" select="."/>
															<xsl:with-param name="format" select="'###.##0,00'"/>
														</xsl:call-template>
													</xsl:for-each>
												</xsl:for-each>
											</xsl:for-each>
										</td>
									</tr>
									<tr id="payableAmountRow">
										<td id="lineTableBudgetTd">
											<xsl:text>Ödenecek Tutar</xsl:text>
										</td>
										<td id="lineTableBudgetTd" style="display:none;{$tlColStyle}">
											<span>
												<xsl:text>&#xA0;</xsl:text>
												<xsl:value-of select="format-number(//n1:Invoice/cac:LegalMonetaryTotal/cbc:PayableAmount * //n1:Invoice/cac:PricingExchangeRate/cbc:CalculationRate, '###.##0,00', 'european')"/>
												<xsl:text> TL</xsl:text>
											</span>
										</td>
										<td id="lineTableBudgetTd">
											<xsl:for-each select="n1:Invoice/cac:LegalMonetaryTotal/cbc:PayableAmount">
												<xsl:call-template name="Curr_Type">
													<xsl:with-param name="valuePath" select="."/>
													<xsl:with-param name="format" select="'###.##0,00'"/>
												</xsl:call-template>
											</xsl:for-each>
										</td>
									</tr>
									<xsl:for-each select="n1:Invoice/cbc:Note">
										<xsl:if test="position() = 1 and .!= ''">
											<tr>
												<td id="lineTableBudgetTd" colspan="3" style="padding-top: 10px;">
													<xsl:value-of select="."/>
												</td>
											</tr>
										</xsl:if>
									</xsl:for-each>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
				<table id="notesTable" style="display:none;{$isNotesTableVisible}">
					<tbody>
						<tr>
							<td id="notesTableTd">
								<b>NOTLAR</b>
							</td>
						</tr>
						<tr align="left" valign="top">
							<td id="notesTableTd">
								<xsl:for-each select="n1:Invoice/cbc:Note">
									<xsl:if test=".!='' and position() != 1">
										<b>* &#xA0;</b>
										<xsl:value-of select="."/>
										<br/>
									</xsl:if>
								</xsl:for-each>
								<xsl:for-each select="//n1:Invoice/cac:TaxTotal/cac:TaxSubtotal">
									<xsl:if test="cbc:Percent=0 and cac:TaxCategory/cac:TaxScheme/cbc:TaxTypeCode='0015'">
										<b>Vergi İstisna Muafiyet Sebebi:</b>
										<xsl:value-of select="cac:TaxCategory/cbc:TaxExemptionReason"/>
										<br/>
									</xsl:if>
								</xsl:for-each>
								<xsl:for-each select="n1:Invoice/cac:PaymentMeans">
									<xsl:if test="cbc:InstructionNote !=''">
										<b>Ödeme Notu :&#xA0;</b>
										<xsl:value-of select="//n1:Invoice/cac:PaymentMeans/cbc:InstructionNote"/>
										<br/>
									</xsl:if>
									<xsl:if test="cbc:PaymentNote !=''">
										<b>Hesap Açıklaması :&#xA0;</b>
										<xsl:value-of select="//n1:Invoice/cac:PaymentMeans/cac:PayeeFinancialAccount/cbc:PaymentNote"/>
										<br/>
									</xsl:if>
								</xsl:for-each>
							</td>
						</tr>
					</tbody>
				</table>
        <table style="width: 100%;margin-top: 10px;margin-bottom: 55px">
          <tbody>
            <tr>
              <td style="width:100%;vertical-align:top;">
                <table id="hesapBilgileri">
                  <thead>
                    <tr>
                      <th colspan="7" style="text-align:center;">BANKA HESAP BİLGİLERİMİZ</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td style="width: 12%;">Garanti Bankası</td>
                      <td style="width: 5%;">TL</td>
                      <td style="width: 27%">TR63 0006 2000 2860 0006 2946 21</td>
                      <td style="width: 9%;">İş Bankası</td>
                      <td style="width: 5%;">TL</td>
                      <td style="width: 27%">TR22 0006 4000 0011 2550 0629 74</td>
                    </tr>
                    <tr>
                      <td>Garanti Bankası</td>
                      <td>USD</td>
                      <td>TR04 0006 2000 2860 0009 0820 08</td>
                      <td>İş Bankası</td>
                      <td>USD</td>
                      <td>TR71 0006 4000 0021 2550 0407 47</td>
                    </tr>
                    <tr>
                      <td>Garanti Bankası</td>
                      <td>EUR</td>
                      <td>TR58 0006 2000 2860 0009 0820 06</td>
                      <td>İş Bankası</td>
                      <td>EUR</td>
                      <td>TR33 0006 4000 0021 2550 0407 52</td>
                    </tr>
                    <tr>
                      <td>Garanti Bankası</td>
                      <td>CHF</td>
                      <td>TR31 0006 2000 2860 0009 0820 07</td>
                      <td>İş Bankası</td>
                      <td>CHF</td>
                      <td>TR46 0006 4000 0021 2550 0379 96</td>
                    </tr>
                  </tbody>
                </table>
              </td>
              <td style=";vertical-align:top;">
              </td>
            </tr>
          </tbody>
        </table>
				<table id="footer">
					<tbody>
						<tr>
							<td>Bizi tercih ettiğiniz için teşekkür ederiz</td>
						</tr>
						<tr>
							<td>
								<xsl:for-each select="//cac:AccountingSupplierParty/cac:Party/cac:Contact">
									<xsl:for-each select="cbc:Telephone">
										<xsl:if test=". !=''">
											<xsl:variable name="telNo">
												<xsl:value-of select="."/>
											</xsl:variable>
											<xsl:text>Tel : </xsl:text>
											<a href="tel: {$telNo}">
												<xsl:value-of select="."/>
											</a>
										</xsl:if>
									</xsl:for-each>
									<xsl:text>&#xA0;&#xA0;</xsl:text>
									<xsl:for-each select="cbc:Telefax">
										<xsl:if test=". !=''">
											<xsl:text>Faks : </xsl:text>
											<xsl:apply-templates/>
										</xsl:if>
									</xsl:for-each>
								</xsl:for-each>
							</td>
						</tr>
						<tr>
							<td>
								<xsl:for-each select="//cac:AccountingSupplierParty/cac:Party/cac:Contact">
									<xsl:for-each select="cbc:ElectronicMail">
										<xsl:if test=". !=''">
											<xsl:variable name="email">
												<xsl:value-of select="."/>
											</xsl:variable>
											<a href="mailto: {$email}">
												<xsl:value-of select="."/>
											</a>
										</xsl:if>
									</xsl:for-each>
								</xsl:for-each>
								<xsl:text>&#xA0;&#xA0;</xsl:text>
								<xsl:for-each select="//n1:Invoice/cac:AccountingSupplierParty/cac:Party/cbc:WebsiteURI">
									<xsl:if test=". !=''">
										<xsl:variable name="web">
											<xsl:value-of select="."/>
										</xsl:variable>
										<a href="http://{$web}">
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
								</xsl:for-each>
							</td>
						</tr>
					</tbody>
				</table>
			</body>
		</html>
	</xsl:template>
	<xsl:template name="dateFormatter">
		<xsl:param name="date"/>
		<xsl:value-of select="substring($date,9,2)"/>.<xsl:value-of select="substring($date,6,2)"/>.<xsl:value-of select="substring($date,1,4)"/></xsl:template>

	<xsl:variable name="dovizliMi">
		<xsl:if test="//n1:Invoice/cbc:DocumentCurrencyCode != 'TRY'">display:table-cell;</xsl:if>
	</xsl:variable>

	<xsl:variable name="tlColStyle">
		<xsl:if test="contains($dovizliMi, 'display:table-cell;')">display:table-cell;</xsl:if>
	</xsl:variable>

	<xsl:variable name="tlRowStyle">
		<xsl:if test="contains($dovizliMi, 'display:table-cell;')">display:table-row;</xsl:if>
	</xsl:variable>

	<xsl:variable name="noteColWidth">
		<xsl:if test="not(contains($dovizliMi, 'display:table-cell;'))">width:64%;</xsl:if>
	</xsl:variable>

	<xsl:variable name="toplamTutarColWidth">
		<xsl:if test="not(contains($dovizliMi, 'display:table-cell;'))">width:36%;</xsl:if>
	</xsl:variable>

	<xsl:variable name="notValues">
		<xsl:for-each select="n1:Invoice/cbc:Note[position() &gt; 1]">
			<xsl:if test=".!=''">display:block;</xsl:if>
		</xsl:for-each>
	</xsl:variable>

	<xsl:variable name="isNotesTableVisible">
		<xsl:if test="contains($notValues, 'display:block;')">display:block;</xsl:if>
	</xsl:variable>

	<xsl:template match="//n1:Invoice/cac:InvoiceLine">
		<tr>
			<td class="lineTableTd" style="text-align:left">
				<span>
					<xsl:text>&#xA0;</xsl:text>
					<xsl:value-of select="./cbc:ID"/>
				</span>
			</td>
			<td class="lineTableTd" style="text-align:left">
				<span>
					<xsl:text>&#xA0;</xsl:text>
					<xsl:value-of select="./cac:Item/cbc:Name"/>
				</span>
			</td>
			<td class="lineTableTd">
				<span>
					<xsl:text>&#xA0;</xsl:text>
					<xsl:value-of select="format-number(./cbc:InvoicedQuantity, '###.###,####', 'european')"/>
				</span>
			</td>
			<td class="lineTableTd">
				<span>
					<xsl:text>&#xA0;</xsl:text>
					<xsl:if test="./cbc:InvoicedQuantity/@unitCode">
						<xsl:for-each select="./cbc:InvoicedQuantity">
							<xsl:text/>
							<xsl:choose>
								<xsl:when test="@unitCode  = '2W'">
									<span>
										<xsl:text> Bidon</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = '4A'">
									<span>
										<xsl:text> Bobin</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = '4B'">
									<span>
										<xsl:text> Kap</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = '5H'">
									<span>
										<xsl:text> Faz</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'A49'">
									<span>
										<xsl:text> Denye</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'A76'">
									<span>
										<xsl:text> Gal.</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'AA'">
									<span>
										<xsl:text> Top</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'AB'">
									<span>
										<xsl:text> Koli</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'ANN'">
									<span>
										<xsl:text> Yıl</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'AS'">
									<span>
										<xsl:text> Asorti</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'B5'">
									<span>
										<xsl:text> Kütük</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'B55'">
									<span>
										<xsl:text> KVM</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'BAR'">
									<span>
										<xsl:text> Bar</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'BD'">
									<span>
										<xsl:text> Pano</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'BG'">
									<span>
										<xsl:text> Torba/Poşet</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'BH'">
									<span>
										<xsl:text> Fırça</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'BJ'">
									<span>
										<xsl:text> Kova</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'BK'">
									<span>
										<xsl:text> Sepet</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'BLD'">
									<span>
										<xsl:text> Varil</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'BO'">
									<span>
										<xsl:text> Şişe</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'BR'">
									<span>
										<xsl:text> Bar</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'BX'">
									<span>
										<xsl:text> Kutu</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'CA'">
									<span>
										<xsl:text> Kutu</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'CGM'">
									<span>
										<xsl:text> Cgm</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'CH'">
									<span>
										<xsl:text> Konteyner</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'CL'">
									<span>
										<xsl:text> Bobin</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'CLT'">
									<span>
										<xsl:text> CLT</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'CMK'">
									<span>
										<xsl:text> CM²</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'CMQ'">
									<span>
										<xsl:text> CM³</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'CMT'">
									<span>
										<xsl:text> CM</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'CS'">
									<span>
										<xsl:text> Kutu</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'CT'">
									<span>
										<xsl:text> Koli</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'CU'">
									<span>
										<xsl:text> Kupa</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'CY'">
									<span>
										<xsl:text> Silindir</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'D61'">
									<span>
										<xsl:text> DK</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'D62'">
									<span>
										<xsl:text> SN</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'D79'">
									<span>
										<xsl:text> Demet</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'D92'">
									<span>
										<xsl:text> Bant</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'D97'">
									<span>
										<xsl:text> Palet</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'DAY'">
									<span>
										<xsl:text> Gün</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'DLT'">
									<span>
										<xsl:text> DLT</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'DMK'">
									<span>
										<xsl:text> DM²</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'DMT'">
									<span>
										<xsl:text> DM</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'DPC'">
									<span>
										<xsl:text> Düzine</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'DPR'">
									<span>
										<xsl:text> Düzine</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'DR'">
									<span>
										<xsl:text> Varil</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'DRL'">
									<span>
										<xsl:text> Düzine</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'DZN'">
									<span>
										<xsl:text> Düzine</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'DZP'">
									<span>
										<xsl:text> Düzine</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'E4'">
									<span>
										<xsl:text> Brüt KG</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'EA'">
									<span>
										<xsl:text> Beher</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'EV'">
									<span>
										<xsl:text> Zarf</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'FOT'">
									<span>
										<xsl:text> Ayak</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'GB'">
									<span>
										<xsl:text> Galon</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'GD'">
									<span>
										<xsl:text> Brüt Varil</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'GLI'">
									<span>
										<xsl:text> Galon</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'GLL'">
									<span>
										<xsl:text> Galon</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'GN'">
									<span>
										<xsl:text> Gross Galon</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'GRM'">
									<span>
										<xsl:text> GR</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'GRO'">
									<span>
										<xsl:text> Brüt</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'GT'">
									<span>
										<xsl:text> Brüt Ton</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'HA'">
									<span>
										<xsl:text> Çile</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'HUR'">
									<span>
										<xsl:text> Saat</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'IE'">
									<span>
										<xsl:text> Kişi</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'INH'">
									<span>
										<xsl:text> İnç</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'K6'">
									<span>
										<xsl:text> KLT</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'KGM'">
									<span>
										<xsl:text> KG</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'KJO'">
									<span>
										<xsl:text> KJO</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'KMK'">
									<span>
										<xsl:text> KM²</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'KTM'">
									<span>
										<xsl:text> KM</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'KWH'">
									<span>
										<xsl:text> KWH</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'KWT'">
									<span>
										<xsl:text> KWT</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'LR'">
									<span>
										<xsl:text> Tabaka</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'LTR'">
									<span>
										<xsl:text> LT</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'MGM'">
									<span>
										<xsl:text> MGM</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'MIN'">
									<span>
										<xsl:text> DK</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'MLT'">
									<span>
										<xsl:text> MLT</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'MMQ'">
									<span>
										<xsl:text> MM³</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'MMT'">
									<span>
										<xsl:text> MM</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'MON'">
									<span>
										<xsl:text> Ay</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'MTK'">
									<span>
										<xsl:text> MT²</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'MTQ'">
									<span>

										<xsl:text> MT³</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'MTR'">
									<span>
										<xsl:text> MT</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'NIU'">
									<span>
										<xsl:text> Adet</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'NT'">
									<span>
										<xsl:text> Net Ton</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'PA'">
									<span>
										<xsl:text> Paket</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'PF'">
									<span>

										<xsl:text> Palet</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'PG'">
									<span>
										<xsl:text> Plaka</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'PL'">
									<span>
										<xsl:text> Kova</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'PR'">
									<span>
										<xsl:text> Çift</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'RD'">
									<span>

										<xsl:text> Çubuk</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'RG'">
									<span>
										<xsl:text> Halka</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'RL'">
									<span>
										<xsl:text> Makara</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'RO'">
									<span>
										<xsl:text> Rulo</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'SA'">
									<span>
										<xsl:text> Çuval</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'SET'">
									<span>
										<xsl:text> Set</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'SO'">
									<span>
										<xsl:text> Makara</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'TN'">
									<span>
										<xsl:text> Teneke</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'TU'">
									<span>
										<xsl:text> Tüp</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'Z3'">
									<span>
										<xsl:text> Fıçı</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'MWH'">
									<span>
										<xsl:text> MWH</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'MAW'">
									<span>
										<xsl:text> Megawatt</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'NCR'">
									<span>
										<xsl:text> Karat</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = '10'">
									<span>
										<xsl:text> Grup</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = '77'">
									<span>
										<xsl:text> Miliinç</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = '2P'">
									<span>
										<xsl:text> Kilobyte</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'AD'">
									<span>
										<xsl:text> Byte</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'Z2'">
									<span>
										<xsl:text> Kasa/Sandık</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'ST'">
									<span>
										<xsl:text> Sayfa</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'D66'">
									<span>
										<xsl:text> Kaset</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'DC'">
									<span>
										<xsl:text> Disk</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = 'DD'">
									<span>
										<xsl:text> Derece</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode  = '26'">
									<span>
										<xsl:text> Ton</xsl:text>
									</span>
								</xsl:when>
								
								
								
								
								
								
								<xsl:when test="@unitCode = 'C62'">
									<span>
										<xsl:text> Adet</xsl:text>
									</span>
								</xsl:when>
								
								
								
								
								
								
								<xsl:when test="@unitCode = 'GRM'">
									<span>
										<xsl:text> GRAM</xsl:text>
									</span>
								</xsl:when>
								
								
								
								
								
								
								<xsl:when test="@unitCode = 'KGM'">
									<span>
										<xsl:text> KİLOGRAM</xsl:text>
									</span>
								</xsl:when>
								
								
								
								
								
								
								
								
								
								
								<xsl:when test="@unitCode = 'KWH'">
									<span>
										<xsl:text> KİLOWATT SAAT</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode = 'KWT'">
									<span>
										<xsl:text> KİLOWATT</xsl:text>
									</span>
								</xsl:when>
								
								<xsl:when test="@unitCode = 'LTR'">
									<span>
										<xsl:text> LİTRE</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode = 'MTK'">
									<span>
										<xsl:text> METRE KARE</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode = 'MTQ'">
									<span>
										<xsl:text> METRE KÜP</xsl:text>
									</span>
								</xsl:when>
								<xsl:when test="@unitCode = 'MTR'">
									<span>
										<xsl:text> METRE</xsl:text>
									</span>
								</xsl:when>
								
								<xsl:when test="@unitCode = 'NCR'">
									<span>
										<xsl:text> KARAT</xsl:text>
									</span>
								</xsl:when>
								
								
								<xsl:when test="@unitCode = 'PR'">
									<span>
										<xsl:text> ÇİFT</xsl:text>
									</span>
								</xsl:when>
								
								<xsl:when test="@unitCode = 'SET'">
									<span>
										<xsl:text> SET</xsl:text>
									</span>
								</xsl:when>
								
								
															</xsl:choose>
						</xsl:for-each>
					</xsl:if>
				</span>
			</td>
			<td class="lineTableTd">
				<span>
					<xsl:text>&#xA0;</xsl:text>
					<xsl:call-template name="Curr_Type">
						<xsl:with-param name="valuePath" select="./cac:Price/cbc:PriceAmount"/>
						<xsl:with-param name="format" select="'###.##0,00######'"/>
					</xsl:call-template>
				</span>
			</td>
			<td class="lineTableTd">
				<span>
					<xsl:text>&#xA0;</xsl:text>
					<xsl:if test="./cac:AllowanceCharge[1]/cbc:MultiplierFactorNumeric">
						<xsl:text> %</xsl:text>
						<xsl:value-of select="format-number(./cac:AllowanceCharge[1]/cbc:MultiplierFactorNumeric * 100, '###.##0,00', 'european')"/>
					</xsl:if>
				</span>
			</td>
			<td class="lineTableTd">
				<span>
					<xsl:text>&#xA0;</xsl:text>
					<xsl:if test="./cac:AllowanceCharge[1]">
						<!--<xsl:if test="./cac:AllowanceCharge[1]/cbc:ChargeIndicator = true() ">+</xsl:if>
						<xsl:if test="./cac:AllowanceCharge[1]/cbc:ChargeIndicator = false() ">-</xsl:if>-->
						<xsl:call-template name="Curr_Type">
							<xsl:with-param name="valuePath" select="./cac:AllowanceCharge[1]/cbc:Amount"/>
							<xsl:with-param name="format" select="'###.##0,00'"/>
						</xsl:call-template>
					</xsl:if>
				</span>
			</td>
			<td class="lineTableTd">
				<span>
					<xsl:text>&#xA0;</xsl:text>
					<xsl:for-each select="./cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cac:TaxScheme">
						<xsl:if test="cbc:TaxTypeCode='0015' ">
							<xsl:text/>
							<xsl:if test="../../cbc:Percent">
								<xsl:text> %</xsl:text>
								<xsl:value-of select="format-number(../../cbc:Percent, '###.##0,00', 'european')"/>
							</xsl:if>
						</xsl:if>
					</xsl:for-each>
				</span>
			</td>
			<td class="lineTableTd">
				<span>
					<xsl:text>&#xA0;</xsl:text>
					<xsl:for-each select="./cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cac:TaxScheme">
						<xsl:if test="cbc:TaxTypeCode='0015' ">
							<xsl:text/>
							<xsl:call-template name="Curr_Type">
								<xsl:with-param name="valuePath" select="../../cbc:TaxAmount"/>

								<xsl:with-param name="format" select="'###.##0,00'"/>
							</xsl:call-template>
						</xsl:if>
					</xsl:for-each>
				</span>
			</td>
			<td class="lineTableTd">
				<span>
					<xsl:text>&#xA0;</xsl:text>
					<xsl:call-template name="Curr_Type">
						<xsl:with-param name="valuePath" select="./cbc:LineExtensionAmount"/>
						<xsl:with-param name="format" select="'###.##0,00'"/>
					</xsl:call-template>
				</span>
			</td>
		</tr>
	</xsl:template>
	<xsl:template match="//n1:Invoice">
		<tr>
			<td>
				<span>
					<xsl:text>&#xA0;</xsl:text>
				</span>
			</td>
			<td>
				<span>
					<xsl:text>&#xA0;</xsl:text>
				</span>
			</td>
			<td>
				<span>
					<xsl:text>&#xA0;</xsl:text>
				</span>
			</td>
			<td>
				<span>
					<xsl:text>&#xA0;</xsl:text>
				</span>
			</td>
			<td>
				<span>
					<xsl:text>&#xA0;</xsl:text>
				</span>
			</td>
			<td>
				<span>
					<xsl:text>&#xA0;</xsl:text>
				</span>
			</td>
			<td>
				<span>
					<xsl:text>&#xA0;</xsl:text>
				</span>
			</td>
			<td>
				<span>
					<xsl:text>&#xA0;</xsl:text>
				</span>
			</td>
			<td>
				<span>
					<xsl:text>&#xA0;</xsl:text>
				</span>
			</td>
			<td>
				<span>
					<xsl:text>&#xA0;</xsl:text>
				</span>
			</td>
		</tr>
	</xsl:template>
	<xsl:template name="Curr_Type">
		<xsl:param name="format"/>
		<xsl:param name="valuePath"/>
		<xsl:value-of select="format-number($valuePath, $format, 'european')"/>
		<xsl:if test="$valuePath/@currencyID">
			<xsl:text> </xsl:text>
			<xsl:choose>
				<xsl:when test="$valuePath/@currencyID = 'TRL' or $valuePath/@currencyID = 'TRY'">
					<xsl:text>TL</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$valuePath/@currencyID"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
